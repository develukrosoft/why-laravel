<?php

namespace Database\Seeders;

use App\Models\ProgramType;
use Illuminate\Database\Seeder;

class ProgramTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Food Pantry',
                'description' => 'Food pantries are self-governing and usually distribute food to their clients on a once-a-month basis'
            ],
            [
                'name' => 'CSFP',
                'description' => 'The Commodity Supplemental Food Program (CSFP) works to improve the health of low-income persons at least 60 years of age by supplementing their diets with nutritious USDA Foods. ',
            ],
            [
                'name' => 'TEFAP',
                'description' => 'The Emergency Food Assistance Program (TEFAP) is a federal program that helps supplement the diets of low-income Americans, including elderly people, by providing them with emergency food assistance at no cost',
            ],
            [
                'name' => 'Soup Kitchen',
                'description' => 'A soup kitchen is a place where cooked food is offered to the hungry usually for free or sometimes at a below-market price.',
            ],
            [
                'name' => 'Food Banks',
                'description' => 'A food bank is the storehouse for millions of pounds of food and other products that go out to the community.',
            ],
            [
                'name' => 'Meal Delivery',
                'description' => 'Meals that are delivered up to five days a week, in senior centers, senior clubs, senior housing complexes, town halls, and other community locations. ',
            ],
            [
                'name' => 'Mobile Pantry',
                'description' => 'The Mobile Food Pantry is a truck used to deliver fresh produce, dairy products, and other food and grocery products directly to distribution sites where people need food.',
            ],
            [
                'name' => 'Medically Tailored Food',
                'description' => 'Medically tailored meals are delivered to individuals living with severe illness through a referral from a medical professional or healthcare plan.',
            ],
            [
                'name' => 'Food Voucher',
                'description' => 'Credit that can be given to family to be used at a market for food.',
            ],
            [
                'name' => 'Client Choice Food Pantry',
                'description' => 'Cleint Choice allows clients to select their own food instead of receiving a pre-packed or standard bag of groceries. With this method, clients do not have to take items they already have, do not like, or cannot eat for health or personal reasons.',
            ],
            [
                'name' => 'Hotline',
                'description' => 'Hotline',
            ],
            [
                'name' => 'Other',
                'description' => 'Other',
            ],
        ];

        foreach ($items as $item) {
            $pt = ProgramType::where('name', $item['name'])->first();
            if (!$pt) {
                ProgramType::create(array_merge($item, ['is_active' => 1]));
            }
        }
    }
}
