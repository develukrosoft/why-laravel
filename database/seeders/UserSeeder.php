<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'admin@whyhunger.com')->first()) {
            User::create([
                'email' => 'admin@whyhunger.com',
                'username' => 'admin',
                'full_name' => 'Admin',
                'role' => 'admin',
                'password' => Hash::make('admin@whyhunger.com')
            ]);
        }
    }
}
