<?php

namespace Database\Seeders;

use App\Models\UserHowFindValue;
use Illuminate\Database\Seeder;

class UserHowFindValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'Google',
            'Facebook',
            'Twitter',
            'Email',
            'Other',
        ];

        foreach ($items as $item) {
            UserHowFindValue::firstOrCreate(['name' => $item]);
        }
    }
}
