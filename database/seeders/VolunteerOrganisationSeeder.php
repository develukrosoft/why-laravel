<?php

namespace Database\Seeders;

use App\Models\Organisation;
use App\Models\OrganisationVolunteer;
use App\Models\VolunteerProfile;
use App\Services\HelperService;
use Illuminate\Database\Seeder;

class VolunteerOrganisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stop = false;
        $count = 1000;
        $offset = 0;
        do {
            $url =
                "https://old-networks.whyhunger.org/data/volunteer_organisation?count={$count}&offset={$offset}";
            $result = HelperService::curl_get_file_contents($url);
            $r = json_decode($result, true);
            if (count($r)) {
                foreach ($r as $c => $item) {
                    echo "$c ";
                    $updated_at = Date('Y-m-d H:i:s',
                        strtotime(!empty($item['updatedAt']['date']) ? $item['updatedAt']['date']
                            : 'now'));
                    $created_at = Date('Y-m-d H:i:s',
                        strtotime(!empty($item['createdAt']['date']) ? $item['createdAt']['date']
                            : 'now'));
                    $volunteer =
                        VolunteerProfile::where(['old_id' => $item['volunteer_id']])->first();
                    $organisation =
                        Organisation::where(['old_id' => $item['organisation_id']])->first();
                    if ($organisation
                        && !OrganisationVolunteer::where('old_id', $item['id'])->first()
                    ) {
                        $ov = OrganisationVolunteer::create([
                            'data' => json_decode($item['data'], true),
                            'status_id' => !empty($item['isDelete'])
                                ? OrganisationVolunteer::STATUS_DELETE
                                : (!empty($item['approve']) ? OrganisationVolunteer::STATUS_APPROVE
                                    : OrganisationVolunteer::STATUS_NEW),
                            'organisation_id' => $organisation->id,
                            'volunteer_id' => $volunteer ? $volunteer->id : null,
                            'approve_at' => !empty($item['approve']) ? $updated_at : null,
                            'old_id' => $item['id'],
                            'updated_at' => $updated_at,
                            'created_at' => $created_at,
                        ]);
                        $ov->update([
                            'updated_at' => $updated_at,
                            'created_at' => $created_at,
                        ]);
                    }
                }
                sleep(1);
            } else {
                $stop = true;
            }
            $offset += $count;
        } while ($stop == false);
    }
}
