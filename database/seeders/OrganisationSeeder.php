<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\City;
use App\Models\Country;
use App\Models\Email;
use App\Models\Organisation;
use App\Models\Phone;
use App\Models\ProgramType;
use App\Models\State;
use App\Services\HelperService;
use Illuminate\Database\Seeder;

class OrganisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stop = false;
        $count = 1000;
        $offset = 0;
        do {
            $url =
                "https://old-networks.whyhunger.org/data/organisations?count={$count}&offset={$offset}";
            $result = HelperService::curl_get_file_contents($url);
            $r = json_decode($result, true);
            if (count($r)) {
                foreach ($r as $c => $item) {
                    echo "$c ";
                    $org = Organisation::where(['old_id' => $item['id']])->first();
                    if (!$org) {
                        $country = Country::where('code', 'US')->first();
                        $item['state'] = !empty($item['state']) ? $item['state'] : 'Other';
                        $item['state_code'] =
                            !empty($item['state_code']) ? $item['state_code'] : 'OTHER';
                        $state = State::firstOrCreate([
                            'code' => $item['state_code'],
                            'country_id' => $country->id
                        ], ['name' => $item['state']]);

                        $item['city'] = !empty($item['city']) ? $item['city'] : 'Other';
                        $city = City::firstOrCreate([
                            'name' => $item['city'],
                            'state_id' => $state->id
                        ]);
                        $zip = !empty($item['zip']) ? intval($item['zip']) : null;
                        $zip = mb_strlen($zip) <= 7 ? $zip : null;
                        $address = Address::create([
                            'country_id' => $country->id,
                            'city_id' => $city->id,
                            'state_id' => $state->id,
                            'address' => !empty($item['address_text']) ? $item['address_text'] : '',
                            'zip' => $zip,
                            'lat' => !empty($item['latitude']) ? $item['latitude'] : '',
                            'lng' => !empty($item['longitude']) ? $item['longitude'] : '',
                        ]);
                        $volunteer_at = !empty($item['volunteerAt']['date']) ? Date('Y-m-d H:i:s',
                            strtotime($item['volunteerAt']['date'])) : Date('Y-m-d H:i:s');
                        $org = Organisation::create([
                            'title' => $item['name'],
                            'status_id' => Organisation::TYPE_APPROVE,
                            'website' => $item['website'],
                            'annual_budget' => $item['annualBudget'],
                            'year_founded' => !empty($item['yearFounded']) ? $item['yearFounded']
                                : Date('Y'),
                            'number_volunteers' => !empty($item['volunteers']) ? $item['volunteers']
                                : null,
                            'number_part_time_staff' => !empty($item['partTime'])
                                ? $item['partTime']
                                : null,
                            'number_full_time_staff' => !empty($item['fullTime'])
                                ? $item['fullTime']
                                : null,
                            'is_receives_newsletter' => !empty($item['receivesNewsletter']) ? 1 : 0,
                            'is_hcsraw' => !empty($item['hcsra']) ? 1 : 0,
                            'hours' => ['text' => $item['foodAcception']],
                            'mission' => $item['missionStatement'],
                            'notes' => $item['notes'],
                            'program_type_description' => $item['otherDescription'],
                            'confirmation_email_id' => Organisation::CONFIRMATION_EMAIL_ORG,
                            'is_front' => 1,
                            'address_id' => $address->id,
                            'postal_address_id' => null,
                            'volunteer_at' => $volunteer_at,
                            'old_id' => $item['id']
                        ]);
                    }
                    if (!empty($item['phones']) && count($item['phones'])) {
                        $phone_ids = [];
                        foreach ($item['phones'] as $phone) {
                            $phone['phone'] = preg_replace('/[^0-9]+/', '',
                                trim($phone['phone'], " '"));
                            $type = Phone::TYPE_PHONE;
                            if (!empty($phone['type'])
                                && array_search($phone['type'], Phone::TYPES) !== false
                            ) {
                                $type = array_search($phone['type'], Phone::TYPES);
                            }
                            if ($phone['phone']) {
                                $p = Phone::firstOrCreate([
                                    'phone' => $phone['phone'],
                                    'ext' => $phone['ext'],
                                    'type' => $type,
                                ]);
                                $phone_ids[] = $p->id;
                            }
                        }
                        if (count($phone_ids)) {
                            $org->phones()->sync($phone_ids);
                        }
                    }
                    if (!empty($item['emails']) && count($item['emails'])) {
                        $email_ids = [];
                        foreach ($item['emails'] as $email) {
                            if ($email) {
                                $p = Email::firstOrCreate(['email' => $email]);
                                $email_ids[] = $p->id;
                            }
                        }

                        if (count($email_ids)) {
                            $org->emails()->sync($email_ids);
                        }
                    }

                    if (!empty($item['types']) && count($item['types'])) {
                        $program_types = [];
                        foreach ($item['types'] as $type) {
                            if ($type) {
                                $p = ProgramType::where(['name' => $type])->first();
                                if($p){
                                    $program_types[] = $p->id;
                                }
                            }
                        }
                        if(!count($program_types)){
                            $p = ProgramType::where(['name' => 'Other'])->first();
                            if($p){
                                $program_types[] = $p->id;
                            }
                        }
                        if (count($program_types)) {
                            $org->types()->sync($program_types);
                        }
                    }
                }
                sleep(1);
            } else {
                $stop = true;
            }
            $offset += $count;
        } while ($stop == false);
    }
}
