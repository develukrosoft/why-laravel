<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Email address that will be used as address "FROM"',
                'slug' => 'email_from',
                'type_id' => Setting::TYPE_TEXT,
                'value' => 'patricia@whyhunger.org',
            ],
            [
                'name' => 'Emails of administrators who will get notifications about "New Organization" requests',
                'slug' => 'email_from_new_org',
                'type_id' => Setting::TYPE_TEXT,
                'value' => 'patricia@whyhunger.org',
            ],
            [
                'name' => 'Confirmation Email',
                'slug' => 'email_template_confirmation',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<p style="text-align:center"><a href="http://whyhunger.org"><input alt="" src="http://whyhunger.org/images/WhyHunger-ID-horiz-CMYK.jpg" style="height:80px; width:200px" type="image" /></a></p>

<p>Hello, {{contact_fullname}}!!!</p>

<p>For more than 40 years, WhyHunger has provided essential services to grassroots groups and individuals in need.&nbsp; The Nourish Network for the Right to Food, collects and distributes information about community-based resources and government programs that address the immediate and long-term needs of struggling families and individuals.</p>

<p>Through our national database of more than 18,000 organizations like yours, we promote community-based solutions to hunger and poverty and work to create connections between the public, private and non-profit sectors.&nbsp; Our goal is to facilitate the exchange of information, resources, and ideas among organizations fighting hunger and poverty.&nbsp; Each year, the WhyHunger Hotline (1-800-5-HUNGRY) receives thousands of calls from organizations, donors, and volunteers looking for information on emergency food distribution, gleaning, government programs, nutrition, funding sources, and other related topics. The Hotline refers individuals to organizations in our database.</p>

<p>Thank you for adding &quot;{{company_name}}&quot; to our network and becoming part of the soluntion.</p>

<p>To confirm your join to WhyHunger network go to {{confirmation_link}}</p>

<p>&nbsp;</p>

<p>Best regards,</p>

<p>WhyHunger</p>',
            ],
            [
                'name' => 'Notification about Confirmation Email for Administrators',
                'slug' => 'email_template_notification_confirmation_admin',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<p>Hi admin.</p>

<p>Client&nbsp;{{contact_fullname}} added join request for &quot;{{company_name}}&quot; to our network.</p>',
            ],
            [
                'name' => 'Template for "Send Update Request" Action',
                'slug' => 'email_template_update_request',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<p style="text-align:center"><a href="http://whyhunger.org"><input alt="" src="http://whyhunger.org/images/WhyHunger-ID-horiz-CMYK.jpg" style="height:120px; width:300px" type="image" /></a></p>

<p>Hello, {{contact_fullname}}!</p>

<p>For more than 40 years, WhyHunger has provided essential services to grassroots groups and individuals in need.&nbsp; The Nourish Network for the Right to Food, collects and distributes information about community-based resources and government programs that address the immediate and long-term needs of struggling families and individuals.</p>

<p>Through our national database of more than 18,000 organizations like yours, we promote community-based solutions to hunger and poverty and work to create connections between the public, private and non-profit sectors.&nbsp; Our goal is to facilitate the exchange of information, resources, and ideas among organizations fighting hunger and poverty.&nbsp; Each year, the WhyHunger Hotline (1-800-5-HUNGRY) receives thousands of calls from organizations, donors, and volunteers looking for information on emergency food distribution, gleaning, government programs, nutrition, funding sources, and other related topics. The Hotline refers individuals to organizations in our database.</p>

<p><strong>We need your help! So that we can provide accurate referrals to the growing number of individuals who call the Hotline and search our database, would you please help us update your member information</strong>? &nbsp;Take a moment to go to the link provided {{update_request_link}} and update your organization&rsquo;s information. If you would like more information about the Nourish Network for the Right to Food, please contact us at 1-800-5HUNGRY (1-800-548-6489) or via email at <a href="mailto:NHC@whyhunger.org">NHC@whyhunger.org</a>.</p>

<p>&nbsp;</p>',
            ],

            [
                'name' => 'Is Page Active',
                'slug' => 'find_food_is_active',
                'type_id' => Setting::TYPE_BOOLEAN,
                'value' => 1,
            ],
            [
                'name' => 'Text / HTML for inactive mode',
                'slug' => 'find_food_inactive_html',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<div>Page Inactive</div>',
            ],
            [
                'name' => 'Is Page Active',
                'slug' => 'join_is_active',
                'type_id' => Setting::TYPE_BOOLEAN,
                'value' => 1,
            ],
            [
                'name' => 'Text / HTML for inactive mode',
                'slug' => 'join_inactive_html',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<div>Page Inactive</div>',
            ],
            [
                'name' => 'Is Page Active',
                'slug' => 'projects_is_active',
                'type_id' => Setting::TYPE_BOOLEAN,
                'value' => 1,
            ],
            [
                'name' => 'Text / HTML for inactive mode',
                'slug' => 'projects_inactive_html',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<h1>Covid-19 Responses.</h1>

<p>NYC</p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><a href="https://www.schools.nyc.gov/freemeals" style="color:blue; text-decoration:underline">https://www.schools.nyc.gov/freemeals</a></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Philadelphia</span></span></p>

<p><a href="https://www.phila.gov/2020-03-28-where-to-find-free-nutritious-food-during-covid-19/">https://www.phila.gov/2020-03-28-where-to-find-free-nutritious-food-during-covid-19/</a></p>

<p>Los Angeles</p>

<p><a href="https://www.google.com/maps/d/u/0/viewer?mid=1_R_MQhVYaKh3A5_8oAtOtlNK2XWjzP2t&amp;ll=34.01970969160954%2C-118.38496199999997&amp;z=10">https://www.google.com/maps/d/u/0/viewer?mid=1_R_MQhVYaKh3A5_8oAtOtlNK2XWjzP2t&amp;ll=34.01970969160954%2C-118.38496199999997&amp;z=10</a></p>

<p>Bay Area</p>

<p><a href="https://www.google.com/maps/d/u/0/viewer?mid=1xfWfgbjULao-rlKJFfWT07hR-StV4Zx9&amp;ll=37.76476066961936%2C-122.32460748709411&amp;z=10">https://www.google.com/maps/d/u/0/viewer?mid=1xfWfgbjULao-rlKJFfWT07hR-StV4Zx9&amp;ll=37.76476066961936%2C-122.32460748709411&amp;z=10</a></p>

<p>Atlanta</p>

<p><a href="https://www.atlantapublicschools.us/Page/62032">https://www.atlantapublicschools.us/Page/62032</a></p>

<p>We will try to update.</p>

<p>If you are looking for a pantry please go to our Find Food page or call 1-800-548-6479.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>',
            ],
            [
                'name' => 'Is Page Active',
                'slug' => 'volunteer_is_active',
                'type_id' => Setting::TYPE_BOOLEAN,
                'value' => 1,
            ],
            [
                'name' => 'Text / HTML for inactive mode',
                'slug' => 'volunteer_inactive_html',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<div>Page Inactive</div>',
            ],
            [
                'name' => 'The number of days that a site that has been updated should not appear in the list',
                'slug' => 'volunteer_days_orgs',
                'type_id' => Setting::TYPE_NUMBER,
                'value' => 90,
            ],
            [
                'name' => 'Is Page Active',
                'slug' => 'calendar_event_is_active',
                'type_id' => Setting::TYPE_BOOLEAN,
                'value' => 1,
            ],
            [
                'name' => 'Text / HTML for inactive mode',
                'slug' => 'calendar_event_inactive_html',
                'type_id' => Setting::TYPE_CKEDITOR,
                'value' => '<div>Page Inactive</div>',
            ],
        ];

        foreach ($items as $item) {
            $setting = Setting::where('slug', $item['slug'])->first();
            if (!$setting) {
                Setting::create($item);
            }
        }
    }
}
