<?php

namespace Database\Seeders;

use App\Models\VolunteerProfile;
use App\Services\HelperService;
use Illuminate\Database\Seeder;

class VolunteerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stop = false;
        $count = 1000;
        $offset = 0;
        do {
            $url =
                "https://networks.whyhunger.org/data/volunteers?count={$count}&offset={$offset}";
            $result = HelperService::curl_get_file_contents($url);
            $r = json_decode($result, true);
            if (count($r)) {
                foreach ($r as $c => $item) {
                    echo "$c ";
                    if(!VolunteerProfile::where('old_id',$item['id'])->first()){
                        $updated_at = Date('Y-m-d H:i:s',
                            strtotime(!empty($item['updatedAt']['date']) ? $item['updatedAt']['date']
                                : 'now'));
                        $created_at = Date('Y-m-d H:i:s',
                            strtotime(!empty($item['createdAt']['date']) ? $item['createdAt']['date']
                                : 'now'));
                        $vp = VolunteerProfile::create([
                            'first_name' => $item['firstName'],
                            'last_name' => $item['lastName'],
                            'email' => $item['email'],
                            'phone' => $item['phone'],
                            'old_id' => $item['id'],
                            'data' => json_decode($item['data'], true),
                            'updated_at' => $updated_at,
                            'created_at' => $created_at,
                        ]);
                        $vp->update([
                            'updated_at' => $updated_at,
                            'created_at' => $created_at,
                        ]);
                    }
                }
                sleep(1);
            } else {
                $stop = true;
            }
            $offset += $count;
        } while ($stop == false);
    }
}
