<?php

namespace Database\Seeders;

use App\Models\OptionCategory;
use App\Models\OptionValue;
use Illuminate\Database\Seeder;

class OrganisationOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'Area Served' => [
                'County',
                'International',
                'National',
                'Neighborhood',
                'Regional',
                'Rural',
                'State',
                'Suburban / Peri-urban',
                'Urban',
            ],
            'Population Served' => [
                'Children',
                'Disabled',
                'Families',
                'Formerly Incarcerated',
                'Homeless',
                'Immigrants',
                'LGBT',
                'Men',
                'People of color',
                'Pregnant Women',
                'Seniors',
                'Unemployed',
                'Women',
                'Low-income',
                'Youth',
                'Veteran',
                'Refugees',
                'Infant',
                'Health Conditions (Cancer/HIV)',
            ],
            'Areas of Interest' => [
                'AAH&P',
                'Best practices and model programs',
                'Capacity Building',
                'Dismantling Racism',
                'Environmental / Climate Justice',
                'Food Security',
                'Funding Resources',
                'Networking with other organizations',
                'Program Profiles',
                'Research / information about food security and anti-hunger',
            ],
            'Healthy Foods' => [
                'Distributes fresh fruits and vegetables',
                'Farm-to-school / Institution programs',
                'Sources fresh food from local/regional farms',
                'Hosts CSAs, farmers markets or provides voucher program for produce',
                'Coordinates gleaning/rescuing of fresh fruits & vegetables',
                'Nutrition education, cooking or shopping classes',
                'Growing food or garden education',
                'Community food hub',
                'Medically tailored meals or food boxes',
                'Hosts healthy community meals',
                'Nutrition Education',
            ],
            'Food is Medicine' => [
                'Provides medically tailored meals/ Groceries',
                'Partners with healthcare clinic providers',
                'WIC',
                'WIC farmers market',
                'Nutrition counseling and education',
            ],
            'Government Programs' => [
                'SNAP (Food Stamps)',
                'CSFP (Commodity Supplemental Food Program)',
                'WIC (Women, Infants, and Children)',
                'TEFAP (The Emergency Food Assistance Program)',
                'SFMNP ( Senior Farmers\' Market Nutrition Program )',
                'FMNP (WIC Farmers\' Market Nutrition Program)',
                'SFSP (Summer Food Service Program)',
                'SBP (School Breakfast Program)',
            ],
            'Non-food services' => [
                'Shelter/shelter referral',
                'Job Assistance',
                'Utility assistance',
                'Housing rental assistance',
                'Non-food emergency assistance',
                'Clothing',
                'Substance abuse assistance',
                'School supplies',
                'Prescription Assistance',
                'Weatherization',
                'Income Tax Assistance',
                'Medicaid Application Assistance',
                'Transportation (gas voucher, public transportation assistance)',
                'Seasonal Services',
            ],
            'Advocacy' => [
                'Voter registration',
                'Lobbying for policy change',
                'Client led lobbying',
                'Anti-oppression training',
                'Client led activism',
                'Other please describe',
                'Incorporates non-food issues and policies',
                'Advocacy education for client advocacy',
                'Advocacy education for donors',
                'Immigration Services',
            ],
            'Languages' => [
                'Spanish',
                'French',
                'Creole',
                'Russian',
                'Chinese',
                'Hindi',
                'Urdu',
                'Vietnamese',
            ],
            'Client Center' => [
                'Cafe Style Meals',
                'Client Choice Food Pantry',
                'Client Volunteer',
                'Client Board Members',
            ],
            'Infant Care' => [
                'Formula/Baby Food',
                'Diapers',
                'Clothing',
                'Furniture',
                'WIC/CSFP',
                'Counseling for First Time Parents',
                'Prenatal/Postnatal Care',
                'Personal Hygiene Items',
            ],
        ];

        foreach ($items as $category => $item) {
            $oc = OptionCategory::firstOrCreate([
                'name' => $category,
                'type' => OptionCategory::TYPE_ORG,
                'is_active' => 1
            ]);

            foreach ($item as $option) {
                OptionValue::firstOrCreate([
                    'category_id' => $oc->id,
                    'value' => $option,
                    'is_active' => 1
                ]);
            }
        }
    }
}
