<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'United States',
                'priority' => 0,
                'code' => 'US',
                'is_active' => 1
            ]
        ];
        foreach ($items as $item) {
            $c = Country::where('name', $item['name'])->first();
            if (!$c) {
                Country::create($item);
            }
        }
    }
}
