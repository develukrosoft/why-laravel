<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFontawesomeIconToManyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_types', function (Blueprint $table) {
            $table->string('fontawesome_icon')->nullable()->after('is_active');
        });

        Schema::table('option_categories', function (Blueprint $table) {
            $table->string('fontawesome_icon')->nullable()->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_types', function (Blueprint $table) {
            $table->dropColumn(['fontawesome_icon']);
        });

        Schema::table('option_categories', function (Blueprint $table) {
            $table->dropColumn(['fontawesome_icon']);
        });
    }
}
