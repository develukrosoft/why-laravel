<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->id();
            $table->string('title')->index();
            $table->text('url');
            $table->text('image_1920x1080')->nullable();
            $table->text('image_1080x1920')->nullable();
            $table->text('description')->nullable();
            $table->integer('type_id')->default(\App\Models\CalendarEvent::TYPE_ADVERTISING)
                ->index();
            $table->integer('status_id')->default(\App\Models\CalendarEvent::STATUS_NEW)->index();
            $table->dateTime('date_at')->nullable()->index();
            $table->dateTime('date_start')->nullable()->index();
            $table->dateTime('date_end')->nullable()->index();
            $table->unsignedBigInteger('contact_id');
            $table->unsignedBigInteger('address_id');
            $table->timestamps();

            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
