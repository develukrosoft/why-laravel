<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModalMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modal_messages', function (Blueprint $table) {
            $table->id();
            $table->text('message');
            $table->integer('show_type_id')->index()->default(\App\Models\ModalMessage::TYPE_DAY);
            $table->dateTime('start_at')->index();
            $table->dateTime('end_at')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modal_messages');
    }
}
