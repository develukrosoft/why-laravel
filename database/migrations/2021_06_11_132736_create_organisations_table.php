<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisations', function (Blueprint $table) {
            $table->id();
            $table->string('title')->index();
            $table->string('website')->nullable()->index();
            $table->string('annual_budget')->nullable()->index();
            $table->integer('status_id')->default(\App\Models\Organisation::TYPE_PENDING)->index();
            $table->integer('year_founded')->default(2021)->index();
            $table->integer('number_volunteers')->nullable()->index();
            $table->integer('number_part_time_staff')->nullable()->index();
            $table->integer('number_full_time_staff')->nullable()->index();
            $table->boolean('is_receives_newsletter')->default(0)->index();
            $table->boolean('is_hcsraw')->default(0)->index();
            $table->boolean('is_front')->default(1)->index();
            $table->json('hours')->nullable();
            $table->text('mission')->nullable();
            $table->text('notes')->nullable();
            $table->text('program_type_description')->nullable();
            $table->dateTime('volunteer_at')->nullable()->index();
            $table->integer('confirmation_email_id')
                ->default(\App\Models\Organisation::CONFIRMATION_EMAIL_ORG)->index();
            $table->unsignedBigInteger('old_id')->nullable()->index();
            $table->unsignedBigInteger('address_id');
            $table->unsignedBigInteger('postal_address_id')->nullable();
            $table->timestamps();

            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('postal_address_id')->references('id')->on('addresses')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisations');
    }
}
