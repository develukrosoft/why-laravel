<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title')->index();
            $table->integer('status_id')->default(\App\Models\Project::STATUS_OPEN)->index();
            $table->date('date_start')->index();
            $table->date('date_end')->index();
            $table->json('operation_days')->nullable();
            $table->time('breakfast_from')->nullable()->index();
            $table->time('breakfast_to')->nullable()->index();
            $table->time('lunch_from')->nullable()->index();
            $table->time('lunch_to')->nullable()->index();
            $table->time('snack_from')->nullable()->index();
            $table->time('snack_to')->nullable()->index();
            $table->time('dinner_from')->nullable()->index();
            $table->time('dinner_to')->nullable()->index();
            $table->text('notes')->nullable();
            $table->string('api_import_hash')->nullable();
            $table->unsignedBigInteger('address_id');
            $table->unsignedBigInteger('postal_address_id')->nullable();
            $table->timestamps();

            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('postal_address_id')->references('id')->on('addresses')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
