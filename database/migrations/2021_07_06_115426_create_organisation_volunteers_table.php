<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganisationVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisation_volunteers', function (Blueprint $table) {
            $table->id();
            $table->json('data')->nullable();
            $table->integer('status_id')->default(\App\Models\OrganisationVolunteer::STATUS_NEW)
                ->index();
            $table->dateTime('approve_at')->nullable()->index();
            $table->unsignedBigInteger('old_id')->nullable()->index();
            $table->unsignedBigInteger('organisation_id');
            $table->unsignedBigInteger('volunteer_id')->nullable();

            $table->foreign('organisation_id')->references('id')->on('organisations')
                ->onDelete('cascade');
            $table->foreign('volunteer_id')->references('id')->on('volunteer_profiles')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisation_volunteers');
    }
}
