<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTagIconIdToManyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->unsignedBigInteger('tag_icon_id')->nullable()->after('address_id');
            $table->foreign('tag_icon_id')
                ->references('id')
                ->on('tag_icons')
                ->onDelete('set null');
        });

        Schema::table('organisations', function (Blueprint $table) {
            $table->unsignedBigInteger('tag_icon_id')->nullable()->after('address_id');
            $table->foreign('tag_icon_id')
                ->references('id')
                ->on('tag_icons')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign(['tag_icon_id']);
            $table->dropColumn(['tag_icon_id']);
        });

        Schema::table('organisations', function (Blueprint $table) {
            $table->dropForeign(['tag_icon_id']);
            $table->dropColumn(['tag_icon_id']);
        });
    }
}
