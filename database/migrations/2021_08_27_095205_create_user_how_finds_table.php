<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserHowFindsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_how_finds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('value_id')->nullable();
            $table->unsignedBigInteger('contact_id')->nullable();
            $table->string('other')->nullable()->index();
            $table->string('parent_type')->nullable()->index();
            $table->unsignedBigInteger('parent_id')->nullable()->index();
            $table->timestamps();

            $table->foreign('value_id')->references('id')->on('user_how_find_values')
                ->onDelete('set null');
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_how_finds');
    }
}
