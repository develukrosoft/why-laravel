<?php

return [
    'subheader' => [
        'change_language' => 'En Español',
        'find_food' => 'FIND FOOD',
        'join_the_network' => 'JOIN THE NETWORK',
        'network' => 'NETWORK',
        'description' => 'Welcome to the WhyHunger database. Find community-based organizations and emergency food providers who are leading by example - actively forging new ideas, improving the health of their communities, and building the movement to end hunger and poverty.',
        'tab_find_food' => 'Find Food',
        'tab_add_a_site' => 'Add a Site',
        'tab_summer_meals' => 'Summer Meals',
        'tab_volunteer' => 'Volunteer',
    ]
];
