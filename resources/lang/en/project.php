<?php

return [
    'index' => [
        'sites_projects' => 'SITES / PROJECTS',
        'pagination_description' => 'To show more sites on the map please go to the next page using the navigation below.',
        'date_range' => 'Date Range',
        'address' => 'Address',
        'search_options' => 'SEARCH OPTIONS',
        'zip' => 'Zip',
        'center_zip' => 'Zip',
        'distance' => 'Distance',
        'miles' => 'Miles',
        'kilometers' => 'Kilometers',
        'additional_options' => 'Additional options',
        'name' => 'Name',
        'country' => 'Country',
        'state' => 'State',
        'city' => 'City',
        'search' => 'Search',
        'reset' => 'Reset',
    ],
    'view' => [
        'notes' => 'Notes',
        'address' => 'Address',
        'site_details' => 'SITE DETAILS',
        'date_range' => 'Date range',
        'operation_days' => 'Days of Operation',
        'breakfast_time' => 'Breakfast Time',
        'lunch_time' => 'Lunch Time',
        'snack_time' => 'Snack Time',
        'dinner_time' => 'Dinner / Supper Time',
    ],
];
