<?php

return [
    'index' => [
        'sites_projects' => 'SITES / PROJECTS',
        'pagination_description' => 'Para mostrar más sitios en el mapa, vaya a la página siguiente utilizando la navegación a continuación.',
        'date_range' => 'Date Range',
        'address' => 'Dirección',
        'search_options' => 'OPCIONES DE BÚSQUEDA',
        'zip' => 'Codigo Postal',
        'center_zip' => 'Codigo postal para el centro de radio',
        'distance' => 'Distancia',
        'miles' => 'Millas',
        'kilometers' => 'Kilómetro',
        'additional_options' => 'Additional options',
        'name' => 'Nombre',
        'country' => 'País',
        'state' => 'State',
        'city' => 'City',
        'search' => 'Buscar',
        'reset' => 'Aclarar',
    ],
    'view' => [
        'notes' => 'Notes',
        'address' => 'Dirección',
        'site_details' => 'SITE DETAILS',
        'date_range' => 'Date range',
        'operation_days' => 'Days of Operation',
        'breakfast_time' => 'Breakfast Time',
        'lunch_time' => 'Lunch Time',
        'snack_time' => 'Snack Time',
        'dinner_time' => 'Dinner / Supper Time',
    ],
];
