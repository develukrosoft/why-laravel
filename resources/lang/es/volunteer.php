<?php

return [
    'index' => [
        'instruction_1' => 'WhyHunger provides a searchable directory of locations offering food assistance to the public.',
        'instruction_2' => 'WhyHunger relies on local partners and volunteers like yourself to maintain an up-to-date resource for the public. In response to the food insecurity crisis escalated by COVID-19, you can now volunteer to help from home! Click “Get Started.”',
        'need_assistance' => 'Need food assistance?',
        'visit' => 'Visit',
        'calling_volunteers' => 'Calling all volunteers! We need your help!',
        'description' => 'The operating hours of food pantries and soup kitchens are constantly changing. The way they are operating is also changing. There are over 60,000 food assistance sites across the United States, and many are temporarily or permanently closed due to Covid-19. Your phone calls and research will guarantee that people in need are going to the places that can actually provide assistance.',
        'admonition' => 'Your task? Help us call food pantries and soup kitchens and confirm their information is up to date. Simple!',
        'enter_zip' => 'Enter ZIP code (optional)',
        'started' => 'Get started',
    ],
    'search'=>[
        'call'=>'Call one or all of these food assistance sites to help verify their information.',
        'call_description'=>'Each call should take 5 minutes or less. You will need a phone to complete this task, and it’s best to complete on a computer.',
        'feedback_form'=>'Feedback form',
        'add_a_site'=>'Add a site',
        'thank_for_time'=>'Thank you for your time!',
        'name'=>'Enter your name',
        'email'=>'Enter your email to receive updates about WhyHunger',
        'feedback'=>'Please take a moment to give us some feedback (optional)',
        'submit'=>'Submit',
    ]
];
