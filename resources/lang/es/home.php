<?php

return [
    'subheader' => [
        'change_language' => 'In English',
        'find_food' => 'ENCONTRAR COMIDA',
        'join_the_network' => 'JOIN THE NETWORK',
        'network' => 'NETWORK',
        'description' => 'Bienvenido a la base de datos WhyHunger. Encuentre organizaciones comunitarias y proveedores de alimentos de emergencia que estén predicando con el ejemplo, forjando activamente nuevas ideas, mejorando la salud de sus comunidades y construyendo el movimiento para acabar con el hambre y la pobreza.',
        'tab_find_food' => 'Encontrar Comida',
        'tab_add_a_site' => 'Agregar un sitio',
        'tab_summer_meals' => 'Comidas de verano',
        'tab_volunteer' => 'Voluntaria/o',
    ]
];
