@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">Find</div>
            <div class="body">
                <div class="input">
                    <select name="country_id">
                        @foreach(\App\Models\UserHowFindValue::all() as $V)
                            <option value="{{$V->id}}" {{!empty($item) && $V->id == $item->value_id?'selected':''}}>{{$V->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.userHowFind.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
