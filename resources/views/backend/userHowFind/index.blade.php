@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">"How did you find us"</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            {{--<div class="group">--}}
                {{--<a href="{{route('backend.userHowFind.form')}}">Add New</a>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>Value</td>
                <td>Parent</td>
                <td>Created At</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->value->name}}</td>
                    <td>
                        @if($item->parent_type == 'organisation' && $item->parent_id)
                            @if(Auth::user()->role == 'admin')
                                <a href="{{route('backend.organisation.form.id',$item->parent_id)}}">{{$item->parent->title}}</a>
                            @else
                                {{$item->parent->title}}
                            @endif
                        @elseif($item->parent_type == 'organisation_volunteer' && $item->parent_id)
                            <a href="{{route('backend.volunteerProfile.form.id',$item->parent_id)}}">Volunteer Form
                                #{{$item->parent_id}}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>{{Date('Y-m-d H:i',strtotime($item->created_at))}}</td>
                    <td>
                        <a href="{{route('backend.userHowFind.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.userHowFind.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
