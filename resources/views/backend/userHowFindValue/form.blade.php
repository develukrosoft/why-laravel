@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">Title</div>
            <div class="body">
                <input type="text" placeholder="Title" name="name" value="{{!empty($item)?$item->name:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Position</div>
            <div class="body">
                <input type="number" placeholder="0" name="position" value="{{!empty($item)?$item->position:1}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Is active (show front)</div>
            <div class="body">
                <div class="input">
                    <select name="is_active">
                        <option value="1">Yes</option>
                        <option value="0" {{!empty($item) && !$item->is_active?'selected':''}}>No</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.userHowFindValue.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
