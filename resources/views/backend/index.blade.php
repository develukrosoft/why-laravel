@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Latest changes in system</div>
            <div>(created, updated & imports)</div>
        </div>
    </div>
    <div class="m_overview">
        @if(Auth::user()->role == 'admin')
            <div class="block">
                <div class="header">
                    <div>Sites / Projects</div>
                    <div class="count">{{$project['count']}}</div>
                </div>
                <table>
                    @foreach($project['items'] as $item)
                        <tr>
                            <td><a href="{{route('backend.project.form.id',$item->id)}}">{{$item->title}}</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="block">
                <div class="header">
                    <div>Organization</div>
                    <div class="count">{{$organisation['count']}}</div>
                </div>
                <table>
                    @foreach($organisation['items'] as $item)
                        <tr>
                            <td><a href="{{route('backend.organisation.form.id',$item->id)}}">{{$item->title}}</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="block">
                <div class="header">
                    <div>Contacts</div>
                    <div class="count">{{$contact['count']}}</div>
                </div>
                <table>
                    @foreach($contact['items'] as $item)
                        <tr>
                            <td>
                                <a href="{{route('backend.contact.form.id',$item->id)}}">{{$item->first_name}} {{$item->last_name}}</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="block">
                <div class="header">
                    <div>Users</div>
                    <div class="count">{{$user['count']}}</div>
                </div>
                <table>
                    @foreach($user['items'] as $item)
                        <tr>
                            <td><a href="{{route('backend.user.form.id',$item->id)}}">{{$item->full_name}}</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
        <div class="block">
            <div class="header">
                <div>Volunteers</div>
                <div class="count">{{$volunteer['count']}}</div>
            </div>
            <table>
                @foreach($volunteer['items'] as $item)
                    <tr>
                        <td>
                            <a href="{{route('backend.volunteerProfile.form.id',$item->id)}}">{{$item->first_name}} {{$item->last_name}}</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="block">
            <div class="header">
                <div>Volunteer Organisations</div>
                <div class="count">{{$volunteer_organisation['count']}}</div>
            </div>
            <table>
                @foreach($volunteer_organisation['items'] as $item)
                    <tr>
                        <td>
                            <a href="{{route('backend.volunteer.form.id',$item->id)}}">{{$item->organisation->title}}</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
