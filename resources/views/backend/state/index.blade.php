@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">States</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.state.index')}}?action=export">CSV export</a>
                <a href="{{route('backend.state.form')}}">Add New</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>Name</td>
                <td>Code</td>
                <td>Country</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->code}}</td>
                    <td>{{$item->country->name}}</td>
                    <td>
                        <a href="{{route('backend.state.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.state.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
