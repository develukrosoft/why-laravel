@extends('backend.partials.layout')

@section('content')
    <div class="volunteer_form">
        <form method="post" id="volunteer_form">
            @if(!empty($item))
                <div class="actions right">
                    <a class="delete" href="{{route('backend.volunteer.action.id',$item->id)}}?action=delete">Delete</a>
                </div>
            @endif
            <div class="question-section">
                <div class="question">
                    <img src="/img/volunteer/speech-red.svg" />
                    <span>Unable to Complete</span>
                </div>
                <div class="answer">
                    <div class="option">
                        <input value="Nobody picked up, and there was no pre-recorded message"
                               type="checkbox" name="answers[unable_complete][unable_complete_1]"
                               {{!empty($item->data['unable_complete']['unable_complete_1'])?'checked':''}}
                               id="unable_complete_1" />
                        <label for="unable_complete_1">Nobody picked up, and there was no pre-recorded
                            message</label>
                    </div>
                    <div class="option wrong_toggle">
                        <input value="Wrong number (number is for a different institution, or number is disconnected)"
                               type="checkbox" name="answers[unable_complete][unable_complete_2]"
                               id="unable_complete_2" {{!empty($item->data['unable_complete']['unable_complete_2'])?'checked':''}}
                        />
                        <label for="unable_complete_2">Wrong number (number is for a different institution,
                            or
                            number is disconnected)</label>
                    </div>
                    <div class="option wrong_option {{!empty($item->data['unable_complete']['unable_complete_2'])?'wrong_active':''}}"
                         data-parent_id="unable_complete_2"
                         style="display: none">
                        <input value="No number was found" name="answers[unable_complete][unable_complete_2_not_found]"
                               {{!empty($item->data['unable_complete']['unable_complete_2_not_found'])?'checked':''}}
                               type="checkbox" id="unable_complete_2_not_found"
                        />
                        <label for="unable_complete_2_not_found">No number was found</label>
                    </div>
                    <div class="option wrong_option {{!empty($item->data['unable_complete']['unable_complete_2'])?'wrong_active':''}}"
                         data-parent_id="unable_complete_2"
                         style="display: none">
                        <label for="unable_complete_2_number">Please perform a web search to find the
                            appropriate
                            number and call it to confirm.</label>
                        <textarea placeholder="+0000000000" name="answers[unable_complete][unable_complete_2_number]"
                                  id="unable_complete_2_number" rows="1"
                        >{{!empty($item->data['unable_complete']['unable_complete_2_number'])?$item->data['unable_complete']['unable_complete_2_number']:''}}</textarea>
                    </div>
                    <div class="option wrong_toggle">
                        <input value="Wrong number (number has changed)" type="checkbox"
                               name="answers[unable_complete][unable_complete_3]"
                               id="unable_complete_3" {{!empty($item->data['unable_complete']['unable_complete_3'])?'checked':''}}
                        />
                        <label for="unable_complete_3">Wrong number (number has changed)</label>
                    </div>
                    <div class="option wrong_option {{!empty($item->data['unable_complete']['unable_complete_3'])?'wrong_active':''}}"
                         data-parent_id="unable_complete_3"
                         style="display: none">
                        <input value="No number was found" name="answers[unable_complete][unable_complete_3_not_found]"
                               {{empty($item->data['unable_complete']['unable_complete_3_number']) && !empty($item->data['unable_complete']['unable_complete_3'])?'checked':''}}
                               type="checkbox" id="unable_complete_3_not_found"
                        />
                        <label for="unable_complete_3_not_found">No number was found</label>
                    </div>
                    <div class="option wrong_option {{!empty($item->data['unable_complete']['unable_complete_3'])?'wrong_active':''}}"
                         data-parent_id="unable_complete_3"
                         style="display: none">
                        <label for="unable_complete_3_number">Please perform a web search to find the
                            appropriate
                            number and call it to confirm.</label>
                        <textarea placeholder="+0000000000" name="answers[unable_complete][unable_complete_3_number]"
                                  id="unable_complete_3_number" rows="1"
                        >{{!empty($item->data['unable_complete']['unable_complete_3_number'])?$item->data['unable_complete']['unable_complete_3_number']:''}}</textarea>
                    </div>
                    <div class="option">
                        <input value="Staff member didn’t speak English" type="checkbox"
                               name="answers[unable_complete][unable_complete_4]"
                               id="unable_complete_4" {{!empty($item->data['unable_complete']['unable_complete_4'])?'checked':''}}
                        />
                        <label for="unable_complete_4">Staff member didn’t speak English</label>
                    </div>
                    <div class="option">
                        <input value="This location is closed indefinitely due to COVID-19" type="checkbox"
                               name="answers[unable_complete][unable_complete_5]"
                               id="unable_complete_5" {{!empty($item->data['unable_complete']['unable_complete_5'])?'checked':''}}
                        />
                        <label for="unable_complete_5">This location is closed indefinitely due to
                            COVID-19</label>
                    </div>
                    <div class="option">
                        <input value="Location does not offer free food assistance to the general public"
                               type="checkbox" name="answers[unable_complete][unable_complete_6]"
                               id="unable_complete_6" {{!empty($item->data['unable_complete']['unable_complete_6'])?'checked':''}}
                        />
                        <label for="unable_complete_6">Location does not offer free food assistance to the
                            general
                            public</label>
                    </div>
                    <div class="option">
                        <input value="Staff weren't able to answer questions at this time"
                               type="checkbox" name="answers[unable_complete][unable_complete_7]"
                               id="unable_complete_7" {{!empty($item->data['unable_complete']['unable_complete_7'])?'checked':''}}
                        />
                        <label for="unable_complete_7">Staff weren't able to answer questions at this
                            time</label>
                    </div>
                    <div class="option">
                        <input value="Other" name="answers[unable_complete][unable_complete_8]"
                               {{!empty($item->data['unable_complete']['unable_complete_8'])?'checked':''}}
                               type="checkbox" id="unable_complete_8"
                        />
                        <label for="unable_complete_8">Other</label>
                    </div>
                    <div class="option">
                        <div class="_label">If other, please provide:</div>
                        <textarea id="unable_complete_other"
                                  rows="2">{{!empty($item->data['unable_complete']['other'])?$item->data['unable_complete']['other']:''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="question-section">
                <div class="question">
                    <img src="/img/volunteer/speech-red.svg" />
                    <span>How confident are you in the answers you've provided today?</span>
                </div>
                <div class="answer">
                    <div class="option">
                        <input value="Very confident" type="checkbox" class="check_one"
                               data-group="confident" name="answers[confident]"
                               {{!empty($item->data['confident']) && $item->data['confident'] == 'Very confident'?'checked':''}}
                               id="confident_1" />
                        <label for="confident_1">Very confident</label>
                    </div>
                    <div class="option">
                        <input value="Somewhat confident" type="checkbox" class="check_one" id="confident_2"
                               name="answers[confident]"
                               data-group="confident" {{!empty($item->data['confident']) && $item->data['confident'] == 'Somewhat confident'?'checked':''}} />
                        <label for="confident_2">Somewhat confident</label>
                    </div>
                    <div class="option">
                        <input value="Not very confident" type="checkbox" class="check_one" id="confident_3"
                               name="answers[confident]"
                               data-group="confident" {{!empty($item->data['confident']) && $item->data['confident'] == 'Not very confident'?'checked':''}} />
                        <label for="confident_3">Not very confident</label>
                    </div>
                </div>
            </div>
            @if(!empty($item->data['number_search']['number']))
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Please perform a web search to find the appropriate number
                                    and call it to confirm:</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <div class="_label">Please perform a web search to find the appropriate number
                                and call it to confirm:
                            </div>
                            <textarea name="answers[number_search][number]" rows="1"
                                      placeholder="+00000000">{{$item->data['number_search']['number']}}</textarea>
                        </div>
                        <div class="option">
                            <div class="_label">Please leave any additional information about this number here:</div>
                            <textarea id="unable_complete_other" name="answers[number_search][description]"
                                      rows="2">{{!empty($item->data['number_search']['description'])?$item->data['number_search']['description']:''}}</textarea>
                        </div>
                    </div>
                </div>
            @endif
            <div class="step">
                <div class="number"><span>1</span></div>
                <div class="title">The Basics</div>
            </div>
            <div class="question-section">
                <div class="question">
                    <img src="/img/volunteer/speech-red.svg" />
                    <span>Can you confirm that organisation name "{{$item->organisation->title}}"?</span>
                </div>
                <div class="answer">
                    <div class="option">
                        <input type="checkbox" class="check_one" data-group="organisation_name"
                               name="answers[organisation_name][correct]"
                               id="correct_organisation_name" {{!empty($item->data['organisation_name']['correct'])?'checked':''}}/>
                        <label for="correct_organisation_name">Yes, that's the correct name</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" class="check_one" data-group="organisation_name"
                               id="not_correct_organisation_name"
                               name="answers[organisation_name][not_correct]" {{!empty($item->data['organisation_name']['not_correct'])?'checked':''}}/>
                        <label for="not_correct_organisation_name">No</label>
                    </div>
                    <div class="option">
                        <div class="_label">If no, please provide the organisation name:
                        </div>
                        <textarea name="answers[organisation_name][new_organisation_name]"
                                  rows="1">{{!empty($item->data['organisation_name']['new_organisation_name'])?$item->data['organisation_name']['new_organisation_name']:''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="question-section">
                <div class="question">
                    <img src="/img/volunteer/speech-red.svg" />
                    <span>Can you confirm that you're located at {{$item->organisation->address->FullAddress}}?</span>
                </div>
                <div class="answer">
                    <div class="option">
                        <input type="checkbox" class="check_one" data-group="address"
                               name="answers[address][correct]"
                               {{!empty($item->data['address']['correct'])?'checked':''}}
                               id="correct_address" />
                        <label for="correct_address">Yes, that's the correct address</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" class="check_one" data-group="address" id="not_correct_address"
                               name="answers[address][not_correct]" {{!empty($item->data['address']['not_correct'])?'checked':''}}/>
                        <label for="not_correct_address">No</label>
                    </div>
                    <div class="option">
                        <div class="_label">If no, please provide the street address (make sure that the spelling of
                            address
                            is correct):
                        </div>
                        <textarea name="answers[address][new_address]"
                                  rows="1">{{!empty($item->data['address']['new_address'])?$item->data['address']['new_address']:''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="question-section">
                <div class="question">
                    <img src="/img/volunteer/speech-red.svg" />
                    <span>Can you confirm that this location provides food assistance to the public? If so, please tell me which programs you have:</span>
                </div>
                <div class="answer _col-2">
                    @foreach(\App\Models\ProgramType::where('is_active',1)->get() as $type)
                        <div class="option">
                            <input type="checkbox" name="answers[program_types][]" value="{{$type->id}}"
                                   id="{{$type->id}}"
                                    {{in_array($type->id,(!empty($item->data['program_types'])?$item->data['program_types']:[]))?'checked':''}}
                                    {{in_array($type->name,(!empty($item->data['program_types'])?$item->data['program_types']:[]))?'checked':''}}
                            />
                            <label for="{{$type->id}}">{{$type->name}}</label>
                            <div class="type_description_help">?</div>
                            <div class="type_description">{{$type->description}}</div>
                        </div>
                    @endforeach
                </div>
            </div>
            @if($item->organisation->website)
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Can you confirm that your website is {{ $item->organisation->website }}?</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="website" id="correct_website"
                                   name="answers[website][correct]" {{!empty($item->data['website']['correct'])?'checked':''}}/>
                            <label for="correct_website">Yes, that's the correct website</label>
                        </div>
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="website" id="not_correct_website"
                                   name="answers[website][not_correct]" {{!empty($item->data['website']['not_correct'])?'checked':''}}/>
                            <label for="not_correct_website">No</label>
                        </div>
                        <div class="option">
                            <div class="_label">If no (or website isn't listed), please provide the website:</div>
                            <textarea name="answers[website][new_website]"
                                      rows="1"> {{!empty($item->data['website']['new_website'])?$item->data['website']['new_website']:''}}</textarea>
                        </div>
                    </div>
                </div>
            @else
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>We don't have a website listed for you. Do you have one?</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="website"
                                   {{!empty($item->data['website']['not_have_website'])?'checked':''}}
                                   name="answers[website][not_have_website]" id="not_have_website" />
                            <label for="not_have_website">No</label>
                        </div>
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="website"
                                   {{!empty($item->data['website']['have_website'])?'checked':''}}
                                   name="answers[website][have_website]" id="have_website" />
                            <label for="have_website">Yes</label>
                        </div>
                        <div class="option">
                            <div class="_label">If yes, please provide the website:</div>
                            <textarea name="answers[website][new_website]"
                                      rows="1">{{!empty($item->data['website']['new_website'])?$item->data['website']['new_website']:''}}</textarea>
                        </div>
                    </div>
                </div>
            @endif
            <div class="step">
                <div class="number"><span>2</span></div>
                <div class="title">Pickup Hours</div>
            </div>
            <div class="question-section">
                <div class="question">
                    <img src="/img/volunteer/speech-red.svg" />
                    <span>What are the hours that you are open to the public?</span>
                </div>
                <div class="answer">
                    @php
                        $days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
                    @endphp
                    @foreach($days as $index=>$day)
                        <div>
                            <div class="day_title">{{ $day }}</div>
                            <div class="day_list" id="day_hours_{{ $loop->index }}" data-day_name="{{ $day }}">
                                <div class="add_hours" data-index="{{ $loop->index }}" data-day_name="{{ $day }}">
                                    <div class="image"><img src="/img/volunteer/plus-red.svg" /></div>
                                    <div class="text">Add hours</div>
                                </div>
                                @if(!empty($item->data['time'][$day]))
                                    @foreach($item->data['time'][$day] as $i=>$d)
                                        <div class="item time_item">
                                            <div class="form change_time">
                                                <div class="from_time">
                                                    <div style="position: relative;">
                                                        <input type="number" class="change_time time_hour"
                                                               value="{{$d['from_time_hour']}}"
                                                               min="0" max="12"
                                                               name="answers[time][{{$day}}][{{$i}}][from_time_hour]" />
                                                        <div class="separator">:</div>
                                                    </div>
                                                    <div><input type="number" class="change_time time_min"
                                                                value="{{$d['from_time_min']}}"
                                                                min="00" max="59"
                                                                name="answers[time][{{$day}}][{{$i}}][from_time_min]" />
                                                    </div>
                                                    <div>
                                                        <select class="change_time time_type"
                                                                name="answers[time][{{$day}}][{{$i}}][from_time_type]">
                                                            <option>AM</option>
                                                            <option {{$d['from_time_type'] == 'PM'?'selected':''}}>
                                                                PM
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="divider"></div>
                                                <div class="to_time">
                                                    <div style="position: relative;">
                                                        <input type="number" class="change_time time_hour"
                                                               value="{{$d['to_time_hour']}}"
                                                               min="0" max="12"
                                                               name="answers[time][{{$day}}][{{$i}}][to_time_hour]" />
                                                        <div class="separator">:</div>
                                                    </div>
                                                    <div><input type="number" class="change_time time_min"
                                                                value="{{$d['to_time_min']}}"
                                                                min="00" max="59"
                                                                name="answers[time][{{$day}}][{{$i}}][to_time_min]" />
                                                    </div>
                                                    <div>
                                                        <select class="change_time time_type"
                                                                name="answers[time][{{$day}}][{{$i}}][to_time_type]">
                                                            <option>AM</option>
                                                            <option {{$d['to_time_type'] == 'PM'?'selected':''}}>PM
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div style="margin-left: 10px;">
                                                    <div>
                                                        <select class="change_time time_day"
                                                                name="answers[time][{{$day}}][{{$i}}][day]">
                                                            <option>Every {{$day}}</option>
                                                            <option {{!empty($d['day']) && $d['day'] == 'Every first '. $day?'selected':''}}>
                                                                Every first {{$day}}</option>
                                                            <option {{!empty($d['day']) && $d['day'] == 'Every second '. $day?'selected':''}}>
                                                                Every second {{$day}}</option>
                                                            <option {{!empty($d['day']) && $d['day'] == 'Every third '. $day?'selected':''}}>
                                                                Every third {{$day}}</option>
                                                            <option {{!empty($d['day']) && $d['day'] == 'Every fourth '. $day?'selected':''}}>
                                                                Every fourth {{$day}}</option>
                                                            <option {{!empty($d['day']) && $d['day'] == 'Every 1st and 2nd '. $day?'selected':''}}>
                                                                Every 1st and 2nd {{$day}}</option>
                                                            <option {{!empty($d['day']) && $d['day'] == 'Every 3rd and 4th '. $day?'selected':''}}>
                                                                Every 3rd and 4th {{$day}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            @if(count($days) !== $loop->index +1)
                                <hr>
                            @endif
                        </div>
                    @endforeach
                    <div class="option">
                        <input type="checkbox" class="check_one" data-group="by_appointment_only"
                               name="answers[by_appointment_only]"
                               id="by_appointment_only" {{!empty($item->data['by_appointment_only'])?'checked':''}}/>
                        <label for="by_appointment_only">By Appointment Only</label>
                    </div>
                </div>

                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>So just to confirm, that's:</span>
                    </div>
                    <div class="answer">
                        <div id="day_time_list">
                        </div>
                    </div>
                </div>
                <div class="step">
                    <div class="number"><span>3</span></div>
                    <div class="title">Organization details</div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Do you provide any additional programs like SNAP sign-up support, support for new parents, WIC, fresh produce distribution, advocacy programs, or nutrition counseling, etc?</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                        <textarea name="answers[additional_details]"
                                  rows="5">{{!empty($item->data['additional_details'])?$item->data['additional_details']:''}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="step">
                    <div class="number"><span>4</span></div>
                    <div class="title">Contact Info</div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>THANK YOU so much for your time and for answering these questions. And thank you for all the important work you are doing to help your communities, especially during this difficult time. We are so grateful for you, and others like you, working to ensure no family goes hungry. Is it possible to get the contact information for the best person to reach in the future, in case we have more questions?</span>
                    </div>
                    <div class="answer _col-2">
                        <div class="w49p">
                            <div class="_label">Phone(s)</div>
                            <div id="phone_list" class="w100p">
                                @if(!empty($item->data['contact']['phone']) && count($item->data['contact']['phone']))
                                    @foreach($item->data['contact']['phone'] as $phone)
                                        <textarea name="answers[contact][phone][c_{{ $loop->index }}][phone]"
                                                  rows="1"
                                                  placeholder="Phone Number">{{ $phone['phone'] }}</textarea>
                                        <textarea name="answers[contact][phone][c_{{ $loop->index }}][ext]" rows="1"
                                                  placeholder="Ext">@if(!empty($phone['ext'])){{ $phone['ext'] }}@endif</textarea>
                                    @endforeach
                                @else
                                    <textarea name="answers[contact][phone][c_1][phone]" rows="1"
                                              placeholder="Phone Number"></textarea>
                                    <textarea name="answers[contact][phone][c_1][ext]" rows="1"
                                              placeholder="Ext"></textarea>
                                @endif
                            </div>
                            <div id="add_phone_form" class="add_item">Add phone</div>
                        </div>
                        <div class="w49p">
                            <div class="_label">Email(s)</div>
                            <div id="email_list" class="w100p">
                                @if(!empty($item->data['contact']['email']) && count($item->data['contact']['email']))
                                    @foreach($item->data['contact']['email'] as $email)
                                        <textarea name="answers[contact][email][]" rows="1"
                                                  placeholder="Email">{{ $email }}</textarea>
                                    @endforeach
                                @else
                                    <textarea name="answers[contact][email][]" rows="1"
                                              placeholder="Email"></textarea>
                                @endif
                            </div>
                            <div id="add_email_form" class="add_item">Add email</div>
                        </div>
                    </div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Finally, what is the best way to re-verify this information with you in the future? Here are a few options:</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="contact_again"
                                   value="Would you like us to call you again?"
                                   {{!empty($item->data['contact_again']['1'])?'checked':''}}
                                   name="answers[contact_again][1]" id="contact_again_1" />
                            <label for="contact_again_1">Would you like us to call you again?</label>
                        </div>
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="contact_again"
                                   {{!empty($item->data['contact_again']['2'])?'checked':''}}
                                   value="Would you like to log in to a system to update your own information as it changes?"
                                   name="answers[contact_again][2]" id="contact_again_2" />
                            <label for="contact_again_2">Would you like to log in to a system to update your own
                                information
                                as
                                it changes?</label>
                        </div>
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="contact_again"
                                   {{!empty($item->data['contact_again']['3'])?'checked':''}}
                                   value="Would you like to confirm via text message on a regular basis?"
                                   name="answers[contact_again][3]" id="contact_again_3" />
                            <label for="contact_again_3">Would you like to confirm via text message on a regular
                                basis?</label>
                        </div>
                    </div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>General pantry notes.</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <div class="_label">Enter any other relevant notes that did not fit into the sections
                                above.
                            </div>
                            <textarea name="answers[contact_notes][note]"
                                      rows="5">{{!empty($item->data['contact_notes']['note'])?$item->data['contact_notes']['note']:''}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <button type="submit" class="save">Save</button>
                    <a href="{{route('backend.volunteer.action.id',$item->id)}}?action=update" type="submit"
                       class="update">Update organisation</a>
                    <a href="{{route('backend.volunteer.action.id',$item->id)}}?action=delete" type="submit"
                       class="delete">Delete</a>
                </div>
            </div>
        </form>
    </div>
@endsection
