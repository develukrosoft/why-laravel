<div class="m_modal" id="volunteerExportModal">
    <div class="body">
        <div class="close close_modal" data-modal_id="volunteerExportModal">X</div>
        <form class="m_form" action="{{route('backend.volunteer.index')}}">
            <input type="hidden" name="action" value="export" />
            <div class="field">
                <div class="body">
                    <div class="input">
                        <div class="label">Date
                            <small>(from - to)</small>
                        </div>
                        <div class="group_input two">
                            <input type="date" name="date_start" required>
                            <input type="date" name="date_end" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="actions">
                <button type="submit" class="save">Export</button>
                <button type="button" class="cancel close_modal" data-modal_id="volunteerExportModal">Cancel</button>
            </div>
        </form>
    </div>
</div>
