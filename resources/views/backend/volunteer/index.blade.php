@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Organization Volunteer</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <button class="show_modal" data-modal_id="volunteerExportModal">CSV export</button>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td style="width: 60px">ID</td>
                <td>Organization</td>
                <td>Volunteer</td>
                <td style="text-align: center;width: 100px">Created At</td>
                <td style="width: 100px">Actions</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>
                        @if(Auth::user()->role == 'admin')
                            <a href="{{route('backend.organisation.form.id',$item->organisation->id)}}">{{$item->organisation->title}}</a>
                        @else
                            {{$item->organisation->title}}
                        @endif
                    </td>
                    <td>
                        @if($item->volunteer_id)
                            <a href="{{route('backend.volunteerProfile.form.id',$item->volunteer_id)}}">{{$item->volunteer->email?$item->volunteer->email:'#'.$item->volunteer_id}}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td style="text-align: center">{{Date('Y-m-d',strtotime($item->created_at))}}
                        <br />{{Date('H:i',strtotime($item->created_at))}}</td>
                    <td>
                        <a href="{{route('backend.volunteer.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.volunteer.action.id',$item->id)}}?action=update">Accept</a>
                        <a href="{{route('backend.volunteer.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
    @include('backend.volunteer.partials.exportModal')
@endsection
