@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">Title</div>
            <div class="body">
                <input type="text" placeholder="Title" name="name" value="{{!empty($item)?$item->name:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Description</div>
            <div class="body">
                <textarea rows="5" placeholder="Description"
                          name="description">{{!empty($item)?$item->description:''}}</textarea>
            </div>
        </div>
        <div class="field">
            <div class="title">Fontawesome icon</div>
            <div class="body">
                <input type="text" placeholder="icon" name="fontawesome_icon" value="{{!empty($item)?$item->fontawesome_icon:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Is active (show front)</div>
            <div class="body">
                <div class="input">
                    <select name="is_active">
                        <option value="1">Yes</option>
                        <option value="0" {{!empty($item) && !$item->is_active?'selected':''}}>No</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.programType.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
