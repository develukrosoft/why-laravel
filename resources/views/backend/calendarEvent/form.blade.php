@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post" enctype="multipart/form-data">
        <div class="field">
            <div class="title">Title</div>
            <div class="body">
                <input type="text" placeholder="Title" name="title" value="{{!empty($item)?$item->title:''}}" required>
            </div>
        </div>
        <div class="field w50" style="display: block;">
            <div class="title">Address</div>
            <div class="body">
                <div class="input">
                    <div class="label">Country</div>
                    <select name="address[country_id]" class="change_country" data-state_id="address_state" required>
                        <option value="">---</option>
                        @foreach(\App\Models\Country::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item->address) && $item->address->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">State</div>
                    <select name="address[state_id]" class="change_state" data-city_id="address_city" required
                            id="address_state">
                        <option value="">---</option>
                        @if(!empty($item->address))
                            @foreach(\App\Services\LocationService::getStatesByCountryId($item->address->country_id) as $s)
                                <option value="{{$s->id}}" {{$item->address->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input">
                    <div class="label">City</div>
                    <input type="text" placeholder="City" name="address[city]" required
                           value="{{!empty($item->address)?$item->address->city->name:''}}">
                </div>
                <div class="input">
                    <div class="label">Address</div>
                    <input type="text" placeholder="Address" name="address[address]" required
                           value="{{!empty($item->address)?$item->address->address:''}}">
                </div>
                <div class="input">
                    <div class="label">Zip</div>
                    <input type="number" placeholder="zip" name="address[zip]" required
                           value="{{!empty($item->address)?$item->address->zip:''}}">
                </div>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Event Details</div>
            <div class="body">
                <div class="input">
                    <div class="label">Type</div>
                    <select name="type_id">
                        @foreach(\App\Models\CalendarEvent::TYPES as $id => $type)
                            <option {{!empty($item) && $item->type_id == $id?'selected':''}} value="{{$id}}">{{$type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Status</div>
                    <select name="status_id">
                        @foreach(\App\Models\CalendarEvent::STATUSES as $id => $status)
                            <option {{!empty($item) && $item->status_id == $id?'selected':''}} value="{{$id}}">{{$status}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Show Date
                        <small>(from - to)</small>
                    </div>
                    <div class="group_input two">
                        <input type="date" name="date_start" required
                               value="{{!empty($item)?Date('Y-m-d',strtotime($item->date_start)):''}}">
                        <input type="date" name="date_end" required
                               value="{{!empty($item)?Date('Y-m-d',strtotime($item->date_end)):''}}">
                    </div>
                </div>
                <div class="input">
                    <div class="label">Event Date</div>
                    <input type="datetime-local" name="date_at"
                           value="{{!empty($item)?str_replace(' ','T',Date('Y-m-d H:i',strtotime($item->date_at))):''}}">
                </div>
                <div class="input">
                    <div class="label">Url</div>
                    <input type="text" name="url" required
                           value="{{!empty($item)?$item->url:''}}">
                </div>
            </div>
        </div>
        @if(empty($item))
            <div class="field">
                <div class="title">Contact</div>
                <div class="body w50">
                    <div class="input">
                        <div class="label">Title</div>
                        <input type="text" placeholder="Title" name="contact[title]"
                               value="{{!empty($item)?$item->title:''}}">
                    </div>
                    <div class="input">
                        <div class="label">First name</div>
                        <input type="text" placeholder="First name" name="contact[first_name]"
                               value="{{!empty($item)?$item->first_name:''}}">
                    </div>
                    <div class="input">
                        <div class="label">Middle initial</div>
                        <input type="text" placeholder="Middle initial" name="contact[middle_initial]"
                               value="{{!empty($item)?$item->middle_initial:''}}">
                    </div>
                    <div class="input">
                        <div class="label">Last name</div>
                        <input type="text" placeholder="Last name" name="contact[last_name]"
                               value="{{!empty($item)?$item->last_name:''}}">
                    </div>
                </div>
                <div class="body w50">
                    <div class="input">
                        <div class="label">Country</div>
                        <select name="contact[address][country_id]" class="change_country"
                                data-state_id="contact_address_state">
                            <option value="">---</option>
                            @foreach(\App\Models\Country::all() as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input">
                        <div class="label">State</div>
                        <select name="contact[address][state_id]"
                                id="contact_address_state">
                            <option value="">---</option>
                        </select>
                    </div>
                    <div class="input">
                        <div class="label">City</div>
                        <input type="text" placeholder="City" name="contact[address][city]">
                    </div>
                    <div class="input">
                        <div class="label">Address</div>
                        <input type="text" placeholder="Address" name="contact[address][address]"
                               value="{{!empty($item->address)?$item->address->address:''}}">
                    </div>
                    <div class="input">
                        <div class="label">Zip</div>
                        <input type="number" placeholder="zip" name="contact[address][zip]"
                               value="{{!empty($item->address)?$item->address->zip:''}}">
                    </div>
                </div>
                <div class="body w50">
                    <div class="label">Phone(s)</div>
                    <div id="phone_items">
                        @if(empty($item) || (!empty($item) && count($item->phones) === 0))
                            @include('backend.organisation.partials.phone')
                        @else
                            @foreach($item->phones as $phone)
                                @include('backend.organisation.partials.phone',['item'=>$phone])
                            @endforeach
                        @endif
                    </div>
                    <button class="add" id="add_phone" type="button">Add</button>
                </div>
                <div class="body w50" style="display: block;">
                    <div class="label">Email(s)</div>
                    <div id="email_items">
                        @if(empty($item) || (!empty($item) && count($item->emails) === 0))
                            @include('backend.organisation.partials.email')
                        @else
                            @foreach($item->emails as $email)
                                @include('backend.organisation.partials.email',['item'=>$email])
                            @endforeach
                        @endif
                    </div>
                    <button class="add" id="add_email" type="button">Add</button>
                </div>
            </div>
        @else
            <div class="field">
                <div class="title">Contact</div>
                <div class="table" style="width: 100%; padding: 10px;">
                    <table>
                        <tr>
                            <td>Full Name</td>
                            <td>Phone</td>
                            <td>Email</td>
                            <td style="width: 100px">Options</td>
                        </tr>
                        @php
                            $contact = $item->contact;
                        @endphp
                        <tr>
                            <td>{{$contact->first_name}} {{$contact->last_name}}</td>
                            <td>
                                @if(count($contact->phones))
                                    @foreach($contact->phones as $phone)
                                        {{$phone->phone}}{{count($contact->phones) - 1===$loop->index?'':', '}}
                                    @endforeach
                                @else
                                    Not set
                                @endif
                            </td>
                            <td>
                                @if(count($contact->emails))
                                    @foreach($contact->emails as $email)
                                        {{$email->email}}{{count($contact->emails) - 1===$loop->index?'':', '}}
                                    @endforeach
                                @else
                                    Not set
                                @endif
                            </td>
                            <td>
                                <a href="{{route('backend.contact.form.id',$contact->id)}}" target="_blank">View</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        @endif
        <div class="field">
            <div class="title">Description</div>
            <div class="body">
                <textarea rows="5" placeholder="Description"
                          name="description">{{!empty($item)?$item->description:''}}</textarea>
            </div>
        </div>
        <div class="field">
            <div class="title">Banners</div>
            <div class="body w50">
                <div class="input">
                    <div class="label">Banner 1920x1080</div>
                    <input type="file" class="upload_image" name="image_1920x1080">
                </div>
                @if(!empty($item->image_1920x1080))
                    <div class="input">
                        <img src="{{!empty($item)?$item->image_1920x1080:''}}" />
                    </div>
                @endif
            </div>
            <div class="body w50">
                <div class="input">
                    <div class="label">Banner 1080x1920</div>
                    <input type="file" class="upload_image" name="image_1080x1920"
                           accept=".jpg, .jpeg, .png">
                </div>
                @if(!empty($item->image_1080x1920))
                    <div class="input">
                        <img src="{{!empty($item)?$item->image_1080x1920:''}}" />
                    </div>
                @endif
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <button type="button" class="cancel">Cancel</button>
        </div>
    </form>
@endsection
