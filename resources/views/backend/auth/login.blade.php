@extends('backend.partials.layout')

@section('content')
    <form class="m_form auth_form" method="post">
        <div class="field">
            <div class="title">Admin Area</div>
            <div class="body">
                <div class="label">Username or Email:</div>
                <input type="text" placeholder="Username or Email" name="email"
                       value="{{!empty($fields['email'])?$fields['email']:''}}" required>
                @if(!empty($errors['email']))
                    <small>{{$errors['email']}}</small>
                @endif
            </div>
            <div class="body">
                <div class="label">Password:</div>
                <input type="password" placeholder="Password" name="password"
                       value="{{!empty($fields['password'])?$fields['password']:''}}" required>
                @if(!empty($errors['password']))
                    <small>{{$errors['password']}}</small>
                @endif
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="cancel">Sign In</button>
        </div>
    </form>
@endsection
