@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Site / Project</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                {{--<a href="{{route('backend.project.index')}}?action=export">CSV export</a>--}}
                <button class="show_modal" data-modal_id="projectExportModal">CSV export</button>
                <a href="{{route('backend.project.form')}}" style="border-right: 0">Add New</a>
                <button class="batch_action" data-action="delete_all" style="border-right: 0">Delete all</button>
                <div class="group">
                    <button class="show_dropdown">Batch Action <span class="_arrow_down"></span>
                    </button>
                    <div class="dropdown">
                        <button class="batch_action" data-action="delete">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m_filters">
        <div class="f_title toggle_filters">Filter / Search</div>
        <div class="filters">
            <form class="m_form">
                <div class="field w50">
                    <div class="body">
                        <div class="input">
                            <div class="label">Title</div>
                            <input type="text" placeholder="title" name="title"
                                   value="{{!empty($filters['title'])?$filters['title']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Status</div>
                            <select name="status_id">
                                <option value="">---</option>
                                @foreach(\App\Models\Project::STATUSES as $id => $status)
                                    <option {{!empty($filters['status_id']) && $filters['status_id'] == $id?'selected':''}} value="{{$id}}">{{$status}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Start After Date</div>
                            <input type="date" name="date_start"
                                   value="{{!empty($filters['date_start'])?$filters['date_start']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">End Before Date</div>
                            <input type="date" name="date_end"
                                   value="{{!empty($filters['date_end'])?$filters['date_end']:''}}">
                        </div>
                    </div>
                </div>
                <div class="field w50">
                    <div class="body">
                        <div class="input">
                            <div class="label">Country</div>
                            <select name="country_id" class="change_country" data-state_id="address_state">
                                <option value="">---</option>
                                @foreach(\App\Models\Country::all() as $c)
                                    <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">State</div>
                            <select name="state_id" class="change_state" data-city_id="address_city"
                                    id="address_state">
                                <option value="">---</option>
                                @if(!empty($filters['country_id']) )
                                    @foreach(\App\Services\LocationService::getStatesByCountryId($filters['country_id']) as $s)
                                        <option value="{{$s->id}}" {{!empty($filters['state_id']) && $filters['state_id'] == $s->id?'selected':''}}>{{$s->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">City</div>
                            <select name="city_id" id="address_city">
                                <option value="">---</option>
                                @if(!empty($filters['state_id']))
                                    @foreach(\App\Services\LocationService::getCitiesByStateId($filters['state_id']) as $c)
                                        <option value="{{$c->id}}" {{!empty($filters['city_id']) && $filters['city_id'] == $s->id?'selected':''}}>{{$c->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Address</div>
                            <input type="text" placeholder="Address" name="address"
                                   value="{{!empty($filters['address'])?$filters['address']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Zip</div>
                            <input type="number" placeholder="zip" name="zip"
                                   value="{{!empty($filters['zip'])?$filters['zip']:''}}">
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <button type="submit" class="save">Filter Record</button>
                    <button type="button" class="save">Reset Filter</button>
                    <button type="reset" class="cancel search_reset">Clean Form</button>
                </div>
                <input type="hidden" value="{{$pagination->perPage()}}" name="per_page" />
            </form>
        </div>
    </div>
    <div class="table">
        <table>
            <tr class="header">
                <td colspan="3">
                    <div>
                        <input type="checkbox" class="select_all" />
                        <label>Select all</label>
                    </div>
                </td>
                <td>
                    <div>
                        <label>Show:</label>
                        <select class="change_per_page">
                            <option value="25" {{$pagination->perPage() == 25?'selected':''}}>25</option>
                            <option value="50" {{$pagination->perPage() == 50?'selected':''}}>50</option>
                            <option value="75" {{$pagination->perPage() == 75?'selected':''}}>75</option>
                            <option value="100" {{$pagination->perPage() == 100?'selected':''}}>100</option>
                        </select>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="width: 25px"></td>
                <td style="width: 60px">ID</td>
                <td>Title</td>
                <td style="width: 80px">Status</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td><input type="checkbox" class="select_one" value="{{$item->id}}" /></td>
                    <td>{{$item->id}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{\App\Models\Project::STATUSES[$item->status_id]}}</td>
                    <td>
                        <a href="{{route('backend.project.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.project.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
    @include('backend.project.partials.exportModal')
@endsection
