@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">API Import</div>
            <div class="body">
                <div class="input">
                    <div class="label">Import Year</div>
                    <select class="change_country" required id="year">
                        @for($i=2016;$i<=Date('Y');$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>
                <div class="input">
                    <div class="label">Previous `page` last ID:</div>
                    <input type="number" placeholder="id" value="0" id="last_id">
                </div>
                <div class="input checkbox_input">
                    <input type="checkbox" id="reset_counter" />
                    <div class="label">Reset counters</div>
                </div>
                <div class="input checkbox_input">
                    <input type="checkbox" id="continue_import" />
                    <div class="label">Continue until all available entries imported</div>
                </div>
                <div class="actions">
                    <button type="button" class="save" id="import_project">Import</button>
                </div>
            </div>
        </div>
        <div id="logs"></div>
    </form>
@endsection
