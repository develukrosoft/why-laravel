@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        @if(!empty($item))
            <div class="actions right">
                <a class="delete" href="{{route('backend.project.action.id',$item->id)}}?action=delete">Delete</a>
            </div>
        @endif
        <div class="field">
            <div class="title">Title</div>
            <div class="body">
                <input type="text" placeholder="Title" name="title" value="{{!empty($item)?$item->title:''}}" required>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Address</div>
            <div class="body">
                <div class="input">
                    <div class="label">Country</div>
                    <select name="address[country_id]" class="change_country" data-state_id="address_state" required>
                        <option value="">---</option>
                        @foreach(\App\Models\Country::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item->address) && $item->address->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">State</div>
                    <select name="address[state_id]" class="change_state" data-city_id="address_city" required
                            id="address_state">
                        <option value="">---</option>
                        @if(!empty($item->address))
                            @foreach(\App\Services\LocationService::getStatesByCountryId($item->address->country_id) as $s)
                                <option value="{{$s->id}}" {{$item->address->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input">
                    <div class="label">City</div>
                    <input type="text" placeholder="City" name="address[city]" required
                           value="{{!empty($item->address)?$item->address->city->name:''}}">
                </div>
                <div class="input">
                    <div class="label">Address</div>
                    <input type="text" placeholder="Address" name="address[address]"
                           value="{{!empty($item->address)?$item->address->address:''}}">
                </div>
                <div class="input">
                    <div class="label">Zip</div>
                    <input type="number" placeholder="zip" name="address[zip]"
                           value="{{!empty($item->address)?$item->address->zip:''}}">
                </div>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Postal Address
                <small>(optional)</small>
            </div>
            <div class="body">
                <div class="input">
                    <div class="label">Country</div>
                    <select name="postal_address[country_id]" class="change_country"
                            data-state_id="postal_address_state">
                        <option value="">---</option>
                        @foreach(\App\Models\Country::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item->postalAddress) && $item->postalAddress->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">State</div>
                    <select name="postal_address[state_id]" class="change_state" data-city_id="postal_address_city"
                            id="postal_address_state">
                        <option value="">---</option>
                        @if(!empty($item->postalAddress))
                            @foreach(\App\Services\LocationService::getStatesByCountryId($item->postalAddress->country_id) as $s)
                                <option value="{{$s->id}}" {{$item->postalAddress->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input">
                    <div class="label">City</div>
                    <input type="text" placeholder="City" name="postal_address[city]"
                           value="{{!empty($item->postalAddress)?$item->postalAddress->city->name:''}}">
                </div>
                <div class="input">
                    <div class="label">Address</div>
                    <input type="text" placeholder="Address" name="postal_address[address]"
                           value="{{!empty($item->postalAddress)?$item->postalAddress->address:''}}">
                </div>

                <div class="input">
                    <div class="label">Zip</div>
                    <input type="number" placeholder="zip" name="postal_address[zip]"
                           value="{{!empty($item->postalAddress)?$item->postalAddress->zip:''}}">
                </div>
            </div>
        </div>
        <div class="field">
            <div class="title">Site / Project Details</div>
            <div class="body w50">
                <div class="input">
                    <div class="label">Status</div>
                    <select name="status_id">
                        @foreach(\App\Models\Project::STATUSES as $id => $status)
                            <option {{!empty($item) && $item->status_id == $id?'selected':''}} value="{{$id}}">{{$status}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Option Sections</div>
                    <select name="tag_icon_id">
                        <option value="">---</option>
                        @foreach(\App\Models\TagIcon::whereIn('parent_type_id',[\App\Models\TagIcon::PARENT_TYPE_ALL,\App\Models\TagIcon::PARENT_TYPE_PROJECT])->get() as $ti)
                            <option value="{{$ti->id}}" {{!empty($item) && $item->tag_icon_id == $ti->id?'selected':''}}>{{$ti->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Date start</div>
                    <input type="date" name="date_start" required
                           value="{{!empty($item)?$item->date_start:''}}">
                </div>
                <div class="input">
                    <div class="label">Date end</div>
                    <input type="date" name="date_end" required
                           value="{{!empty($item)?$item->date_end:''}}">
                </div>
                <div class="input">
                    <div class="label">Operation days</div>
                    <div class="checkbox_group">
                        @php
                            $days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
                        @endphp
                        @foreach($days as $day)
                            <div>
                                <input type="checkbox" name="operation_days[]"
                                       value="{{$day}}" {{!empty($item->operation_days) && in_array($day,$item->operation_days)?'checked':''}}>
                                <div>{{$day}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="body w50">
                <div class="input">
                    <div class="label">Breakfast
                        <small>(time from - to)</small>
                    </div>
                    <div class="group_select">
                        <div class="block_select">
                            <select name="breakfast[from][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->breakfast_from) && explode(':',$item->breakfast_from)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="breakfast[from][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->breakfast_from) && explode(':',$item->breakfast_from)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="block_select">
                            <select name="breakfast[to][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->breakfast_to) && explode(':',$item->breakfast_to)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="breakfast[to][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->breakfast_to) && explode(':',$item->breakfast_to)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="input">
                    <div class="label">Lunch
                        <small>(time from - to)</small>
                    </div>
                    <div class="group_select">
                        <div class="block_select">
                            <select name="lunch[from][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->lunch_from) && explode(':',$item->lunch_from)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="lunch[from][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->lunch_from) && explode(':',$item->lunch_from)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="block_select">
                            <select name="lunch[to][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->lunch_to) && explode(':',$item->lunch_to)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="lunch[to][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->lunch_to) && explode(':',$item->lunch_to)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="input">
                    <div class="label">Snack
                        <small>(time from - to)</small>
                    </div>
                    <div class="group_select">
                        <div class="block_select">
                            <select name="snack[from][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->snack_from) && explode(':',$item->snack_from)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="snack[from][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->snack_from) && explode(':',$item->snack_from)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="block_select">
                            <select name="snack[to][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->snack_to) && explode(':',$item->snack_to)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="snack[to][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->snack_to) && explode(':',$item->snack_to)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="input">
                    <div class="label">Dinner
                        <small>(time from - to)</small>
                    </div>
                    <div class="group_select">
                        <div class="block_select">
                            <select name="dinner[from][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->dinner_from) && explode(':',$item->dinner_from)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="dinner[from][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->dinner_from) && explode(':',$item->dinner_from)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="block_select">
                            <select name="dinner[to][hour]">
                                <option value="">---</option>
                                @for($i=0;$i<24;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->dinner_to) && explode(':',$item->dinner_to)[0] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <div class="colon">:</div>
                            <select name="dinner[to][min]">
                                <option value="">---</option>
                                @for($i=0;$i<60;$i++)
                                    @php
                                        $i = mb_strlen($i) == 1 ? '0' . $i : $i;
                                    @endphp
                                    <option value="{{$i}}" {{!empty($item->dinner_to) && explode(':',$item->dinner_to)[1] == $i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!empty($item))
            <div class="field">
                <div class="title">Contacts</div>
                <div class="table" style="width: 100%; padding: 10px;">
                    <table>
                        <tr>
                            <td>Full Name</td>
                            <td>Phone</td>
                            <td>Email</td>
                            <td style="width: 100px">Options</td>
                        </tr>
                        @foreach($item->contacts as $contact)
                            <tr>
                                <td>{{$contact->first_name}} {{$contact->last_name}}</td>
                                <td>
                                    @if(count($contact->phones))
                                        @foreach($contact->phones as $phone)
                                            {{$phone->phone}}{{count($contact->phones) - 1===$loop->index?'':', '}}
                                        @endforeach
                                    @else
                                        Not set
                                    @endif
                                </td>
                                <td>
                                    @if(count($contact->emails))
                                        @foreach($contact->emails as $email)
                                            {{$email->email}}{{count($contact->emails) - 1===$loop->index?'':', '}}
                                        @endforeach
                                    @else
                                        Not set
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('backend.project.action.id',$item->id)}}?action=delete_contact&contact_id={{$contact->id}}">Remove</a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="{{route('backend.contact.index')}}?project_id={{$item->id}}"
                                >Add new</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        @endif
        <div class="field">
            <div class="title">Notes</div>
            <div class="body">
                <textarea rows="5" placeholder="Notes" name="notes">{{!empty($item)?$item->notes:''}}</textarea>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <button type="button" class="cancel">Cancel</button>
        </div>
    </form>
@endsection
