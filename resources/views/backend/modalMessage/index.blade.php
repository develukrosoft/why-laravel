@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Modal Messages</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.modalMessage.form')}}">Add New</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>ID</td>
                <td>Start Date</td>
                <td>End Date</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{Date('Y-m-d H:i',strtotime($item->start_at))}}</td>
                    <td>{{Date('Y-m-d H:i',strtotime($item->end_at))}}</td>
                    <td>
                        <a href="{{route('backend.modalMessage.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.modalMessage.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
