@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">Message</div>
            <div class="body">
                <textarea name="message" rows="10" class="_ckeditor"
                          id="message">{{!empty($item)?$item->message:''}}</textarea>
            </div>
        </div>
        <div class="field">
            <div class="title">Start Date</div>
            <div class="body">
                <input name="start_at" type="datetime-local"
                       value="{{!empty($item)?str_replace(' ','T',Date('Y-m-d H:i',strtotime($item->start_at))):''}}" />
            </div>
        </div>
        <div class="field">
            <div class="title">End Date</div>
            <div class="body">
                <input name="end_at" type="datetime-local"
                       value="{{!empty($item)?str_replace(' ','T',Date('Y-m-d H:i',strtotime($item->end_at))):''}}" />
            </div>
        </div>
        <div class="field">
            <div class="title">Class name</div>
            <div class="body">
                <input name="class_name" type="text" value="{{!empty($item)?$item->class_name:''}}" />
            </div>
        </div>
        <div class="field">
            <div class="title">Show once in</div>
            <div class="body">
                <div class="input">
                    <select name="show_type_id">
                        @foreach(\App\Models\ModalMessage::TYPES as $id=>$type)
                            <option value="{{$id}}" {{!empty($item) && $item->show_type_id == $id?'selected':''}}>{{$type}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="title">Language</div>
            <div class="body">
                <div class="input">
                    <select name="locale">
                        <option value="all" {{!empty($item) && $item->locale == 'all'?'selected':''}}>All</option>
                        <option value="en" {{!empty($item) && $item->locale == 'en'?'selected':''}}>EN</option>
                        <option value="es" {{!empty($item) && $item->locale == 'es'?'selected':''}}>ES</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="title">Order</div>
            <div class="body">
                <input name="position" type="number" min="1" value="{{!empty($item)?$item->position:1}}" />
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.city.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
