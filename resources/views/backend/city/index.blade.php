@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Cities</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.city.form')}}">Add New</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>Name</td>
                <td>State</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->state->name}}</td>
                    <td>
                        <a href="{{route('backend.city.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.city.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
