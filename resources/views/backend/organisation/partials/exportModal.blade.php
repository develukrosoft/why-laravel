<div class="m_modal" id="orgExportModal">
    <div class="body">
        <div class="close close_modal" data-modal_id="orgExportModal">X</div>
        <form class="m_form" action="{{route('backend.organisation.index')}}">
            @foreach (!empty($filters)?$filters:[] as $id => $value)
                @if(is_array($value))
                    @foreach ($value as $v)
                        <input type="hidden" name="{{$id}}[]" value="{{$v}}" />
                    @endforeach
                @else
                    <input type="hidden" name="{{$id}}" value="{{$value}}" />
                @endif
            @endforeach
            <input type="hidden" name="action" value="export" />
            <div class="field">
                <div class="body options">
                    @foreach($fields as $id=>$name)
                        <div class="input">
                            <input type="checkbox" value="{{$name}}" name="fields[{{$id}}]" />
                            <div class="label">{{$name}}</div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="actions">
                <button type="submit" class="save">Export</button>
                <button type="button" class="cancel close_modal" data-modal_id="orgExportModal">Cancel</button>
            </div>
        </form>
    </div>
</div>
