@extends('backend.partials.layout')

@section('content')
    @php
        $query_search = '';
        foreach (!empty($filters)?$filters:[] as $id => $value) {
            if (is_array($value)) {
                foreach ($value as $v){
                    $query_search .= '&'.$id . '[]=' . $v;
                }
            } else {
                $query_search .= '&'.$id . '=' . $value;
            }
        }
        if(empty($filters['s_f'])){
            $filters['s_f'] = 'id';
            $filters['s_o'] = 'desc';
        }
    @endphp
    <div class="m_header">
        <div class="title">
            <div class="name">Organizations</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                {{--<a href="{{route('backend.organisation.index')}}?action=export{{$query_search}}">CSV export</a>--}}
                <button class="show_modal" data-modal_id="orgExportModal">CSV export</button>
                <a href="{{route('backend.organisation.form')}}">Add New</a>
            </div>
            <form class="group">
                <button class="show_dropdown" type="button">Blank Fields <span class="_arrow_down"></span>
                </button>
                <button>Show Blank</button>
                <div class="dropdown">
                    <input type="hidden" name="blank" value="1" />
                    <div>
                        <input type="checkbox" name="address"
                               id="blank_address" {{!empty($blanks['address'])?'checked':''}}>
                        <label for="blank_address">Address</label>
                    </div>
                    <div>
                        <input type="checkbox" name="zip" id="blank_zip" {{!empty($blanks['zip'])?'checked':''}}>
                        <label for="blank_zip">Zip</label>
                    </div>
                    <div>
                        <input type="checkbox" name="phone" id="blank_phone" {{!empty($blanks['phone'])?'checked':''}}>
                        <label for="blank_phone">Phone</label>
                    </div>
                    <div>
                        <input type="checkbox" name="email" id="blank_phone" {{!empty($blanks['email'])?'checked':''}}>
                        <label for="blank_phone">Email</label>
                    </div>
                    <div>
                        <input type="checkbox" name="pending" id="blank_pending" {{!empty($blanks['pending'])?'checked':''}}>
                        <label for="blank_pending">Pending organizations</label>
                    </div>
                    <div>
                        <input type="checkbox" name="website"
                               id="blank_website" {{!empty($blanks['website'])?'checked':''}}>
                        <label for="blank_website">Website</label>
                    </div>
                </div>
            </form>
            <div class="group">
                <button class="mass_mail">Mass Mail</button>
                <button class="show_dropdown">Batch Action <span class="_arrow_down"></span>
                </button>
                <div class="dropdown">
                    <button class="batch_action" data-action="approve">Approve</button>
                    <button class="batch_action" data-action="pending">Set as Pending</button>
                    <button class="batch_action" data-action="delete">Delete</button>
                </div>
            </div>
            <div class="group">
                <a href="{{route('backend.organisation.index')}}?action=override_pending">Override Pending</a>
            </div>
        </div>
    </div>
    <form class="m_mails">
        <div class="body">
            <div class="input">
                <div class="title">Mass Mail Template:</div>
                <textarea rows="10" id="mass_mail" class="_ckeditor"></textarea>
            </div>
            <div class="instruction">
                <div class="title">Template shortcodes:</div>
                <ul>
                    <li>{name} - Organization Name</li>
                    <li>{current_date} - Current Date</li>
                    <li>{update_request_link} - Update Request Link</li>
                </ul>
                <button type="button" class="batch_action" data-action="mass_mail">Send Message to Selected
                    Organizations
                </button>
            </div>
        </div>
    </form>
    <div class="m_filters">
        <div class="f_title toggle_filters">Filter / Search</div>
        <div class="filters">
            <form class="m_form">
                <div class="field w50">
                    <div class="body">
                        <div class="input">
                            <div class="label">Title</div>
                            <input type="text" placeholder="title" name="title"
                                   value="{{!empty($filters['title'])?$filters['title']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Status</div>
                            <select name="status_id">
                                <option value="unset">---</option>
                                @foreach(\App\Models\Organisation::STATUSES as $id=>$name)
                                    <option value="{{$id}}" {{!empty($filters['status_id']) && $filters['status_id'] == $id?'selected':''}}>{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Year founded</div>
                            <select name="year_founded">
                                <option value="">---</option>
                                @for($i=Date('Y');$i>=1900;$i--)
                                    <option {{!empty($filters['year_founded']) && $filters['year_founded'] == $i?'selected':''}} value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Is Harry Chapin Self Reliance Award Winner</div>
                            <select name="is_hcsraw">
                                <option value="unset">---</option>
                                <option value="0" {{isset($filters['is_hcsraw']) && $filters['is_hcsraw'] == 0?'selected':''}}>
                                    No
                                </option>
                                <option value="1" {{!empty($filters['is_hcsraw']) && $filters['is_hcsraw'] == 1?'selected':''}}>
                                    Yes
                                </option>
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Receives Newsletter?</div>
                            <select name="is_receives_newsletter">
                                <option value="unset">---</option>
                                <option value="0" {{isset($filters['is_receives_newsletter']) && $filters['is_receives_newsletter'] == 0?'selected':''}}>
                                    No
                                </option>
                                <option value="1" {{!empty($filters['is_receives_newsletter']) && $filters['is_receives_newsletter'] == 1?'selected':''}}>
                                    Yes
                                </option>
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Annual budget
                                <small>(from - to)</small>
                            </div>
                            <div class="group_input two">
                                <input type="number" placeholder="$ From" name="annual_budget_from"
                                       value="{{!empty($filters['annual_budget_from'])?$filters['annual_budget_from']:''}}">
                                <input type="number" placeholder="$ To" name="annual_budget_to"
                                       value="{{!empty($filters['annual_budget_to'])?$filters['annual_budget_to']:''}}">
                            </div>
                        </div>
                        <div class="input">
                            <div class="label">Number of Volunteers
                                <small>(from - to)</small>
                            </div>
                            <div class="group_input two">
                                <input type="number" placeholder="From" name="number_volunteers_from"
                                       value="{{!empty($filters['number_volunteers_from'])?$filters['number_volunteers_from']:''}}">
                                <input type="number" placeholder="To" name="number_volunteers_to"
                                       value="{{!empty($filters['number_volunteers_to'])?$filters['number_volunteers_to']:''}}">
                            </div>
                        </div>
                        <div class="input">
                            <div class="label">Number of Part-Time Staff
                                <small>(from - to)</small>
                            </div>
                            <div class="group_input two">
                                <input type="number" placeholder="From" name="number_part_time_staff_from"
                                       value="{{!empty($filters['number_part_time_staff_from'])?$filters['number_part_time_staff_from']:''}}">
                                <input type="number" placeholder="To" name="number_part_time_staff_to"
                                       value="{{!empty($filters['number_part_time_staff_to'])?$filters['number_part_time_staff_to']:''}}">
                            </div>
                        </div>
                        <div class="input">
                            <div class="label">Number of Full-Time Staff
                                <small>(from - to)</small>
                            </div>
                            <div class="group_input two">
                                <input type="number" placeholder="From" name="number_full_time_staff_from"
                                       value="{{!empty($filters['number_full_time_staff_from'])?$filters['number_full_time_staff_from']:''}}">
                                <input type="number" placeholder="To" name="number_full_time_staff_to"
                                       value="{{!empty($filters['number_full_time_staff_to'])?$filters['number_full_time_staff_to']:''}}">
                            </div>
                        </div>
                        <div class="input">
                            <div class="label">Mail exists</div>
                            <select name="mail_exists">
                                <option value="unset">All</option>
                                <option value="0" {{isset($filters['mail_exists']) && $filters['mail_exists'] == 0?'selected':''}}>
                                    No
                                </option>
                                <option value="1" {{!empty($filters['mail_exists']) && $filters['mail_exists'] == 1?'selected':''}}>
                                    Yes
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field w50">
                    <div class="body">
                        <div class="input">
                            <div class="label">Hide front</div>
                            <select name="is_front">
                                <option value="unset">---</option>
                                <option value="0" {{isset($filters['is_front']) && $filters['is_front'] == 0?'selected':''}}>
                                    No
                                </option>
                                <option value="1" {{!empty($filters['is_front']) && $filters['is_front'] == 1?'selected':''}}>
                                    Yes
                                </option>
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Program Type</div>
                            <select name="program_type_id">
                                <option value="">---</option>
                                @foreach(\App\Models\ProgramType::all() as $pt)
                                    <option value="{{$pt->id}}" {{!empty($filters['program_type_id']) && $filters['program_type_id'] == $pt->id?'selected':''}}>{{$pt->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Phone</div>
                            <input type="text" placeholder="Phone" name="phone"
                                   value="{{!empty($filters['phone'])?$filters['phone']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Email</div>
                            <input type="text" placeholder="Email" name="email"
                                   value="{{!empty($filters['email'])?$filters['email']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Country</div>
                            <select name="country_id" class="change_country" data-state_id="address_state">
                                <option value="">---</option>
                                @foreach(\App\Models\Country::all() as $c)
                                    <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">State</div>
                            <select name="state_id" class="change_state" data-city_id="address_city"
                                    id="address_state">
                                <option value="">---</option>
                                @if(!empty($filters['country_id']) )
                                    @foreach(\App\Services\LocationService::getStatesByCountryId($filters['country_id']) as $s)
                                        <option value="{{$s->id}}" {{!empty($filters['state_id']) && $filters['state_id'] == $s->id?'selected':''}}>{{$s->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">City</div>
                            <select name="city_id" id="address_city">
                                <option value="">---</option>
                                @if(!empty($filters['state_id']))
                                    @foreach(\App\Services\LocationService::getCitiesByStateId($filters['state_id']) as $c)
                                        <option value="{{$c->id}}" {{!empty($filters['city_id']) && $filters['city_id'] == $s->id?'selected':''}}>{{$c->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="input">
                            <div class="label">Address</div>
                            <input type="text" placeholder="Address" name="address"
                                   value="{{!empty($filters['address'])?$filters['address']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Zip</div>
                            <input type="number" placeholder="zip" name="zip"
                                   value="{{!empty($filters['zip'])?$filters['zip']:''}}">
                        </div>
                    </div>
                </div>
                @foreach(\App\Models\OptionCategory::all() as $c)
                    <div class="field">
                        <div class="title">{{$c->name}}</div>
                        <div class="body options">
                            @foreach($c->values as $v)
                                <div class="input">
                                    <input type="checkbox" value="{{$v->id}}"
                                           name="options[]" {{!empty($filters['options']) && in_array($v->id,$filters['options']) !== false?'checked':''  }}/>
                                    <div class="label">{{$v->value}}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="actions">
                    <button type="submit" class="save">Filter Record</button>
                    <button type="button" class="save">Reset Filter</button>
                    <button type="reset" class="cancel search_reset">Clean Form</button>
                </div>
                <input type="hidden" value="{{$pagination->perPage()}}" name="per_page" />
            </form>
        </div>
    </div>
    <div class="table">
        <table>
            <tr class="header">
                <td colspan="3">
                    <div>
                        <input type="checkbox" class="select_all" />
                        <label>Select all</label>
                    </div>
                </td>
                <td>
                    <div style="padding-right: 5px">
                        <label>Show:</label>
                        <select class="change_per_page">
                            <option value="25" {{$pagination->perPage() == 25?'selected':''}}>25</option>
                            <option value="50" {{$pagination->perPage() == 50?'selected':''}}>50</option>
                            <option value="75" {{$pagination->perPage() == 75?'selected':''}}>75</option>
                            <option value="100" {{$pagination->perPage() == 100?'selected':''}}>100</option>
                        </select>
                    </div>
                </td>
                <td>
                    <div>
                        <label>&nbsp;</label>
                        <select class="change_status_id">
                            <option value="unset">Status</option>
                            @foreach(\App\Models\Organisation::STATUSES as $id=>$name)
                                <option value="{{$id}}" {{!empty($filters['status_id']) && $filters['status_id'] == $id?'selected':''}}>{{$name}}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="width: 25px"></td>
                <td style="width: 60px; {{$filters['s_f'] == 'id'?'font-weight:bold':''}}" class="organisation_sort">
                    <a href="{{route(Route::current()->getName())}}?page=1&s_f=id&s_o={{$filters['s_f'] == 'id'?($filters['s_o'] == 'asc'?'desc':'asc'):'asc'}}">
                        ID
                        @if($filters['s_f'] == 'id')
                            <span class="_arrow_down {{$filters['s_o'] == 'asc'?'revert':''}}"></span>
                        @endif
                    </a>
                </td>
                <td style="{{$filters['s_f'] == 'title'?'font-weight:bold':''}}">
                    <a href="{{route(Route::current()->getName())}}?page=1&s_f=title&s_o={{$filters['s_f'] == 'title'?($filters['s_o'] == 'asc'?'desc':'asc'):'asc'}}">
                        Title
                        @if($filters['s_f'] == 'title')
                            <span class="_arrow_down {{$filters['s_o'] == 'asc'?'revert':''}}"></span>
                        @endif
                    </a>
                </td>
                <td style="width: 120px">Program type</td>
                <td style="width: 80px">Status</td>
                <td style="width: 120px">Phone</td>
                <td style="width: 120px">Email</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td><input type="checkbox" class="select_one" value="{{$item->id}}" /></td>
                    <td>{{$item->id}}</td>
                    <td>{{$item->title}}</td>
                    <td>
                        @foreach($item->types as $type)
                            {{$type->name}}{{count($item->types) - 1===$loop->index?'':', '}}
                        @endforeach
                    </td>
                    <td>{{\App\Models\Organisation::STATUSES[$item->status_id]}}</td>
                    <td>
                        @if(count($item->phones))
                            @foreach($item->phones as $phone)
                                {{$phone->phone}}{{count($item->phones) - 1===$loop->index?'':', '}}
                            @endforeach
                        @else
                            Not set
                        @endif
                    </td>
                    <td>
                        @if(count($item->emails))
                            @foreach($item->emails as $email)
                                {{$email->email}}{{count($item->emails) - 1===$loop->index?'':', '}}
                            @endforeach
                        @else
                            Not set
                        @endif
                    </td>
                    <td>
                        @if($item->status_id == \App\Models\Organisation::TYPE_PENDING)
                            <a href="{{route('backend.organisation.action.id',$item->id)}}?action=approve">Approve</a>
                            <br />
                        @endif
                        <a href="{{route('backend.organisation.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.organisation.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
    @include('backend.organisation.partials.exportModal')
@endsection
