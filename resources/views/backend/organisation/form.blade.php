@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        @if(!empty($item))
            <div class="actions right">
                <a class="delete" href="{{route('backend.organisation.action.id',$item->id)}}?action=delete">Delete</a>
            </div>
        @endif
        <div class="field">
            <div class="title">Title</div>
            <div class="body">
                <input type="text" placeholder="Title" name="title" value="{{!empty($item)?$item->title:''}}" required>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Address</div>
            <div class="body">
                <div class="input">
                    <div class="label">Country</div>
                    <select name="address[country_id]" class="change_country" data-state_id="address_state" required>
                        <option value="">---</option>
                        @foreach(\App\Models\Country::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item->address) && $item->address->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">State</div>
                    <select name="address[state_id]" required
                            id="address_state">
                        <option value="">---</option>
                        @if(!empty($item->address))
                            @foreach(\App\Services\LocationService::getStatesByCountryId($item->address->country_id) as $s)
                                <option value="{{$s->id}}" {{$item->address->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input">
                    <div class="label">City</div>
                    <input type="text" placeholder="City" name="address[city]" required
                           value="{{!empty($item->address)?$item->address->city->name:''}}">
                </div>
                <div class="input">
                    <div class="label">Address</div>
                    <input type="text" placeholder="Address" name="address[address]"
                           value="{{!empty($item->address)?$item->address->address:''}}">
                </div>
                <div class="input">
                    <div class="label">Zip</div>
                    <input type="number" placeholder="zip" name="address[zip]"
                           value="{{!empty($item->address)?$item->address->zip:''}}">
                </div>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Postal Address
                <small>(optional)</small>
            </div>
            <div class="body">
                <div class="input">
                    <div class="label">Country</div>
                    <select name="postal_address[country_id]" class="change_country"
                            data-state_id="postal_address_state">
                        <option value="">---</option>
                        @foreach(\App\Models\Country::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item->postalAddress) && $item->postalAddress->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">State</div>
                    <select name="postal_address[state_id]"
                            id="postal_address_state">
                        <option value="">---</option>
                        @if(!empty($item->postalAddress))
                            @foreach(\App\Services\LocationService::getStatesByCountryId($item->postalAddress->country_id) as $s)
                                <option value="{{$s->id}}" {{$item->postalAddress->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input">
                    <div class="label">City</div>
                    <input type="text" placeholder="City" name="postal_address[city]"
                           value="{{!empty($item->postalAddress)?$item->postalAddress->city->name:''}}">
                </div>
                <div class="input">
                    <div class="label">Address</div>
                    <input type="text" placeholder="Address" name="postal_address[address]"
                           value="{{!empty($item->postalAddress)?$item->postalAddress->address:''}}">
                </div>

                <div class="input">
                    <div class="label">Zip</div>
                    <input type="number" placeholder="zip" name="postal_address[zip]"
                           value="{{!empty($item->postalAddress)?$item->postalAddress->zip:''}}">
                </div>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Phone(s)</div>
            <div class="body">
                <div id="phone_items">
                    @if(empty($item) || (!empty($item) && count($item->phones) === 0))
                        @include('backend.organisation.partials.phone')
                    @else
                        @foreach($item->phones as $phone)
                            @include('backend.organisation.partials.phone',['item'=>$phone])
                        @endforeach
                    @endif
                </div>
                <button class="add" id="add_phone" type="button">Add</button>
            </div>
        </div>
        <div class="field w50" style="display: block;">
            <div class="title">Email(s)</div>
            <div class="body">
                <div id="email_items">
                    @if(empty($item) || (!empty($item) && count($item->emails) === 0))
                        @include('backend.organisation.partials.email')
                    @else
                        @foreach($item->emails as $email)
                            @include('backend.organisation.partials.email',['item'=>$email])
                        @endforeach
                    @endif
                </div>
                <button class="add" id="add_email" type="button">Add</button>
            </div>
        </div>
        <div class="field">
            <div class="title">Details</div>
            <div class="body w50">
                <div class="input">
                    <div class="label">Status</div>
                    <select name="status_id">
                        @foreach(\App\Models\Organisation::STATUSES as $id=>$name)
                            <option value="{{$id}}" {{!empty($item) && $item->status_id == $id?'selected':''}}>{{$name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Option Sections</div>
                    <select name="tag_icon_id">
                        <option value="">---</option>
                        @foreach(\App\Models\TagIcon::whereIn('parent_type_id',[\App\Models\TagIcon::PARENT_TYPE_ALL,\App\Models\TagIcon::PARENT_TYPE_ORG])->get() as $ti)
                            <option value="{{$ti->id}}" {{!empty($item) && $item->tag_icon_id == $ti->id?'selected':''}}>{{$ti->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Website</div>
                    <input type="text" placeholder="Website" name="website" value="{{!empty($item)?$item->website:''}}">
                </div>
                <div class="input">
                    <div class="label">Annual budget</div>
                    <input type="text" placeholder="$" name="annual_budget"
                           value="{{!empty($item)?$item->annual_budget:''}}">
                </div>
                <div class="input">
                    <div class="label">Year founded</div>
                    <select name="year_founded">
                        @for($i=Date('Y');$i>=1900;$i--)
                            <option {{!empty($item) && $item->year_founded == $i?'selected':''}} value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>

                <div class="input">
                    <div class="label">Number of Volunteers</div>
                    <input type="number" placeholder="0" name="number_volunteers"
                           value="{{!empty($item)?$item->number_volunteers:''}}">
                </div>
                <div class="input">
                    <div class="label">Number of Part-Time Staff</div>
                    <input type="number" placeholder="0" name="number_part_time_staff"
                           value="{{!empty($item)?$item->number_part_time_staff:''}}">
                </div>
                <div class="input">
                    <div class="label">Number of Full-Time Staff</div>
                    <input type="number" placeholder="0" name="number_full_time_staff"
                           value="{{!empty($item)?$item->number_full_time_staff:''}}">
                </div>
            </div>
            <div class="body w50">
                <div class="input">
                    <div class="label">Program type</div>
                    <select multiple name="program_types[]" required>
                        @foreach(\App\Models\ProgramType::all() as $type)
                            <option value="{{$type->id}}" {{!empty($item->types) && in_array($type->id,$item->types()->pluck('type_id')->toArray()) !== false?'selected':''}}>{{$type->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">Program Type Description
                        <small>(optional)</small>
                    </div>
                    <input type="text" placeholder="Description" name="program_type_description"
                           value="{{!empty($item)?$item->program_type_description:''}}">
                </div>
                <div class="input">
                    <div class="label">Organization Hours</div>
                    <input type="text" placeholder="Hours" name="hours[text]"
                           value="{{!empty($item->hours['text'])?$item->hours['text']:''}}">
                </div>
                <div class="input">
                    <div class="label">Receives Newsletter?</div>
                    <select name="is_receives_newsletter">
                        <option value="0">No</option>
                        <option value="1" {{!empty($item) && $item->is_receives_newsletter?'selected':''}}>Yes</option>
                    </select>
                </div>
                <div class="input">
                    <div class="label">Is Harry Chapin Self Reliance Award Winner</div>
                    <select name="is_hcsraw">
                        <option value="0">No</option>
                        <option value="1" {{!empty($item) && $item->is_hcsraw?'selected':''}}>Yes</option>
                    </select>
                </div>
                <div class="input">
                    <div class="label">Hide front</div>
                    <select name="is_front">
                        <option value="1">No</option>
                        <option value="0" {{!empty($item) && !$item->is_front?'selected':''}}>Yes</option>
                    </select>
                </div>
            </div>
        </div>
        @if(!empty($item))
            <div class="field">
                <div class="title">Contacts</div>
                <div class="table" style="width: 100%; padding: 10px;">
                    <table>
                        <tr>
                            <td>Full Name</td>
                            <td>Phone</td>
                            <td>Email</td>
                            <td style="width: 100px">Options</td>
                        </tr>
                        @foreach($item->contacts as $contact)
                            <tr>
                                <td>{{$contact->first_name}} {{$contact->last_name}}</td>
                                <td>
                                    @if(count($contact->phones))
                                        @foreach($contact->phones as $phone)
                                            {{$phone->phone}}{{count($contact->phones) - 1===$loop->index?'':', '}}
                                        @endforeach
                                    @else
                                        Not set
                                    @endif
                                </td>
                                <td>
                                    @if(count($contact->emails))
                                        @foreach($contact->emails as $email)
                                            {{$email->email}}{{count($contact->emails) - 1===$loop->index?'':', '}}
                                        @endforeach
                                    @else
                                        Not set
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('backend.organisation.action.id',$item->id)}}?action=delete_contact&contact_id={{$contact->id}}">Remove</a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="{{route('backend.contact.index')}}?organisation_id={{$item->id}}"
                                >Add new</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        @endif
        <div class="field">
            <div class="title">Mission Statement</div>
            <div class="body">
                <textarea rows="5" placeholder="Mission Statement"
                          name="mission">{{!empty($item)?$item->mission:''}}</textarea>
            </div>
        </div>
        <div class="field">
            <div class="title">Notes</div>
            <div class="body">
                <textarea rows="5" placeholder="Notes" name="notes">{{!empty($item)?$item->notes:''}}</textarea>
            </div>
        </div>
        <div class="field">
            <div class="title">SEND CONFIRMATION MESSAGE TO:</div>
            <div class="body">
                <select name="confirmation_email_id">
                    @foreach(\App\Models\Organisation::CONFIRMATION_EMAILS as $id=>$name)
                        <option value="{{$id}}" {{!empty($item) && $item->confirmation_email_id?'selected':''}}>{{$name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @foreach(\App\Models\OptionCategory::all() as $c)
            <div class="field">
                <div class="title">{{$c->name}}</div>
                <div class="body options">
                    @foreach($c->values as $v)
                        <div class="input">
                            <input type="checkbox" value="{{$v->id}}"
                                   name="options[]" {{!empty($item->options) && in_array($v->id,$item->options()->pluck('option_id')->toArray()) !== false?'checked':''}}/>
                            <div class="label">{{$v->value}}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <button type="button" class="cancel">Cancel</button>
        </div>
    </form>
@endsection
