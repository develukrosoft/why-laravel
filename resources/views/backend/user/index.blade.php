@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Users</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.user.form')}}">Add New</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>ID</td>
                <td>Full name</td>
                <td>Username</td>
                <td>Email</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->full_name}}</td>
                    <td>{{$item->username}}</td>
                    <td>{{$item->email}}</td>
                    <td>
                        <a href="{{route('backend.user.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.user.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
