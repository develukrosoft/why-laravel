@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">Full name</div>
            <div class="body">
                <input type="text" placeholder="Full name" name="full_name"
                       value="{{!empty($item)?$item->full_name:''}}" required>
            </div>
        </div>
        <div class="field">
            <div class="title">Username</div>
            <div class="body">
                <input type="text" placeholder="Username" name="username" value="{{!empty($item)?$item->username:''}}"
                       required>
            </div>
        </div>
        <div class="field">
            <div class="title">Email</div>
            <div class="body">
                <input type="email" placeholder="Email" name="email" value="{{!empty($item)?$item->email:''}}" required>
            </div>
        </div>
        <div class="field">
            <div class="title">Password</div>
            <div class="body">
                <input type="password" placeholder="Password" name="password">
            </div>
        </div>
        <div class="field">
            <div class="title">Role</div>
            <div class="body">
                <div class="input">
                    <select name="role">
                        @foreach(\App\Models\User::ROLES as $id=>$role)
                            <option value="{{$id}}" {{!empty($item) && $id == $item->role?'selected':''}}>{{$role}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.user.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
