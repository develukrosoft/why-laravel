@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field w50" style="display: block; margin-top: 15px;">
            <div class="title">Contact Detail
                <small>(optional)</small>
            </div>
            <div class="body">
                <div class="input">
                    <div class="label">Title</div>
                    <input type="text" placeholder="Title" name="title"
                           value="{{!empty($item)?$item->title:''}}">
                </div>
                <div class="input">
                    <div class="label">First name</div>
                    <input type="text" placeholder="First name" name="first_name"
                           value="{{!empty($item)?$item->first_name:''}}">
                </div>
                <div class="input">
                    <div class="label">Middle initial</div>
                    <input type="text" placeholder="Middle initial" name="middle_initial"
                           value="{{!empty($item)?$item->middle_initial:''}}">
                </div>
                <div class="input">
                    <div class="label">Last name</div>
                    <input type="text" placeholder="Last name" name="last_name"
                           value="{{!empty($item)?$item->last_name:''}}">
                </div>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Address</div>
            <div class="body">
                <div class="input">
                    <div class="label">Country</div>
                    <select name="address[country_id]" class="change_country" data-state_id="address_state" >
                        <option value="">---</option>
                        @foreach(\App\Models\Country::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item->address) && $item->address->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input">
                    <div class="label">State</div>
                    <select name="address[state_id]"
                            id="address_state">
                        <option value="">---</option>
                        @if(!empty($item->address))
                            @foreach(\App\Services\LocationService::getStatesByCountryId($item->address->country_id) as $s)
                                <option value="{{$s->id}}" {{$item->address->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="input">
                    <div class="label">City</div>
                    <input type="text" placeholder="City" name="address[city]"
                           value="{{!empty($item->address)?$item->address->city->name:''}}">
                </div>
                <div class="input">
                    <div class="label">Address</div>
                    <input type="text" placeholder="Address" name="address[address]"
                           value="{{!empty($item->address)?$item->address->address:''}}">
                </div>
                <div class="input">
                    <div class="label">Zip</div>
                    <input type="number" placeholder="zip" name="address[zip]"
                           value="{{!empty($item->address)?$item->address->zip:''}}">
                </div>
            </div>
        </div>
        <div class="field w50">
            <div class="title">Phone(s)</div>
            <div class="body">
                <div id="phone_items">
                    @if(empty($item) || (!empty($item) && count($item->phones) === 0))
                        @include('backend.organisation.partials.phone')
                    @else
                        @foreach($item->phones as $phone)
                            @include('backend.organisation.partials.phone',['item'=>$phone])
                        @endforeach
                    @endif
                </div>
                <button class="add" id="add_phone" type="button">Add</button>
            </div>
        </div>
        <div class="field w50" style="display: block;">
            <div class="title">Email(s)</div>
            <div class="body">
                <div id="email_items">
                    @if(empty($item) || (!empty($item) && count($item->emails) === 0))
                        @include('backend.organisation.partials.email')
                    @else
                        @foreach($item->emails as $email)
                            @include('backend.organisation.partials.email',['item'=>$email])
                        @endforeach
                    @endif
                </div>
                <button class="add" id="add_email" type="button">Add</button>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.contact.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
