@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Contacts</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                @if(empty($filters['attach_organisation_id']) && empty($filters['attach_project_id']))
                    <a href="{{route('backend.contact.form')}}">Add New</a>
                @endif
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Phone</td>
                <td>Email</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->first_name}}</td>
                    <td>{{$item->last_name}}</td>
                    <td>
                        @if(count($item->phones))
                            @foreach($item->phones as $phone)
                                {{$phone->phone}}{{count($item->phones) - 1===$loop->index?'':', '}}
                            @endforeach
                        @else
                            Not set
                        @endif
                    </td>
                    <td>
                        @if(count($item->emails))
                            @foreach($item->emails as $email)
                                {{$email->email}}{{count($item->emails) - 1===$loop->index?'':', '}}
                            @endforeach
                        @else
                            Not set
                        @endif
                    </td>
                    <td>
                        @if(!empty($filters['attach_organisation_id']))
                            <a href="{{route('backend.organisation.action.id',$filters['attach_organisation_id'])}}?action=attach_contact&contact_id={{$item->id}}"
                            >Attach This</a>
                        @elseif(!empty($filters['attach_project_id']))
                            <a href="{{route('backend.project.action.id',$filters['attach_project_id'])}}?action=attach_contact&contact_id={{$item->id}}"
                            >Attach This</a>
                        @else
                            <a href="{{route('backend.contact.form.id',$item->id)}}">Edit</a>
                            <br />
                            <a href="{{route('backend.contact.action.id',$item->id)}}?action=delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
