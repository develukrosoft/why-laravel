@extends('backend.partials.layout')

@section('content')
    @include('backend.duplicate.partials.tabs')
    <div class="m_header">
        <div class="title">
            <div class="name">Duplicates</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.duplicate.organisation.remove')}}">Remove Duplicates</a>
            </div>
        </div>
    </div>

    <div class="table">
        <table>
            <tr>
                <td>Name</td>
                <td>Repeats</td>
                <td style="width: 100px">Actions</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->title}}</td>
                    @php
                        $key = 'COUNT(title)';
                    @endphp
                    <td>{{$item->$key}}</td>
                    <td>
                        <a href="{{route('backend.duplicate.organisation.join')}}?title={{$item->title}}">Join</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
