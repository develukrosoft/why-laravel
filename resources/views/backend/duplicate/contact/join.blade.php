@extends('backend.partials.layout')

@section('content')
    @include('backend.duplicate.partials.tabs')
    <div class="m_header">
        <div class="title">
            <div class="name">Duplicates</div>
            <div>has {{$items->count()}} records</div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td style="width: 100px">Actions</td>
            </tr>
            @foreach($items as $item)
                <tr>
                    <td><a href="{{route('backend.contact.form.id',$item->id)}}">{{$item->id}}</a></td>
                    <td><a href="{{route('backend.contact.form.id',$item->id)}}">{{$item->first_name}} {{$item->last_name}}</a></td>
                    <td>
                        <a href="{{route('backend.contact.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <form class="m_join" method="post">
        <input type="hidden" name="title" value="{{$item->title}}" />
        <button class="title">Join Into</button>
        <div class="input">
            <select name="contact_id">
                @foreach($items as $item)
                    <option value="{{$item->id}}">{{$item->id}} - {{$item->first_name}} {{$item->last_name}}</option>
                @endforeach
            </select>
        </div>
    </form>
@endsection
