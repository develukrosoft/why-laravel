@extends('backend.partials.layout')

@section('content')
    @include('backend.duplicate.partials.tabs')
    <div class="m_header">
        <div class="title">
            <div class="name">Duplicates</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.duplicate.contact.remove')}}">Remove Duplicates</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>Name</td>
                <td>Repeats</td>
                <td style="width: 100px">Actions</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->first_name}} {{$item->last_name}}</td>
                    <td>{{$item->count}}</td>
                    <td>
                        <a href="{{route('backend.duplicate.contact.join')}}?first_name={{$item->first_name}}&last_name={{$item->last_name}}">Join</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
