<div class="m_tabs">
    <div class="tabs">
        <a href="{{route('backend.duplicate.organisation.index')}}"
           class="item {{strpos(Route::current()->getName(),'backend.duplicate.organisation.')!==false?'active':''}}">Organization</a>
        <a href="{{route('backend.duplicate.project.index')}}"
           class="item {{strpos(Route::current()->getName(),'backend.duplicate.project.')!==false?'active':''}}">Project</a>
        <a href="{{route('backend.duplicate.contact.index')}}"
           class="item {{strpos(Route::current()->getName(),'backend.duplicate.contact.')!==false?'active':''}}">Contact</a>
    </div>
</div>
