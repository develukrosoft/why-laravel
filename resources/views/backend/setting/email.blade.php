@extends('backend.partials.layout')

@section('content')
    @include('backend.setting.partials.tabs')
    <form class="m_form" method="post">
        @foreach($settings as $s)
            @include('backend.setting.partials.item')
        @endforeach
        <div class="actions">
            <button type="submit" class="save">Save / Update</button>
        </div>
    </form>
@endsection
