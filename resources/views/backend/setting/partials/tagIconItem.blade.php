@php
    $_id = !empty($item)?$item->id:md5(strtotime('now').rand(111111,999999));
@endphp
<tr>
    <td>
        <div class="input">
            <select name="item[{{$_id}}][parent_type_id]">
                @foreach(\App\Models\TagIcon::TYPES as $id=>$name)
                    <option value="{{$id}}" {{!empty($item) && $item->parent_type_id == $id?'selected':''}}>{{$name}}</option>
                @endforeach
            </select>
        </div>
    </td>
    <td>
        <div class="input">
            <input type="text" placeholder="name" name="item[{{$_id}}][name]" required
                   value="{{!empty($item)?$item->name:''}}">
        </div>
    </td>
    <td>
        <div style="display: flex">
            @if(!empty($item) && $item->icon_url)
                <img style="max-width: 30px; height: 30px; object-fit: cover;margin-right: 5px"
                     src="{{$item->icon_url}}" />
            @endif
            <input type="file" id="file_{{$_id}}" style="display: none" name="item[{{$_id}}][image]" />
            <a href="#" class="select_file" data-id="#file_{{$_id}}" style="margin: auto 0">Select file (30x30px)</a>
        </div>
    </td>
    <td>
        <a href="#" class="delete_parent" data-parent="parent2">Delete</a>
    </td>
</tr>
