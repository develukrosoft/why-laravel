<div class="m_tabs">
    <div class="tabs">
        <a href="{{route('backend.setting.system')}}"
           class="item {{Route::current()->getName() == 'backend.setting.system'?'active':''}}">System</a>
        <a href="{{route('backend.setting.email')}}"
           class="item {{Route::current()->getName() == 'backend.setting.email'?'active':''}}">Email</a>
        <a href="{{route('backend.setting.frontend')}}"
           class="item {{Route::current()->getName() == 'backend.setting.frontend'?'active':''}}">Frontend</a>
    </div>
</div>
