<div class="field">
    <div class="title">{{$s->name}}</div>
    <div class="body">
        @if($s->type_id == \App\Models\Setting::TYPE_NUMBER)
            <input type="number" name="settings[{{$s->id}}]" value="{{$s->value}}">
        @elseif($s->type_id == \App\Models\Setting::TYPE_DOUBLE)
            <input type="number" step="0.01" name="settings[{{$s->id}}]" value="{{$s->value}}">
        @elseif($s->type_id == \App\Models\Setting::TYPE_EMAIL)
            <input type="email" name="settings[{{$s->id}}]" value="{{$s->value}}">
        @elseif($s->type_id == \App\Models\Setting::TYPE_BOOLEAN)
            <select name="settings[{{$s->id}}]">
                <option value="1">Yes</option>
                <option value="0" {{!$s->value?'selected':''}}>No</option>
            </select>
        @elseif($s->type_id == \App\Models\Setting::TYPE_CKEDITOR)
            <textarea name="settings[{{$s->id}}]" rows="10" class="_ckeditor"
                      id="settings[{{$s->id}}]">{{$s->value}}</textarea>
        @else
            <input type="text" name="settings[{{$s->id}}]" value="{{$s->value}}">
        @endif
    </div>
</div>
