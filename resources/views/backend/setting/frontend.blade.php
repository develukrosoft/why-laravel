@extends('backend.partials.layout')

@section('content')
    @include('backend.setting.partials.tabs')
    <div class="m_tabs">
        <div class="tabs style2">
            <button class="item select_tab {{!$tab_active || $tab_active == 'tab_orgs'?'active':''}}" data-tab="tab_orgs">Find Food</button>
            <button class="item select_tab {{$tab_active == 'tab_join'?'active':''}}" data-tab="tab_join">Join</button>
            <button class="item select_tab {{$tab_active == 'tab_projects'?'active':''}}" data-tab="tab_projects">Sites</button>
            <button class="item select_tab {{$tab_active == 'tab_volunteer'?'active':''}}" data-tab="tab_volunteer">Volunteer</button>
            <button class="item select_tab {{$tab_active == 'tab_calendar_event'?'active':''}}" data-tab="tab_volunteer">Volunteer</button>
        </div>
        <div id="tab_orgs" class="tab {{!$tab_active || $tab_active == 'tab_orgs'?'active':''}}">
            <form class="m_form" method="post">
                <input type="hidden" name="tab_id" value="tab_orgs" />
                @foreach($settings['tab_orgs'] as $s)
                    @include('backend.setting.partials.item')
                @endforeach
                <div class="actions">
                    <button type="submit" class="save">Save / Update</button>
                </div>
            </form>
        </div>
        <div id="tab_join" class="tab {{$tab_active == 'tab_join'?'active':''}}">
            <form class="m_form" method="post">
                <input type="hidden" name="tab_id" value="tab_join" />
                @foreach($settings['tab_join'] as $s)
                    @include('backend.setting.partials.item')
                @endforeach
                <div class="actions">
                    <button type="submit" class="save">Save / Update</button>
                </div>
            </form>
        </div>
        <div id="tab_projects" class="tab {{$tab_active == 'tab_projects'?'active':''}}">
            <form class="m_form" method="post">
                <input type="hidden" name="tab_id" value="tab_projects" />
                @foreach($settings['tab_projects'] as $s)
                    @include('backend.setting.partials.item')
                @endforeach
                <div class="actions">
                    <button type="submit" class="save">Save / Update</button>
                </div>
            </form>
        </div>
        <div id="tab_volunteer" class="tab {{$tab_active == 'tab_volunteer'?'active':''}}">
            <form class="m_form" method="post">
                <input type="hidden" name="tab_id" value="tab_volunteer" />
                @foreach($settings['tab_volunteer'] as $s)
                    @include('backend.setting.partials.item')
                @endforeach
                <div class="actions">
                    <button type="submit" class="save">Save / Update</button>
                </div>
            </form>
        </div><div id="tab_calendar_event" class="tab {{$tab_active == 'tab_calendar_event'?'active':''}}">
            <form class="m_form" method="post">
                <input type="hidden" name="tab_id" value="tab_calendar_event" />
                @foreach($settings['tab_calendar_event'] as $s)
                    @include('backend.setting.partials.item')
                @endforeach
                <div class="actions">
                    <button type="submit" class="save">Save / Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
