@extends('backend.partials.layout')

@section('content')
    @include('backend.setting.partials.tabs')
    <form method="post" enctype="multipart/form-data">
        <div class="m_header">
            <div class="title">
                <div class="name">Option Sections</div>
            </div>
        </div>
        <div class="table">
            <table id="system_table">
                <tr>
                    <td>Related</td>
                    <td>Name</td>
                    <td>Icon</td>
                    <td style="width: 100px">Options</td>
                </tr>
                @foreach($pagination->items() as $item)
                    @include('backend.setting.partials.tagIconItem')
                @endforeach
            </table>
            @include('backend.partials.pagination')
        </div>
        <div class="m_actions">
            <button type="button" class="cancel" id="add_new_tag_icon">Add new</button>
            <button type="submit" class="save">Save / Update options</button>
        </div>
    </form>
@endsection
