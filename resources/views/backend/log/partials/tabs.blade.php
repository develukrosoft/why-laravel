<div class="m_tabs">
    <div class="tabs">
        <a href="{{route('backend.log.email')}}"
           class="item {{Route::current()->getName() == 'backend.log.email'?'active':''}}">Email</a>
        <a href="{{route('backend.log.org.delete')}}"
           class="item {{Route::current()->getName() == 'backend.log.org.delete'?'active':''}}">Organization deleted</a>
        <a href="{{route('backend.log.org.update')}}"
           class="item {{Route::current()->getName() == 'backend.log.org.update'?'active':''}}">Organization updated</a>
        <a href="{{route('backend.log.org.import')}}"
           class="item {{Route::current()->getName() == 'backend.log.org.import'?'active':''}}">Organization
            imported</a>
    </div>
</div>
