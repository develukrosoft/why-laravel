@extends('backend.partials.layout')

@section('content')
    @include('backend.log.partials.tabs')
    <div class="m_header">
        <div class="title">
            <div class="name">Logs</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
    </div>
    <div class="m_filters">
        <div class="f_title toggle_filters">Filter / Search</div>
        <div class="filters">
            <form class="m_form">
                <div class="field w50">
                    <div class="body">
                        <div class="input">
                            <div class="label">Title</div>
                            <input type="text" placeholder="Title" name="title"
                                   value="{{!empty($filters['title'])?$filters['title']:''}}">
                        </div>
                        <div class="input">
                            <div class="label">Created At
                                <small>(from - to)</small>
                            </div>
                            <div class="group_input two">
                                <input type="date" placeholder="From" name="from_created_at"
                                       value="{{!empty($filters['from_created_at'])?$filters['from_created_at']:''}}">
                                <input type="date" placeholder="To" name="to_created_at"
                                       value="{{!empty($filters['to_created_at'])?$filters['to_created_at']:''}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field w50">
                    <div class="body">
                        <div class="input">
                            <div class="label">User</div>
                            <select name="user_id">
                                <option value="">---</option>
                                @foreach(\App\Models\User::all() as $user)
                                    <option value="{{$user->id}}" {{!empty($filters['user_id']) && $filters['user_id'] == $user->id?'selected':''}}>{{$user->full_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <button type="submit" class="save">Filter Record</button>
                    <button type="button" class="save">Reset Filter</button>
                    <button type="reset" class="cancel search_reset">Clean Form</button>
                </div>
                <input type="hidden" value="{{$pagination->perPage()}}" name="per_page" />
            </form>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>Title</td>
                <td>User</td>
                <td>Deleted At</td>
                {{--<td style="width: 100px">Options</td>--}}
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->title}}</td>
                    <td>
                        @if($item->user)
                            <a href="{{route('backend.user.form.id',$item->user_id)}}">{{$item->user->full_name}}</a>
                        @endif
                    </td>
                    <td>
                        {{Date('Y-m-d H:i',strtotime($item->created_at))}}
                    </td>
                    {{--<td>--}}
                    {{--<a href="{{route('backend.country.form.id',$item->id)}}">Edit</a>--}}
                    {{--<br />--}}
                    {{--<a href="{{route('backend.country.action.id',$item->id)}}?action=delete">Delete</a>--}}
                    {{--</td>--}}
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
