@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="body">
                @foreach($fields as $key=>$field)
                    <div class="input">
                        <div class="label">{{$field}}</div>
                        <select name="mapping[{{$key}}]">
                            <option value="0">---</option>
                            @foreach($headers as $h_id=>$header)
                                <option value="{{$h_id + 1}}"
                                        {{strtolower($header) == strtolower($key)?'selected':''}}
                                        {{strtolower($header) == strtolower($field)?'selected':''}}
                                >{{$header}}</option>
                            @endforeach
                        </select>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Import</button>
        </div>
    </form>
@endsection
