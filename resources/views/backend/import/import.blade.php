@extends('backend.partials.layout')

@section('content')
    <div class="m_import">
        <div class="header">Import Progress:<span id="import_current_row">{{$item->current_row - 1}}</span>/{{$item->total_rows - 1}}</div>
        <div class="progress">
            Process <span id="import_process_percent">{{($item->current_row-1)?((($item->total_rows-1)-($item->current_row-1)) * 100)/($item->total_rows-1):'0'}}</span>
            %
            <div class="line" style="height: 100%;"></div>
        </div>
        <ul class="list">
        </ul>
    </div>
@endsection
