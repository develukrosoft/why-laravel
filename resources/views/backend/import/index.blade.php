@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post" enctype="multipart/form-data">
        <div class="field">
            <div class="title">Data source (.csv format)</div>
            <div class="body">
                <input type="file" placeholder="Title" name="file">
            </div>
        </div>
        <div class="field">
            <div class="title">Import type</div>
            <div class="body">
                <div class="input">
                    <select name="type_id">
                        <option value="{{\App\Models\ImportFile::TYPE_ORGANISATION}}">Organizations</option>
                        <option value="{{\App\Models\ImportFile::TYPE_PROJECT}}">Projects</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Upload and Import CSV File with Data</button>
        </div>
    </form>
@endsection
