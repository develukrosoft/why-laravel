@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        <div class="field">
            <div class="title">Title</div>
            <div class="body">
                <input type="text" placeholder="Title" name="value" value="{{!empty($item)?$item->value:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Category</div>
            <div class="body">
                <div class="input">
                    <select name="category_id">
                        @foreach(\App\Models\OptionCategory::all() as $c)
                            <option value="{{$c->id}}" {{!empty($item) && $c->id == $item->category_id?'selected':''}}>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="title">Is active (show front)</div>
            <div class="body">
                <div class="input">
                    <select name="is_active">
                        <option value="1">Yes</option>
                        <option value="0" {{!empty($item) && !$item->is_active?'selected':''}}>No</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.optionValue.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
@endsection
