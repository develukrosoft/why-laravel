@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Option Values</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.optionValue.form')}}">Add New</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td>Title</td>
                <td>Category</td>
                <td style="width: 100px">Options</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->value}}</td>
                    <td>{{$item->category->name}}</td>
                    <td>
                        <a href="{{route('backend.optionValue.form.id',$item->id)}}">Edit</a>
                        <br />
                        <a href="{{route('backend.optionValue.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
