<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    <meta name="viewport" content="width=768, initial-scale=1">

    <title>{{ config('app.name', 'WhyHunger') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/backend.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
</head>
<body>
<div id="app">
    @include('backend.partials.header')
    <main>
        @if(Auth::check())
            @include('backend.partials.sidebar')
        @endif
        <div class="_content" style="{{Auth::check()?'':'width:100%'}}">
            @if(session('alert_message'))
                <div class="_alert_message">
                    {{session('alert_message')}}
                </div>
            @endif
            @yield('content')
        </div>
    </main>
    @include('backend.partials.routes')
</div>
<script type="text/javascript" defer>
  setTimeout(function () {
    const items = document.getElementsByClassName('_ckeditor')
    for (let i = 0; i < items.length; i++) {
      CKEDITOR.replace(items[i].id, {
        filebrowserUploadUrl: "{{route('backend.upload')}}",
        filebrowserUploadMethod: 'form'
      })
    }
  }, 1000)
</script>
</body>
</html>
