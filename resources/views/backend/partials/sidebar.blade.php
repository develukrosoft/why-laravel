<div class="_sidebar">
    <a class="item {{strpos(Route::current()->getName(),'backend.index')!==false?'active':''}}"
       href="{{route('backend.index')}}">Overview</a>
    @if(Auth::user()->role == 'admin')
        <a class="item {{strpos(Route::current()->getName(),'backend.project.')!==false && Route::current()->getName() !== 'backend.project.apiImport'?'active':''}}"
           href="{{route('backend.project.index')}}">Sites / Project</a>
        <a class="item {{strpos(Route::current()->getName(),'backend.organisation.')!==false?'active':''}}"
           href="{{route('backend.organisation.index')}}">Find Food</a>
        <a class="item {{strpos(Route::current()->getName(),'backend.contact.')!==false?'active':''}}"
           href="{{route('backend.contact.index')}}">Contacts</a>
        <a class="item {{strpos(Route::current()->getName(),'backend.duplicate.')!==false?'active':''}}"
           href="{{route('backend.duplicate.organisation.index')}}">Duplicates</a>

        <a class="item" href="#"> </a>
    @endif

    <a class="item {{strpos(Route::current()->getName(),'backend.volunteer.')!==false?'active':''}}"
       href="{{route('backend.volunteer.index')}}">Volunteer (Organization)</a>
    <a class="item {{strpos(Route::current()->getName(),'backend.volunteerProfile.')!==false?'active':''}}"
       href="{{route('backend.volunteerProfile.index')}}">Volunteer Profiles</a>
    <a class="item {{strpos(Route::current()->getName(),'backend.userHowFind.')!==false?'active':''}}"
       href="{{route('backend.userHowFind.index')}}">"How did you find us"</a>

    <a class="item" href="#"> </a>


    @if(Auth::user()->role == 'admin')

        <a class="item {{strpos(Route::current()->getName(),'backend.user.')!==false?'active':''}}"
           href="{{route('backend.user.index')}}">Users</a>

        <a class="item {{strpos(Route::current()->getName(),'backend.setting.')!==false?'active':''}}"
           href="{{route('backend.setting.system')}}">System Options</a>

        <a class="item {{Route::current()->getName() == 'backend.import'?'active':''}}"
           href="{{route('backend.import.index')}}">Import Data</a>

        <a class="item {{Route::current()->getName() == 'backend.project.apiImport'?'active':''}}"
           href="{{route('backend.project.apiImport')}}">API Import</a>

        <a class="item {{Route::current()->getName() == 'backend.log.'?'active':''}}"
           href="{{route('backend.log.email')}}">Report logs</a>
        <a class="item {{strpos(Route::current()->getName(),'backend.calendarEvent.')!==false?'active':''}}"
           href="{{route('backend.calendarEvent.index')}}">Calendar Events</a>

        <a class="item {{strpos(Route::current()->getName(),'backend.modalMessage.')!==false?'active':''}}"
           href="{{route('backend.modalMessage.index')}}">Modal Messages</a>

        <a class="item" href="#"> </a>

        <a class="item {{strpos(Route::current()->getName(),'backend.programType.')!==false?'active':''}}"
           href="{{route('backend.programType.index')}}">Program Types</a>

        <a class="item {{strpos(Route::current()->getName(),'backend.optionCategory.')!==false?'active':''}}"
           href="{{route('backend.optionCategory.index')}}">Option Categories </a>

        <a class="item {{strpos(Route::current()->getName(),'backend.optionValue.')!==false?'active':''}}"
           href="{{route('backend.optionValue.index')}}">Option Values</a>


        <a class="item {{strpos(Route::current()->getName(),'backend.userHowFindValue.')!==false?'active':''}}"
           href="{{route('backend.userHowFindValue.index')}}">"How did you find us" values</a>


        <a class="item {{strpos(Route::current()->getName(),'backend.country.')!==false?'active':''}}"
           href="{{route('backend.country.index')}}">Countries</a>

        <a class="item {{strpos(Route::current()->getName(),'backend.state.')!==false?'active':''}}"
           href="{{route('backend.state.index')}}">States</a>

        <a class="item {{strpos(Route::current()->getName(),'backend.city.')!==false?'active':''}}"
           href="{{route('backend.city.index')}}">Cities</a>
    @endif

</div>
