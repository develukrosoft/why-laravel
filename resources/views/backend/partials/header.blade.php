<div class="_header">
    <div class="logo">
        <img src="{{asset('/img/why-logo-white.png')}}" />
        <span>WhyHunger</span>
    </div>
    @if(Auth::check())
        <div class="action">
            Logged in as: {{Auth::user()->username}} ( <a href="{{route('backend.auth.logout')}}">logout</a> )
        </div>
    @endif
</div>
