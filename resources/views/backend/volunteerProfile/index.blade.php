@extends('backend.partials.layout')

@section('content')
    <div class="m_header">
        <div class="title">
            <div class="name">Organization Volunteer</div>
            <div>has {{$pagination->total()}} records</div>
        </div>
        <div class="actions">
            <div class="group">
                <a href="{{route('backend.volunteerProfile.index')}}?action=export">CSV export</a>
            </div>
        </div>
    </div>
    <div class="table">
        <table>
            <tr>
                <td style="width: 60px">ID</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Email</td>
                <td style="text-align: center;width: 100px">Created At</td>
                <td style="width: 100px">Actions</td>
            </tr>
            @foreach($pagination->items() as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->first_name}}</td>
                    <td>{{$item->last_name}}</td>
                    <td>{{$item->email}}</td>

                    <td style="text-align: center">{{Date('Y-m-d',strtotime($item->created_at))}}
                        <br />{{Date('H:i',strtotime($item->created_at))}}</td>
                    <td>
                        <a href="{{route('backend.volunteerProfile.form.id',$item->id)}}">View</a>
                        <a href="{{route('backend.volunteerProfile.action.id',$item->id)}}?action=delete">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        @include('backend.partials.pagination')
    </div>
@endsection
