@extends('backend.partials.layout')

@section('content')
    <form class="m_form" method="post">
        @if(!empty($item))
            <div class="actions right">
                <a class="delete"
                   href="{{route('backend.volunteerProfile.action.id',$item->id)}}?action=delete">Delete</a>
            </div>
        @endif
        <div class="field">
            <div class="title">First name</div>
            <div class="body">
                <input type="text" placeholder="First name" name="first_name"
                       value="{{!empty($item)?$item->first_name:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Last name</div>
            <div class="body">
                <input type="text" placeholder="Last name" name="last_name"
                       value="{{!empty($item)?$item->last_name:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Email</div>
            <div class="body">
                <input type="email" placeholder="Email" name="email" value="{{!empty($item)?$item->email:''}}">
            </div>
        </div>
        <div class="field">
            <div class="title">Phone</div>
            <div class="body">
                <input type="text" placeholder="Phone" name="phone" value="{{!empty($item)?$item->phone:''}}">
            </div>
        </div>
        <div class="actions">
            <button type="submit" class="save">Save</button>
            <a href="{{route('backend.volunteerProfile.index')}}" class="cancel">Cancel</a>
        </div>
    </form>
    @if(!empty($item->data['feedback']) && is_array($item->data['feedback']))
        <div class="m_header">
            <div class="title">
                <div class="name">Feedback Volunteer</div>
            </div>
        </div>
        <div class="table">
            <table>
                <tr>
                    <td>Value</td>
                    <td style="text-align: right;width: 150px">Created At</td>
                </tr>
                @foreach($item->data['feedback'] as $feedback)
                    @if(!empty($feedback['value']))
                        <tr>
                            <td>{{$feedback['value']}}</td>
                            <td style="text-align: right">{{$feedback['date']}}</td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    @endif
    @if($pagination->total())
        <div class="m_header">
            <div class="title">
                <div class="name">Organizations Volunteer</div>
                <div>has {{$pagination->total()}} records</div>
            </div>
        </div>
        <div class="table">
            <table>
                <tr>
                    <td>Organization</td>
                    <td>Volunteer Form</td>
                    <td style="text-align: right;width: 150px">Created At</td>
                </tr>
                @foreach($pagination->items() as $ov)
                    <tr>
                        <td>
                            <a href="{{route('backend.organisation.form.id',$ov->organisation->id)}}">{{$ov->organisation->title}}</a>
                        </td>
                        <td><a href="{{route('backend.volunteer.form.id',$ov->id)}}" target="_blank">View</a></td>
                        <td style="text-align: right">{{Date('Y-m-d H:i',strtotime($ov->created_at))}}</td>
                    </tr>
                @endforeach
            </table>
            @php
                $route = route('backend.volunteerProfile.form.id',$item->id);
            @endphp
            @include('backend.partials.pagination')
        </div>
    @endif
@endsection
