@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        <script src="/map/googleMapSearch.js"></script>
        <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtxZ_0z7-XpGOPc_ZgtGW9adm1BIfevv4&callback=initMap&libraries=&v=weekly"
                defer
        ></script>
    @endpush
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')

    </div>
    <div class="_container_map">
        <div class="search">
            <div class="actions">
                <input type="text" id="map_keyword">
                <button id="map_search">Search</button>
            </div>
        </div>
        <div id="map"></div>
        <div id="loader_gif">
            <img src="/img/googleMap/loader.gif" />
        </div>
    </div>
@endsection
