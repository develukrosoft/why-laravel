<div class="_header">
    <div class="menu">
        <div class="show_adaptive_menu">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" role="img"
                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-bars fa-w-14 fa-2x">
                <path fill="currentColor"
                      d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"
                      class=""></path>
            </svg>
        </div>
        <a href="{{route('front.index')}}" class="logo">
            <img src="/img/why-logo-red.png">
            <div class="text">WhyHunger</div>
        </a>
        <div class="adaptive_menu">
            <div class="show_sub_menu">
                <div class="title">our roots<img src="/img/arrow_down.svg"></div>
                <div class="sub_menu">
                    <a target="_blank" href="https://whyhunger.org/who-we-are/our-mission/">Our Mission</a>
                    <a target="_blank" href="https://whyhunger.org/who-we-are/board-staff/">Board & Staff</a>
                    <a target="_blank" href="https://whyhunger.org/who-we-are/history/">History</a>
                    <a target="_blank" href="https://whyhunger.org/who-we-are/financials/">Financials</a>
                </div>
            </div>
            <div class="show_sub_menu">
                <div class="title">our works<img src="/img/arrow_down.svg"></div>
                <div class="sub_menu">
                    <a target="_blank" href="https://whyhunger.org/how-we-work/">How We Work</a>
                    <a target="_blank" href="https://whyhunger.org/programs/">Programs</a>
                    <a target="_blank" href="https://whyhunger.org/impacts/">Impacts</a>
                    <a target="_blank" href="https://whyhunger.org/storytelling/">Storytelling</a>
                    <a target="_blank" href="https://whyhunger.org/whats-new/publications/">Publications</a>
                </div>
            </div>
            <div class="show_sub_menu">
                <div class="title">artists<img src="/img/arrow_down.svg"></div>
                <div class="sub_menu">
                    <a target="_blank" href="https://whyhunger.org/artists/about-aahp/">About AAH&P</a>
                    <a target="_blank" href="https://whyhunger.org/artists/artists-in-action/">Artists in Action</a>
                    <a target="_blank" href="https://whyhunger.org/artists/homemade-jams/">Homemade Jams</a>
                    <a target="_blank" href="https://whyhunger.org/artists/contact-us/">Contact us</a>
                </div>
            </div>
            <div class="show_sub_menu">
                <div class="title">resources<img src="/img/arrow_down.svg"></div>
                <div class="sub_menu">
                    <a target="_blank" href="https://whyhunger.org/just-the-facts/">Just the facts</a>
                    <a target="_blank" href="https://whyhunger.org/guides-reports/">Guides & Reports</a>
                    <a target="_blank" href="https://whyhunger.org/find-food/">Find Food</a>
                    <a target="_blank" href="https://whyhunger.org/news/">News</a>
                </div>
            </div>
            <div class="show_sub_menu">
                <div class="title">get involved<img src="/img/arrow_down.svg"></div>
                <div class="sub_menu">
                    <a target="_blank" href="https://whyhunger.org/get-involved/donate/">Donate</a>
                    <a target="_blank" href="https://whyhunger.org/get-involved/corporate-partnerships/">Corporate
                        Partnerships</a>
                    <a target="_blank" href="https://hungerthon.org/">Hungerthon</a>
                    <a target="_blank" href="https://whyhunger.org/get-involved/fundraise/">Fundraise</a>
                    <a target="_blank" href="https://whyhunger.org/get-involved/events/">Events</a>
                    <a target="_blank" href="https://whyhunger.org/get-involved/volunteer/">Volunteer</a>
                    <a target="_blank" href="https://whyhunger.org/get-involved/outreach-tools/">Outreach Tools</a>
                    <a target="_blank" href="https://whyhunger.org/get-involved/young-professionals-group/">Young
                        Professionals Group</a>
                    <a target="_blank" href="https://whyhunger.org/contact-us/">Contact Us</a>
                </div>
            </div>
            <div class="show_sub_menu">
                <div class="title">what's new<img src="/img/arrow_down.svg"></div>
                <div class="sub_menu">
                    <a target="_blank" href="https://whyhunger.org/category/category/blog/">Blog</a>
                    <a target="_blank" href="https://whyhunger.org/whats-new/social/">Social</a>
                    <a target="_blank" href="https://whyhunger.org/whats-new/press/">Press</a>
                    <a target="_blank" href="https://whyhunger.org/whats-new/recent-publications/">Recent
                        Publications</a>
                    <a target="_blank" href="https://whyhunger.org/whats-new/sign-up/">Sign Up for Our Newsletter</a>
                </div>
            </div>
        </div>
    </div>
    <div class="actions">
        <a class="search" href="https://whyhunger.org/">
            <svg enable-background="new 0 0 32 32" id="Glyph" version="1.1" viewBox="0 0 32 32" xml:space="preserve"
                 xmlns="http://www.w3.org/2000/svg"><path
                        d="M27.414,24.586l-5.077-5.077C23.386,17.928,24,16.035,24,14c0-5.514-4.486-10-10-10S4,8.486,4,14  s4.486,10,10,10c2.035,0,3.928-0.614,5.509-1.663l5.077,5.077c0.78,0.781,2.048,0.781,2.828,0  C28.195,26.633,28.195,25.367,27.414,24.586z M7,14c0-3.86,3.14-7,7-7s7,3.14,7,7s-3.14,7-7,7S7,17.86,7,14z"
                        id="XMLID_223_" /></svg>
            <div class="text">SEARCH</div>
        </a>
        <a class="volunteer" href="{{route('front.volunteer.index')}}">
            <svg enable-background="new 0 0 24 24" id="Layer_1" version="1.0" viewBox="0 0 24 24" xml:space="preserve"
                 xmlns="http://www.w3.org/2000/svg"><g>
                    <path d="M9,9c0-1.7,1.3-3,3-3s3,1.3,3,3c0,1.7-1.3,3-3,3S9,10.7,9,9z M12,14c-4.6,0-6,3.3-6,3.3V19h12v-1.7C18,17.3,16.6,14,12,14z   " />
                </g>
                <g>
                    <g>
                        <circle cx="18.5" cy="8.5" r="2.5" />
                    </g>
                    <g>
                        <path d="M18.5,13c-1.2,0-2.1,0.3-2.8,0.8c2.3,1.1,3.2,3,3.2,3.2l0,0.1H23v-1.3C23,15.7,21.9,13,18.5,13z" />
                    </g>
                </g>
                <g>
                    <g>
                        <circle cx="18.5" cy="8.5" r="2.5" />
                    </g>
                    <g>
                        <path d="M18.5,13c-1.2,0-2.1,0.3-2.8,0.8c2.3,1.1,3.2,3,3.2,3.2l0,0.1H23v-1.3C23,15.7,21.9,13,18.5,13z" />
                    </g>
                </g>
                <g>
                    <g>
                        <circle cx="5.5" cy="8.5" r="2.5" />
                    </g>
                    <g>
                        <path d="M5.5,13c1.2,0,2.1,0.3,2.8,0.8c-2.3,1.1-3.2,3-3.2,3.2l0,0.1H1v-1.3C1,15.7,2.1,13,5.5,13z" />
                    </g>
                </g></svg>
            <div class="text">VOLUNTEER</div>
        </a>
        <button class="donate">
            <img src="/img/donate-red.png">
        </button>
    </div>
</div>
