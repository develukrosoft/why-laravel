<div class="_subheader">
    <div class="title">
        <div class="text">
            @if(strpos(Route::current()->getName(),'front.project.')!==false)
                {{__('home.subheader.network')}}
            @elseif(Route::current()->getName()=='front.organisation.add')
                {{__('home.subheader.join_the_network')}}
            @else
                {{__('home.subheader.find_food')}}
            @endif
        </div>
        <a href="{{route('front.locale.set',App::isLocale('en')?'es':'en')}}" class="change_language">{{__('home.subheader.change_language')}}</a>
    </div>
    <div class="description">
        {{__('home.subheader.description')}}
    </div>
</div>
