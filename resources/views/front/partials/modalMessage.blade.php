<div class="m_modal" id="modal_{{$modal->id}}" style="display: block">
    <div class="body">
        <div class="close close_modal {{$modal->class_name}}" data-modal_id="modal_{{$modal->id}}"
             data-show_type="{{$modal->show_type_id}}">X
        </div>
        <div>
            {!! $modal->message !!}
        </div>
    </div>
</div>
