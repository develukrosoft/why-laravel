<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'WhyHunger') }}</title>

    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&amp;subset=greek-ext,greek,cyrillic-ext,latin-ext,latin,vietnamese,cyrillic"
          type="text/css" media="all">
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,italic,600,600italic,700,700italic,800,800italic&amp;subset=greek-ext,greek,cyrillic-ext,latin-ext,latin,vietnamese,cyrillic"
          type="text/css" media="all">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          type="text/css" media="all">

    <!-- Scripts -->
    <script src="{{ asset('js/front.js') }}" defer></script>

    <script type="text/javascript">
      var onloadCallback = function () {
        if (document.getElementById('recaptchav2')) {
          grecaptcha.render('recaptchav2', {
            'sitekey': '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
          })
        }
      }
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
            async defer>
    </script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47012513-1"></script>
    <script>
      window.dataLayer = window.dataLayer || []

      function gtag () {dataLayer.push(arguments)}

      gtag('js', new Date())

      gtag('config', 'UA-47012513-1')
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 1050556730 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1050556730"></script>
    <script>
      window.dataLayer = window.dataLayer || []

      function gtag () {dataLayer.push(arguments)}

      gtag('js', new Date())

      gtag('config', 'AW-1050556730')
    </script>
    <script>
      window.addEventListener('load', function () {
        jQuery('input[value="Get started"]').click(function () {
          gtag('event', 'click',

            {'event_category': 'button', 'event_label': 'get started'}
          )
        })
      })
    </script>
    <!-- Styles -->
    <link href="{{ asset('css/front.css') }}" rel="stylesheet">

    @stack('head_scripts')
</head>
<body>
<div id="app">
    @include('front.partials.header')
    <main>
        @yield('content')
    </main>
    @include('front.partials.footer')
    @include('front.partials.routes')
    @if(session('alert_message'))
        <input type="hidden" id="alert_message" value="{{session('alert_message')}}" />
    @endif
    @include('front.partials.modal')
    @php
        $show_modal = false;
    @endphp
    @foreach(\App\Services\ModalMessageService::getModals() as $modal)
        @if(strtotime($modal->end_at)>=strtotime('now') && strtotime($modal->start_at)<=strtotime('now') && empty($_COOKIE['modal_'.$modal->id]) && !$show_modal)
            @include('front.partials.modalMessage')
            @php
                $show_modal = true;
            @endphp
        @endif
    @endforeach
    @stack('footer_scripts')
</div>
</body>
<script>
  window.addEventListener('load', function () {

    if (window.location.href.indexOf('/') != -1) {

      for (var i = 0; i < document.querySelectorAll('[type="submit"]').length; i++) {
        document.querySelectorAll('[type="submit"]')[i].onclick = function () {

          gtag('event', 'conversion', {'send_to': 'AW-1050556730/QB5oCJDAwZADELry-PQD'})

        }
      }

    }

    if (window.location.href.indexOf('/add') != -1) {

      for (var i = 0; i < document.querySelectorAll('.blue').length; i++) {
        document.querySelectorAll('.blue')[i].onclick = function () {

          gtag('event', 'conversion', {'send_to': 'AW-1050556730/KGFwCNiCk5ADELry-PQD'})

        }
      }

    }

    if (window.location.href.indexOf('/volunteer') != -1) {

      for (var i = 0; i < document.querySelectorAll('[value="Get started"]').length; i++) {
        document.querySelectorAll('[value="Get started"]')[i].onclick = function () {

          gtag('event', 'conversion', {'send_to': 'AW-1050556730/_8XyCKTLwZADELry-PQD'})

        }
      }

    }

    if (window.location.href.indexOf('/form') != -1) {

      for (var i = 0; i < document.querySelectorAll('.blue').length; i++) {
        document.querySelectorAll('.blue')[i].onclick = function () {

          gtag('event', 'conversion', {'send_to': 'AW-1050556730/hFwOCPrQwZADELry-PQD'})

        }
      }

    }

  })
</script>

</html>
