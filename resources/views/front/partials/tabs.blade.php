<div class="_tabs">
    <a href="{{route('front.index')}}"
       class="item {{Route::current()->getName() == 'front.index' || Route::current()->getName() == 'front.organisation.id'|| Route::current()->getName() == 'front.organisation.search'?'active':''}}"
    >{{__('home.subheader.tab_find_food')}}</a>

    <a href="{{route('front.organisation.add')}}"
       class="item {{Route::current()->getName() == 'front.organisation.add'?'active':''}}"
    >{{__('home.subheader.tab_add_a_site')}}</a>

    <a href="{{route('front.project.index')}}"
       class="item {{strpos(Route::current()->getName(),'front.project.')!==false?'active':''}}"
    >{{__('home.subheader.tab_summer_meals')}}</a>

    <a href="{{route('front.volunteer.index')}}"
       class="item {{strpos(Route::current()->getName(),'front.volunteer.')!==false?'active':''}}"
    >{{__('home.subheader.tab_volunteer')}}</a>

    <a href="{{route('front.calendarEvent.index')}}"
       class="item {{strpos(Route::current()->getName(),'front.calendarEvent.index')!==false?'active':''}}"
    >{{__('event.index.events')}}</a>
    <a href="{{route('front.calendarEvent.form')}}"
       class="item {{strpos(Route::current()->getName(),'front.calendarEvent.form')!==false?'active':''}}"
    >Add event</a>
</div>
