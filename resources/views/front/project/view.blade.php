@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        @include('front.partials.mapScripts')
    @endpush
    <input type="hidden" class="location_cords" data-lat="{{$item->address->lat}}"
           data-lng="{{$item->address->lng}}" data-title="{{$item->title}}" data-id="{{$item->id}}" data-center="true">
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        <div class="organisation">
            <div class="left">
                <div class="title">{{$item->title}}
                    @if($item->tagIcon && $item->tagIcon->icon_url)
                        <img src="{{$item->tagIcon->icon_url}}"
                             style="max-width: 30px;height: 30px;object-fit: cover;" />
                    @endif
                </div>
                <div id="map"></div>
                <div class="information">
                    @if($item->notes)
                        <div class="field">
                            <div class="title">{{__('project.view.notes')}}:</div>
                            <div>{{$item->notes}}</div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="right">
                <div class="title">{{__('project.view.site_details')}}</div>
                <div class="details">
                    <div class="field"><b>{{__('project.view.date_range')}}: </b>{{Date('d F Y',strtotime($item->date_start))}}
                        - {{Date('d F Y',strtotime($item->date_end))}}</div>
                    @if($item->address)
                        <div class="field"><b>{{__('project.view.address')}}: </b>{{$item->address->full_address}}</div>
                    @endif
                    {{--<div class="field"><b>Contact: </b>--}}
                        {{--<ul>--}}
                            {{--<li>--}}
                                {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" role="img"--}}
                                     {{--style="width: 10px;"--}}
                                     {{--xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"--}}
                                     {{--class="svg-inline--fa fa-user fa-w-14 fa-2x">--}}
                                    {{--<path fill="currentColor"--}}
                                          {{--d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"--}}
                                          {{--class=""></path>--}}
                                {{--</svg>--}}
                                {{--Don Kao +123456--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    @if($item->operation_days)
                        <div class="field"><b>{{__('project.view.operation_days')}}: </b>
                            <ul>
                                @foreach($item->operation_days as $day)
                                    <li>{{$day}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if($item->breakfast_from && $item->breakfast_to)
                        <div class="field">
                            <b>{{__('project.view.breakfast_time')}}:</b>
                            {{\App\Services\DateService::getTimePMAM($item->breakfast_from)}}
                            - {{\App\Services\DateService::getTimePMAM($item->breakfast_to)}}
                        </div>
                    @endif
                    @if($item->lunch_from && $item->lunch_to)
                        <div class="field">
                            <b>{{__('project.view.lunch_time')}}:</b>
                            {{\App\Services\DateService::getTimePMAM($item->lunch_from)}}
                            - {{\App\Services\DateService::getTimePMAM($item->lunch_to)}}
                        </div>
                    @endif
                    @if($item->snack_from && $item->snack_to)
                        <div class="field">
                            <b>{{__('project.view.snack_time')}}:</b>
                            {{\App\Services\DateService::getTimePMAM($item->snack_from)}}
                            - {{\App\Services\DateService::getTimePMAM($item->snack_to)}}
                        </div>
                    @endif
                    @if($item->dinner_from && $item->dinner_to)
                        <div class="field">
                            <b>{{__('project.view.dinner_time')}}:</b>
                            {{\App\Services\DateService::getTimePMAM($item->dinner_from)}}
                            - {{\App\Services\DateService::getTimePMAM($item->dinner_to)}}
                        </div>
                    @endif
                </div>
                @if($events->count())
                    <div class="organisations">
                        <div class="list" style="width: 100%">
                            <div class="title" style="margin-top: 10px;">{{__('event.index.events')}}:</div>
                            <div class="items">
                                @foreach($events as $item)
                                    @include('front.calendarEvent.partials.listItem')
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
