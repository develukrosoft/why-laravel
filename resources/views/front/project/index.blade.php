@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        @include('front.partials.mapScripts')
    @endpush
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        @if(!\App\Services\SettingService::getValueBySlug('projects_is_active',true))
            <div class="inactive_page">
                {!! \App\Services\SettingService::getValueBySlug('projects_inactive_html','<div>Inactive</div>') !!}
            </div>
        @else
            <div class="organisations">
                <div class="list">
                    <div class="title">{{__('project.index.sites_projects')}}: {{$pagination->total()}}</div>
                    <div id="map"></div>
                    @if($pagination->currentPage() > 1 || $pagination->hasMorePages())
                        <div class="description">
                            {{__('project.index.pagination_description')}}
                        </div>
                    @endif
                    <div class="pagination">
                        @include('front.pagination.pagination')
                    </div>
                    <div class="items">
                        @if(!$pagination->total())
                            If you are having difficulties locating a site please contact us at
                            1800-5-Hungry or email NCH@whyhunger.org
                        @endif
                        @foreach($pagination->items() as $item)
                            <input type="hidden" class="location_cords" data-lat="{{$item->address->lat}}"
                                   data-lng="{{$item->address->lng}}" data-title="{{$item->title}}"
                                   data-id="{{$item->id}}" {{$loop->index == 0?'data-center="true"':''}}>
                            <div class="item">
                                <div class="title"><a
                                            href="{{route('front.project.id',$item->id)}}">{{$item->title}}
                                        @if($item->tagIcon && $item->tagIcon->icon_url)
                                            <img src="{{$item->tagIcon->icon_url}}"
                                                 style="max-width: 30px;height: 30px;object-fit: cover;" />
                                        @endif
                                    </a>
                                </div>
                                <div class="field"><b>{{__('project.index.date_range')}}
                                        : </b>{{Date('d F Y',strtotime($item->date_start))}}
                                    - {{Date('d F Y',strtotime($item->date_end))}}
                                </div>
                                @if($item->address)
                                    <div class="field"><b>{{__('project.index.address')}}
                                            : </b>{{$item->address->full_address}}
                                    </div>
                                @endif
                                {{--<div class="field"><b>CONTACT: </b>Don Pedro</div>--}}
                                {{--<div class="field"><b>CONTACT phone: </b>+12345</div>--}}
                            </div>
                        @endforeach
                    </div>
                    @if($pagination->currentPage() > 1 || $pagination->hasMorePages())
                        <div class="description">
                            {{__('project.index.pagination_description')}}
                        </div>
                    @endif
                    <div class="pagination">
                        @include('front.pagination.pagination')
                    </div>
                </div>
                <form class="options" id="search_form">
                    <input type="hidden" name="s" value="1" />
                    <div class="title">{{__('project.index.search_options')}}</div>
                    <div class="inputs">
                        <div class="input">
                            <div class="label">{{__('project.index.center_zip')}}</div>
                            <input type="number" name="center_zip"
                                   value="{{!empty($filters['center_zip'])?$filters['center_zip']:''}}" />
                        </div>
                        <div class="input">
                            <div class="label">{{__('project.index.distance')}}</div>
                            <div class="group">
                                <input type="number" name="distance"
                                       value="{{!empty($filters['distance'])?$filters['distance']:''}}" />
                                <select name="radius_quantity">
                                    <option value="3959">{{__('project.index.miles')}}</option>
                                    <option value="6371" {{!empty($filters['radius_quantity']) && $filters['radius_quantity'] == 6371?'selected':''}}>
                                        {{__('project.index.kilometers')}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <button class="additional_button"
                                type="button">{{__('project.index.additional_options')}}</button>
                        <div class="additional_options">
                            <div class="input">
                                <div class="label">{{__('project.index.name')}}</div>
                                <input type="text" name="title"
                                       value="{{!empty($filters['title'])?$filters['title']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('project.index.country')}}</div>
                                <select class="change_country" data-state_id="search_state" name="country_id">
                                    @if(\App\Models\Country::where('priority','>=','1')->count())
                                        @foreach(\App\Models\Country::where('priority','>=',1)->orderBy('name')->get() as $c)
                                            <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                            @php
                                                if(empty($filters['country_id'])){
                                                    $filters['country_id'] = $c->id;
                                                }
                                            @endphp
                                        @endforeach
                                    @endif
                                    @foreach(\App\Models\Country::where('priority','<=',0)->orderBy('name')->get() as $c)
                                        <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                        @php
                                            if(empty($filters['country_id'])){
                                                $filters['country_id'] = $c->id;
                                            }
                                        @endphp
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('project.index.state')}}</div>
                                <select class="change_state" id="search_state" data-city_id="search_city"
                                        name="state_id">
                                    <option value="">---</option>
                                    @if(!empty($filters['country_id']))
                                        @foreach(\App\Services\LocationService::getStatesByCountryId($filters['country_id']) as $s)
                                            <option value="{{$s->id}}" {{!empty($filters['state_id']) && $filters['state_id'] == $s->id?'selected':''}}>{{$s->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('project.index.city')}}</div>
                                <select id="search_city" name="city_id">
                                    <option value="">---</option>
                                    @if(!empty($filters['state_id']))
                                        @foreach(\App\Services\LocationService::getCitiesByStateId($filters['state_id']) as $c)
                                            <option value="{{$c->id}}" {{!empty($filters['city_id']) && $filters['city_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('project.index.address')}}</div>
                                <input type="text" name="address"
                                       value="{{!empty($filters['address'])?$filters['address']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('project.index.zip')}}</div>
                                <input type="number" name="zip"
                                       value="{{!empty($filters['zip'])?$filters['zip']:''}}" />
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        <button class="blue" type="submit">{{__('project.index.search')}}</button>
                        <button class="search_reset blue" type="reset">{{__('project.index.reset')}}</button>
                    </div>
                    @if($events->count())
                        <div class="list" style="width: 100%">
                            <div class="title" style="margin-top: 10px;">{{__('event.index.events')}}:</div>
                            <div class="items">
                                @foreach($events as $item)
                                    @php
                                        $hide_coords = true;
                                    @endphp
                                    @include('front.calendarEvent.partials.listItem')
                                @endforeach
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        @endif
    </div>
@endsection
