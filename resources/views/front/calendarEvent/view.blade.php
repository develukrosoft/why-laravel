@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        @include('front.partials.mapScripts')
    @endpush
    <input type="hidden" class="location_cords" data-lat="{{$item->address->lat}}"
           data-lng="{{$item->address->lng}}" data-title="{{$item->title}}" data-id="{{$item->id}}" data-center="true">
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        <div class="organisation">
            <div class="left">
                <div class="title">{{$item->title}}</div>
                <div id="map"></div>
                <div class="information">
                    @if($item->description)
                        <div class="field">
                            <div class="title">{{__('event.view.description')}}:</div>
                            <div>{{$item->description}}</div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="right">
                <div class="title">{{__('event.view.event_details')}}</div>
                <div class="details">
                    <div class="field"><b>{{__('event.view.date_at')}}: </b>{{Date('m/d/Y',strtotime($item->date_at))}}
                    </div>
                    <div class="field"><b>{{__('event.view.url')}}:</b> <a href="{{$item->url}}">{{$item->url}}</a>
                    </div>
                    @if($item->address)
                        <div class="field"><b>{{__('event.view.address')}}: </b>{{$item->address->full_address}}</div>
                    @endif
                    {{--<div class="field"><b>Contact: </b>--}}
                    {{--<ul>--}}
                    {{--<li>--}}
                    {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" role="img"--}}
                    {{--style="width: 10px;"--}}
                    {{--xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"--}}
                    {{--class="svg-inline--fa fa-user fa-w-14 fa-2x">--}}
                    {{--<path fill="currentColor"--}}
                    {{--d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"--}}
                    {{--class=""></path>--}}
                    {{--</svg>--}}
                    {{--Don Kao +123456--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
