@extends('front.partials.layout')

@section('content')
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        @if(!\App\Services\SettingService::getValueBySlug('calendar_event_is_active',true))
            <div class="inactive_page">
                {!! \App\Services\SettingService::getValueBySlug('calendar_event_inactive_html','<div>Inactive</div>') !!}
            </div>
        @else
            <form class="organisation_form" id="org_add_form" method="post" enctype="multipart/form-data">
                <div class="title">
                    {{__('event.form.event_info_1')}} <span
                            style="font-size: 20px; text-transform: initial;">{{__('event.form.event_info_2')}}</span>
                </div>
                <div class="fields">
                    <div class="field">
                        <div class="title">{{__('event.form.title')}}</div>
                        <div class="body">
                            <input type="text" name="title" required />
                        </div>
                    </div>
                    <div class="field w50">
                        <div class="title">{{__('event.form.address')}}</div>
                        <div class="body">
                            <div class="input">
                                <div class="label">{{__('event.form.country')}}</div>
                                <select name="address[country_id]" class="change_country" data-state_id="address_state"
                                        required>
                                    <option value="">---</option>
                                    @foreach(\App\Models\Country::all() as $c)
                                        <option value="{{$c->id}}" {{!empty($item->address) && $item->address->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.state')}}</div>
                                <select name="address[state_id]" required id="address_state">
                                    <option value="">---</option>
                                    @if(!empty($item->address))
                                        @foreach(\App\Services\LocationService::getStatesByCountryId($item->address->country_id) as $s)
                                            <option value="{{$s->id}}" {{$item->address->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.city')}}</div>
                                <input type="text" placeholder="City" name="address[city]" required
                                       value="{{!empty($item->address)?$item->address->city->name:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.address')}}</div>
                                <input type="text" placeholder="Address" name="address[address]" required
                                       value="{{!empty($item->address)?$item->address->address:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.zip')}}</div>
                                <input type="number" placeholder="zip" name="address[zip]" required
                                       value="{{!empty($item->address)?$item->address->zip:''}}">
                            </div>
                        </div>
                    </div>
                    <div class="field w50">
                        <div class="title">{{__('event.form.details')}}</div>
                        <div class="body">
                            <div class="input">
                                <div class="label">{{__('event.form.type')}}</div>
                                <select name="type_id">
                                    @foreach(\App\Models\CalendarEvent::TYPES as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.date_start')}}</div>
                                <input type="date" name="date_start" required>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.date_end')}}</div>
                                <input type="date" name="date_end" required>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.date_at')}}</div>
                                <input type="datetime-local" name="date_at">
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.url')}}</div>
                                <input type="text" name="url" placeholder="https://" required>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('event.form.description')}}</div>
                        <div class="body">
                            <textarea rows="5" placeholder="Description" required
                                      name="description"></textarea>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('event.form.images')}}</div>
                        <div class="body w50">
                            <div class="input">
                                <div class="label">{{__('event.form.image_1920x1080')}}</div>
                                <input type="file" class="upload_image" name="image_1920x1080"
                                       required>
                            </div>
                        </div>
                        <div class="body w50">
                            <div class="input">
                                <div class="label">{{__('event.form.image_1080x1920')}}</div>
                                <input type="file" class="upload_image" name="image_1080x1920"
                                       accept=".jpg, .jpeg, .png"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('event.form.contact_details')}}</div>
                        <div class="body w50">
                            <div class="input">
                                <div class="label">{{__('event.form.first_name')}}</div>
                                <input type="text" placeholder="First Name" name="contact[first_name]"
                                       required>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.last_name')}}</div>
                                <input type="text" placeholder="Last Name" name="contact[last_name]"
                                       required>
                            </div>
                        </div>
                        <div class="body w50">
                            <div class="input">
                                <div class="label">{{__('event.form.phone')}}</div>
                                <input type="text" placeholder="Phone" name="contact[phone]" required>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.form.email')}}</div>
                                <input type="email" placeholder="Email" name="contact[email]" required>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('event.form.captcha')}}</div>
                        <div class="body">
                            <div id="recaptchav2"></div>
                        </div>
                    </div>
                    <div class="actions">
                        <button class="blue">{{__('event.form.send_request')}}</button>
                    </div>
                </div>
            </form>
        @endif
    </div>
@endsection
