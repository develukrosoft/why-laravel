@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        @include('front.partials.mapScripts')
    @endpush
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        @if(!\App\Services\SettingService::getValueBySlug('calendar_event_is_active',true))
            <div class="inactive_page">
                {!! \App\Services\SettingService::getValueBySlug('calendar_event_inactive_html','<div>Inactive</div>') !!}
            </div>
        @else
            <div class="organisations">
                <div class="list">
                    <div class="title">{{__('event.index.events')}}: {{$pagination->total()}}</div>
                    <div id="map"></div>
                    <div class="orders">
                        <div class="item">{{__('organisation.index.results_order')}}:</div>
                        <div class="item order search_order" data-sort="title">{{__('event.index.by_title')}}</div>
                        <div class="item order search_order" data-sort="date_at">{{__('event.index.by_date')}}</div>
                        <div class="item order search_order" data-sort="address">{{__('event.index.by_address')}}</div>
                    </div>
                    @if($pagination->currentPage() > 1 || $pagination->hasMorePages())
                        <div class="description">
                            {{__('event.index.pagination_description')}}
                        </div>
                    @endif
                    <div class="pagination">
                        @include('front.pagination.pagination')
                    </div>
                    <div class="items">
                        @if(!$pagination->total())
                            If you are having difficulties locating a site please contact us at
                            1800-5-Hungry or email NCH@whyhunger.org
                        @endif
                        @foreach($pagination->items() as $item)
                            @include('front.calendarEvent.partials.listItem')
                        @endforeach
                    </div>
                    @if($pagination->currentPage() > 1 || $pagination->hasMorePages())
                        <div class="description">
                            {{__('event.index.pagination_description')}}
                        </div>
                    @endif
                    <div class="pagination">
                        @include('front.pagination.pagination')
                    </div>
                </div>
                <form class="options" id="search_form">
                    <input type="hidden" name="s" value="1" />
                    <input type="hidden" id="search_sort_field" name="s_f"
                           value="{{!empty($filters['s_f'])?$filters['s_f']:'id'}}" />
                    <input type="hidden" id="search_sort_order" name="s_o"
                           value="{{!empty($filters['s_o'])?$filters['s_o']:'desc'}}" />
                    <div class="title">{{__('event.index.search_options')}}</div>
                    <div class="inputs">
                        <div class="input">
                            <div class="label">{{__('event.index.center_zip')}}</div>
                            <input type="number" name="center_zip"
                                   value="{{!empty($filters['center_zip'])?$filters['center_zip']:''}}" />
                        </div>
                        <div class="input">
                            <div class="label">{{__('event.index.distance')}}</div>
                            <div class="group">
                                <input type="number" name="distance"
                                       value="{{!empty($filters['distance'])?$filters['distance']:''}}" />
                                <select name="radius_quantity">
                                    <option value="3959">{{__('event.index.miles')}}</option>
                                    <option value="6371" {{!empty($filters['radius_quantity']) && $filters['radius_quantity'] == 6371?'selected':''}}>
                                        {{__('event.index.kilometers')}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <button class="additional_button"
                                type="button">{{__('event.index.additional_options')}}</button>
                        <div class="additional_options">
                            <div class="input">
                                <div class="label">{{__('event.index.name')}}</div>
                                <input type="text" name="title"
                                       value="{{!empty($filters['title'])?$filters['title']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.index.date_at')}}</div>
                                <input type="date" name="date_at"
                                       value="{{!empty($filters['date_at'])?$filters['date_at']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.index.country')}}</div>
                                <select class="change_country" data-state_id="search_state" name="country_id">
                                    @if(\App\Models\Country::where('priority','>=','1')->count())
                                        @foreach(\App\Models\Country::where('priority','>=',1)->orderBy('name')->get() as $c)
                                            <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                            @php
                                                if(empty($filters['country_id'])){
                                                    $filters['country_id'] = $c->id;
                                                }
                                            @endphp
                                        @endforeach
                                    @endif
                                    @foreach(\App\Models\Country::where('priority','<=',0)->orderBy('name')->get() as $c)
                                        <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                        @php
                                            if(empty($filters['country_id'])){
                                                $filters['country_id'] = $c->id;
                                            }
                                        @endphp
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.index.state')}}</div>
                                <select class="change_state" id="search_state" data-city_id="search_city"
                                        name="state_id">
                                    <option value="">---</option>
                                    @if(!empty($filters['country_id']))
                                        @foreach(\App\Services\LocationService::getStatesByCountryId($filters['country_id']) as $s)
                                            <option value="{{$s->id}}" {{!empty($filters['state_id']) && $filters['state_id'] == $s->id?'selected':''}}>{{$s->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.index.city')}}</div>
                                <select id="search_city" name="city_id">
                                    <option value="">---</option>
                                    @if(!empty($filters['state_id']))
                                        @foreach(\App\Services\LocationService::getCitiesByStateId($filters['state_id']) as $c)
                                            <option value="{{$c->id}}" {{!empty($filters['city_id']) && $filters['city_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.index.address')}}</div>
                                <input type="text" name="address"
                                       value="{{!empty($filters['address'])?$filters['address']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('event.index.zip')}}</div>
                                <input type="number" name="zip"
                                       value="{{!empty($filters['zip'])?$filters['zip']:''}}" />
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        <button class="blue" type="submit">{{__('event.index.search')}}</button>
                        <button class="search_reset blue" type="reset">{{__('event.index.reset')}}</button>
                    </div>
                </form>
            </div>
        @endif
    </div>
@endsection
