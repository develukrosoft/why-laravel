@if(empty($hide_coords))
    <input type="hidden" class="location_cords" data-lat="{{$item->address->lat}}"
           data-lng="{{$item->address->lng}}" data-title="{{$item->title}}"
           data-id="{{$item->id}}" {{$loop->index == 0?'data-center="true"':''}}>
@endif
<div class="item" style="display: flex">
    <div>
        <div class="title">
            <a href="{{route('front.calendarEvent.id',$item->id)}}">{{$item->title}}</a>
        </div>
        @if($item->date_at)
            <div class="field"><b>{{__('event.index.date_at')}}
                    : </b>{{Date('m/d/Y',strtotime($item->date_at))}}
            </div>
        @endif
        @if($item->address)
            <div class="field"><b>{{__('event.index.address')}}
                    : </b>{{$item->address->full_address}}
            </div>
        @endif
        @if($item->image_1920x1080)
            <div style="" class="block_img">
                <img src="{{$item->image_1920x1080}}"
                     class="img" />
            </div>
        @endif
    </div>
    {{--<div class="field"><b>CONTACT: </b>Don Pedro</div>--}}
    {{--<div class="field"><b>CONTACT phone: </b>+12345</div>--}}
    {{--@if($item->image_1920x1080)--}}
    {{--<div style="width: 50%">--}}
    {{--<img src="{{$item->image_1920x1080}}"--}}
    {{--style="width: 100%; height: 100%; max-height: 200px; object-fit: cover" />--}}
    {{--</div>--}}
    {{--@endif--}}
</div>
