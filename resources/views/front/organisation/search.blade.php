@extends('front.partials.layout')

@section('content')
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        <form class="organisations" id="search_form" action="{{route('front.index')}}">
            <div class="org_options">
                <div class="org_title">{{__('organisation.search.find_food')}}
                    <div class="arrow">❭</div>
                    {{__('organisation.search.advanced_search')}}
                </div>
                @foreach(\App\Models\OptionCategory::where('is_active',1)->get() as $c)
                    <div class="field">
                        <div class="title show_options"><span class="_arrow_down"></span> {{$c->name}}</div>
                        <div class="body" style="display: none">
                            @foreach($c->values as $v)
                                <div class="input">
                                    <input type="checkbox" value="{{$v->id}}" id="value_{{$v->id}}"
                                           name="options[]" {{!empty($item->options) && in_array($v->id,$item->options()->pluck('option_id')->toArray()) !== false?'checked':''  }}/>
                                    <label class="label" for="value_{{$v->id}}">{{$v->value}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="options">
                <input type="hidden" name="s" value="1" />
                <input type="hidden" id="search_sort_field" name="s_f" value="id" />
                <input type="hidden" id="search_sort_order" name="s_o" value="desc" />
                <div class="title">{{__('organisation.search.search_options')}}</div>
                <div class="inputs">
                    <div class="input">
                        <div class="label">{{__('organisation.search.program_type')}}</div>
                        <select name="program_type_id">
                            <option value="">---</option>
                            @foreach(\App\Models\ProgramType::where('is_active',1)->get() as $type)
                                <option value="{{$type->id}}" {{!empty($filters['program_type_id']) && $filters['program_type_id'] == $type->id?'selected':''}}>{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input">
                        <div class="label">{{__('organisation.search.name')}}</div>
                        <input type="text" name="title"
                               value="{{!empty($filters['title'])?$filters['title']:''}}" />
                    </div>
                    <div class="input">
                        <div class="label">{{__('organisation.search.country')}}</div>
                        <select class="change_country" data-state_id="search_state" name="country_id">
                            @if(\App\Models\Country::where('priority','>=','1')->count())
                                @foreach(\App\Models\Country::where('priority','>=',1)->orderBy('name')->get() as $c)
                                    <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                    @php
                                        if(empty($filters['country_id'])){
                                            $filters['country_id'] = $c->id;
                                        }
                                    @endphp
                                @endforeach
                            @endif
                            @foreach(\App\Models\Country::where('priority','<=',0)->orderBy('name')->get() as $c)
                                <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                    @php
                                        if(empty($filters['country_id'])){
                                            $filters['country_id'] = $c->id;
                                        }
                                    @endphp
                                @endforeach
                        </select>
                    </div>
                    <div class="input">
                        <div class="label">{{__('organisation.search.state')}}</div>
                        <select class="change_state" id="search_state" data-city_id="search_city" name="state_id">
                            <option value="">---</option>
                            @if(!empty($filters['country_id']))
                                @foreach(\App\Services\LocationService::getStatesByCountryId($filters['country_id']) as $s)
                                    <option value="{{$s->id}}" {{!empty($filters['state_id']) && $filters['state_id'] == $s->id?'selected':''}}>{{$s->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="input">
                        <div class="label">{{__('organisation.search.city')}}</div>
                        <select id="search_city" name="city_id">
                            <option value="">---</option>
                            @if(!empty($filters['city_id']))
                                @foreach(\App\Services\LocationService::getCitiesByStateId($filters['city_id']) as $c)
                                    <option value="{{$c->id}}" {{!empty($filters['city_id']) && $filters['city_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="input">
                        <div class="label">{{__('organisation.search.address')}}</div>
                        <input type="text" name="address"
                               value="{{!empty($filters['address'])?$filters['address']:''}}" />
                    </div>
                    <div class="input">
                        <div class="label">{{__('organisation.search.zip')}}</div>
                        <input type="number" name="zip" value="{{!empty($filters['zip'])?$filters['zip']:''}}" />
                    </div>
                </div>
                <div class="actions">
                    <button class="blue" type="submit">{{__('organisation.search.search')}}</button>
                    <button class="search_reset blue" type="reset">{{__('organisation.search.reset')}}</button>
                </div>
            </div>
        </form>
    </div>
@endsection
