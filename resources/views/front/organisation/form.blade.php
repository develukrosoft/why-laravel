@extends('front.partials.layout')

@section('content')
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        @if(!\App\Services\SettingService::getValueBySlug('join_is_active',true))
            <div class="inactive_page">
                {!! \App\Services\SettingService::getValueBySlug('join_inactive_html','<div>Inactive</div>') !!}
            </div>
        @else
            <form class="organisation_form" id="org_add_form" method="post">
                <div class="title">
                    @if(!empty($item))
                        {{__('organisation.form.update_organisation_info')}}
                    @else
                        {{__('organisation.form.organisation_info_1')}} <span
                                style="font-size: 20px; text-transform: initial;">{{__('organisation.form.organisation_info_2')}}</span>
                    @endif
                </div>
                <div class="fields">
                    <div class="field">
                        <div class="title">{{__('organisation.form.title')}}</div>
                        <div class="body">
                            <input type="text" name="title" required value="{{!empty($item)?$item->title:''}}" />
                        </div>
                    </div>
                    <div class="field w50">
                        <div class="title">{{__('organisation.form.address')}}</div>
                        <div class="body">
                            <div class="input">
                                <div class="label">{{__('organisation.form.country')}}</div>
                                <select name="address[country_id]" class="change_country" data-state_id="address_state"
                                        required>
                                    <option value="">---</option>
                                    @foreach(\App\Models\Country::all() as $c)
                                        <option value="{{$c->id}}" {{!empty($item->address) && $item->address->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.state')}}</div>
                                <select name="address[state_id]" required id="address_state">
                                    <option value="">---</option>
                                    @if(!empty($item->address))
                                        @foreach(\App\Services\LocationService::getStatesByCountryId($item->address->country_id) as $s)
                                            <option value="{{$s->id}}" {{$item->address->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.city')}}</div>
                                <input type="text" placeholder="City" name="address[city]" required
                                       value="{{!empty($item->address)?$item->address->city->name:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.address')}}</div>
                                <input type="text" placeholder="Address" name="address[address]" required
                                       value="{{!empty($item->address)?$item->address->address:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.zip')}}</div>
                                <input type="number" placeholder="zip" name="address[zip]" required
                                       value="{{!empty($item->address)?$item->address->zip:''}}">
                            </div>
                        </div>
                    </div>
                    <div class="field w50">
                        <div class="title">{{__('organisation.form.postal_address')}}
                            <small>{{__('organisation.form.optional')}}</small>
                        </div>
                        <div class="body">
                            <div class="input">
                                <div class="label">{{__('organisation.form.country')}}</div>
                                <select name="postal_address[country_id]" class="change_country"
                                        data-state_id="postal_address_state">
                                    <option value="">---</option>
                                    @foreach(\App\Models\Country::all() as $c)
                                        <option value="{{$c->id}}" {{!empty($item->postalAddress) && $item->postalAddress->country_id == $c->id?'selected':''}}>{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.state')}}</div>
                                <select name="postal_address[state_id]"
                                        id="postal_address_state">
                                    <option value="">---</option>
                                    @if(!empty($item->postalAddress))
                                        @foreach(\App\Services\LocationService::getStatesByCountryId($item->postalAddress->country_id) as $s)
                                            <option value="{{$s->id}}" {{$item->postalAddress->state_id == $s->id?'selected':''}}>{{$s->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.city')}}</div>
                                <input type="text" placeholder="City" name="postal_address[city]"
                                       value="{{!empty($item->postalAddress)?$item->postalAddress->city->name:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.address')}}</div>
                                <input type="text" placeholder="Address" name="postal_address[address]"
                                       value="{{!empty($item->postalAddress)?$item->postalAddress->address:''}}">
                            </div>

                            <div class="input">
                                <div class="label">{{__('organisation.form.zip')}}</div>
                                <input type="number" placeholder="zip" name="postal_address[zip]"
                                       value="{{!empty($item->postalAddress)?$item->postalAddress->zip:''}}">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('organisation.form.phones_emails')}}</div>
                        <div class="body w50">
                            <div class="title">{{__('organisation.form.phones')}}</div>
                            <div id="phone_items">
                                @if(empty($item) || (!empty($item) && count($item->phones) === 0))
                                    @include('front.organisation.partials.phone')
                                @else
                                    @foreach($item->phones as $phone)
                                        @include('front.organisation.partials.phone',['item'=>$phone])
                                    @endforeach
                                @endif
                            </div>
                            <button class="add" id="add_phone" type="button">{{__('organisation.form.add')}}</button>
                        </div>
                        <div class="body w50">
                            <div class="title">{{__('organisation.form.emails')}}</div>
                            <div id="email_items">
                                @if(empty($item) || (!empty($item) && count($item->emails) === 0))
                                    @include('front.organisation.partials.email')
                                @else
                                    @foreach($item->emails as $email)
                                        @include('front.organisation.partials.email',['item'=>$email])
                                    @endforeach
                                @endif
                            </div>
                            <button class="add" id="add_email" type="button">{{__('organisation.form.add')}}</button>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('organisation.form.details')}}</div>
                        <div class="body w50">
                            <div class="input">
                                <div class="label">{{__('organisation.form.website')}}</div>
                                <input type="text" placeholder="Website" name="website"
                                       value="{{!empty($item)?$item->website:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.annual_budget')}}</div>
                                <input type="text" placeholder="$" name="annual_budget"
                                       value="{{!empty($item)?$item->annual_budget:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.year_founded')}}</div>
                                <select name="year_founded">
                                    @for($i=Date('Y');$i>=1900;$i--)
                                        <option value="{{$i}}" {{!empty($item) && $item->year_founded == $i?'selected':''}}>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.number_volunteers')}}</div>
                                <input type="number" placeholder="0" name="number_volunteers"
                                       value="{{!empty($item)?$item->number_volunteers:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.number_part_time_staff')}}</div>
                                <input type="number" placeholder="0" name="number_part_time_staff"
                                       value="{{!empty($item)?$item->number_part_time_staff:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.number_full_time_staff')}}</div>
                                <input type="number" placeholder="0" name="number_full_time_staff"
                                       value="{{!empty($item)?$item->number_full_time_staff:''}}">
                            </div>
                        </div>
                        <div class="body w50">
                            <div class="input">
                                <div class="label">{{__('organisation.form.program_types')}}</div>
                                <select multiple name="program_types[]" required>
                                    @foreach(\App\Models\ProgramType::all() as $type)
                                        <option value="{{$type->id}}" {{!empty($item->types) && in_array($type->id,$item->types()->pluck('type_id')->toArray()) !== false?'selected':''}}>{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.program_type_description')}}
                                    <small>{{__('organisation.form.optional')}}</small>
                                </div>
                                <input type="text" placeholder="Description" name="program_type_description"
                                       value="{{!empty($item)?$item->program_type_description:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.hours')}}</div>
                                <input type="text" placeholder="Hours" name="hours"
                                       value="{{!empty($item->hours['text'])?$item->hours['text']:''}}">
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.is_receives_newsletter')}}</div>
                                <select name="is_receives_newsletter">
                                    <option value="0">No</option>
                                    <option value="1" {{!empty($item) && $item->is_receives_newsletter?'selected':''}}>
                                        Yes
                                    </option>
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.form.is_hcsraw')}}</div>
                                <select name="is_hcsraw">
                                    <option value="0">No</option>
                                    <option value="1" {{!empty($item) && $item->is_hcsraw?'selected':''}}>Yes</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('organisation.form.mission')}}</div>
                        <div class="body">
                            <textarea rows="5" placeholder="Mission Statement"
                                      name="mission">{{!empty($item)?$item->mission:''}}</textarea>
                        </div>
                    </div>
                    <div class="field">
                        <div class="title">{{__('organisation.form.notes')}}</div>
                        <div class="body">
                            <textarea rows="5" placeholder="Notes"
                                      name="notes">{{!empty($item)?$item->notes:''}}</textarea>
                        </div>
                    </div>
                    <div class="title" style="margin-top:10px">{{__('organisation.form.organisation_details')}}
                        <small>{{__('organisation.form.optional_upper')}}</small>
                    </div>
                    @foreach(\App\Models\OptionCategory::all() as $c)
                        <div class="field">
                            <div class="title">{{$c->name}}</div>
                            <div class="body options">
                                @foreach($c->values as $v)
                                    <div class="input">
                                        <input type="checkbox" value="{{$v->id}}"
                                               {{!empty($item->options) && in_array($v->id,$item->options()->pluck('option_id')->toArray()) !== false?'checked':''}}
                                               name="options[]" />
                                        <label class="label">{{$v->value}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                    @if(empty($item))
                        <div class="field">
                            <div class="title">{{__('organisation.form.confirmation_email')}}:</div>
                            <div class="body">
                                <select name="confirmation_email_id" id="confirmation_email_id">
                                    @foreach(\App\Models\Organisation::CONFIRMATION_EMAILS as $id=>$name)
                                        <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="field" id="contact_details" style="display: none">
                            <div class="title">{{__('organisation.form.contact_details')}}</div>
                            <div class="body w50">
                                <div class="input">
                                    <div class="label">{{__('organisation.form.first_name')}}</div>
                                    <input type="text" placeholder="First Name" name="new_contact[first_name]"
                                    >
                                </div>
                                <div class="input">
                                    <div class="label">{{__('organisation.form.last_name')}}</div>
                                    <input type="text" placeholder="Last Name" name="new_contact[last_name]"
                                    >
                                </div>
                            </div>
                            <div class="body w50">
                                <div class="input">
                                    <div class="label">{{__('organisation.form.phone')}}</div>
                                    <input type="text" placeholder="Phone" name="new_contact[phone]">
                                </div>
                                <div class="input">
                                    <div class="label">{{__('organisation.form.email')}}</div>
                                    <input type="email" placeholder="Email" name="new_contact[email]">
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="title">{{__('organisation.form.how_did_you_find_us')}}</div>
                            <div class="body">
                                <div class="input">
                                    <select name="how_find" required id="how_find">
                                        @foreach(\App\Models\UserHowFindValue::all() as $v)
                                            <option value="{{$v->id}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input" id="how_find_other" style="display: none">
                                    <input type="text" placeholder="Other value" name="how_find_other">
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="field">
                        <div class="title">{{__('organisation.form.captcha')}}</div>
                        <div class="body">
                            <div id="recaptchav2"></div>
                        </div>
                    </div>
                </div>

                <div class="actions">
                    <button class="blue">{{__('organisation.form.send_request')}}</button>
                </div>
            </form>
            <input type="hidden" value="{{\App\Models\Organisation::CONFIRMATION_EMAIL_ORG}}"
                   id="confirmation_email_org" />
            @php
                $value = \App\Models\UserHowFindValue::where('name','Other')->first();
            @endphp
            <input type="hidden" value="{{$value?$value->id:0}}" id="how_find_other_id" />
        @endif
    </div>
@endsection
