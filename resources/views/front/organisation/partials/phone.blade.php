@php
    $_id = !empty($item)?$item->id:'new_'.strtotime('now').rand(999,9999);
@endphp
<div class="input info_list">
    <select name="phones[{{$_id}}][type]">
        @foreach( \App\Models\Phone::TYPES as $id=>$type)
            <option value="{{$id}}" {{!empty($item) && $item->type == $id?'selected':''}}>{{$type}}</option>
        @endforeach
    </select>
    <input type="text" placeholder="Phone" name="phones[{{$_id}}][phone]" value="{{!empty($item)?$item->phone:''}}" required>
    <input type="text" placeholder="Ext." name="phones[{{$_id}}][ext]" value="{{!empty($item)?$item->ext:''}}">
    <button class="delete delete_parent" type="button">{{__('organisation.form.delete')}}</button>
</div>
