@php
    $_id = !empty($item)?$item->id:'new_'.strtotime('now').rand(999,9999);
@endphp
<div class="input info_list">
    <input type="email" placeholder="email" name="emails[{{$_id}}][email]" value="{{!empty($item)?$item->email:''}}" required>
    <button class="delete delete_parent" type="button">{{__('organisation.form.delete')}}</button>
</div>
