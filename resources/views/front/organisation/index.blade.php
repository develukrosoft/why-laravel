@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        @include('front.partials.mapScripts')
    @endpush
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        @if(!\App\Services\SettingService::getValueBySlug('find_food_is_active',true))
            <div class="inactive_page">
                {!! \App\Services\SettingService::getValueBySlug('find_food_inactive_html','<div>Inactive</div>') !!}
            </div>
        @else
            <div class="organisations">
                <div class="list">
                    <div class="title">{{__('organisation.index.find_food')}}: {{$pagination->total()}}</div>
                    <div id="map"></div>
                    <div class="orders">
                        <div class="item">{{__('organisation.index.results_order')}}:</div>
                        <div class="item order search_order"
                             data-sort="title">{{__('organisation.index.by_title')}}</div>
                        <div class="item order search_order"
                             data-sort="type_id">{{__('organisation.index.by_program_type')}}</div>
                        <div class="item order search_order"
                             data-sort="address">{{__('organisation.index.by_address')}}</div>
                    </div>
                    @if($pagination->currentPage() > 1 || $pagination->hasMorePages())
                        <div class="description">
                            {{__('organisation.index.pagination_description')}}
                        </div>
                    @endif
                    <div class="pagination">
                        @include('front.pagination.pagination')
                        {{--<button class="print">Print Results</button>--}}
                    </div>
                    <div class="items">
                        @if(!$pagination->total())
                            If you are having difficulties locating a site please contact us at
                            1800-5-Hungry or email NCH@whyhunger.org
                        @endif
                        @foreach($pagination->items() as $item)
                            <input type="hidden" class="location_cords" data-lat="{{$item->address->lat}}"
                                   data-lng="{{$item->address->lng}}" data-title="{{$item->title}}"
                                   data-id="{{$item->id}}" {{$loop->index == 0?'data-center="true"':''}}>
                            <div class="item">
                                <div class="title"><a
                                            href="{{route('front.organisation.id',$item->id)}}">
                                        {{$item->title}}
                                        @if($item->tagIcon && $item->tagIcon->icon_url)
                                            <img src="{{$item->tagIcon->icon_url}}"
                                                 style="max-width: 30px;height: 30px;object-fit: cover;" />
                                        @endif
                                        @foreach($item->types as $type)
                                            {!! $type->fontawesome_icon !!}
                                        @endforeach
                                        @foreach($item->categories as $category)
                                            {!! $category->fontawesome_icon !!}
                                        @endforeach
                                    </a>
                                </div>
                                @if(count($item->types))
                                    <div class="type"><b>{{__('organisation.index.type')}}: </b>
                                        @foreach($item->types as $type)
                                            {{$type->name}}{{count($item->types) - 1===$loop->index?'':', '}}
                                        @endforeach
                                    </div>
                                @endif
                                @if($item->address)
                                    <div class="field"><b>{{__('organisation.index.address')}}
                                            : </b>{{$item->address->full_address}}
                                    </div>
                                @endif
                                @if(count($item->phones))
                                    <div class="field"><b>{{__('organisation.index.phones')}}: </b>
                                        @foreach($item->phones as $phone)
                                            {{substr($phone->phone,0,3)}}-{{substr($phone->phone,3,3)}}
                                            -{{substr($phone->phone,6)}} {{$phone->ext?'ext.'.$phone->ext:''}}{{count($item->phones) - 1===$loop->index?'':', '}}
                                        @endforeach
                                    </div>
                                @endif
                                @if(!empty($item->hours['text']))
                                    <div class="field"><b>{{__('organisation.index.hours')}}
                                            : </b>{{$item->hours['text']}}</div>
                                @endif
                                @if($item->CategoryAndOptions)
                                    <div class="quick_details">
                                        <div class="name"><span
                                                    class="_arrow_down"></span> {{__('organisation.index.quick_details')}}
                                        </div>
                                        <div class="details">
                                            <div class="detail">
                                                @foreach($item->CategoryAndOptions as $category=>$options)
                                                    <div class="field"><b>{{$category}}: </b>
                                                        @foreach($options as $option)
                                                            {{$option}}{{count($options) - 1===$loop->index?'':', '}}
                                                        @endforeach
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                    @if($pagination->currentPage() > 1 || $pagination->hasMorePages())
                        <div class="description">
                            {{__('organisation.index.pagination_description')}}
                        </div>
                    @endif
                    <div class="pagination">
                        @include('front.pagination.pagination')
                    </div>
                </div>
                <form class="options" id="search_form">
                    <input type="hidden" name="s" value="1" />
                    <input type="hidden" id="search_sort_field" name="s_f"
                           value="{{!empty($filters['s_f'])?$filters['s_f']:'id'}}" />
                    <input type="hidden" id="search_sort_order" name="s_o"
                           value="{{!empty($filters['s_o'])?$filters['s_o']:'desc'}}" />
                    <div class="title">{{__('organisation.index.search_options')}}</div>
                    <div class="inputs">
                        <div class="input">
                            <div class="label">{{__('organisation.index.center_zip')}}</div>
                            <input type="number" name="center_zip"
                                   value="{{!empty($filters['center_zip'])?$filters['center_zip']:''}}" />
                        </div>
                        <div class="input">
                            <div class="label">{{__('organisation.index.distance')}}</div>
                            <div class="group">
                                <input type="number" name="distance"
                                       value="{{!empty($filters['distance'])?$filters['distance']:''}}" />
                                <select name="radius_quantity">
                                    <option value="3959">{{__('organisation.index.miles')}}</option>
                                    <option value="6371" {{!empty($filters['radius_quantity']) && $filters['radius_quantity'] == 6371?'selected':''}}>
                                        {{__('organisation.index.kilometers')}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="input">
                            <div class="label">{{__('organisation.index.program_type')}}</div>
                            <select name="program_type_id">
                                <option value="">---</option>
                                @foreach(\App\Models\ProgramType::where('is_active',1)->get() as $type)
                                    <option value="{{$type->id}}" {{!empty($filters['program_type_id']) && $filters['program_type_id'] == $type->id?'selected':''}}>{{$type->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="additional_button"
                                type="button">{{__('organisation.index.additional_options')}}</button>
                        <div class="additional_options">
                            <div class="input">
                                <div class="label">{{__('organisation.index.name')}}</div>
                                <input type="text" name="title"
                                       value="{{!empty($filters['title'])?$filters['title']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.index.country')}}</div>
                                <select class="change_country" data-state_id="search_state" name="country_id">
                                    @if(\App\Models\Country::where('priority','>=','1')->count())
                                        @foreach(\App\Models\Country::where('priority','>=',1)->orderBy('name')->get() as $c)
                                            <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                            @php
                                                if(empty($filters['country_id'])){
                                                    $filters['country_id'] = $c->id;
                                                }
                                            @endphp
                                        @endforeach
                                    @endif
                                    @foreach(\App\Models\Country::where('priority','<=',0)->orderBy('name')->get() as $c)
                                        <option value="{{$c->id}}" {{!empty($filters['country_id']) && $filters['country_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                        @php
                                            if(empty($filters['country_id'])){
                                                $filters['country_id'] = $c->id;
                                            }
                                        @endphp
                                    @endforeach
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.index.state')}}</div>
                                <select class="change_state" id="search_state" data-city_id="search_city"
                                        name="state_id">
                                    <option value="">---</option>
                                    @if(!empty($filters['country_id']))
                                        @foreach(\App\Services\LocationService::getStatesByCountryId($filters['country_id']) as $s)
                                            <option value="{{$s->id}}" {{!empty($filters['state_id']) && $filters['state_id'] == $s->id?'selected':''}}>{{$s->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.index.city')}}</div>
                                <select id="search_city" name="city_id">
                                    <option value="">---</option>
                                    @if(!empty($filters['state_id']))
                                        @foreach(\App\Services\LocationService::getCitiesByStateId($filters['state_id']) as $c)
                                            <option value="{{$c->id}}" {{!empty($filters['city_id']) && $filters['city_id'] == $c->id?'selected':''}}>{{$c->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.index.address')}}</div>
                                <input type="text" name="address"
                                       value="{{!empty($filters['address'])?$filters['address']:''}}" />
                            </div>
                            <div class="input">
                                <div class="label">{{__('organisation.index.zip')}}</div>
                                <input type="number" name="zip"
                                       value="{{!empty($filters['zip'])?$filters['zip']:''}}" />
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        <button class="blue" type="submit">{{__('organisation.index.search')}}</button>
                        <button class="search_reset blue" type="reset">{{__('organisation.index.reset')}}</button>
                        <a class="white"
                           href="{{route('front.organisation.search')}}">{{__('organisation.index.advanced_search')}}</a>
                    </div>
                    @if($events->count())
                        <div class="list" style="width: 100%;">
                            <div class="title" style="margin-top: 10px;">{{__('event.index.events')}}:</div>
                            <div class="items">
                                @foreach($events as $item)
                                    @php
                                        $hide_coords = true;
                                    @endphp
                                    @include('front.calendarEvent.partials.listItem')
                                @endforeach
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        @endif
    </div>
@endsection
