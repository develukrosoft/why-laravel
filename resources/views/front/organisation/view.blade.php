@extends('front.partials.layout')

@section('content')
    @push('footer_scripts')
        @include('front.partials.mapScripts')
    @endpush
    <input type="hidden" class="location_cords" data-lat="{{$item->address->lat}}"
           data-address_id="{{$item->address->id}}"
           data-lng="{{$item->address->lng}}" data-title="{{$item->title}}" data-id="{{$item->id}}" data-center="true">
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        <div class="organisation">
            <div class="left">
                <div class="title">{{$item->title}}
                    @if($item->tagIcon && $item->tagIcon->icon_url)
                        <img src="{{$item->tagIcon->icon_url}}"
                             style="max-width: 30px;height: 30px;object-fit: cover;" />
                    @endif
                    @foreach($item->types as $type)
                        {!! $type->fontawesome_icon !!}
                    @endforeach
                    @foreach($item->categories as $category)
                        {!! $category->fontawesome_icon !!}
                    @endforeach
                </div>
                <div id="map"></div>
                <div class="information">
                    @if($item->program_type_description)
                        <div class="field">
                            <div class="title">{{__('organisation.view.program_type_description')}}:</div>
                            <div>{{$item->program_type_description}}</div>
                        </div>
                    @endif
                    @if($item->mission)
                        <div class="field">
                            <div class="title">{{__('organisation.view.mission')}}:</div>
                            <div>{{$item->mission}}</div>
                        </div>
                    @endif
                    @if($item->notes)
                        <div class="field">
                            <div class="title">{{__('organisation.view.notes')}}:</div>
                            <div>{!! $item->notes !!}</div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="right">
                <div class="title">{{__('organisation.view.organisation_details')}}</div>
                <div class="details">
                    @if($item->address)
                        <div class="field"><b>{{__('organisation.view.address')}}: </b>{{$item->address->full_address}}
                        </div>
                    @endif
                    @if($item->website)
                        <div class="field"><b>{{__('organisation.form.website')}}: </b>{{$item->website}}</div>
                    @endif
                    @if(!empty($item->hours['text']))
                        <div class="field"><b>{{__('organisation.view.hours')}}: </b>{{$item->hours['text']}}</div>
                    @endif
                    @if(count($item->phones))
                        <div class="field"><b>{{__('organisation.view.phones')}}: </b>
                            <ul>
                                @foreach($item->phones as $phone)
                                    <li>
                                        {{substr($phone->phone,0,3)}}-{{substr($phone->phone,3,3)}}
                                        -{{substr($phone->phone,6)}} {{$phone->ext?'ext.'.$phone->ext:''}}{{count($item->phones) - 1===$loop->index?'':', '}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--<div class="field"><b>Organization Contact: </b>--}}
                    {{--<ul>--}}
                    {{--<li>--}}
                    {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" role="img"--}}
                    {{--style="width: 10px;"--}}
                    {{--xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"--}}
                    {{--class="svg-inline--fa fa-user fa-w-14 fa-2x">--}}
                    {{--<path fill="currentColor"--}}
                    {{--d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"--}}
                    {{--class=""></path>--}}
                    {{--</svg>--}}
                    {{--Don Kao--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    @if(count($item->types))
                        <div class="field"><b>{{__('organisation.view.type')}}: </b> @foreach($item->types as $type)
                                {{$type->name}}{{count($item->types) - 1===$loop->index?'':', '}}
                            @endforeach
                        </div>
                    @endif
                    @if(count($item->CategoryAndOptions))
                        @foreach($item->CategoryAndOptions as $category=>$options)
                            <div class="field"><b>{{$category}}: </b>
                                <ul>
                                    @foreach($options as $option)
                                        <li>{{$option}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    @endif
                </div>
                @if($events->count())
                    <div class="organisations">
                        <div class="list" style="width: 100%">
                            <div class="title" style="margin-top: 10px;">{{__('event.index.events')}}:</div>
                            <div class="items">
                                @foreach($events as $item)
                                    @include('front.calendarEvent.partials.listItem')
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
