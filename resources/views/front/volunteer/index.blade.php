@extends('front.partials.layout')

@section('content')
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        @if(!\App\Services\SettingService::getValueBySlug('volunteer_is_active',true))
            <div class="inactive_page">
                {!! \App\Services\SettingService::getValueBySlug('volunteer_inactive_html','<div>Inactive</div>') !!}
            </div>
        @else
            <div class="volunteer_index">
                <div class="left-panel">
                    <div class="logo">
                        <img src="https://whyhunger.org/wp-content/uploads/2017/04/WhyHunger_WebsiteLogo-01-e1492807552348-1-e1493224274299.png">
                    </div>
                    <div class="text">
                        <b>{{__('volunteer.index.instruction_1')}}</b>
                        <br />
                        {{__('volunteer.index.instruction_2')}}
                    </div>
                    <div class="assistance">
                        <b>{{__('volunteer.index.need_assistance')}}</b>
                        <br />
                        {{__('volunteer.index.visit')}} <a
                                href="https://WhyHunger.org/FindFood">WhyHunger.org/FindFood</a>
                    </div>
                </div>
                <div class="right-panel">
                    <div class="title">{{__('volunteer.index.calling_volunteers')}}</div>
                    <div class="description">
                        {{__('volunteer.index.description')}}
                    </div>
                    <div class="admonition">
                        {{__('volunteer.index.admonition')}}
                    </div>
                    <form action="{{route('front.volunteer.search')}}">
                        <input name="zip" type="text" placeholder="{{__('volunteer.index.enter_zip')}}" />
                        @if(!Cookie::has('uhf_id'))
                            <div class="field">
                                <div class="title">{{__('organisation.form.how_did_you_find_us')}}:</div>
                                <div class="body">
                                    <div class="input">
                                        <select name="how_find" required id="how_find">
                                            @foreach(\App\Models\UserHowFindValue::all() as $v)
                                                <option value="{{$v->id}}">{{$v->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input" id="how_find_other" style="display: none">
                                        <input type="text" placeholder="Other value" name="how_find_other">
                                    </div>
                                </div>
                            </div>
                        @endif
                        <input type="submit" value="{{__('volunteer.index.started')}}" />
                    </form>
                </div>
            </div>
        @endif
    </div>
    @php
        $value = \App\Models\UserHowFindValue::where('name','Other')->first();
    @endphp
    <input type="hidden" value="{{$value?$value->id:0}}" id="how_find_other_id" />
@endsection
