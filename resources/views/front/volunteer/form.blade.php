@extends('front.partials.layout')

@section('content')
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        <div class="volunteer_form">
            <a class="back"
               href="#">
                <div class="icon">
                    <img src="/img/volunteer/leftarrow_red.svg" />
                </div>
                <div class="text">Back to your list</div>
            </a>
            <div class="instruction">
                <div class="image">
                    <img src="/img/volunteer/instructions.svg" />
                </div>
                <div class="text">
                    <h2>PREPARING FOR THE CALL</h2>
                    <div class="list">
                        <ul>
                            <li>Review all the questions before calling.
                            </li>
                            <li>Answer all the questions as best you can. Then click “Submit”.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <form method="post" id="volunteer_form">

                <div class="contact">
                    <div class="image">
                        <img src="/img/volunteer/telephone.svg" />
                    </div>
                    <div class="text">
                        <h4>When you're ready...</h4>
                        @if(count($item->phones))
                            @if(count($item->phones)>1)
                                <h2>{{ $item->title }}:</h2>
                                @foreach($item->phones as $phone)
                                    <h2>{{ $item->phones[0]->phone }} {{ $item->phones[0]->ext?'ext.'.$item->phones[0]->ext:''  }}</h2>
                                @endforeach
                            @else
                                <h2>{{ $item->title }}
                                    : {{ $item->phones[0]->phone }} {{ $item->phones[0]->ext?'ext.'.$item->phones[0]->ext:''  }}</h2>
                            @endif
                        @else
                            <div class="question-section">
                                <div class="answer" style="border: none; padding: 0">
                                    <div class="option">
                                        <div class="_label">Please perform a web search to find the appropriate number
                                            and
                                            call
                                            it
                                            to confirm:
                                        </div>
                                        <textarea name="answers[number_search][number]" rows="1"
                                                  placeholder="+00000000"></textarea>
                                    </div>
                                    <div class="option">
                                        <div class="_label">Please leave any additional information about this number
                                            here:
                                        </div>
                                        <textarea name="answers[number_search][description]" rows="1"></textarea>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="complete">Unable to complete this task or gather information? <a href="#" class="show_modal"
                                                                                                     data-modal_id="modalFormUncomplete">Click
                                here to tell us
                                what
                                happened</a>
                        </div>
                    </div>
                </div>
                <div class="call-text">
                    <div class="image">
                        <img src="/img/volunteer/speech-red.svg" />
                    </div>
                    <div class="text">
                        I'm volunteering to verify sites for WhyHunger's Find Food database, a resources that helps
                        people
                        locate
                        food sites.
                        <br />
                        <br />
                        "We have your food site listed on our database and I just want to make sure our information is
                        up to
                        date.
                        Do you have a couple of minutes?
                    </div>
                </div>
                <input type="hidden" value="{{ $item->id }}" name="organisation_id" />
                <div class="step">
                    <div class="number"><span>1</span></div>
                    <div class="title">The Basics</div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Can you confirm that organisation name "{{ $item->title }}"?</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="organisation_name"
                                   name="answers[organisation_name][correct]"
                                   id="correct_organisation_name" />
                            <label for="correct_organisation_name">Yes, that's the correct name</label>
                        </div>
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="organisation_name"
                                   id="not_correct_organisation_name"
                                   name="answers[organisation_name][not_correct]" />
                            <label for="not_correct_organisation_name">No</label>
                        </div>
                        <div class="option">
                            <div class="_label">If no, please provide the organisation name:
                            </div>
                            <textarea name="answers[organisation_name][new_organisation_name]" rows="1"></textarea>
                        </div>
                    </div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Can you confirm that you're located at {{$item->address->FullAddress}}?</span>
                    </div>
                    <div class="answer">
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="address"
                                   name="answers[address][correct]"
                                   id="correct_address" />
                            <label for="correct_address">Yes, that's the correct address</label>
                        </div>
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="address" id="not_correct_address"
                                   name="answers[address][not_correct]" />
                            <label for="not_correct_address">No</label>
                        </div>
                        <div class="option">
                            <div class="_label">If no, please provide the street address (make sure that the spelling of
                                address
                                is correct):
                            </div>
                            <textarea name="answers[address][new_address]" rows="1"></textarea>
                        </div>
                    </div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>Can you confirm that this location provides food assistance to the public? If so, please tell me which programs you have:</span>
                    </div>
                    <div class="answer _col-2">
                        @foreach(\App\Models\ProgramType::where('is_active',1)->get() as $type)
                            <div class="option">
                                <input type="checkbox" name="answers[program_types][]" value="{{$type->id}}"
                                       id="{{$type->id}}" />
                                <label for="{{$type->id}}">{{$type->name}}</label>
                                <div class="type_description_help">?</div>
                                <div class="type_description">{{$type->description}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @if($item->website)
                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>Can you confirm that your website is {{ $item->website }}?</span>
                        </div>
                        <div class="answer">
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="website" id="correct_website"
                                       name="answers[website][correct]" />
                                <label for="correct_website">Yes, that's the correct website</label>
                            </div>
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="website" id="not_correct_website"
                                       name="answers[website][not_correct]" />
                                <label for="not_correct_website">No</label>
                            </div>
                            <div class="option">
                                <div class="_label">If no (or website isn't listed), please provide the website:</div>
                                <textarea name="answers[website][new_website]" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>We don't have a website listed for you. Do you have one?</span>
                        </div>
                        <div class="answer">
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="website"
                                       name="answers[website][not_have_website]" id="not_have_website" />
                                <label for="not_have_website">No</label>
                            </div>
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="website"
                                       name="answers[website][have_website]" id="have_website" />
                                <label for="have_website">Yes</label>
                            </div>
                            <div class="option">
                                <div class="_label">If yes, please provide the website:</div>
                                <textarea name="answers[website][new_website]" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="step">
                    <div class="number"><span>2</span></div>
                    <div class="title">Pickup Hours</div>
                </div>
                <div class="question-section">
                    <div class="question">
                        <img src="/img/volunteer/speech-red.svg" />
                        <span>What are the hours that you are open to the public?</span>
                    </div>
                    <div class="answer">
                        @php
                            $days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
                        @endphp
                        @foreach($days as $index=>$day)
                            <div>
                                <div class="day_title">{{ $day }}</div>
                                <div class="day_list" id="day_hours_{{ $loop->index }}" data-day_name="{{ $day }}">
                                    <div class="add_hours" data-index="{{ $loop->index }}" data-day_name="{{ $day }}">
                                        <div class="image"><img src="/img/volunteer/plus-red.svg" /></div>
                                        <div class="text">Add hours</div>
                                    </div>
                                </div>
                                @if(count($days) !== $loop->index +1)
                                    <hr>
                                @endif
                            </div>
                        @endforeach
                        <div class="option">
                            <input type="checkbox" class="check_one" data-group="by_appointment_only"
                                   name="answers[by_appointment_only]" id="by_appointment_only" />
                            <label for="by_appointment_only">By Appointment Only</label>
                        </div>
                    </div>

                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>So just to confirm, that's:</span>
                        </div>
                        <div class="answer">
                            <div id="day_time_list">
                            </div>
                        </div>
                    </div>
                    <div class="step">
                        <div class="number"><span>3</span></div>
                        <div class="title">Organization details</div>
                    </div>
                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>Do you provide any additional programs like SNAP sign-up support, support for new parents, WIC, fresh produce distribution, advocacy programs, or nutrition counseling, etc?</span>
                        </div>
                        <div class="answer">
                            <div class="option">
                                <textarea name="answers[additional_details]" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="step">
                        <div class="number"><span>4</span></div>
                        <div class="title">Contact Info</div>
                    </div>
                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>THANK YOU so much for your time and for answering these questions. And thank you for all the important work you are doing to help your communities, especially during this difficult time. We are so grateful for you, and others like you, working to ensure no family goes hungry. Is it possible to get the contact information for the best person to reach in the future, in case we have more questions?</span>
                        </div>
                        <div class="answer _col-2">
                            <div class="w49p">
                                <div class="_label">Phone(s)</div>
                                <div id="phone_list" class="w100p">
                                    @if(count($item->phones))
                                        @foreach($item->phones as $phone)
                                            <textarea name="answers[contact][phone][c_{{ $loop->index }}][phone]"
                                                      rows="1"
                                                      placeholder="Phone Number">{{ $phone->phone }}</textarea>
                                            <textarea name="answers[contact][phone][c_{{ $loop->index }}][ext]" rows="1"
                                                      placeholder="Ext">@if($phone->ext){{ $phone->ext }}@endif</textarea>
                                        @endforeach
                                    @else
                                        <textarea name="answers[contact][phone][c_1][phone]" rows="1"
                                                  placeholder="Phone Number"></textarea>
                                        <textarea name="answers[contact][phone][c_1][ext]" rows="1"
                                                  placeholder="Ext"></textarea>
                                    @endif
                                </div>
                                <div id="add_phone_form" class="add_item">Add phone</div>
                            </div>
                            <div class="w49p">
                                <div class="_label">Email(s)</div>
                                <div id="email_list" class="w100p">
                                    @if(count($item->emails))
                                        @foreach($item->emails as $email)
                                            <textarea name="answers[contact][email][]" rows="1"
                                                      placeholder="Email">{{ $email->email }}</textarea>
                                        @endforeach
                                    @else
                                        <textarea name="answers[contact][email][]" rows="1"
                                                  placeholder="Email"></textarea>
                                    @endif
                                </div>
                                <div id="add_email_form" class="add_item">Add email</div>
                            </div>
                        </div>
                    </div>
                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>Finally, what is the best way to re-verify this information with you in the future? Here are a few options:</span>
                        </div>
                        <div class="answer">
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="contact_again"
                                       value="Would you like us to call you again?"
                                       name="answers[contact_again][1]" id="contact_again_1" />
                                <label for="contact_again_1">Would you like us to call you again?</label>
                            </div>
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="contact_again"
                                       value="Would you like to log in to a system to update your own information as it changes?"
                                       name="answers[contact_again][2]" id="contact_again_2" />
                                <label for="contact_again_2">Would you like to log in to a system to update your own
                                    information
                                    as
                                    it changes?</label>
                            </div>
                            <div class="option">
                                <input type="checkbox" class="check_one" data-group="contact_again"
                                       value="Would you like to confirm via text message on a regular basis?"
                                       name="answers[contact_again][3]" id="contact_again_3" />
                                <label for="contact_again_3">Would you like to confirm via text message on a regular
                                    basis?</label>
                            </div>
                        </div>
                    </div>
                    <div class="question-section">
                        <div class="question">
                            <img src="/img/volunteer/speech-red.svg" />
                            <span>General pantry notes.</span>
                        </div>
                        <div class="answer">
                            <div class="option">
                                <div class="_label">Enter any other relevant notes that did not fit into the sections
                                    above.
                                </div>
                                <textarea name="answers[contact_notes][note]" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="send_request">
                        <div>
                            <input type="button" class="show_modal" data-modal_id="modalFormSuccess" value="Submit" />
                        </div>
                        <button class="unable show_modal" data-modal_id="modalFormUncomplete" type="button">
                            Unable to complete this task?
                        </button>
                    </div>
                    <input type="hidden" id="confident" name="answers[confident]" value="" />
                </div>
            </form>
            <div id="modalFormSuccess" class="volunteer_modal _modal">
                <div class="modal-dialog body">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close_modal" data-modal_id="modalFormSuccess">&times;</button>
                            <div class="title">Nice work! It looks like you were able to collect some
                                information!</div>
                        </div>
                        <div class="modal-body">
                            <div class="title">How confident are you in the answers you've provided today?</div>
                            <div class="options" id="confident_list">
                                <div class="option">
                                    <input value="Very confident" type="checkbox" class="check_one"
                                           data-group="confident"
                                           id="confident_1" />
                                    <label for="confident_1">Very confident</label>
                                </div>
                                <div class="option">
                                    <input value="Somewhat confident" type="checkbox" class="check_one" id="confident_2"
                                           data-group="confident" />
                                    <label for="confident_2">Somewhat confident</label>
                                </div>
                                <div class="option">
                                    <input value="Not very confident" type="checkbox" class="check_one" id="confident_3"
                                           data-group="confident" />
                                    <label for="confident_3">Not very confident</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="submit_form_confident" type="button" class="btn" data-dismiss="modal">Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modalFormUncomplete" class="volunteer_modal _modal">
                <div class="modal-dialog body">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close_modal" data-modal_id="modalFormUncomplete">&times;</button>
                            <div class="title">Unable to Complete</div>
                        </div>
                        <div class="modal-body">
                            <div class="title">Please choose which best describes your situation:</div>
                            <div class="options" id="unable_list">
                                <div class="option">
                                    <input value="Nobody picked up, and there was no pre-recorded message"
                                           type="checkbox"
                                           id="unable_complete_1" />
                                    <label for="unable_complete_1">Nobody picked up, and there was no pre-recorded
                                        message</label>
                                </div>
                                <div class="option wrong_toggle">
                                    <input value="Wrong number (number is for a different institution, or number is disconnected)"
                                           type="checkbox" id="unable_complete_2"
                                    />
                                    <label for="unable_complete_2">Wrong number (number is for a different institution,
                                        or
                                        number is disconnected)</label>
                                </div>
                                <div class="option wrong_option" data-parent_id="unable_complete_2"
                                     style="display: none">
                                    <input value="No number was found"
                                           type="checkbox" id="unable_complete_2_not_found"
                                    />
                                    <label for="unable_complete_2_not_found">No number was found</label>
                                </div>
                                <div class="option wrong_option" data-parent_id="unable_complete_2"
                                     style="display: none">
                                    <label for="unable_complete_2_number">Please perform a web search to find the
                                        appropriate
                                        number and call it to confirm.</label>
                                    <input placeholder="+0000000000"
                                           type="text" id="unable_complete_2_number"
                                    />
                                </div>
                                <div class="option wrong_toggle">
                                    <input value="Wrong number (number has changed)" type="checkbox"
                                           id="unable_complete_3"
                                    />
                                    <label for="unable_complete_3">Wrong number (number has changed)</label>
                                </div>
                                <div class="option wrong_option" data-parent_id="unable_complete_3"
                                     style="display: none">
                                    <input value="No number was found"
                                           type="checkbox" id="unable_complete_3_not_found"
                                    />
                                    <label for="unable_complete_3_not_found">No number was found</label>
                                </div>
                                <div class="option wrong_option" data-parent_id="unable_complete_3"
                                     style="display: none">
                                    <label for="unable_complete_3_number">Please perform a web search to find the
                                        appropriate
                                        number and call it to confirm.</label>
                                    <input placeholder="+0000000000"
                                           type="text" id="unable_complete_3_number"
                                    />
                                </div>
                                <div class="option">
                                    <input value="Staff member didn’t speak English" type="checkbox"
                                           id="unable_complete_4"
                                    />
                                    <label for="unable_complete_4">Staff member didn’t speak English</label>
                                </div>
                                <div class="option">
                                    <input value="This location is closed indefinitely due to COVID-19" type="checkbox"
                                           id="unable_complete_5"
                                    />
                                    <label for="unable_complete_5">This location is closed indefinitely due to
                                        COVID-19</label>
                                </div>
                                <div class="option">
                                    <input value="Location does not offer free food assistance to the general public"
                                           type="checkbox" id="unable_complete_6"
                                    />
                                    <label for="unable_complete_6">Location does not offer free food assistance to the
                                        general
                                        public</label>
                                </div>
                                <div class="option">
                                    <input value="Staff weren't able to answer questions at this time"
                                           type="checkbox" id="unable_complete_7"
                                    />
                                    <label for="unable_complete_7">Staff weren't able to answer questions at this
                                        time</label>
                                </div>
                                <div class="option">
                                    <input value="Other"
                                           type="checkbox" id="unable_complete_8"
                                    />
                                    <label for="unable_complete_8">Other</label>
                                </div>
                                <div class="option">
                                    <div class="_label">If other, please provide:</div>
                                    <textarea id="unable_complete_other" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="submit_form_unable" type="button" class="btn" data-dismiss="modal">Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
