@extends('front.partials.layout')

@section('content')
    <div class="_container">
        @include('front.partials.subheader')
        @include('front.partials.tabs')
        <div class="volunteer_search">

            <div class="list">
                <div class="title">{{__('volunteer.search.call')}}</div>
                <div class="sub_title">{{__('volunteer.search.call_description')}}</div>
                @foreach($items as $item)
                    <div class="item">
                        <div class="left-panel">
                            <div class="title">{{ $item->title }}</div>
                            <div class="sub_title">
                                {{ $item->address->FullAddress }}
                            </div>
                        </div>
                        <div class="right-panel">
                            <a href="{{route('front.volunteer.form',['item'=>$item->id])}}">Start <img
                                        src="/img/arrow-right-white.svg" /></a>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="footer_actions">
                <button id="feedback_form_toggle">{{__('volunteer.search.feedback_form')}}</button>
                <a href="{{route('front.organisation.add')}}">{{__('volunteer.search.add_a_site')}}</a>
            </div>
            <form class="feedback_form" action="{{route('front.volunteer.feedback')}}" method="POST"
                  style="{{Cookie::get('volunteer_id') == false?'':'display:none;'}}"
            >
                <div class="title">{{__('volunteer.search.thank_for_time')}}</div>
                <div class="input_label">{{__('volunteer.search.name')}}</div>
                <div style="display: flex; justify-content: space-between">
                    <input type="text" name="first_name" placeholder="First name" style="width: 49%" required />
                    <input type="text" name="last_name" placeholder="Last name" style="width: 49%" required />
                </div>
                <div class="input_label">{{__('volunteer.search.email')}}</div>
                <div><input type="email" name="email" placeholder="Your email" required/></div>
                <div class="input_label">{{__('volunteer.search.feedback')}}</div>
                <div class="footer">
                    <textarea name="feedback"></textarea>
                    <div>
                        <button type="submit">{{__('volunteer.search.submit')}}</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection
