$(document).ready(function() {
    $(document).on('mouseenter', '.show_sub_menu', function() {
        $(this).find('.sub_menu').fadeIn(300)
    });
    $(document).on('mouseleave', '.show_sub_menu', function() {
        $(this).find('.sub_menu').fadeOut(300)
    });

    $('.show_adaptive_menu').click(function() {
        $('.adaptive_menu').slideToggle(200);
    });
});
