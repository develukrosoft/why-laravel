$(document).ready(function() {
    $('.add_hours').click(function() {
        if ($(this).hasClass('disabled')) {
            return;
        }
        let index = $(this).data('index');
        let day_name = $(this).data('day_name');
        let count = $(this).parent().find('.item').length + 1;
        let html = '<div class="item time_item">' +
            '                            <div class="form change_time">' +
            '                                <div class="from_time">' +
            '                                    <div style="position: relative;">' +
            '                                        <input type="number" class="change_time time_hour" value="08" min="0" max="12" name="answers[time][' + day_name + '][n_' + count + '][from_time_hour]"/>' +
            '                                        <div class="separator">:</div>' +
            '                                    </div>' +
            '                                    <div><input type="number" class="change_time time_min" value="00" min="00" max="59" name="answers[time][' + day_name + '][n_' + count + '][from_time_min]"/></div>' +
            '                                    <div>' +
            '                                        <select class="change_time time_type" name="answers[time][' + day_name + '][n_' + count + '][from_time_type]">' +
            '                                            <option>AM</option>' +
            '                                            <option>PM</option>' +
            '                                        </select>' +
            '                                    </div>' +
            '                                </div>' +
            '                                <div class="divider"></div>' +
            '                                <div class="to_time">' +
            '                                    <div style="position: relative;">' +
            '                                        <input type="number" class="change_time time_hour" value="08" min="0" max="12" name="answers[time][' + day_name + '][n_' + count + '][to_time_hour]"/>' +
            '                                        <div class="separator">:</div>' +
            '                                    </div>' +
            '                                    <div><input type="number" class="change_time time_min" value="00" min="00" max="59" name="answers[time][' + day_name + '][n_' + count + '][to_time_min]"/></div>' +
            '                                    <div>' +
            '                                        <select class="change_time time_type" name="answers[time][' + day_name + '][n_' + count + '][to_time_type]">' +
            '                                            <option>AM</option>' +
            '                                            <option selected>PM</option>' +
            '                                        </select>' +
            '                                    </div>' +
            '                                </div>' +
            '                                <div style="margin-left: 10px;">' +
            '                                   <div>' +
            '                                       <select class="change_time time_day" ' +
            '                                       name="answers[time][' + day_name + '][n_' + count + '][day]">' +
            '                                           <option>Every ' + day_name + '</option>' +
            '                                           <option>Every first ' + day_name + '</option>' +
            '                                           <option>Every second ' + day_name + '</option>' +
            '                                           <option>Every third ' + day_name + '</option>' +
            '                                           <option>Every fourth ' + day_name + '</option>' +
            '                                           <option>Every 1st and 2nd ' + day_name + '</option>' +
            '                                           <option>Every 3rd and 4th ' + day_name + '</option>' +
            '                                       </select>' +
            '                                   </div>' +
            '                                </div>'
        '                            </div>' +
        '                        </div>';
        let day_hours = $('#day_hours_' + index);
        day_hours.append(html);
        if (day_hours.find('.item').length >= 3) {
            $(this).hide();
        }

        changeTime();
    });
    changeTime();
    $(document).on('change', '.change_time', function() {
        changeTime();
    }).on('click', '.change_time', function() {
        changeTime();
    }).on('keyup', '.change_time', function() {
        changeTime();
    });

    function changeTime() {
        let html = '';
        let elems = $('.day_list');
        elems.each(function(index, elem) {
            let day_name = '<span class="day_name">' + $(elem).data('day_name') + ':</span>';
            let items = $(elem).find('.item');
            let times = '';
            items.each(function(index, item) {
                if (times) {
                    times += ' , ';
                }
                let from_time = $(item).find('.from_time');
                let to_time = $(item).find('.to_time');
                let time_day = $(item).find('.time_day');
                let from = from_time.find('.time_hour').val() + ':' + from_time.find('.time_min').val() + ' ' + from_time.find('.time_type').val();
                let to = to_time.find('.time_hour').val() + ':' + to_time.find('.time_min').val() + ' ' + to_time.find('.time_type').val();
                times += time_day.val() + ' ' + from + ' - ' + to;
            });
            if ($('#by_appointment_only').prop('checked')) {
                times = '<span class="closed">Appointment Only</span>';
            }
            else if (!times) {
                times = '<span class="closed">Closed</span>';
            } else {
                times = '<span class="day_times">' + times + '</span>';
            }
            html += '<div>' + day_name + times + '</div>';
        });
        $('#day_time_list').html(html);
    }

    $('#add_phone_form').click(function() {
        let phone_list = $('#phone_list');
        let c = phone_list.find('textarea').length;
        let html = '<textarea name="answers[contact][phone][n_' + (c + 1) + '][phone]" rows="1" placeholder="Phone Number"></textarea>' +
            '<textarea name="answers[contact][phone][n_' + (c + 1) + '][ext]" rows="1" placeholder="Ext."></textarea>';
        phone_list.append(html);
        if (phone_list.find('textarea').length >= 10) {
            $(this).hide();
        }
    });

    $('#add_email_form').click(function() {
        let html = '<textarea name="answers[contact][email][]" rows="1" placeholder="Email"></textarea>';
        let email_list = $('#email_list');
        email_list.append(html);
        if (email_list.find('textarea').length >= 5) {
            $(this).hide();
        }
    });

    $('#by_appointment_only').click(function() {
        byAppointmentOnly();
    }).change(function() {
        byAppointmentOnly();
    });

    function byAppointmentOnly() {
        if ($('#by_appointment_only').prop('checked')) {
            $('.add_hours').addClass('disabled');
            $('.time_item').remove();
        } else {
            $('.add_hours').removeClass('disabled');
        }
        changeTime();
    }

    $('.check_one').click(function() {
        let group = $(this).data('group');
        let elems = $('.check_one[data-group="' + group + '"]');
        elems.each((index, item) => {
            if (item !== this) {
                $(item).prop('checked', false);
            }
        });
    });

    $('.type_description_help').mouseenter(function() {
        $(this).parent().find('.type_description').fadeIn(200);
    }).mouseleave(function() {
        $('.type_description').fadeOut(200);
    });

    $('.wrong_toggle').click(function() {
        let input = $(this).find('input');
        let id = input.prop('id');
        let additional_block = $('div[data-parent_id="' + id + '"]');

        $(this).toggleClass('wrong_active');
        if (input.prop('checked')) {
            additional_block.show(100);
        } else {
            additional_block.hide(100);
        }
    });

    $('#submit_form_confident').click(function() {
        let elems = $('#confident_list').find('input');
        elems.each(function(index, item) {
            if ($(item).prop('checked')) {
                $('#confident').val($(item).val());
            }
        });
        $('#volunteer_form').submit();
    });

    $('#submit_form_unable').click(function(e) {
        e.preventDefault();
        let elems = $('#unable_list').find('input');
        $('.unable_complete').remove();
        elems.each(function(index, item) {
            if ($(item).prop('checked')) {
                let id = $(item).prop('id');
                $('#volunteer_form').append('<input type="hidden" class="unable_complete" name="answers[unable_complete][' + id + ']" value="' + $(item).val() + '" />')
                if (id === 'unable_complete_2_not_found') {
                    $('#volunteer_form').append('<input type="hidden" class="unable_complete" name="answers[unable_complete][unable_complete_2_not_found]" value="on" />')
                    $('#volunteer_form').append('<input type="hidden" class="unable_complete" name="answers[unable_complete][unable_complete_2_number]" value="' + $('#unable_complete_2_number').val() + '" />')
                }
                if (id === 'unable_complete_3_not_found') {
                    $('#volunteer_form').append('<input type="hidden" class="unable_complete" name="answers[unable_complete][unable_complete_3_not_found]" value="on" />')
                    $('#volunteer_form').append('<input type="hidden" class="unable_complete" name="answers[unable_complete][unable_complete_3_number]" value="' + $('#unable_complete_3_number').val() + '" />')
                }
            }
        });
        let other = $('#unable_complete_other').val();
        if (other) {
            $('#volunteer_form').append('<input type="hidden" class="unable_complete" name="answers[unable_complete][other]" value="' + other + '" />')
        }
        if (($('#unable_complete_2_not_found').prop('checked') || $('#unable_complete_3_not_found').prop('checked')) || (!$('#unable_complete_2').prop('checked') && !$('#unable_complete_3').prop('checked'))) {
            $('#volunteer_form').submit();
        } else {
            $('#modalFormUncomplete').hide();
        }
    });

    $('#feedback_form_toggle').click(function() {
        $('.feedback_form').slideToggle(500);
    });

});
