$(document).ready(function() {
    $('.additional_button').click(function() {
        $('.additional_options').slideToggle(300);
    });
    $('.quick_details').click(function() {
        $(this).toggleClass('active');
        $(this).find('.details').slideToggle(300);
    });

    $('.show_options').click(function() {
        $(this).toggleClass('active');
        $(this).parent().find('.body').slideToggle(300);
    });

    $('.search_reset').click(function() {
        $('#search_city').html('<option value="">---</option>');
        $('#search_state').html('<option value="">---</option>');
    });
    $('.search_order').click(function() {
        $('#search_sort_field').val($(this).data('sort'));

        const search_sort_order = $('#search_sort_order');
        search_sort_order.val(search_sort_order.val() === 'asc' ? 'desc' : 'asc');
        $('#search_form').submit();
    });

    $('#org_add_form').submit(function() {
        let recaptcha = document.forms["org_add_form"]["g-recaptcha-response"].value;
        if (recaptcha === "") {
            showModal('', 'Please fill CAPTCHA');
            return false;
        }
    });

    $('#confirmation_email_id').change(function() {
        if ($(this).val() !== $('#confirmation_email_org').val()) {
            $('#contact_details').slideDown(300);
            $('#contact_details input').prop('required', true);
        } else {
            $('#contact_details').slideUp(300);
            $('#contact_details input').prop('required', false);
        }
    });

    $('#how_find').change(function() {
        if ($(this).val() === $('#how_find_other_id').val()) {
            $('#how_find_other').slideDown(300);
            $('#how_find_other input').prop('required', true);
        } else {
            $('#how_find_other').slideUp(300);
            $('#how_find_other input').prop('required', false);
        }
    });
});
