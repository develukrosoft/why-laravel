$(document).ready(function() {
    $('#import_project').click(function(e) {
        e.preventDefault();

        $.post('', {
            year: $('#year').val(),
            last_id: $('#last_id').val(),
            reset_counter: $('#reset_counter').prop('checked') ? 1 : 0,
            continue_import: $('#continue_import').prop('checked') ? 1 : 0,
        }, (data) => {
            if ($('#reset_counter').prop('checked')) {
                $('#last_id').val(0);
                $('#reset_counter').prop('checked', false)
            }
            if (data.logs && data.logs.length) {
                $('#logs').append(data.logs);
                $('#last_id').val(data.last_id);
                if ($('#continue_import').prop('checked')) {
                    $('#import_project').click();
                }
            }
        });
    });
});
