$(document).ready(function() {
    if ($('#route_current').val() === 'backend.import.import') {
        importRow();
    }

    function importRow() {
        $.post('', {}, (data) => {
            if (!data.success) {
                $('#import_current_row').html(data.item.current_row - 1);
                let percent = (100 - (((data.item.total_rows - 1) - (data.item.current_row - 1)) * 100) / (data.item.total_rows - 1)).toFixed(0);
                $('#import_process_percent').html(percent);
                $('.line').css('width', percent + '%');
                setTimeout(function() {
                    importRow();
                }, 100);
            } else {
                $('#import_process_percent').html(100);
                $('.line').css('width', 100 + '%');
            }
            $('.list').prepend(data.text);
        });
    }
});
