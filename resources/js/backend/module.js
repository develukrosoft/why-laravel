$(document).ready(function () {
  setTimeout(function () {
    $('._alert_message').hide(100)
  }, 5000)

  $('.show_dropdown').click(function () {
    $(this).toggleClass('active')
    $(this).parent().find('.dropdown').toggle()
  })

  $('.mass_mail').click(function () {
    $(this).toggleClass('active')
    $('.m_mails').slideToggle()
  })

  $('.toggle_filters').click(function () {
    $(this).toggleClass('active')
    $('.filters').slideToggle(300)
  })

  $('.batch_action').click(function () {
    let action = $(this).data('action')
    let ids = []
    if (action !== 'delete_all') {
      $.each($('.select_one:checked'), function (index, item) {
        ids.push($(item).val())
      })
      if (!ids.length) {
        alert('Select orgs')
        return false
      }
    }

    let result = confirm('You are sure?')
    if (result) {
      let message = ''
      if (action === 'mass_mail') {
        message = CKEDITOR.instances.mass_mail.getData()
        if (message.length <= 10) {
          alert('Mass Mail Template is required')
          return false
        }
      }
      $.post('', {
        batch: action,
        ids: ids,
        mail_template: message,
      }, (data) => {
        if (data.status === 200) {
          location.reload()
        } else {
          alert('Error')
        }
      })
    }
  })

  $('.select_all').click(function () {
    $('.select_one').attr('checked', true)
  })

  $('.select_tab').click(function () {
    $('.tab.active').removeClass('active')
    $('.select_tab.active').removeClass('active')
    $(this).addClass('active')
    $('#' + $(this).data('tab')).addClass('active')
  })

  $('.change_per_page').change(function () {
    location.href = $('#_pagination_first_page').val() + '&per_page=' + $(this).val()
  })

  $('.change_status_id').change(function () {
    location.href = $('#_pagination_first_page').val() + '&table_status_id=' + $(this).val()
  })

  $('.close_modal').click(function () {
    $('#' + $(this).data('modal_id')).hide()
    $('body').css('overflow', 'auto')
  })

  $('.show_modal').click(function () {
    $('#' + $(this).data('modal_id')).show()
    $('body').css('overflow', 'hidden')
  })
})
