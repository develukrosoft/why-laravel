let map = null;
let lat = 39.10652688856946;
let lng = -94.58589170968715;
let markers = [];
let loader = document.getElementById('loader_gif');

async function getJsonMarkers(keyword = null) {
    // $.each($('.location_cords'), (index, item) => {
    //     const position = new google.maps.LatLng($(item).data('lat'), $(item).data('lng'));
    //     let marker = new google.maps.Marker({
    //         position: position,
    //         map: map,
    //         title: $(item).data('title')
    //     });
    //
    //     marker.addListener("click", function() {
    //         const url = $('#front_organisation_id').val().slice(0, -1) + $(item).data('id');
    //         window.open(url, '_blank');
    //     });
    //     if ($(item).data('center')) {
    //         map.setCenter(position)
    //     }
    // });

    let url = `?lat=${lat}&lng=${lng}`;
    if (keyword) {
        url += `&keyword=${keyword}`
    }
    await fetch(url)
        .then(async function(response) {
            await response;
            if (response.status === 200) {
                let locations = await response.json();
                locations.locations.map(function(location) {
                    const position = new google.maps.LatLng(location.lat, location.lng);
                    let marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: location.title
                    });
                    markers.push(marker);
                });
            }
        });
    return true;
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: { lat: lat, lng: lng }
    });

    map.addListener('click', function(e) {
        lat = e.latLng.lat();
        lng = e.latLng.lng();
        const position = new google.maps.LatLng(lat, lng);
        map.setCenter(position)
    });

    setTimeout(() => {
        loader.style.display = 'none';
    }, 1000);
}

function removeMarkers() {
    if (markers && markers.length) {
        for(let i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    }
}

document.getElementById('map_search').addEventListener('click', async (event) => {
    loader.style.display = 'flex';
    removeMarkers();
    const map_keyword = document.getElementById('map_keyword');
    await getJsonMarkers(map_keyword.value);
    loader.style.display = 'none';
});



