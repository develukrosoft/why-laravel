let map = null;

async function getJsonMarkers() {
    $.each($('.location_cords'), (index, item) => {
        const position = new google.maps.LatLng($(item).data('lat'), $(item).data('lng'));
        let marker = new google.maps.Marker({
            position: position,
            map: map,
            title: $(item).data('title')
        });

        marker.addListener("click", function() {
            const url = $('#front_organisation_id').val().slice(0, -1) + $(item).data('id');
            window.open(url, '_blank');
        });
        if ($(item).data('center')) {
            map.setCenter(position)
        }
    });
    return true;
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: { lat: 32.8209296, lng: -97.0117489 }
    });
    getJsonMarkers();
}
