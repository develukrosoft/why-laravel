require('./global');
require('./front/header');
require('./front/organisation');
require('./front/volunteer');

window.showModal = function(message = null, title = null) {
    if (title) {
        $('#_modal_title').html(title);
    } else {
        $('#_modal_title').hide();
    }

    if (message) {
        $('#_modal_message').html(message);
    } else {
        $('#_modal_message').hide();
    }

    $('#_modal').css('display', 'flex');
};

$('#_modal_close').click(function() {
    $('#_modal').hide();
});
let alert_message = $('#alert_message').val();
if (alert_message) {
    showModal('', alert_message);
}

$(document).on('click', '.show_modal', function(e) {
    e.preventDefault();
    $('#' + $(this).data('modal_id')).show();
});
$(document).on('click', '.close_modal', function(e) {
    e.preventDefault();
    const modal_id = $(this).data('modal_id');
    $('#' + modal_id).hide();
    const show_type = parseInt($(this).data('show_type'));
    if (show_type === 1) {
        setCookie(modal_id, modal_id, { 'max-age': 86400 });
    } else if (show_type === 2) {
        setCookie(modal_id, modal_id);
    }
});
