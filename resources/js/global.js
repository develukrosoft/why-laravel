window._ = require('lodash')
window.$ = window.jQuery = require('jquery')

window.setCookie = function (name, value, options = {}) {
  options = {
    path: '/',
    ...options
  }
  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString()
  }
  let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value)
  for (let optionKey in options) {
    updatedCookie += '; ' + optionKey
    let optionValue = options[optionKey]
    if (optionValue !== true) {
      updatedCookie += '=' + optionValue
    }
  }
  document.cookie = updatedCookie
}
//setCookie('user', 'John', {secure: true, 'max-age': 3600});

window.getCookie = function (name) {
  let matches = document.cookie.match(new RegExp(
    '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
  ))
  return matches ? decodeURIComponent(matches[1]) : undefined
}
//getCookie(name)

window.deleteCookie = function (name) {
  setCookie(name, '', {
    'max-age': -1
  })
}

$(document).ready(function () {
  $(document).on('change', '.change_country', function () {
    const country_id = parseInt($(this).val())
    const state = $('#' + $(this).data('state_id'))
    if (country_id) {
      $.get($('#api_render_state_by_country').val() + '?country_id=' + country_id, (data) => {
        state.html(data)
      })
    } else {
      state.html('<option value="">---</option>')
      $('#' + state.data('city_id')).html('<option value="">---</option>')
    }
  })

  $(document).on('change', '.change_state', function () {
    const state_id = parseInt($(this).val())
    const city = $('#' + $(this).data('city_id'))
    if (state_id) {
      $.get($('#api_render_city_by_state').val() + '?state_id=' + state_id, (data) => {
        city.html(data)
      })
    } else {
      city.html('<option value="">---</option>')
    }
  })

  $('#add_phone').click(function () {
    $.get($('#api_organisation_render_phone').val(), function (data) {
      $('#phone_items').append(data)
    })
  })

  $('#add_email').click(function () {
    $.get($('#api_organisation_render_email').val(), function (data) {
      $('#email_items').append(data)
    })
  })

  $(document).on('click', '.delete_parent', function () {
    switch ($(this).data('parent')) {
      case 'parent2':
        $(this).parent().parent().remove()
        break
      default:
        $(this).parent().remove()
        break
    }
  })

  $(document).on('change', '.upload_image', function () {
    let file = this.files[0]
    if (file && file.type.indexOf('image/') === 0) {

    } else {
      $(this).val('')
      alert('Select image (only .jpg, .jpeg, .png)')
    }
  })
})
