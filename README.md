## Workspace setup

```
docker build --build-arg APPLICATION_UID=`id -u` -t network-whyhunger:devel -f Dockerfile.devel .
docker run -d \
    --network workspace \
    --name network-whyhunger.workspace \
    -u `id -u` \
    -v $(pwd):/app:delegated \
    -e JWT_SECRET=secret \
    -e APP_ENV=devel \
    -e APP_DEBUG=true \
    -e APP_DOMAIN=network-whyhunger.workspace \
    -e APP_KEY=base64:+6RaViKGwxrGU3CzsPEwaa+c/zsyV/wGiRfzp9P5nnk= \
    -e APP_URL=http://network-whyhunger.workspace \
    -e APP_TIMEZONE=UTC \
    -e DATABASE_URL=mysql://root:toor@mariadb.workspace:3306/whyhunger \
    -e DB_CONNECTION=mysql \
    -e DB_HOST=mariadb.workspace \
    -e DB_USERNAME=root \
    -e DB_PASSWORD=toor \
    -e DB_DATABASE=whyhunger \
    -e XDEBUG_CONFIG=remote_host=host.docker.internal \
    network-whyhunger:devel
```
