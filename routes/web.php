<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['as' => 'front.'], function () {
    Route::get('/locale/set/{locale}',
        [App\Http\Controllers\Front\FrontController::class, 'setLocale'])->name('locale.set');


    Route::get('/volunteer/search/form', function () {
        return redirect(route('front.volunteer.index'));
    });
    Route::get('/join', function () {
        return redirect(route('front.organisation.add'));
    });
    Route::get('/join', function () {
        return redirect(route('front.organisation.add'));
    });

    Route::match(['get', 'post'], '/sms',
        [App\Http\Controllers\Front\FrontController::class, 'sms'])->name('front.sms');
    Route::get('/data/organisations',
        [App\Http\Controllers\Front\FrontController::class, 'dataOrganisations'])
        ->name('front.dataOrganisations');

    Route::match(['get', 'post'], '/',
        [App\Http\Controllers\Front\OrganisationController::class, 'index'])
        ->name('index');
    Route::group(['prefix' => 'organisation', 'as' => 'organisation.'], function () {
        Route::get('/search',
            [App\Http\Controllers\Front\OrganisationController::class, 'search'])
            ->name('search');
        Route::match(['get', 'post'], '/add',
            [App\Http\Controllers\Front\OrganisationController::class, 'add'])
            ->name('add');
        Route::match(['get', 'post'], '/update/{hash}',
            [App\Http\Controllers\Front\OrganisationController::class, 'update'])
            ->name('update');
        Route::get('/{item}',
            [App\Http\Controllers\Front\OrganisationController::class, 'view'])
            ->name('id');
        Route::get('/confirm/{hash}',
            [App\Http\Controllers\Front\OrganisationController::class, 'confirm'])
            ->name('confirm');
    });
    Route::group(['prefix' => 'project', 'as' => 'project.'], function () {
        Route::get('/', [App\Http\Controllers\Front\ProjectController::class, 'index'])
            ->name('index');
        Route::get('/{item}',
            [App\Http\Controllers\Front\ProjectController::class, 'view'])
            ->name('id');
    });
    Route::group(['prefix' => 'event', 'as' => 'calendarEvent.'], function () {
        Route::get('/', [App\Http\Controllers\Front\CalendarEventController::class, 'index'])
            ->name('index');
        Route::match(['get', 'post'], '/form',
            [App\Http\Controllers\Front\CalendarEventController::class, 'form'])
            ->name('form');
        Route::get('/{item}',
            [App\Http\Controllers\Front\CalendarEventController::class, 'view'])
            ->name('id');
    });
    Route::group(['prefix' => 'googleMap', 'as' => 'googleMap.'], function () {
        Route::get('/', [App\Http\Controllers\Front\GoogleMapController::class, 'index'])
            ->name('index');
    });
    Route::group(['prefix' => 'volunteer', 'as' => 'volunteer.'], function () {
        Route::get('/', [App\Http\Controllers\Front\VolunteerController::class, 'index'])
            ->name('index');
        Route::get('/search', [App\Http\Controllers\Front\VolunteerController::class, 'search'])
            ->name('search');
        Route::match(['get', 'post'], '/form/{item}',
            [App\Http\Controllers\Front\VolunteerController::class, 'form'])
            ->name('form');

        Route::match(['get', 'post'], '/feedback',
            [App\Http\Controllers\Front\VolunteerController::class, 'feedback'])
            ->name('feedback');
    });

    Route::group(['prefix' => 'render', 'as' => 'render.'], function () {
        Route::get('/stateByCountry',
            [App\Http\Controllers\Front\RenderController::class, 'stateByCountry'])
            ->name('stateByCountry');
        Route::get('/cityByState',
            [App\Http\Controllers\Front\RenderController::class, 'cityByState'])
            ->name('cityByState');
        Route::get('/phone',
            [App\Http\Controllers\Front\RenderController::class, 'renderPhone'])
            ->name('phone');
        Route::get('/email',
            [App\Http\Controllers\Front\RenderController::class, 'renderEmail'])
            ->name('email');
    });
});
Route::group(['prefix' => 'backend', 'as' => 'backend.'], function () {
    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
        Route::match(['get', 'post'], '/login',
            [\App\Http\Controllers\Backend\AuthController::class, 'login'])->name('login');
        Route::match(['get', 'post'], '/logout',
            [\App\Http\Controllers\Backend\AuthController::class, 'logout'])->name('logout');
    });
});

Route::group(['prefix' => 'backend', 'as' => 'backend.', 'middleware' => 'auth'], function () {
    Route::get('/',
        [App\Http\Controllers\Backend\BackendController::class, 'index'])
        ->name('index');
    Route::post('upload_image',
        [App\Http\Controllers\Backend\UploadController::class, 'uploadImage'])
        ->name('upload');
    Route::group(['middleware' => 'isAdmin'], function () {
        Route::group(['prefix' => 'organisation', 'as' => 'organisation.'], function () {
            Route::match(['get', 'post'], '/',
                [App\Http\Controllers\Backend\OrganisationController::class, 'index'])
                ->name('index');
            Route::get('/form',
                [App\Http\Controllers\Backend\OrganisationController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\OrganisationController::class, 'form'])
                ->name('form.id');
            Route::post('/form',
                [App\Http\Controllers\Backend\OrganisationController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\OrganisationController::class, 'form']);

            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\OrganisationController::class, 'action'])
                ->name('action.id');

            Route::get('/render/phone',
                [App\Http\Controllers\Backend\OrganisationController::class, 'renderPhone'])
                ->name('render.phone');
            Route::get('/render/email',
                [App\Http\Controllers\Backend\OrganisationController::class, 'renderEmail'])
                ->name('render.email');
        });
        Route::group(['prefix' => 'project', 'as' => 'project.'], function () {
            Route::match(['get', 'post'], '/',
                [App\Http\Controllers\Backend\ProjectController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\ProjectController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/apiImport',
                [App\Http\Controllers\Backend\ProjectController::class, 'apiImport'])
                ->name('apiImport');

            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\ProjectController::class, 'form'])
                ->name('form.id');

            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\ProjectController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'calendarEvent', 'as' => 'calendarEvent.'], function () {
            Route::match(['get', 'post'], '/',
                [App\Http\Controllers\Backend\CalendarEventController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\CalendarEventController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\CalendarEventController::class, 'form'])
                ->name('form.id');
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\CalendarEventController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'programType', 'as' => 'programType.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\ProgramTypeController::class, 'index'])
                ->name('index');
            Route::get('/form', [App\Http\Controllers\Backend\ProgramTypeController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\ProgramTypeController::class, 'form'])
                ->name('form.id');
            Route::post('/form',
                [App\Http\Controllers\Backend\ProgramTypeController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\ProgramTypeController::class, 'form']);
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\ProgramTypeController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'optionCategory', 'as' => 'optionCategory.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\OptionCategoryController::class, 'index'])
                ->name('index');
            Route::get('/form',
                [App\Http\Controllers\Backend\OptionCategoryController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\OptionCategoryController::class, 'form'])
                ->name('form.id');
            Route::post('/form',
                [App\Http\Controllers\Backend\OptionCategoryController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\OptionCategoryController::class, 'form']);
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\OptionCategoryController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'optionValue', 'as' => 'optionValue.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\OptionValueController::class, 'index'])
                ->name('index');
            Route::get('/form', [App\Http\Controllers\Backend\OptionValueController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\OptionValueController::class, 'form'])
                ->name('form.id');
            Route::post('/form',
                [App\Http\Controllers\Backend\OptionValueController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\OptionValueController::class, 'form']);
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\OptionValueController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'country', 'as' => 'country.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\CountryController::class, 'index'])
                ->name('index');
            Route::get('/form', [App\Http\Controllers\Backend\CountryController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\CountryController::class, 'form'])
                ->name('form.id');
            Route::post('/form', [App\Http\Controllers\Backend\CountryController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\CountryController::class, 'form']);
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\CountryController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'city', 'as' => 'city.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\CityController::class, 'index'])
                ->name('index');
            Route::get('/form', [App\Http\Controllers\Backend\CityController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\CityController::class, 'form'])
                ->name('form.id');
            Route::post('/form', [App\Http\Controllers\Backend\CityController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\CityController::class, 'form']);
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\CityController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\ContactController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\ContactController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\ContactController::class, 'form'])
                ->name('form.id');
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\ContactController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'state', 'as' => 'state.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\StateController::class, 'index'])
                ->name('index');
            Route::get('/form', [App\Http\Controllers\Backend\StateController::class, 'form'])
                ->name('form');
            Route::get('/form/{id}',
                [App\Http\Controllers\Backend\StateController::class, 'form'])
                ->name('form.id');
            Route::post('/form', [App\Http\Controllers\Backend\StateController::class, 'form']);
            Route::post('/form/{id}',
                [App\Http\Controllers\Backend\StateController::class, 'form']);
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\StateController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'modalMessage', 'as' => 'modalMessage.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\ModalMessageController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\ModalMessageController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\ModalMessageController::class, 'form'])
                ->name('form.id');
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\ModalMessageController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
            Route::get('/', [App\Http\Controllers\Backend\UserController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\UserController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\UserController::class, 'form'])
                ->name('form.id');
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\UserController::class, 'action'])
                ->name('action.id');
        });
        Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
            Route::match(['get', 'post'], '/email',
                [App\Http\Controllers\Backend\SettingController::class, 'email'])
                ->name('email');
            Route::match(['get', 'post'], '/system',
                [App\Http\Controllers\Backend\SettingController::class, 'system'])
                ->name('system');
            Route::match(['get'], '/renderTagIcon',
                [App\Http\Controllers\Backend\SettingController::class, 'renderTagIcon'])
                ->name('renderTagIcon');
            Route::match(['get', 'post'], '/frontend',
                [App\Http\Controllers\Backend\SettingController::class, 'frontend'])
                ->name('frontend');
        });
        Route::group(['prefix' => 'log', 'as' => 'log.'], function () {
            Route::get('/email', [App\Http\Controllers\Backend\LogController::class, 'email'])
                ->name('email');
            Route::group(['prefix' => 'org', 'as' => 'org.'], function () {
                Route::get('/delete',
                    [App\Http\Controllers\Backend\LogController::class, 'orgDelete'])
                    ->name('delete');
                Route::get('/update',
                    [App\Http\Controllers\Backend\LogController::class, 'orgUpdate'])
                    ->name('update');
                Route::get('/import',
                    [App\Http\Controllers\Backend\LogController::class, 'orgImport'])
                    ->name('import');
            });
        });
        Route::group(['prefix' => 'duplicate', 'as' => 'duplicate.'], function () {
            Route::group(['prefix' => 'organisation', 'as' => 'organisation.'], function () {
                Route::get('/',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'organisation'])
                    ->name('index');
                Route::match(['get', 'post'], '/join',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'organisationJoin'])
                    ->name('join');
                Route::match(['get', 'post'], '/remove',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'organisationRemove'])
                    ->name('remove');
            });
            Route::group(['prefix' => 'project', 'as' => 'project.'], function () {
                Route::get('/',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'project'])
                    ->name('index');
                Route::match(['get', 'post'], '/join',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'projectJoin'])
                    ->name('join');
                Route::match(['get', 'post'], '/remove',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'projectRemove'])
                    ->name('remove');
            });
            Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
                Route::get('/',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'contact'])
                    ->name('index');
                Route::match(['get', 'post'], '/join',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'contactJoin'])
                    ->name('join');
                Route::match(['get', 'post'], '/remove',
                    [App\Http\Controllers\Backend\DuplicateController::class, 'contactRemove'])
                    ->name('remove');
            });
        });
        Route::group(['prefix' => 'import', 'as' => 'import.'], function () {
            Route::match(['get', 'post'], '/',
                [App\Http\Controllers\Backend\ImportController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/mapping/{item}',
                [App\Http\Controllers\Backend\ImportController::class, 'mapping'])
                ->name('mapping');
            Route::match(['get', 'post'], '/{item}',
                [App\Http\Controllers\Backend\ImportController::class, 'import'])
                ->name('import');
        });
    });

    Route::group(['prefix' => 'userHowFindValue', 'as' => 'userHowFindValue.'], function () {
        Route::match(['get', 'post'], '/',
            [App\Http\Controllers\Backend\UserHowFindValueController::class, 'index'])
            ->name('index');
        Route::match(['get', 'post'], '/form',
            [App\Http\Controllers\Backend\UserHowFindValueController::class, 'form'])
            ->name('form');
        Route::match(['get', 'post'], '/form/{id}',
            [App\Http\Controllers\Backend\UserHowFindValueController::class, 'form'])
            ->name('form.id');

        Route::get('/action/{id}',
            [App\Http\Controllers\Backend\UserHowFindValueController::class, 'action'])
            ->name('action.id');
    });

    Route::group(['middleware' => 'isVolunteer'], function () {
        Route::group(['prefix' => 'userHowFind', 'as' => 'userHowFind.'], function () {
            Route::match(['get', 'post'], '/',
                [App\Http\Controllers\Backend\UserHowFindController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\UserHowFindController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\UserHowFindController::class, 'form'])
                ->name('form.id');

            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\UserHowFindController::class, 'action'])
                ->name('action.id');
        });

        Route::group(['prefix' => 'volunteer', 'as' => 'volunteer.'], function () {
            Route::match(['get', 'post'], '/',
                [App\Http\Controllers\Backend\VolunteerController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\VolunteerController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\VolunteerController::class, 'form'])
                ->name('form.id');

            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\VolunteerController::class, 'action'])
                ->name('action.id');
        });

        Route::group(['prefix' => 'volunteerProfile', 'as' => 'volunteerProfile.'], function () {
            Route::get('/',
                [App\Http\Controllers\Backend\VolunteerProfileController::class, 'index'])
                ->name('index');
            Route::match(['get', 'post'], '/form',
                [App\Http\Controllers\Backend\VolunteerProfileController::class, 'form'])
                ->name('form');
            Route::match(['get', 'post'], '/form/{id}',
                [App\Http\Controllers\Backend\VolunteerProfileController::class, 'form'])
                ->name('form.id');
            Route::get('/action/{id}',
                [App\Http\Controllers\Backend\VolunteerProfileController::class, 'action'])
                ->name('action.id');
        });
    });

    Route::group(['prefix' => 'render', 'as' => 'render.'], function () {
        Route::get('/stateByCountry',
            [App\Http\Controllers\Backend\RenderController::class, 'stateByCountry'])
            ->name('stateByCountry');
        Route::get('/cityByState',
            [App\Http\Controllers\Backend\RenderController::class, 'cityByState'])
            ->name('cityByState');
    });
});

Route::any('{query}',
    function () {
        return redirect(route('front.index'));
    })
    ->where('query', '.*');
