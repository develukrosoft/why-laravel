<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportFile extends Model
{
    protected $fillable = ['type_id', 'path', 'current_row', 'total_rows', 'mapping'];

    const TYPE_ORGANISATION = 1;
    const TYPE_PROJECT = 2;

    protected $casts = [
        'mapping' => 'json'
    ];
}
