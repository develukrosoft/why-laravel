<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationConfirmation extends Model
{
    protected $fillable = ['organisation_id', 'hash', 'status_id', 'type_id'];

    const STATUS_NEW = 1;
    const STATUS_DONE = 2;

    const TYPE_NEW = 1;
    const TYPE_UPDATE = 2;
}
