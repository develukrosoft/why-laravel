<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHowFindValue extends Model
{
    protected $fillable = ['name', 'position', 'is_active'];
}
