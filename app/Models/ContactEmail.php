<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactEmail extends Model
{
    protected $fillable = ['contact_id', 'email_id'];
    public $timestamps = false;
}
