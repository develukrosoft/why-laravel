<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
    protected $fillable = ['email', 'type_id', 'parent_type_id', 'parent_id', 'user_id'];

    const PARENT_TYPE_ORG = 1;

    const TYPE_ORG_UPDATE_REQUEST = 1;
    const TYPE_ORG_NEW_REQUEST = 2;
    const TYPE_ORG_NEW_REQUEST_ADMIN = 3;

    const TYPES = [
        self::TYPE_ORG_UPDATE_REQUEST => 'Organization Update Request',
        self::TYPE_ORG_NEW_REQUEST => 'Add New Organisation Request Confirmation',
        self::TYPE_ORG_NEW_REQUEST_ADMIN => 'Add New Organisation Request Admin Confirmation',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function parent()
    {
        switch ($this->parent_type_id) {
            case self::PARENT_TYPE_ORG:
                return $this->hasOne(Organisation::class, 'id', 'parent_id');
                break;
        }
        return $this->hasOne(Organisation::class, 'id', 'parent_id')->where('id', '-1');
    }
}
