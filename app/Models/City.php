<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name', 'state_id', 'is_active'];

    public function state()
    {
        return $this->hasOne(State::class, 'id',
            'state_id');
    }
}
