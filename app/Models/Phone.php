<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    const TYPE_PHONE = 'phone';
    const TYPE_MOBILE = 'mobile';
    const TYPE_FAX = 'fax';

    const TYPES = [
        self::TYPE_PHONE => 'Phone',
        self::TYPE_MOBILE => 'Mobile',
        self::TYPE_FAX => 'Fax',
    ];

    protected $fillable = ['type', 'phone', 'ext'];
}
