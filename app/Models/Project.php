<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    const STATUS_OPEN = 1;
    const STATUS_CLOSED = 2;
    const STATUS_CAMPS = 3;
    const STATUS_MIGRANT = 4;

    const STATUSES = [
        self::STATUS_OPEN => 'Open',
        self::STATUS_CLOSED => 'Closed',
        self::STATUS_CAMPS => 'Camps',
        self::STATUS_MIGRANT => 'Migrant',
    ];

    protected $fillable = [
        'title',
        'status_id',
        'date_start',
        'date_end',
        'operation_days',
        'breakfast_from',
        'breakfast_to',
        'lunch_from',
        'lunch_to',
        'snack_from',
        'snack_to',
        'dinner_from',
        'dinner_to',
        'notes',
        'address_id',
        'api_import_hash',
        'postal_address_id',
        'tag_icon_id'
    ];

    protected $casts = ['operation_days' => 'json'];


    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function tagIcon()
    {
        return $this->hasOne(TagIcon::class, 'id', 'tag_icon_id');
    }

    public function postalAddress()
    {
        return $this->hasOne(Address::class, 'id', 'postal_address_id');
    }

    public function phones()
    {
        return $this->belongsToMany(Phone::class, 'project_phones', 'project_id',
            'phone_id');
    }

    public function emails()
    {
        return $this->belongsToMany(Email::class, 'project_emails', 'project_id',
            'email_id');
    }

    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'project_contacts', 'project_id',
            'contact_id');
    }
}
