<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    const TYPE_PENDING = 1;
    const TYPE_APPROVE = 2;
    const TYPE_CANCEL = 3;
    const STATUSES = [
        self::TYPE_PENDING => 'Pending',
        self::TYPE_APPROVE => 'Approved',
        self::TYPE_CANCEL => 'Canceled',
    ];

    const CONFIRMATION_EMAIL_ORG = 1;
    const CONFIRMATION_EMAIL_CONTACT = 2;
    const CONFIRMATION_EMAIL_BOTH = 3;
    const CONFIRMATION_EMAILS = [
        self::CONFIRMATION_EMAIL_ORG => 'Organization Email',
        self::CONFIRMATION_EMAIL_CONTACT => 'Contact Email',
        self::CONFIRMATION_EMAIL_BOTH => 'Use Both Emails',
    ];

    protected $fillable = [
        'title',
        'status_id',
        'website',
        'annual_budget',
        'year_founded',
        'number_volunteers',
        'number_part_time_staff',
        'number_full_time_staff',
        'is_receives_newsletter',
        'is_hcsraw',
        'hours',
        'mission',
        'notes',
        'program_type_description',
        'confirmation_email_id',
        'is_front',
        'address_id',
        'postal_address_id',
        'volunteer_at',
        'old_id',
        'tag_icon_id',
    ];

    protected $casts = ['hours' => 'json'];

    public function types()
    {
        return $this->belongsToMany(ProgramType::class, 'organisation_types', 'organisation_id',
            'type_id');
    }

    public function phones()
    {
        return $this->belongsToMany(Phone::class, 'organisation_phones', 'organisation_id',
            'phone_id');
    }

    public function emails()
    {
        return $this->belongsToMany(Email::class, 'organisation_emails', 'organisation_id',
            'email_id');
    }

    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'organisation_contacts', 'organisation_id',
            'contact_id');
    }

    public function options()
    {
        return $this->belongsToMany(OptionValue::class, 'organisation_options', 'organisation_id',
            'option_id');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function tagIcon()
    {
        return $this->hasOne(TagIcon::class, 'id', 'tag_icon_id');
    }

    public function postalAddress()
    {
        return $this->hasOne(Address::class, 'id', 'postal_address_id');
    }

    public function getCategoryAndOptionsAttribute()
    {
        $items = [];
        foreach ($this->options as $option) {
            if (empty($items[$option->category->name])) {
                $items[$option->category->name] = [];
            }
            $items[$option->category->name][] = $option->value;
        }
        return $items;
    }

    public function getCategoriesAttribute()
    {
        $items = [];
        foreach ($this->options as $option) {
            $items[$option->category->id] = $option->category;
        }
        return $items;
    }
}
