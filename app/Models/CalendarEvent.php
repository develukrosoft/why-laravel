<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    protected $fillable = [
        'title',
        'url',
        'type_id',
        'status_id',
        'date_at',
        'date_start',
        'date_end',
        'contact_id',
        'address_id',
        'description',
        'image_1920x1080',
        'image_1080x1920',
    ];

    const TYPE_ADVERTISING = 1;
    const TYPE_ANNOUNCEMENT = 2;

    const STATUS_NEW = 1;
    const STATUS_APPROVE = 2;
    const STATUS_CANCEL = 3;

    const STATUSES = [
        self::STATUS_NEW => 'New',
        self::STATUS_APPROVE => 'Approved',
        self::STATUS_CANCEL => 'Cancel',
    ];

    const TYPES = [
        self::TYPE_ADVERTISING => 'Advertising',
        self::TYPE_ANNOUNCEMENT => 'Announcement',
    ];

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function contact()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }
}
