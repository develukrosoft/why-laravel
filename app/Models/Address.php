<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'country_id',
        'city_id',
        'state_id',
        'address',
        'zip',
        'lat',
        'lng',
    ];

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    protected $appends = ['full_address'];

    public function getFullAddressAttribute()
    {
        $full = '';
        $items = [
            $this->address,
            $this->city->name,
            $this->state->name,
            "{$this->zip} {$this->country->name}"
        ];
        foreach ($items as $id => $item) {
            if ($item) {
                $full .= $item . (count($items) - 1 === $id ? '' : ', ');
            }
        }
        return $full;
    }

    public function getShortAddressAttribute()
    {
        $short = '';
        $items = [
            $this->address,
            $this->city->name,
            "{$this->zip}"
        ];
        foreach ($items as $id => $item) {
            if ($item) {
                $short .= $item . (count($items) - 1 === $id ? '' : ', ');
            }
        }
        return $short;
    }
}
