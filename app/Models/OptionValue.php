<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionValue extends Model
{
    protected $fillable = ['category_id', 'value', 'is_active'];

    public function category()
    {
        return $this->hasOne(OptionCategory::class, 'id',
            'category_id');
    }
}
