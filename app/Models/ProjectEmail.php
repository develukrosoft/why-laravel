<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectEmail extends Model
{
    protected $fillable = ['project_id', 'email_id'];
    public $timestamps = false;
}
