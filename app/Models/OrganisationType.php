<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationType extends Model
{
    protected $fillable = ['organisation_id', 'type_id'];
    public $timestamps = false;
}
