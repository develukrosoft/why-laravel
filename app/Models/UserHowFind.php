<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHowFind extends Model
{
    protected $fillable = ['value_id', 'contact_id', 'other', 'parent_type', 'parent_id'];

    const TYPE_ORGANISATION = 'organisation';
    const TYPE_ORGANISATION_VOLUNTEER = 'organisation_volunteer';

    public function value()
    {
        return $this->hasOne(UserHowFindValue::class, 'id', 'value_id');
    }

    public function organisation()
    {
        return $this->hasOne(Organisation::class, 'id', 'parent_id');
    }

    public function orgVolunteer()
    {
        return $this->hasOne(OrganisationVolunteer::class, 'id', 'parent_id');
    }

    public function parent()
    {
        switch ($this->parent_type) {
            case self::TYPE_ORGANISATION:
                return $this->organisation;
                break;
            case self::TYPE_ORGANISATION_VOLUNTEER:
                return $this->orgVolunteer;
                break;
        }
        return $this->organisation()->where('id', '-1');
    }
}
