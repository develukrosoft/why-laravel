<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    const TYPE_TEXT = 1;
    const TYPE_EMAIL = 2;
    const TYPE_NUMBER = 3;
    const TYPE_DOUBLE = 4;
    const TYPE_CKEDITOR = 5;
    const TYPE_BOOLEAN = 6;

    protected $fillable = ['name', 'slug', 'type_id', 'value'];
}
