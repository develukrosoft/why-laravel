<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPhone extends Model
{
    protected $fillable = ['project_id', 'phone_id'];
    public $timestamps = false;
}
