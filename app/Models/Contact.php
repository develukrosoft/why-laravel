<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['title', 'first_name', 'middle_initial', 'last_name', 'address_id'];

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function phones()
    {
        return $this->belongsToMany(Phone::class, 'contact_phones', 'contact_id',
            'phone_id');
    }

    public function emails()
    {
        return $this->belongsToMany(Email::class, 'contact_emails', 'contact_id',
            'email_id');
    }
}
