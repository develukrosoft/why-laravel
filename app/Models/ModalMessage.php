<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModalMessage extends Model
{
    protected $fillable = [
        'show_type_id',
        'message',
        'start_at',
        'end_at',
        'locale',
        'position',
        'class_name'
    ];

    const TYPE_DAY = 1;
    const TYPE_SESSION = 2;

    const TYPES = [
        self::TYPE_DAY => '1 per day',
        self::TYPE_SESSION => 'Session',
    ];
}
