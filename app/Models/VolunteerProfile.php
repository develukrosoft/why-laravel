<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VolunteerProfile extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'data',
        'last_at',
        'old_id'
    ];
    protected $casts = ['data' => 'json'];

    public function organisationVolunteer()
    {
        return $this->hasOne(OrganisationVolunteer::class, 'volunteer_id', 'id');
    }
}
