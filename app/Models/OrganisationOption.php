<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationOption extends Model
{
    public $timestamps = false;
}
