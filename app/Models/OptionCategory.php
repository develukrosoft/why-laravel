<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionCategory extends Model
{
    const TYPE_ORG = 'organisation';
    const TYPES = [
        self::TYPE_ORG => 'organisation'
    ];
    protected $fillable = ['name', 'type', 'is_active', 'fontawesome_icon'];

    public function values()
    {
        return $this->hasMany(OptionValue::class, 'category_id',
            'id');
    }
}
