<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationVolunteer extends Model
{
    protected $fillable = [
        'data',
        'status_id',
        'organisation_id',
        'volunteer_id',
        'approve_at',
        'old_id'
    ];
    protected $casts = ['data' => 'json'];
    const STATUS_NEW = 1;
    const STATUS_APPROVE = 2;
    const STATUS_DELETE = 3;


    const STATUSES = [
        self::STATUS_NEW => 'New',
        self::STATUS_APPROVE => 'Approved',
        self::STATUS_DELETE => 'Deleted',
    ];

    public function organisation()
    {
        return $this->hasOne(Organisation::class, 'id', 'organisation_id');
    }

    public function volunteer()
    {
        return $this->hasOne(VolunteerProfile::class, 'id', 'volunteer_id');
    }
}
