<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagIcon extends Model
{
    protected $fillable = [
        'parent_type_id',
        'name',
        'icon_url',
    ];

    const PARENT_TYPE_ALL = 1;
    const PARENT_TYPE_ORG = 2;
    const PARENT_TYPE_PROJECT = 3;

    const TYPES = [
        self::PARENT_TYPE_ALL => 'All',
        self::PARENT_TYPE_ORG => 'Organization',
        self::PARENT_TYPE_PROJECT => 'Sites\Project',
    ];
}
