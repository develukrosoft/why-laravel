<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationPhone extends Model
{
    protected $fillable = ['organisation_id', 'phone_id'];
    public $timestamps = false;

}
