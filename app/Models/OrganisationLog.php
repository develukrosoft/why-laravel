<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationLog extends Model
{
    protected $fillable = ['title', 'type_id', 'user_id', 'organisation_id'];

    const TYPE_DELETE = 1;
    const TYPE_UPDATE = 2;
    const TYPE_IMPORT = 3;
}
