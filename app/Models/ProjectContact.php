<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectContact extends Model
{
    protected $fillable = ['project_id', 'contact_id'];
    public $timestamps = false;
}
