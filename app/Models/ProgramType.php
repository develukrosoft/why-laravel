<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramType extends Model
{
    protected $fillable = ['name', 'description', 'is_active', 'fontawesome_icon'];
}
