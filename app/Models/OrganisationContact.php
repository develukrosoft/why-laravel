<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationContact extends Model
{
    protected $fillable = ['organisation_id', 'contact_id'];
    public $timestamps = false;
}
