<?php

namespace App\Services;

class ImportService
{
    static function getRow($source_file, $source_row)
    {
        $row = 0;
        $success = false;
        $row_data = null;
        $header_data = null;
        if (($handle = fopen($source_file, 'r')) !== false) {
            while (($data = fgetcsv($handle)) !== false && $success !== true) {
                if ($row === 0) {
                    foreach ($data as $k => $v) {
                        $header_data[$k] = mb_convert_encoding($v, "UTF-8");
                    }
                }
                if ($row == $source_row) {
                    foreach ($data as $k => $v) {
                        $row_data[$k] = mb_convert_encoding($v, "UTF-8");
                    }
                    $success = true;
                }
                $row++;
            }
        }
        return [
            'data' => $success ? $row_data : false,
            'header' => $header_data
        ];
    }

    static function getHeaders($source_file)
    {
        $success = false;
        $header_data = null;
        if (($handle = fopen($source_file, 'r')) !== false) {
            while (($data = fgetcsv($handle)) !== false && $success !== true) {
                foreach ($data as $k => $v) {
                    $header_data[$k] = mb_convert_encoding($v, "UTF-8");
                }
                $success = true;
            }
        }
        return $header_data;
    }

    static function getTotalRows($source_file)
    {
        $row = 0;
        $row_data = null;
        if (($handle = fopen($source_file, 'r')) !== false) {
            while (($data = fgetcsv($handle)) !== false) {
                $row++;
            }
        }
        return $row;
    }
}
