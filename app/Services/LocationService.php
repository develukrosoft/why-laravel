<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Spatie\Geocoder\Facades\Geocoder;

class LocationService
{
    static function getStatesByCountryId($country_id)
    {
        return State::where('country_id', $country_id)->orderBy('name')->get();
    }

    static function getCitiesByStateId($state_id)
    {
        return City::where('state_id', $state_id)->orderBy('name')->get();
    }

    static function getDataByAddress($address, $country_id = null, $state_id = null)
    {
        try {
            $res = Geocoder::getCoordinatesForAddress($address);
        } catch (\Exception $e) {

        }
        $data = [
            'lat' => !empty($res['lat']) ? $res['lat'] : '',
            'lng' => !empty($res['lng']) ? $res['lng'] : '',
            'address' => '',
            'zip' => '',
            'city' => 'Unknown',
            'city_id' => null,
            'state' => 'Unknown',
            'state_code' => 'Unknown',
        ];
        $street_number = '';
        $route = '';
        if (!empty($res['address_components'])) {
            foreach ($res['address_components'] as $ac) {
                if (in_array('street_number', $ac->types)) {
                    $street_number = $ac->long_name;
                }
                if (in_array('route', $ac->types)) {
                    $route = $ac->long_name;
                }
                if (in_array('postal_code', $ac->types)) {
                    $data['zip'] = $ac->long_name;
                }
                if (in_array('locality', $ac->types)) {
                    $data['city'] = $ac->long_name;
                }
                if (in_array('administrative_area_level_1', $ac->types)) {
                    $data['state'] = $ac->long_name;
                    $data['state_code'] = $ac->short_name;
                }
            }

            if ($street_number && $route) {
                $data['address'] = "$street_number $route";
            }
        }
        $country = Country::where('code', 'US')->first();

        if (!empty($data['city']) && !empty($data['state']) && !empty($data['state_code'])) {
            $state = State::where(['code' => $data['state_code'], 'country_id' => $country->id])
                ->first();
            if (!$state) {
                $state = State::create([
                    'code' => $data['state_code'],
                    'name' => $data['state'],
                    'country_id' => $country->id,
                ]);
            }
            $data['state_id'] = $state->id;
            $city = City::firstOrCreate(['name' => $data['city'], 'state_id' => $state->id]);
            $data['city_id'] = $city->id;
        }

        if ($country_id && $state_id && empty($data['city_id'])) {
            $city = City::firstOrCreate([
                'state_id' => $state_id,
                'name' => 'Unknown',
            ]);
            $data['city_id'] = $city->id;
        }

        $data['country_id'] = $country->id;
        return $data;
    }
}
