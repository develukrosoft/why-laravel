<?php

namespace App\Services;

use App\Models\Setting;

class SettingService
{
    static function getValueBySlug($slug, $default = null)
    {
        $setting = Setting::where('slug', $slug)->first();
        return $setting ? $setting->value : $default;
    }
}
