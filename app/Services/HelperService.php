<?php

namespace App\Services;

class HelperService
{
    static function getTimes($time)
    {
        try {
            $parts = explode('-', $time);
            $parts = [trim($parts[0]), trim($parts[1])];
            $bf_apm = strpos($parts[0], 'AM') ? 'AM' : 'PM';
            $time_from =
                explode(':', trim(str_replace(['AM', 'PM'], '', $parts[0])));
            $time_from = ($bf_apm == 'AM'
                    ? $time_from[0] : ($time_from[0] + 12)) . ':'
                . $time_from[1];

            $apm = strpos($parts[1], 'AM') ? 'AM' : 'PM';
            $time_to =
                explode(':', str_replace(['AM', 'PM'], '', $parts[1]));
            $time_to = ($apm == 'AM'
                    ? $time_to[0] : ($time_to[0] + 12)) . ':'
                . $time_to[1];
            $time_from = explode(':', preg_replace('/[^0-9:]/', '', $time_from));
            $time_to = explode(':', preg_replace('/[^0-9:]/', '', $time_to));
            $time_from =
                substr($time_from[0] ?? '00', 0, 2) . ':' . substr($time_from[1] ?? '00', 0, 2);
            $time_to = substr($time_to[0] ?? '00', 0, 2) . ':' . substr($time_to[1] ?? '00', 0, 2);
            return [
                $time_from,
                $time_to,
            ];
        } catch (\Exception $e) {
        }
        return [null, null];
    }

    static function getTime($time)
    {
        try {
            $time = explode(':', preg_replace('/[^0-9:]/', '', $time));
            $time = substr(mb_strlen($time[0]) ? $time[0] : '00', 0, 2) . ':'
                . substr(mb_strlen($time[1]) ? $time[1] : '00', 0, 2);
            return $time;
        } catch (\Exception $e) {
        }
        return null;
    }

    static function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) {
            return $contents;
        } else {
            return false;
        }
    }
}
