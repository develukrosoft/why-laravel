<?php

namespace App\Services;


class DateService
{
    static function getTimePMAM($time)
    {
        $_time = explode(':',$time);
        $time = ($_time[0] > 12 ? ($_time[0] - 12) : $_time[0]) . ':' . $_time[1];
        return (mb_strlen(explode(':',$time)[0]) == 1 ? '0' . $time : $time) . ($_time[0] > 12 ? 'PM' : 'AM');
    }
}
