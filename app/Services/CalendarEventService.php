<?php

namespace App\Services;

use App\Models\Address;
use App\Models\CalendarEvent;
use App\Models\City;

class CalendarEventService
{
    static $baseModel = CalendarEvent::class;

    static function save($data, $item = null)
    {
        $address = $item ? $item->address : null;
        if (!empty($data['address']['country_id'])) {
            $city = City::firstOrCreate([
                'name' => $data['address']['city'],
                'state_id' => $data['address']['state_id']
            ]);
            $data['address']['city_id'] = $city->id;

            if ($address) {
                $address->update($data['address']);
            } else {
                $address = Address::create($data['address']);
            }
        }

        if (!$address->lat || !$address->lng) {
            $new_address =
                LocationService::getDataByAddress("{$address->address}, {$address->country->code} {$address->zip}",
                    $address->country_id, $address->state_id);
            $address->update([
                'address' => !empty($new_address['address']) ? $new_address['address']
                    : $address->address,
                'zip' => !empty($new_address['zip']) ? $new_address['zip']
                    : $address->zip,
                'lat' => $new_address['lat'],
                'lng' => $new_address['lng'],
                'city_id' => $new_address['city_id'],
            ]);
        }

        $data['address_id'] = $address->id;

        if (!empty($data['image_1920x1080'])) {
            $data['image_1920x1080'] = self::pictureUpload($data['image_1920x1080']);
        } else {
            unset($data['image_1920x1080']);
        }
        if (!empty($data['image_1080x1920'])) {
            $data['image_1080x1920'] = self::pictureUpload($data['image_1080x1920']);
        } else {
            unset($data['image_1080x1920']);
        }
        if ($item) {
            $item->update($data);
        } else {
            $contact = ContactService::save(array_merge($data['contact'],
                ['phones' => $data['phones'], 'emails' => $data['emails']]));
            $data['contact_id'] = $contact->id;
            $item = self::$baseModel::create($data);
        }

        $item->refresh();

        return $item;
    }

    static function pictureUpload($file)
    {
        try {
            $publicDirectory = public_path('/images/events');
            if (!is_dir($publicDirectory)) {
                mkdir($publicDirectory, 0777, true);
            }
            $full_name = md5(rand(10000000, 99999999) . time()) . '.' . $file->getClientOriginalExtension();
            $file->move($publicDirectory, $full_name);
            return '/images/events/' . $full_name;
        } catch (\Exception $e) {
        }
        return null;
    }
}
