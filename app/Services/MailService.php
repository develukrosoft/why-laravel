<?php

namespace App\Services;


class MailService
{
    static function getMailByTemplateName($template_name, $data)
    {
        $mail_template = SettingService::getValueBySlug($template_name, '<div>Mail</div>');
        return self::getMailByTemplate($mail_template, $data);
    }

    static function getMailByTemplate($template, $data)
    {
        $content = preg_replace(
            [
                '/{{contact_fullname}}/m',
                '/{{company_name}}/m',
                '/{{confirmation_link}}/m',
                '/{name}/m',
                '/{current_date}/m',
                '/{update_request_link}/m',
            ],
            [
                !empty($data['contact_fullname']) ? $data['contact_fullname'] : '',
                !empty($data['company_name']) ? $data['company_name'] : '',
                !empty($data['confirmation_link']) ? $data['confirmation_link'] : '',
                !empty($data['name']) ? $data['name'] : '',
                Date('Y-m-d'),
                !empty($data['update_request_link']) ? $data['update_request_link'] : '',
            ],
            $template
        );
        return $content;
    }
}
