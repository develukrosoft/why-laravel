<?php

namespace App\Services;

use App\Models\Address;
use App\Models\City;
use App\Models\Contact;
use App\Models\ContactEmail;
use App\Models\ContactPhone;
use App\Models\Email;
use App\Models\Phone;

class ContactService
{
    static $baseModel = Contact::class;

    static function save($data, $item = null)
    {
        $address = $item ? $item->address : null;
        if (!empty($data['address']['country_id'])) {
            $city = City::firstOrCreate([
                'name' => $data['address']['city'],
                'state_id' => $data['address']['state_id']
            ]);
            $data['address']['city_id'] =$city->id;

            if ($address) {
                $address->update($data['address']);
            } else {
                $address = Address::create($data['address']);
            }
        }

        if ($address && (!$address->lat || !$address->lng)) {
            $new_address =
                LocationService::getDataByAddress("{$address->address}, {$address->country->code} {$address->zip}",
                    $address->country_id, $address->state_id);
            $address->update([
                'address' => !empty($new_address['address']) ? $new_address['address']
                    : $address->address,
                'zip' => !empty($new_address['zip']) ? $new_address['zip']
                    : $address->zip,
                'lat' => $new_address['lat'],
                'lng' => $new_address['lng'],
                'city_id' => $new_address['city_id'],
            ]);
        }

        $data['address_id'] = $address ? $address->id : null;


        if ($item) {
            $item->update($data);
            $item->refresh();
        } else {
            $item = self::$baseModel::create($data);
        }

        $phone_ids = [];

        if (!empty($data['phones']) && count($data['phones'])) {
            foreach ($data['phones'] as $phone) {
                if (!empty($phone['phone'])) {
                    $phone =
                        preg_replace('/[^0-9]+/', '',
                            trim($phone['phone'], " '"));
                    $ext = preg_replace('/[^0-9]+/', '',
                        trim(!empty($phone['ext']) ? $phone['ext'] : '',
                            " '"));
                    $p = Phone::firstOrCreate(['phone' => $phone, 'ext' => $ext]);
                    $phone_ids[] = $p->id;
                }
            }
        }

        if (count($phone_ids)) {
            $item->phones()->sync($phone_ids);
        } else {
            ContactPhone::where('contact_id', $item->id)->delete();
        }

        $email_ids = [];

        if (!empty($data['emails']) && count($data['emails'])) {
            foreach ($data['emails'] as $email) {
                if (!empty($email['email'])) {
                    $email = trim($email['email']);
                    $e = Email::firstOrCreate(['email' => $email]);
                    $email_ids[] = $e->id;
                }
            }
        }
        if (count($email_ids)) {
            $item->emails()->sync($email_ids);
        } else {
            ContactEmail::where('contact_id', $item->id)->delete();
        }

        return $item;
    }
}
