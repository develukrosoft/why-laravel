<?php

namespace App\Services;


class TagIconService
{
    static function pictureUpload($file)
    {
//        try {
            $publicDirectory = public_path('/images/tag_icon');
            if (!is_dir($publicDirectory)) {
                mkdir($publicDirectory, 0777, true);
            }
            $full_name =
                md5(rand(10000000, 99999999) . time()) . '.' . $file->getClientOriginalExtension();
            $file->move($publicDirectory, $full_name);
            return '/images/tag_icon/' . $full_name;
//        } catch (\Exception $e) {
//        }
        return null;
    }
}
