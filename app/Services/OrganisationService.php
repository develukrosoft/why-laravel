<?php

namespace App\Services;

use App\Models\Address;
use App\Models\City;
use App\Models\Contact;
use App\Models\Email;
use App\Models\Organisation;
use App\Models\OrganisationEmail;
use App\Models\OrganisationLog;
use App\Models\OrganisationOption;
use App\Models\OrganisationPhone;
use App\Models\OrganisationType;
use App\Models\Phone;
use App\Models\UserHowFind;
use App\Models\UserHowFindValue;
use Illuminate\Support\Facades\Auth;

class OrganisationService
{
    static $baseModel = Organisation::class;

    static function form($request, $item = null)
    {
        $data = $request->only([
            'title',
            'website',
            'annual_budget',
            'year_founded',
            'number_volunteers',
            'number_part_time_staff',
            'number_full_time_staff',
            'is_receives_newsletter',
            'is_hcsraw',
            'hours',
            'mission',
            'notes',
            'address',
            'postal_address',
            'program_types',
            'program_type_description',
            'phones',
            'emails',
            'options',
            'new_contact',
            'how_find',
            'status_id',
            'is_front',
            'how_find_other',
            'tag_icon_id',
        ]);

        $address = $item ? $item->address : null;
        if (!empty($data['address']['country_id'])) {
            $city = City::firstOrCreate([
                'name' => $data['address']['city'],
                'state_id' => $data['address']['state_id']
            ]);
            $data['address']['city_id'] = $city->id;;
            unset($data['address']['city']);
            if ($address) {
                $address->update($data['address']);
            } else {
                $address = Address::create($data['address']);
            }
        }

        if (!$address->lat || !$address->lng) {
            $new_address =
                LocationService::getDataByAddress("{$address->address}, {$address->country->code} {$address->zip}",
                    $address->country_id, $address->state_id);
            $address->update([
                'address' => !empty($new_address['address']) ? $new_address['address']
                    : $address->address,
                'zip' => !empty($new_address['zip']) ? $new_address['zip']
                    : $address->zip,
                'lat' => $new_address['lat'],
                'lng' => $new_address['lng'],
                'city_id' => $new_address['city_id'],
            ]);
        }

        $data['address_id'] = $address->id;

        $postal_address = $item ? $item->postalAddress : null;
        if (!empty($data['postal_address']['country_id'])
            && !empty($data['postal_address']['state_id'])
            && !empty($data['postal_address']['city'])
        ) {
            $city = City::firstOrCreate([
                'name' => $data['postal_address']['city'],
                'state_id' => $data['postal_address']['state_id']
            ]);
            $data['postal_address']['city_id'] = $city->id;;
            if ($postal_address) {
                $postal_address->update($data['postal_address']);
            } else {
                $postal_address = Address::create($data['postal_address']);
            }
        }

        if ($postal_address && (!$postal_address->lat || !$postal_address->lng)) {
            $new_address =
                LocationService::getDataByAddress("{$postal_address->address}, {$postal_address->country->code} {$postal_address->zip}",
                    $postal_address->country_id, $postal_address->state_id);
            $postal_address->update([
                'address' => !empty($new_address['address']) ? $new_address['address']
                    : $postal_address->address,
                'zip' => !empty($new_address['zip']) ? $new_address['zip']
                    : $postal_address->zip,
                'lat' => $new_address['lat'],
                'lng' => $new_address['lng'],
                'city_id' => $new_address['city_id'],
            ]);
        }

        $data['postal_address_id'] = $postal_address ? $postal_address->id : null;

        if ($item) {
            OrganisationLog::create([
                'title' => $item->title,
                'organisation_id' => $item->id,
                'type_id' => OrganisationLog::TYPE_UPDATE,
                'user_id' => Auth::id()
            ]);
            $item->update($data);
        } else {
            $item = self::$baseModel::create($data);
        }


        if (!empty($data['program_types']) && count($data['program_types'])) {
            $item->types()->sync($data['program_types']);
        } else {
            OrganisationType::where('organisation_id', $item->id)->delete();
        }

        if (!empty($data['options']) && count($data['options'])) {
            $item->options()->sync($data['options']);
        } else {
            OrganisationOption::where('organisation_id', $item->id)->delete();
        }

        $phone_ids = [];

        if (!empty($data['phones']) && count($data['phones'])) {
            foreach ($data['phones'] as $phone) {
                if (!empty($phone['phone'])) {
                    $p = Phone::firstOrCreate($phone);
                    $phone_ids[] = $p->id;
                }
            }
        }

        if (count($phone_ids)) {
            $item->phones()->sync($phone_ids);
        } else {
            OrganisationPhone::where('organisation_id', $item->id)->delete();
        }

        $email_ids = [];

        if (!empty($data['emails']) && count($data['emails'])) {
            foreach ($data['emails'] as $email) {
                if (!empty($email['email'])) {
                    $e = Email::firstOrCreate($email);
                    $email_ids[] = $e->id;
                }
            }
        }
        if (count($email_ids)) {
            $item->emails()->sync($email_ids);
        } else {
            OrganisationEmail::where('organisation_id', $item->id)->delete();
        }

        if (!empty($data['new_contact'])) {
            $contact = Contact::create([
                'first_name' => !empty($data['new_contact']['first_name'])
                    ? $data['new_contact']['first_name'] : '',
                'last_name' => !empty($data['new_contact']['last_name'])
                    ? $data['new_contact']['last_name'] : '',
            ]);

            $phone =
                preg_replace('/[^0-9]+/', '',
                    trim($data['new_contact']['phone'], " '"));
            if ($phone) {
                $phone = Phone::create(['phone' => $phone]);
                $contact->phones()->sync([$phone->id]);
            }
            if (!empty($data['new_contact']['email'])) {
                $email = Email::create(['email' => $data['new_contact']['email']]);
                $contact->emails()->sync([$email->id]);
            }

            $item->contacts()->sync([$contact->id]);
        }
        if (!empty($data['how_find']) && $uhfv = UserHowFindValue::find($data['how_find'])) {
            UserHowFind::create([
                'value_id' => $uhfv->id,
                'parent_type' => UserHowFind::TYPE_ORGANISATION,
                'parent_id' => $item->id,
                'how_find_other' => !empty($data['how_find_other']) ? $data['how_find_other'] : null
            ]);
        }

        $item->refresh();


        return $item;
    }
}
