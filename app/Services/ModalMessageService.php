<?php

namespace App\Services;

use App\Models\ModalMessage;
use Illuminate\Support\Facades\Cookie;

class ModalMessageService
{
    static $baseModel = ModalMessage::class;

    static function getModals()
    {
        //        $modals = \Cache::get('modals');
        //        if ($modals === null) {
        $locale = strtolower(Cookie::get('locale', 'en'));
        $modals = self::$baseModel::where('end_at', '>=', Date('Y-m-d'))
            ->where('start_at', '<=', Date('Y-m-d'))
            ->whereIn('locale', ['all', $locale])
            ->orderBy('position', 'ASC')
            ->get();
        //            \Cache::put('modals', $modals, 86400);
        //        }
        return $modals;
    }
}
