<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\CalendarEvent;
use App\Models\Contact;
use App\Models\Organisation;
use App\Models\Project;
use Illuminate\Console\Command;

class AddressClearCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addresses:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Address::cursor() as $address) {
            $find = true;
            $org = Organisation::where('address_id', $address->id)->first();
            if (!$org) {
                $project = Project::where('address_id', $address->id)->first();
                if (!$project) {
                    $contact = Contact::where('address_id', $address->id)->first();
                    if (!$contact) {
                        $event = CalendarEvent::where('address_id', $address->id)->first();
                        if (!$event) {
                            $find = false;
                        }
                    }
                }
            }

            if (!$find) {
                $address->delete();
            }
        }

        return 0;
    }
}
