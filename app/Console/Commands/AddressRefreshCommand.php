<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Services\LocationService;
use Illuminate\Console\Command;

class AddressRefreshCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addresses:refresh {address_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $address = Address::find($this->argument('address_id'));
        if($address){
            $new_address = LocationService::getDataByAddress("{$address->address}, {$address->country->code} {$address->zip}",
                $address->country_id, $address->state_id);
            $address->update([
                'address' => !empty($new_address['address']) ? $new_address['address']
                    : $address->address,
                'zip' => !empty($new_address['zip']) ? $new_address['zip']
                    : $address->zip,
                'lat' => $new_address['lat'],
                'lng' => $new_address['lng'],
                'city_id' => $new_address['city_id'],
            ]);
        }

        return 0;
    }
}
