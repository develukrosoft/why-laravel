<?php

namespace App\Console\Commands;

use App\Models\Address;
use Illuminate\Console\Command;

class AddressesZipNormalizeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addresses:zip_normalize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Address::cursor() as $address) {
            if (mb_strlen($address->zip) === 4) {
                $address->update(['zip' => '0' . $address->zip]);
            }
        }
        return 0;
    }
}
