<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class Language
{
    public function handle($request, Closure $next)
    {
        if (Cookie::has('locale') && in_array(Cookie::get('locale'), ['en', 'es'])) {
            App::setLocale(Cookie::get('locale'));
        } else {
            App::setLocale(config('app.fallback_locale'));
        }
        return $next($request);
    }
}
