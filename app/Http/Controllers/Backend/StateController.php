<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public $baseModel = State::class;
    public $viewName = 'state';

    public function index(Request $request)
    {
        if ($action = $request->get('action')) {
            switch ($action) {
                case 'export':
                    $columns = [
                        'ID',
                        'Name',
                        'Code',
                        'Count Use',
                        'Country',
                    ];
                    $headers = [
                        "Content-type" => "text/csv",
                        "Content-Disposition" => "attachment; filename=export.csv",
                        "Pragma" => "no-cache",
                        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                        "Expires" => "0"
                    ];
                    $callback = function () use ($columns) {
                        $file = fopen('php://output', 'w');
                        fputcsv($file, $columns);

                        foreach (State::cursor() as $state) {

                            $row = [
                                $state->id,
                                $state->name,
                                $state->code,
                                Address::where('state_id', $state->id)->count(),
                                $state->country->name,
                            ];

                            fputcsv($file, $row);
                        }
                        fclose($file);
                    };
                    return response()->stream($callback, 200, $headers);
                    break;
            }
        }

        return view("backend.$this->viewName.index",
            ['pagination' => $this->baseModel::paginate(10, ['*'], 'page', $request->get('page'))]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->only(['name', 'code', 'country_id', 'is_active']);
            $data['code'] = strtoupper($data['code']);
            if ($item) {
                $item->update($data);
            } else {
                $item = $this->baseModel::create($data);
            }
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
