<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\TagIcon;
use App\Services\TagIconService;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public $baseModel = Setting::class;
    public $viewName = 'setting';

    public function email(Request $request)
    {
        if ($request->isMethod('post')) {
            $settings = $request->get('settings');
            foreach ($settings as $id => $s) {
                if ($setting = $this->baseModel::find($id)) {
                    $setting->update(['value' => $s]);
                }
            }
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.email"));
        }
        return view("backend.$this->viewName.email",
            [
                'settings' => $this->baseModel::whereIn('slug', [
                    //                    'email_from',
                    'email_from_new_org',
                    'email_template_confirmation',
                    'email_template_notification_confirmation_admin',
                    'email_template_update_request',
                ])->get()
            ]);
    }

    public function system(Request $request)
    {
        if ($request->isMethod('post')) {
            $ids = [];
            $items = $request->get('item', []);
            $files = $request->allFiles();
            foreach ($items as $id => $data) {
                if (!empty($files['item'][$id]['image'])) {
                    $data['icon_url'] = TagIconService::pictureUpload($files['item'][$id]['image']);
                }
                if ($item = TagIcon::find($id)) {
                    $item->update($data);
                } else {
                    $item = TagIcon::create($data);
                }
                $ids[] = $item->id;
            }
            TagIcon::whereNotIn('id', $ids)->delete();
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.system"));
        }
        return view("backend.$this->viewName.system",
            [
                'pagination' => TagIcon::paginate(1000, ['*'], 'page',
                    $request->get('page')),
            ]);
    }

    public function renderTagIcon(Request $request)
    {
        return [
            'render' => view("backend.$this->viewName.partials.tagIconItem")->render()
        ];
    }

    public function frontend(Request $request)
    {
        if ($request->isMethod('post')) {
            $settings = $request->get('settings');
            foreach ($settings as $id => $s) {
                if ($setting = $this->baseModel::find($id)) {
                    $setting->update(['value' => $s]);
                }
            }
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.frontend") . '?tab='
                . $request->get('tab_id'));
        }
        return view("backend.$this->viewName.frontend",
            [
                'settings' => [
                    'tab_orgs' => $this->baseModel::whereIn('slug', [
                        'find_food_is_active',
                        'find_food_inactive_html',
                    ])->get(),
                    'tab_join' => $this->baseModel::whereIn('slug', [
                        'join_is_active',
                        'join_inactive_html',
                    ])->get(),
                    'tab_projects' => $this->baseModel::whereIn('slug', [
                        'projects_is_active',
                        'projects_inactive_html',
                    ])->get(),
                    'tab_volunteer' => $this->baseModel::whereIn('slug', [
                        'volunteer_orgs_updated',
                        'volunteer_is_active',
                        'volunteer_inactive_html',
                    ])->get(),
                    'tab_calendar_event' => $this->baseModel::whereIn('slug', [
                        'calendar_event_is_active',
                        'calendar_event_inactive_html',
                    ])->get(),
                ],
                'tab_active' => $request->get('tab')
            ]);
    }
}
