<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\City;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Phone;
use App\Models\Project;
use App\Models\ProjectContact;
use App\Models\State;
use App\Services\HelperService;
use App\Services\LocationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public $baseModel = Project::class;
    public $viewName = 'project';

    public function index(Request $request)
    {
        if ($action = $request->get('batch')) {
            switch ($action) {
                case 'delete':
                    $this->baseModel::whereIn('id', $request->get('ids'))
                        ->delete();
                    break;
                case 'delete_all':
                    $this->baseModel::where('id', '!=', -1)->delete();
                    break;
            }
            return ['status' => 200];
        }

        $query = $this->baseModel::query();

        $filters = $request->only([
            'country_id',
            'city_id',
            'state_id',
            'address',
            'zip',
            'title',
            'status_id',
            'date_start',
            'date_end',
            'per_page',
        ]);

        if (!empty($filters['title'])) {
            $query->where('title', 'LIKE', '%' . $filters['title'] . '%');
        }

        if (!empty($filters['status_id'])) {
            $query->where('status_id', $filters['status_id']);
        }
        if (!empty($filters['date_start'])) {
            $query->where('date_start', '>=', $filters['date_start']);
        }
        if (!empty($filters['date_end'])) {
            $query->where('date_end', '<=', $filters['date_end']);
        }

        if (!empty($filters['country_id']) || !empty($filters['address'])
            || !empty($filters['zip'])
        ) {
            $query->whereHas('address', function (Builder $query) use ($filters) {
                if (!empty($filters['country_id'])) {
                    $query->where('country_id', $filters['country_id']);
                }
                if (!empty($filters['state_id'])) {
                    $query->where('state_id', $filters['state_id']);
                }
                if (!empty($filters['city_id'])) {
                    $query->where('city_id', $filters['city_id']);
                }
                if (!empty($filters['address'])) {
                    $query->where('address', 'LIKE', '%' . $filters['address'] . '%');
                }
                if (!empty($filters['zip'])) {
                    $query->where('zip', $filters['zip']);
                }
            });
        }
        $fields = [
            'title' => 'Title',
            'status_id' => 'Status',
            'address_country' => 'Address - Country',
            'address_state' => 'Address - State',
            'address_city' => 'Address - City',
            'address' => 'Address',
            'address_zip' => 'Address - Zip',
            'notes' => 'Notes',
            'date_start' => 'Date start',
            'date_end' => 'Date end',
            'operation_days' => 'Operation days',
            'breakfast' => 'Breakfast',
            'lunch' => 'Lunch',
            'snack' => 'Snack',
            'dinner' => 'Dinner',
        ];
        if ($action = $request->get('action')) {
            switch ($action) {
                case 'export':
                    $columns = array_merge(['id' => 'ID'], $request->get('fields', []));

                    //                    $columns = [
                    //                        'ID',
                    //                        'Title',
                    //                        'Status',
                    //                        'Country',
                    //                        'State',
                    //                        'City',
                    //                        'Address',
                    //                        'Zip',
                    //                        'Notes',
                    //                        'Date start',
                    //                        'Date end',
                    //                        'Operation days',
                    //                        'Breakfast',
                    //                        'Lunch',
                    //                        'Snack',
                    //                        'Dinner',
                    //                    ];
                    $headers = [
                        "Content-type" => "text/csv",
                        "Content-Disposition" => "attachment; filename=export.csv",
                        "Pragma" => "no-cache",
                        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                        "Expires" => "0"
                    ];
                    $callback = function () use ($columns, $query) {
                        $file = fopen('php://output', 'w');
                        fputcsv($file, $columns);

                        foreach ($query->cursor() as $item) {
                            $days = '';
                            if (is_array($item->operation_days)
                                || is_object($item->operation_days)
                            ) {
                                foreach ($item->operation_days as $id => $day) {
                                    $days .= $day .
                                        (count($item->operation_days) - 1 === $id ? '' : ', ');
                                }
                            }
                            $row = [
                                $item->id,
                                $item->title,
                                Project::STATUSES[$item->status_id],
                                $item->address->country->name,
                                $item->address->state->name,
                                $item->address->city->name,
                                $item->address->address,
                                $item->address->zip,
                                $item->notes,
                                Date('Y-m-d', strtotime($item->date_start)),
                                Date('Y-m-d', strtotime($item->date_end)),
                                $days,
                                $item->breakfast_from . ' - ' . $item->breakfast_to,
                                $item->lunch_from . ' - ' . $item->lunch_to,
                                $item->snack_from . ' - ' . $item->snack_to,
                                $item->dinner_from . ' - ' . $item->dinner_to,
                            ];

                            $row = [];
                            foreach ($columns as $column => $name) {
                                if ($column === 'address_country') {
                                    $row[] = $item->address->country->name;
                                } elseif ($column === 'address_state') {
                                    $row[] = $item->address->state->name;
                                } elseif ($column === 'address_city') {
                                    $row[] = $item->address->city->name;
                                } elseif ($column === 'address') {
                                    $row[] = $item->address->address;
                                } elseif ($column === 'address_zip') {
                                    $row[] = $item->address->zip;
                                } elseif ($column === 'created_at') {
                                    $row[] = Date('Y-m-d H:i:s', strtotime($item->created_at));
                                } elseif ($column === 'date_start') {
                                    $row[] = Date('Y-m-d H:i:s', strtotime($item->date_start));
                                } elseif ($column === 'date_end') {
                                    $row[] = Date('Y-m-d H:i:s', strtotime($item->date_end));
                                } elseif ($column === 'operation_days') {
                                    $row[] = $days;
                                } elseif ($column === 'breakfast') {
                                    $row[] = $item->breakfast_from . ' - ' . $item->breakfast_to;
                                } elseif ($column === 'lunch') {
                                    $row[] = $item->lunch_from . ' - ' . $item->lunch_to;
                                } elseif ($column === 'snack') {
                                    $row[] = $item->snack_from . ' - ' . $item->snack_to;
                                } elseif ($column === 'dinner') {
                                    $row[] = $item->dinner_from . ' - ' . $item->dinner_to;
                                } elseif ($column === 'status_id') {
                                    $row[] = Project::STATUSES[$item->status_id];
                                } else {
                                    $row[] = $item->$column;
                                }
                            }
                            fputcsv($file, $row);
                        }

                        fclose($file);
                    };
                    return response()->stream($callback, 200, $headers);
                    break;
            }
        }

        return view("backend.$this->viewName.index",
            [
                'pagination' => $query->paginate($request->get('per_page', 25), ['*'], 'page',
                    $request->get('page')),
                'filters' => $filters,
                'fields' => $fields
            ]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->only([
                'title',
                'status_id',
                'date_start',
                'date_end',
                'operation_days',
                'breakfast',
                'lunch',
                'snack',
                'dinner',
                'notes',
                'address',
                'postal_address',
                'tag_icon_id',
            ]);

            if (!empty(!empty($data['breakfast']['from']['hour']))) {
                $data['breakfast_from'] =
                    $data['breakfast']['from']['hour'] . ':'
                    . (!empty($data['breakfast']['from']['min']) ? $data['breakfast']['from']['min']
                        : '00');
            }
            if (!empty(!empty($data['breakfast']['to']['hour']))) {
                $data['breakfast_to'] =
                    $data['breakfast']['to']['hour'] . ':'
                    . (!empty($data['breakfast']['to']['min']) ? $data['breakfast']['to']['min']
                        : '00');
            }

            if (!empty(!empty($data['lunch']['from']['hour']))) {
                $data['lunch_from'] =
                    $data['lunch']['from']['hour'] . ':'
                    . (!empty($data['lunch']['from']['min']) ? $data['lunch']['from']['min']
                        : '00');
            }
            if (!empty(!empty($data['lunch']['to']['hour']))) {
                $data['lunch_to'] =
                    $data['lunch']['to']['hour'] . ':'
                    . (!empty($data['lunch']['to']['min']) ? $data['lunch']['to']['min']
                        : '00');
            }

            if (!empty(!empty($data['snack']['from']['hour']))) {
                $data['snack_from'] =
                    $data['snack']['from']['hour'] . ':'
                    . (!empty($data['snack']['from']['min']) ? $data['snack']['from']['min']
                        : '00');
            }
            if (!empty(!empty($data['snack']['to']['hour']))) {
                $data['snack_to'] =
                    $data['snack']['to']['hour'] . ':'
                    . (!empty($data['snack']['to']['min']) ? $data['snack']['to']['min']
                        : '00');
            }

            if (!empty(!empty($data['dinner']['from']['hour']))) {
                $data['dinner_from'] =
                    $data['dinner']['from']['hour'] . ':'
                    . (!empty($data['dinner']['from']['min']) ? $data['dinner']['from']['min']
                        : '00');
            }
            if (!empty(!empty($data['dinner']['to']['hour']))) {
                $data['dinner_to'] =
                    $data['dinner']['to']['hour'] . ':'
                    . (!empty($data['dinner']['to']['min']) ? $data['dinner']['to']['min']
                        : '00');
            }

            $address = $item ? $item->address : null;
            if (!empty($data['address']['country_id'])) {
                $city = City::firstOrCreate([
                    'name' => $data['address']['city'],
                    'state_id' => $data['address']['state_id']
                ]);
                $data['address']['city_id'] = $city->id;

                if ($address) {
                    $address->update($data['address']);
                } else {
                    $address = Address::create($data['address']);
                }
            }

            if (!$address->lat || !$address->lng) {
                $new_address =
                    LocationService::getDataByAddress("{$address->address}, {$address->country->code} {$address->zip}",
                        $address->country_id, $address->state_id);
                $address->update([
                    'address' => !empty($new_address['address']) ? $new_address['address']
                        : $address->address,
                    'zip' => !empty($new_address['zip']) ? $new_address['zip']
                        : $address->zip,
                    'lat' => $new_address['lat'],
                    'lng' => $new_address['lng'],
                    'city_id' => $new_address['city_id'],
                ]);
            }

            $data['address_id'] = $address->id;


            $postal_address = $item ? $item->postalAddress : null;
            if (!empty($data['postal_address']['country_id'])
                && !empty($data['postal_address']['state_id'])
                && !empty($data['postal_address']['city'])
            ) {
                $city = City::firstOrCreate([
                    'name' => $data['postal_address']['city'],
                    'state_id' => $data['postal_address']['state_id']
                ]);
                $data['postal_address']['city_id'] = $city->id;

                if ($postal_address) {
                    $postal_address->update($data['postal_address']);
                } else {
                    $postal_address = Address::create($data['postal_address']);
                }
            }

            if ($postal_address && (!$postal_address->lat || !$postal_address->lng)) {
                $new_address =
                    LocationService::getDataByAddress("{$postal_address->address}, {$postal_address->country->code} {$postal_address->zip}",
                        $postal_address->country_id, $postal_address->state_id);
                $postal_address->update([
                    'address' => !empty($new_address['address']) ? $new_address['address']
                        : $postal_address->address,
                    'zip' => !empty($new_address['zip']) ? $new_address['zip']
                        : $postal_address->zip,
                    'lat' => $new_address['lat'],
                    'lng' => $new_address['lng'],
                    'city_id' => $new_address['city_id'],
                ]);
            }

            $data['postal_address_id'] = $postal_address ? $postal_address->id : null;

            if ($item) {
                $item->update($data);
            } else {
                $item = $this->baseModel::create($data);
            }

            $item->refresh();

            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
                case 'delete_contact':
                    ProjectContact::where([
                        'project_id' => $id,
                        'contact_id' => $request->get('contact_id')
                    ])->delete();
                    return redirect(route("backend.$this->viewName.form.id", ['id' => $id]));
                    break;
                case 'attach_contact':
                    ProjectContact::create([
                        'project_id' => $id,
                        'contact_id' => $request->get('contact_id')
                    ]);
                    return redirect(route("backend.$this->viewName.form.id", ['id' => $id]));
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }

    public function apiImport(Request $request)
    {
        $filters = [
            'year' => 2016,
            'last_id' => 0
        ];
        $logs = [];
        if ($request->isMethod('post')) {
            $year = $request->get('year', $filters['year']);
            $last_id = $request->get('last_id', $filters['last_id']);
            if ($year >= 2021) {
                $url =
                    'https://services1.arcgis.com/RLQu0rK7h4kbsBq5/arcgis/rest/services/Summer_Meal_Sites_'
                    . $year . '_view/FeatureServer/0/query?where=OBJECTID%3E' . $last_id
                    . '&f=pjson&resultRecordCount=10&outFields=*';
            } else {
                $url =
                    'https://services1.arcgis.com/RLQu0rK7h4kbsBq5/ArcGIS/rest/services/Summer_Meal_Sites_'
                    . $year . '/FeatureServer/0/query?where=OBJECTID%3E' . $last_id
                    . '&f=pjson&resultRecordCount=10&outFields=*';
            }
            $json = file_get_contents($url);
            $r = json_decode($json);
            if ($r && isset($r->features)) {
                foreach ($r->features as $_item) {
                    $item = $_item->attributes;

                    $project = Project::where('api_import_hash', md5($item->OBJECTID))->first();
                    if (!$project) {
                        $project = Project::where('title', trim($item->siteName))->first();
                    }
                    $last_id = $item->OBJECTID;
                    if ($project) {
                        $logs[] = "<div>Same hash found (Name: $project->title), skipped.</div>";
                    } else {
                        $item->address =
                            !empty($item->siteAddress) ? $item->siteAddress : $item->address;
                        if (!isset($item->address) || strlen($item->address) < 5) {
                            $logs[] =
                                "<div>Address not found. (Name: $item->siteName), skipped.</div>";
                        } else {
                            $status_id = Project::STATUS_OPEN;
                            if (isset($item->siteStatus) && trim($item->siteStatus) != '') {
                                $s_id = array_search($item->siteStatus, Project::STATUSES);
                                $status_id = $s_id ? $s_id : $status_id;
                            }
                            $date_start = Date('Y-m-d H:i:s');
                            $date_end = Date('Y-m-d H:i:s');
                            if (isset($item->startDate) && trim($item->startDate) != '') {
                                $date_start =
                                    Date('Y-m-d H:i:s', $item->startDate / 1000);
                            }
                            if (isset($item->endDate) && trim($item->endDate) != '') {
                                $date_end = Date('Y-m-d H:i:s', $item->endDate / 1000);
                            }
                            $notes = !empty($item->hoursOpen) ? $item->hoursOpen : '';
                            $operation_days = [];
                            if (isset($item->daysofOperation)) {
                                $shorts = [
                                    'M' => 'Monday',
                                    'T' => 'Tuesday',
                                    'W' => 'Wednesday',
                                    'TH' => 'Thursday',
                                    'F' => 'Friday',
                                    'SA' => 'Saturday',
                                    'SU' => 'Sunday'
                                ];
                                $do = strtoupper($item->daysofOperation);
                                foreach ($shorts as $short => $full) {
                                    $pos = strpos($do, $short);
                                    if ($pos !== false
                                        && ($short !== 'T'
                                            || ($short == 'T'
                                                && (strrpos($do, 'TH') !== $pos
                                                    || !strpos($do, 'TH')))
                                        )
                                    ) {
                                        $operation_days[] = $full;
                                    }
                                }
                            }
                            $parts = HelperService::getTimes($item->breakfastTime);
                            $breakfast_from = $parts[0];
                            $breakfast_to = $parts[1];

                            $parts = HelperService::getTimes($item->lunchTime);
                            $lunch_from = $parts[0];
                            $lunch_to = $parts[1];

                            $snack_time = !empty($item->snackTime) ? $item->snackTime : '';
                            if (!empty($item->snackTimeAM)) {
                                $snack_time = $item->snackTimeAM;
                            }
                            if (!empty($item->snackTimePM)) {
                                $snack_time = $item->snackTimePM;
                            }
                            $parts = HelperService::getTimes($snack_time);
                            $snack_from = $parts[0];
                            $snack_to = $parts[1];

                            $parts = HelperService::getTimes($item->dinnerSupperTime);
                            $dinner_from = $parts[0];
                            $dinner_to = $parts[1];

                            $country = Country::where('code', 'US')->first();

                            $state = !empty($item->siteState) ? $item->siteState : $item->state;
                            $state = State::where(['country_id' => $country->id])
                                ->where(function ($q) use ($state) {
                                    $q->where('code', $state)
                                        ->orWhere('name', $state);
                                })->first();
                            $city = ucfirst(strtolower(!empty($item->siteCity) ? $item->siteCity
                                : $item->city_1));
                            $city =
                                City::firstOrCreate(['name' => $city, 'state_id' => $state->id]);

                            $zip = !empty($item->siteZip) ? $item->siteZip : $item->zip;

                            $adr = !empty($item->siteAddress) ? $item->siteAddress : $item->address;
                            $new_address = [];

                            if (strpos($city->name, $adr) !== false) {
                                $_adr = mb_substr($adr, 0, strpos($city->name, $adr));

                                $new_address = [
                                    'lat' => !empty($_item->geometry->y) ? $_item->geometry->y : '',
                                    'lng' => !empty($_item->geometry->x) ? $_item->geometry->x : '',
                                    'address' => $_adr,
                                    'city_id' => $city->id,
                                ];
                            }

                            if (empty($new_address['lat']) || empty($new_address['lng'])) {
                                $new_address = LocationService::getDataByAddress($adr, $country->id,
                                    $state->id);
                            }

                            $address_data = [
                                'country_id' => $country->id,
                                'state_id' => $state->id,
                                'city_id' => $new_address['city_id'],
                                'zip' => preg_replace('/[^0-9]+/', '', $zip),
                                'address' => !empty($new_address['address'])
                                    ? $new_address['address'] : $adr,
                                'lat' => $new_address['lat'],
                                'lng' => $new_address['lng'],
                            ];
                            $address = Address::create($address_data);
                            $phone = null;
                            if (isset($item->sitePhone)) {
                                $phone = '+' . preg_replace('/[^0-9]+/', '',
                                        trim($item->sitePhone, " '"));
                            }
                            $data = [
                                'title' => trim($item->siteName),
                                'api_import_hash' => md5($item->OBJECTID),
                                'status_id' => $status_id,
                                'date_start' => $date_start,
                                'date_end' => $date_end,
                                'notes' => $notes,
                                'operation_days' => $operation_days,
                                'breakfast_from' => $breakfast_from
                                    ? HelperService::getTime($breakfast_from) : null,
                                'breakfast_to' => $breakfast_to
                                    ? HelperService::getTime($breakfast_to) : null,
                                'lunch_from' => $lunch_from ? HelperService::getTime($lunch_from)
                                    : null,
                                'lunch_to' => $lunch_to ? HelperService::getTime($lunch_to) : null,
                                'dinner_from' => $dinner_from ? HelperService::getTime($dinner_from)
                                    : null,
                                'dinner_to' => $dinner_to ? HelperService::getTime($dinner_to)
                                    : null,
                                'snack_to' => $snack_to ? HelperService::getTime($snack_to) : null,
                                'snack_from' => $snack_from ? HelperService::getTime($snack_from)
                                    : null,
                                'address_id' => $address->id
                            ];
                            $project = Project::create($data);
                            if ($phone) {
                                $phone = Phone::firstOrCreate([
                                    'phone' => $phone,
                                    'type' => 'phone',
                                    'ext' => $item->ext
                                ]);
                                $project->phones()->sync([$phone->id]);
                            }


                            if ($item->contactFirstName || $item->contactLastName) {
                                $contact = Contact::create([
                                    'first_name' => !empty($item->contactFirstName)
                                        ? $item->contactFirstName : '',
                                    'last_name' => !empty($item->contactLastName)
                                        ? $item->contactLastName : '',
                                ]);
                                if (!empty($item->contactPhone)) {
                                    $phone =
                                        Phone::firstOrCreate([
                                            'phone' => $item->contactPhone,
                                            'type' => 'phone'
                                        ]);
                                    $contact->phones()->sync([$phone->id]);
                                }
                            }
                            $logs[] = "<div>Added. Name: $project->title</div>";
                        }
                    }
                }
            }
            return ['logs' => $logs, 'last_id' => $last_id];
        }
        return view("backend.$this->viewName.apiImport", ['filters' => $filters]);
    }

}
