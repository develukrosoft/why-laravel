<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OrganisationVolunteer;
use App\Models\VolunteerProfile;
use Illuminate\Http\Request;

class VolunteerProfileController extends Controller
{
    public $baseModel = VolunteerProfile::class;
    public $viewName = 'volunteerProfile';

    public function index(Request $request)
    {

        if ($action = $request->get('action')) {
            switch ($action) {
                case 'export':
                    $columns = [
                        'ID',
                        'First Name',
                        'Last Name',
                        'Email',
                        'Phone',
                        'Last At',
                        'Feedback',
                    ];
                    $headers = [
                        "Content-type" => "text/csv",
                        "Content-Disposition" => "attachment; filename=export.csv",
                        "Pragma" => "no-cache",
                        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                        "Expires" => "0"
                    ];
                    $query = $this->baseModel::query();

                    $callback = function () use ($columns, $query) {
                        $file = fopen('php://output', 'w');
                        fputcsv($file, $columns);

                        foreach ($query->cursor() as $item) {
                            $days = '';
                            if (is_array($item->operation_days)
                                || is_object($item->operation_days)
                            ) {
                                foreach ($item->operation_days as $id => $day) {
                                    $days .= $day .
                                        (count($item->operation_days) - 1 === $id ? '' : ', ');
                                }
                            }
                            $row = [
                                $item->id,
                                $item->first_name,
                                $item->last_name,
                                $item->email,
                                $item->phone,
                                $item->last_at ? Date('Y-m-d H:i', strtotime($item->last_at)) : '-',
                            ];
                            if (!empty($item->data['feedback'])
                                && is_array($item->data['feedback'])
                            ) {
                                foreach ($item->data['feedback'] as $feedback) {
                                    if (!empty($feedback['value'])) {
                                        $row[] = $feedback['value'];
                                    }
                                }
                            }
                            fputcsv($file, $row);
                        }

                        fclose($file);
                    };
                    return response()->stream($callback, 200, $headers);
                    break;
            }
        }

        return view("backend.$this->viewName.index",
            ['pagination' => $this->baseModel::paginate(10, ['*'], 'page', $request->get('page'))]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->only(['first_name', 'last_name', 'email', 'phone']);
            $data['phone'] =
                '+' . preg_replace('/[^0-9]+/', '',
                    trim($data['phone'], " '"));
            if ($item) {
                $item->update($data);
            } else {
                $item = $this->baseModel::create($data);
            }
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", [
            'item' => $item,
            'pagination' => OrganisationVolunteer::where('volunteer_id', $item->id)
                ->paginate(10, ['*'], 'page', $request->get('page'))
        ]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
