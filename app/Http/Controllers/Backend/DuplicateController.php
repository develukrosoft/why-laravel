<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\CalendarEvent;
use App\Models\Contact;
use App\Models\ContactEmail;
use App\Models\ContactPhone;
use App\Models\Organisation;
use App\Models\OrganisationContact;
use App\Models\OrganisationVolunteer;
use App\Models\Project;
use App\Models\ProjectContact;
use App\Models\UserHowFind;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DuplicateController extends Controller
{
    public function organisation(Request $request)
    {
        $query = DB::table('organisations')
            ->select('title', (DB::raw('COUNT(title)')))
            ->groupBy('title')
            ->having(DB::raw('COUNT(title)'), '>', 1);

        return view("backend.duplicate.organisation.index",
            [
                'pagination' => $query->paginate(20, ['*'], 'page', $request->get('page')),
            ]);
    }

    public function organisationJoin(Request $request)
    {
        if ($request->isMethod('post')) {
            $main = Organisation::find($request->get('organisation_id'));
            $items = Organisation::where('title', $request->get('title'))
                ->whereNotIn('id', [$main->id])->get();
            $phone_ids = [];
            $email_ids = [];
            $option_ids = [];
            $type_ids = [];
            $contact_ids = [];

            foreach ($items as $item) {
                foreach ($item->phones as $p) {
                    $phone_ids[] = $p->id;
                }
                foreach ($item->emails as $e) {
                    $email_ids[] = $e->id;
                }
                foreach ($item->options as $o) {
                    $option_ids[] = $o->id;
                }
                foreach ($item->types as $t) {
                    $type_ids[] = $t->id;
                }
                foreach ($item->contacts as $c) {
                    $contact_ids[] = $c->id;
                }
            }
            foreach ($main->phones as $p) {
                $phone_ids[] = $p->id;
            }
            foreach ($main->emails as $e) {
                $email_ids[] = $e->id;
            }
            foreach ($main->options as $o) {
                $option_ids[] = $o->id;
            }
            foreach ($main->types as $t) {
                $type_ids[] = $t->id;
            }
            foreach ($main->contacts as $c) {
                $contact_ids[] = $c->id;
            }
            $main->phones()->sync($phone_ids);
            $main->emails()->sync($email_ids);
            $main->options()->sync($option_ids);
            $main->types()->sync($type_ids);
            $main->contacts()->sync($contact_ids);
            Organisation::where('title', $request->get('title'))
                ->whereNotIn('id', [$main->id])->delete();
            return redirect(route("backend.organisation.form.id", $main->id));
        }
        $items = Organisation::where('title', $request->get('title'))->get();
        return view("backend.duplicate.organisation.join",
            [
                'items' => $items
            ]);
    }

    public function organisationRemove()
    {
        $query = DB::table('organisations')
            ->select('title', (DB::raw('COUNT(title)')))
            ->groupBy('title')
            ->having(DB::raw('COUNT(title)'), '>', 1);
        foreach ($query->get() as $item) {
            $main = Organisation::where('title', $item->title)->first();
            $other = Organisation::where('title', $item->title)->whereNotIn('id', [$main->id])
                ->get();
            OrganisationVolunteer::whereIn('organisation_id', $other->pluck('id')->toArray())
                ->update(['organisation_id' => $main->id]);
            Organisation::where('title', $item->title)->whereNotIn('id', [$main->id])->delete();
        }

        return redirect(route('backend.duplicate.organisation.index'));
    }

    public function project(Request $request)
    {
        $query = DB::table('projects')
            ->select('title', (DB::raw('COUNT(title)')))
            ->groupBy('title')
            ->having(DB::raw('COUNT(title)'), '>', 1);

        return view("backend.duplicate.project.index",
            [
                'pagination' => $query->paginate(20, ['*'], 'page', $request->get('page')),
            ]);
    }

    public function projectJoin(Request $request)
    {
        if ($request->isMethod('post')) {
            $main = Project::find($request->get('project_id'));
            $items = Project::where('title', $request->get('title'))
                ->whereNotIn('id', [$main->id])->get();

            $contact_ids = [];
            $data = [
                'operation_days' => $main->operation_days,
                'breakfast_from' => $main->breakfast_from,
                'breakfast_to' => $main->breakfast_to,
                'lunch_from' => $main->lunch_from,
                'lunch_to' => $main->lunch_to,
                'snack_from' => $main->snack_from,
                'snack_to' => $main->snack_to,
                'dinner_from' => $main->dinner_from,
                'dinner_to' => $main->dinner_to,
                'notes' => $main->notes,
            ];
            foreach ($items as $item) {
                foreach ($item->contacts as $c) {
                    $contact_ids[] = $c->id;
                }
                foreach ($data as $key => $value) {
                    if (!$value && $item->$key) {
                        $data[$key] = $item->$key;
                    }
                }

            }

            foreach ($main->contacts as $c) {
                $contact_ids[] = $c->id;
            }

            $main->contacts()->sync($contact_ids);
            $main->update($data);
            Project::where('title', $request->get('title'))
                ->whereNotIn('id', [$main->id])->delete();
            return redirect(route("backend.project.form.id", $main->id));
        }
        $items = Project::where('title', $request->get('title'))->get();
        return view("backend.duplicate.project.join",
            [
                'items' => $items
            ]);
    }

    public function projectRemove()
    {
        $query = DB::table('projects')
            ->select('title', (DB::raw('COUNT(title)')))
            ->groupBy('title')
            ->having(DB::raw('COUNT(title)'), '>', 1);
        foreach ($query->get() as $item) {
            $main = Project::where('title', $item->title)->first();
            Project::where('title', $item->title)->whereNotIn('id', [$main->id])->delete();
        }

        return redirect(route('backend.duplicate.project.index'));
    }

    public function contact(Request $request)
    {
        //        $query = DB::table('contacts')
        //            ->select('first_name', (DB::raw('COUNT(title)')))
        //            ->groupBy('title')
        //            ->having(DB::raw('COUNT(title)'), '>', 1);
        $query = Contact::select('first_name', 'last_name', DB::raw('COUNT(*) as count'))
            ->groupBy('first_name', 'last_name')
            ->having('count', '>', 1);
        return view("backend.duplicate.contact.index",
            [
                'pagination' => $query->paginate(20, ['*'], 'page', $request->get('page')),
            ]);
    }

    public function contactJoin(Request $request)
    {
        if ($request->isMethod('post')) {
            $main = Contact::find($request->get('contact_id'));
            $items = Contact::where([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name')
            ])
                ->whereNotIn('id', [$main->id])->get();
            $phone_ids = [];
            $email_ids = [];

            foreach ($items as $item) {
                foreach ($item->phones as $p) {
                    $phone_ids[] = $p->id;
                }
                foreach ($item->emails as $e) {
                    $email_ids[] = $e->id;
                }
            }

            foreach ($main->phones as $p) {
                $phone_ids[] = $p->id;
            }
            foreach ($main->emails as $e) {
                $email_ids[] = $e->id;
            }

            $main->phones()->sync($phone_ids);
            $main->emails()->sync($email_ids);
            $contact_ids = Contact::where([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name')
            ])->whereNotIn('id', [$main->id])->get()->pluck('id')->toArray();
            $organisation_ids = OrganisationContact::whereIn('contact_id', $contact_ids)->get()
                ->pluck('organisation_id')->toArray();
            foreach (Organisation::whereIn('id', $organisation_ids)->get() as $org) {
                OrganisationContact::firstOrCreate([
                    'organisation_id' => $org->id,
                    'contact_id' => $main->id
                ]);
            }
            $project_ids = ProjectContact::whereIn('contact_id', $contact_ids)->get()
                ->pluck('organisation_id')->toArray();
            foreach (Project::whereIn('id', $project_ids)->get() as $project) {
                ProjectContact::firstOrCreate([
                    'organisation_id' => $project->id,
                    'contact_id' => $main->id
                ]);
            }
            Contact::whereIn('id', $contact_ids)->delete();
            return redirect(route("backend.contact.form.id", $main->id));
        }
        $items = Contact::where([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name')
        ])->get();
        return view("backend.duplicate.contact.join",
            [
                'items' => $items
            ]);
    }


    public function contactRemove()
    {
        $query = Contact::select('first_name', 'last_name', DB::raw('COUNT(*) as count'))
            ->groupBy('first_name', 'last_name')
            ->having('count', '>', 1);
        foreach ($query->get() as $item) {
            $main = Contact::where([
                'first_name' => $item->first_name,
                'last_name' => $item->last_name,
            ])->first();
            $other = Contact::where([
                'first_name' => $item->first_name,
                'last_name' => $item->last_name,
            ])->whereNotIn('id', [$main->id])->get();
            $other_ids = $other->pluck('id')->toArray();
            foreach (
                OrganisationContact::whereIn('contact_id', $other_ids)->get() as
                $oc
            ) {
                if (OrganisationContact::where([
                    'contact_id' => $main->id,
                    'organisation_id' => $oc->organisation_id
                ])->first()
                ) {
                    $oc->delete();
                } else {
                    $oc->update(['contact_id' => $main->id]);
                }
            }
            foreach (
                ProjectContact::whereIn('contact_id', $other_ids)->get() as
                $pc
            ) {
                if (ProjectContact::where([
                    'contact_id' => $main->id,
                    'project_id' => $pc->project_id
                ])->first()
                ) {
                    $pc->delete();
                } else {
                    $pc->update(['contact_id' => $main->id]);
                }
            }
            foreach (
                ContactPhone::whereIn('contact_id', $other_ids)->get() as
                $cp
            ) {
                if (ContactPhone::where([
                    'contact_id' => $main->id,
                    'phone_id' => $cp->phone_id
                ])->first()
                ) {
                    $cp->delete();
                } else {
                    $cp->update(['contact_id' => $main->id]);
                }
            }
            foreach (
                ContactEmail::whereIn('contact_id', $other_ids)->get() as
                $ce
            ) {
                if (ContactEmail::where([
                    'contact_id' => $main->id,
                    'email_id' => $ce->email_id
                ])->first()
                ) {
                    $ce->delete();
                } else {
                    $ce->update(['contact_id' => $main->id]);
                }
            }

            UserHowFind::whereIn('contact_id', $other_ids)
                ->update(['contact_id' => $main->id]);
            CalendarEvent::whereIn('contact_id', $other_ids)
                ->update(['contact_id' => $main->id]);

            Contact::where([
                'first_name' => $item->first_name,
                'last_name' => $item->last_name,
            ])->whereNotIn('id', [$main->id])->delete();
        }

        return redirect(route('backend.duplicate.contact.index'));
    }
}
