<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Email;
use App\Models\Organisation;
use App\Models\OrganisationEmail;
use App\Models\OrganisationPhone;
use App\Models\OrganisationVolunteer;
use App\Models\Phone;
use App\Models\ProgramType;
use App\Models\VolunteerProfile;
use App\Services\LocationService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class VolunteerController extends Controller
{
    public $baseModel = OrganisationVolunteer::class;
    public $viewName = 'volunteer';

    public function index(Request $request)
    {
        $query = $this->baseModel::where('status_id', $this->baseModel::STATUS_NEW);

        if ($action = $request->get('action')) {
            switch ($action) {
                case 'export':
                    $query = $this->baseModel::query();
                    $date_start = $request->get('date_start');
                    $date_end = $request->get('date_end');
                    $date_start_end = $date_start && $date_end ? "$date_start - $date_end" : '';
                    if ($date_start && $date_end) {
                        $date_start = Date('Y-m-d', strtotime($date_start)) . ' 00:00:00';
                        $query->where('created_at', '>=', $date_start);
                        $date_end = Date('Y-m-d', strtotime($date_end)) . ' 23:59:59';
                        $query->where('created_at', '<=', $date_end);
                    }

                    $items = $query->get();
                    if ($items->count()) {
                        $letters = range('A', 'Z');
                        foreach (range('A', 'Z') as $letterOne) {
                            foreach (range('A', 'Z') as $letterTwo) {
                                $letters[] = $letterOne . $letterTwo;
                            }
                        }

                        $spreadsheet = new Spreadsheet();
                        $spreadsheet->removeSheetByIndex(0);
                        $myWorkSheet =
                            new Worksheet($spreadsheet, 'Summary');
                        $spreadsheet->addSheet($myWorkSheet, 0);
                        $spreadsheet->setActiveSheetIndex(0);
                        $activeSheet = $spreadsheet->getActiveSheet();

                        $row = 1;
                        $activeSheet->mergeCells("A$row:B$row");
                        $activeSheet->getStyle("A$row:B$row")->getAlignment()
                            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                        $activeSheet->setCellValue("A$row", 'VOLUNTEER CONVERSION/RETENTION STATS');
                        $activeSheet->getStyle("A$row")->getFont()->setBold(true);

                        $row++;
                        $count_ovp = VolunteerProfile::count();
                        $activeSheet->setCellValue("A$row", 'Total number of volunteer sign-ups');
                        $activeSheet->setCellValue("B$row", $count_ovp);
                        if ($date_start_end) {
                            $row++;
                            $count_ovp_d = VolunteerProfile::where('created_at', '>=', $date_start)
                                ->where('created_at', '<=', $date_end)->count();
                            $activeSheet->setCellValue("A$row",
                                "Total number of volunteer sign-ups ($date_start_end)");
                            $activeSheet->setCellValue("B$row", $count_ovp_d);
                        }

                        $row++;
                        $count_ov_dva = VolunteerProfile::where('last_at', '>=',
                            Date('Y-m-d', strtotime("-30 day")) . ' 00:00:00')->count();

                        $activeSheet->setCellValue("A$row",
                            'Current Number of Active Volunteers (30 days)');
                        $activeSheet->setCellValue("B$row", $count_ov_dva);

                        $row++;
                        $percent_active =
                            $count_ov_dva && $count_ovp ? round(($count_ov_dva * 100 / $count_ovp))
                                : 0;

                        $activeSheet->setCellValue("A$row", '% of Current Active Volunteers');
                        $activeSheet->setCellValue("B$row",
                            $percent_active > 0 ? $percent_active : '-');

                        if ($date_start_end) {
                            $row++;
                            $count_ov_dva_d = VolunteerProfile::where('last_at', '>=', $date_start)
                                ->where('last_at', '<=', $date_end)->count();

                            $activeSheet->setCellValue("A$row",
                                "Current Number of Active Volunteers ($date_start_end)");
                            $activeSheet->setCellValue("B$row", $count_ov_dva_d);

                            $row++;
                            $percent_active =
                                $count_ov_dva_d && $count_ovp ? round(($count_ov_dva_d * 100
                                    / $count_ovp))
                                    : 0;

                            $activeSheet->setCellValue("A$row",
                                "% of Current Active Volunteers ($date_start_end)");
                            $activeSheet->setCellValue("B$row",
                                $percent_active > 0 ? $percent_active : '-');
                        }

                        $row++;
                        $activeSheet->mergeCells("A$row:B$row");
                        $activeSheet->getStyle("A$row:B$row")->getAlignment()
                            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $activeSheet->setCellValue("A$row", 'DATABASE STATS');
                        $activeSheet->getStyle("A$row")->getFont()->setBold(true);

                        $row++;
                        $count_o = Organisation::count();
                        $activeSheet->setCellValue("A$row", 'Number of sites contacted');
                        $activeSheet->setCellValue("B$row", $count_o);

                        $row++;
                        $count_ov = OrganisationVolunteer::count();
                        $activeSheet->setCellValue("A$row", 'Number of schedules updated');
                        $activeSheet->setCellValue("B$row", $count_ov);

                        $row++;
                        $percent_updated =
                            $count_ov && $count_o ? round(($count_ov * 100 / $count_o), 2) : 0;

                        $activeSheet->setCellValue("A$row", '% of schedules updated');
                        $activeSheet->setCellValue("B$row",
                            $percent_updated > 0.01 ? $percent_updated : '-');

                        if ($date_start_end) {
                            $row++;
                            $count_ov_d =
                                OrganisationVolunteer::where('created_at', '>=', $date_start)
                                    ->where('created_at', '<=', $date_end)->count();
                            $activeSheet->setCellValue("A$row",
                                "Number of schedules updated ($date_start_end)");
                            $activeSheet->setCellValue("B$row", $count_ov_d);

                            $row++;
                            $percent_updated =
                                $count_ov_d && $count_o ? round(($count_ov_d * 100 / $count_o), 2)
                                    : 0;

                            $activeSheet->setCellValue("A$row",
                                "% of schedules updated ($date_start_end)");
                            $activeSheet->setCellValue("B$row",
                                $percent_updated > 0 ? $percent_updated : '-');
                        }

                        $row++;
                        $count_ov = OrganisationVolunteer::where('status_id',
                            OrganisationVolunteer::STATUS_APPROVE)->count();
                        $activeSheet->setCellValue("A$row", 'Number of sites is approve');
                        $activeSheet->setCellValue("B$row", $count_ov);
                        if ($date_start_end) {
                            $row++;
                            $count_ov = OrganisationVolunteer::where('status_id',
                                OrganisationVolunteer::STATUS_APPROVE)
                                ->where('created_at', '>=', $date_start)
                                ->where('created_at', '<=', $date_end)->count();
                            $activeSheet->setCellValue("A$row",
                                "Number of sites is approve ($date_start_end)");
                            $activeSheet->setCellValue("B$row", $count_ov);
                        }

                        $row++;
                        $number_days = SettingService::getValueBySlug('volunteer_days_orgs', 90);
                        $volunteer_at = Date('Y-m-d', strtotime("-$number_days day")) . ' 00:00:00';

                        $count_o_va =
                            Organisation::where('volunteer_at', '>=', $volunteer_at)->count();
                        $activeSheet->setCellValue("A$row",
                            'Organizations are closed for search by volunteers');
                        $activeSheet->setCellValue("B$row", $count_o_va);

                        $activeSheet->getColumnDimension("A")->setAutoSize(true);
                        $activeSheet->getColumnDimension("B")->setAutoSize(true);

                        $myWorkSheet =
                            new Worksheet($spreadsheet, 'Volunteer Retention Stats');
                        $spreadsheet->addSheet($myWorkSheet, 1);
                        $spreadsheet->setActiveSheetIndex(1);
                        $activeSheet = $spreadsheet->getActiveSheet();

                        $row = 1;
                        $activeSheet->getStyle("A$row:B$row")->getAlignment()
                            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $activeSheet->setCellValue("A$row", 'Volunteer Sign-Up and Status');
                        $activeSheet->getStyle("A$row")->getFont()->setBold(true);

                        $row++;
                        $activeSheet->setCellValue("A$row", 'Total number of volunteer sign-ups');
                        $activeSheet->setCellValue("B$row", $count_ovp);

                        $row++;
                        $activeSheet->setCellValue("A$row", 'Current Active Volunteers (30 days)');
                        $activeSheet->setCellValue("B$row", $count_ov_dva);
                        if ($date_start_end && !empty($count_ov_dva_d)) {
                            $row++;
                            $activeSheet->setCellValue("A$row",
                                "Current Active Volunteers ($date_start_end)");
                            $activeSheet->setCellValue("B$row", $count_ov_dva_d);
                        }

                        $count_ov_dva_prev_year =
                            count(array_unique(OrganisationVolunteer::where('created_at', '>=',
                                Date('Y-m-d',
                                    strtotime('first day of january last year')) . ' 00:00:00')
                                ->where('created_at', '<=', Date('Y-m-d',
                                        strtotime('last day of december last year')) . ' 23:59:59')
                                ->get('volunteer_id')->pluck('volunteer_id')->toArray()));

                        if ($count_ov_dva_prev_year > 0) {
                            $row++;
                            $year = Date('Y', strtotime('first day of january last year'));
                            $activeSheet->setCellValue("A$row", $year . ' Active Volunteers');
                            $activeSheet->setCellValue("B$row", $count_ov_dva_prev_year);
                        }

                        $count_ov_dva_prev_year = count(array_unique(OrganisationVolunteer::
                        where('created_at', '>=', Date('Y-m-d',
                                strtotime('first day of january this year')) . ' 00:00:00')
                            ->where('created_at', '<=', Date('Y-m-d',
                                    strtotime('last day of december this year')) . ' 23:59:59')
                            ->get('volunteer_id')->pluck('volunteer_id')->toArray()));

                        $row++;
                        $year = Date('Y');
                        $activeSheet->setCellValue("A$row", $year . ' Active Volunteers');
                        $activeSheet->setCellValue("B$row", $count_ov_dva_prev_year);

                        for ($i = 1; $i <= 12; $i++) {
                            $month = strtolower(Date('F', strtotime(Date('Y') . '-' . $i . '-1')));
                            $count_ov_dva_month =
                                count(array_unique(OrganisationVolunteer::where('created_at', '>=',
                                    Date('Y-m-d',
                                        strtotime("first day of $month this year")) . ' 00:00:00')
                                    ->where('created_at', '<=', Date('Y-m-d',
                                            strtotime("last day of $month this year"))
                                        . ' 23:59:59')
                                    ->get('volunteer_id')->pluck('volunteer_id')->toArray()));
                            $row++;
                            $activeSheet->setCellValue("A$row",
                                Date('F', strtotime(Date('Y') . '-' . $i . '-1'))
                                . ' - Active Volunteers');
                            $activeSheet->setCellValue("B$row", $count_ov_dva_month);
                        }

                        $activeSheet->getColumnDimension("A")->setAutoSize(true);


                        $myWorkSheet =
                            new Worksheet($spreadsheet, 'Sites Called by Volunteer');
                        $spreadsheet->addSheet($myWorkSheet, 2);
                        $spreadsheet->setActiveSheetIndex(2);
                        $activeSheet = $spreadsheet->getActiveSheet();
                        $row = 1;
                        $activeSheet->setCellValue("A$row", 'Volunteer Name');
                        $activeSheet->setCellValue("B$row", '#Sites Called');
                        $activeSheet->getStyle("A$row")->getFont()->setBold(true);
                        $activeSheet->getStyle("B$row")->getFont()->setBold(true);
                        $total = 0;
                        foreach (VolunteerProfile::all() as $volunteer) {
                            $row++;
                            $name = trim($volunteer->first_name . ' ' . $volunteer->last_name);
                            $name = mb_strlen($name) >= 3 ? $name : $volunteer->email;
                            $name =
                                mb_strlen($name) >= 3 ? $name : ('Volunteer #' . $volunteer->id);
                            $count = OrganisationVolunteer::where('volunteer_id', $volunteer->id)
                                ->count();
                            $activeSheet->setCellValue("A$row", $name);
                            $activeSheet->setCellValue("B$row", $count);
                            $total += $count;
                        }

                        $row++;
                        $activeSheet->setCellValue("A$row", 'Grand Total');
                        $activeSheet->setCellValue("B$row", $total);

                        $activeSheet->getStyle("A$row")->getFont()->setBold(true);
                        $activeSheet->getStyle("B$row")->getFont()->setBold(true);

                        $activeSheet->getColumnDimension("A")->setAutoSize(true);
                        $activeSheet->getColumnDimension("B")->setAutoSize(true);
                        if ($date_start_end) {
                            $myWorkSheet =
                                new Worksheet($spreadsheet,
                                    "Called($date_start_end)");
                            $spreadsheet->addSheet($myWorkSheet, 2);
                            $spreadsheet->setActiveSheetIndex(2);
                            $activeSheet = $spreadsheet->getActiveSheet();
                            $row = 1;
                            $activeSheet->setCellValue("A$row", 'Volunteer Name');
                            $activeSheet->setCellValue("B$row", '#Sites Called');
                            $activeSheet->getStyle("A$row")->getFont()->setBold(true);
                            $activeSheet->getStyle("B$row")->getFont()->setBold(true);
                            $total = 0;
                            foreach (VolunteerProfile::all() as $volunteer) {
                                $row++;
                                $name = trim($volunteer->first_name . ' ' . $volunteer->last_name);
                                $name = mb_strlen($name) >= 3 ? $name : $volunteer->email;
                                $name =
                                    mb_strlen($name) >= 3 ? $name
                                        : ('Volunteer #' . $volunteer->id);
                                $count =
                                    OrganisationVolunteer::where('volunteer_id', $volunteer->id)
                                        ->where('created_at', '>=', $date_start)
                                        ->where('created_at', '<=', $date_end)
                                        ->count();
                                $activeSheet->setCellValue("A$row", $name);
                                $activeSheet->setCellValue("B$row", $count);
                                $total += $count;
                            }

                            $row++;
                            $activeSheet->setCellValue("A$row", "Grand Total");
                            $activeSheet->setCellValue("B$row", $total);

                            $activeSheet->getStyle("A$row")->getFont()->setBold(true);
                            $activeSheet->getStyle("B$row")->getFont()->setBold(true);

                            $activeSheet->getColumnDimension("A")->setAutoSize(true);
                            $activeSheet->getColumnDimension("B")->setAutoSize(true);
                        }

                        $_title = "Data ($date_start_end)";
                        $columns = [
                            'ID',
                            'Status',
                            'Created At',
                            'Volunteer ID',
                            'Volunteer Email',
                            'Volunteer Name',
                            'Please perform a web search to find the appropriate number and call it to confirm:',
                            'Unable to Complete',
                            'How confident are you in the answers you\'ve provided today?',
                            'Can you confirm that organisation name?',
                            'Can you confirm that you\'re located?',
                            'Program Types',
                            'Can you confirm that your website?',
                            'We don\'t have a website listed for you. Do you have one?',
                            'What are the hours that you are open to the public?',
                            'Do you provide any additional programs like SNAP sign-up support, support for new parents, WIC, fresh produce distribution, advocacy programs, or nutrition counseling, etc?',
                            'Phones',
                            'Emails',
                            'Finally, what is the best way to re-verify this information with you in the future? Here are a few options:',
                            'General pantry notes.',
                        ];
                        $myWorkSheet =
                            new Worksheet($spreadsheet, $_title);
                        $spreadsheet->addSheet($myWorkSheet, 3);
                        $spreadsheet->setActiveSheetIndex(3);
                        $activeSheet = $spreadsheet->getActiveSheet();
                        $count = 0;
                        foreach ($columns as $letter => $value) {
                            if (is_array($value) || !$value) {
                                $value = ' ';
                            }
                            $letter = $letters[$count] . 1;
                            $activeSheet->setCellValue($letter, $value);
                            $count++;
                        }


                        $writer = new Xlsx($spreadsheet);

                        //                        $publicDirectory = public_path('volunteer');
                        //                        if (!is_dir($publicDirectory)) {
                        //                            mkdir($publicDirectory, 0777, true);
                        //                        }

                        foreach ($items as $count_ov => $ov) {
                            $unable_complete = '';
                            if (!empty($ov->data['unable_complete'])
                                && is_array($ov->data['unable_complete'])
                            ) {
                                foreach ($ov->data['unable_complete'] as $uc) {
                                    if ($unable_complete) {
                                        $unable_complete .= ', ';
                                    }
                                    $unable_complete .= $uc;
                                }
                            }
                            $number_search = !empty($ov->data['number_search']['number'])
                                ? ($ov->data['number_search']['number'] . ' '
                                    . (!empty($ov->data['number_search']['description'])
                                        ? $ov->data['number_search']['description'] : ''))
                                : '-';

                            $types = '';
                            if (!empty($ov->data['program_types'])
                                && is_array($ov->data['program_types'])
                            ) {
                                foreach ($ov->data['program_types'] as $uc) {
                                    if ($pt = ProgramType::find($uc)) {
                                        if ($types) {
                                            $types .= ', ';
                                        }
                                        $types .= $pt->name;
                                    }
                                }
                            }
                            $hours = '';
                            if (!empty($ov->data['time'])) {
                                foreach ($ov->data['time'] as $day => $times) {
                                    if ($hours) {
                                        $hours .= ', ';
                                    }
                                    $hours .= "{$day}: ";
                                    $t = '';
                                    foreach ($times as $time) {
                                        if ($t) {
                                            $t .= ', ';
                                        }
                                        $d = !empty($time['day']) ? ($time['day'] . ' ') : '';
                                        $t .= "{$d}{$time['from_time_hour']}:{$time['from_time_min']} {$time['from_time_type']} - {$time['to_time_hour']}:{$time['to_time_min']} {$time['to_time_type']}";
                                    }
                                    $hours .= $t;
                                }
                            }

                            if (!empty($ov->data['by_appointment_only'])) {
                                $hours = 'By Appointment Only';
                            }

                            $phones = '';
                            if (!empty($data['contact']['phone'])
                                && count($data['contact']['phone'])
                            ) {
                                foreach ($data['contact']['phone'] as $phone) {
                                    if (!empty($phone['phone'])) {
                                        $phone =
                                            preg_replace('/[^0-9]+/', '',
                                                trim($phone['phone'], " '"));
                                        $ext = preg_replace('/[^0-9]+/', '',
                                            trim(!empty($phone['ext']) ? $phone['ext'] : '',
                                                " '"));
                                        if ($phones) {
                                            $phones .= ', ';
                                        }
                                        $phones .= $phone . ($ext ? (' ext.' . $ext) : '');
                                    }
                                }
                            }

                            $emails = '';
                            if (!empty($data['contact']['email'])
                                && count($data['contact']['email'])
                            ) {
                                foreach ($data['contact']['email'] as $email) {
                                    $email = trim($email);
                                    if (!empty($email)) {
                                        if ($emails) {
                                            $emails .= ', ';
                                        }
                                        $emails .= $email;
                                    }
                                }
                            }
                            $contact_again = '-';
                            if (!empty($ov->data['contact_again'])
                                && is_array($ov->data['contact_again'])
                            ) {
                                foreach ($ov->data['contact_again'] as $ca) {
                                    $contact_again = $ca;
                                }
                            }

                            $notes = '-';

                            if (!empty($ov->data['contact_notes']['note'])) {
                                $notes = $ov->data['contact_notes']['note'];
                            }
                            $row = [
                                $ov->id,
                                $this->baseModel::STATUSES[$ov->status_id],
                                Date('Y-m-d H:i:s', strtotime($ov->created_at)),
                                $ov->volunteer ? $ov->volunteer->id : '-',
                                $ov->volunteer ? $ov->volunteer->email : '-',
                                $ov->volunteer
                                    ? "{$ov->volunteer->first_name} {$ov->volunteer->last_name}"
                                    : '-',
                                $number_search,
                                $unable_complete ?? '-',
                                $this->getDataValue($ov->data, 'organisation_name'),
                                $this->getDataValue($ov->data, 'address'),
                                $types,
                                $this->getDataValue($ov->data, 'website'),
                                $this->getDataValue($ov->data, 'have_website'),
                                $hours ?? '-',
                                !empty($ov->data['additional_details'])
                                    ? $ov->data['additional_details'] : '',
                                $phones,
                                $emails,
                                $contact_again,
                                $notes,
                            ];
                            $count_letter = 0;
                            foreach ($row as $r) {
                                $activeSheet->setCellValue($letters[$count_letter] . ($count_ov
                                        + 2), $r);
                                $count_letter++;
                            }
                        }
                        $spreadsheet->getActiveSheet()->getStyle($letters[0] . "1:"
                            . $letters[count($letters) - 1] . "1")->getFont()->setBold(true);
                        for ($j = 0; $j < count($columns); $j++) {
                            $spreadsheet->getActiveSheet()->getColumnDimension($letters[$j])
                                ->setAutoSize(true);
                        }
                        $writer->setPreCalculateFormulas(false);
                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition: attachment; filename="export.xlsx"');
                        $writer->save('php://output');
                        die("Can't close php://output");
                    } else {
                        $request->session()->flash('alert_message', 'Data is empty');
                    }
                    break;
            }
        }

        return view("backend.$this->viewName.index",
            [
                'pagination' => $query->orderBy('id', 'desc')->paginate(10, ['*'], 'page',
                    $request->get('page')),
            ]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->get('answers', []);
            $item->update(['data' => $data]);
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'update':
                    $organisation = $item->organisation;
                    $update = [];

                    $data = $item->data;
                    if (!empty($data['organisation_name']['new_organisation_name'])) {
                        $update['title'] = $data['organisation_name']['new_organisation_name'];
                    }
                    if (!empty($data['website']['new_website'])) {
                        $update['website'] = $data['website']['new_website'];
                    }
                    if (!empty($data['contact_notes']['note'])) {
                        $update['notes'] = $data['contact_notes']['note'];
                    }

                    if (!empty($data['program_types']) && count($data['program_types'])) {
                        $program_types = [];
                        foreach ($data['program_types'] as $pt) {
                            $_id = preg_replace('/[^0-9]+/', '', trim($pt, " '"));
                            $pt = null;
                            if ($_id) {
                                $pt = ProgramType::find($_id);
                            }
                            if (!$pt) {
                                ProgramType::where('name', $pt)->first();
                            }
                            if ($pt) {
                                $program_types[] = $pt->id;
                            }
                        }
                        $organisation->types()->sync($program_types);
                    }

                    $phone_ids = [];

                    if (!empty($data['contact']['phone']) && count($data['contact']['phone'])) {
                        foreach ($data['contact']['phone'] as $phone) {
                            if (!empty($phone['phone'])) {
                                $phone =
                                    preg_replace('/[^0-9]+/', '',
                                        trim($phone['phone'], " '"));
                                $ext = preg_replace('/[^0-9]+/', '',
                                    trim(!empty($phone['ext']) ? $phone['ext'] : '',
                                        " '"));
                                $p = Phone::firstOrCreate(['phone' => $phone, 'ext' => $ext]);
                                $phone_ids[] = $p->id;
                            }
                        }
                    }

                    if (count($phone_ids)) {
                        $organisation->phones()->sync($phone_ids);
                    } else {
                        OrganisationPhone::where('organisation_id', $organisation->id)->delete();
                    }

                    $email_ids = [];

                    if (!empty($data['contact']['email']) && count($data['contact']['email'])) {
                        foreach ($data['contact']['email'] as $email) {
                            $email = trim($email);
                            if (!empty($email)) {
                                $e = Email::firstOrCreate(['email' => $email]);
                                $email_ids[] = $e->id;
                            }
                        }
                    }
                    if (count($email_ids)) {
                        $organisation->emails()->sync($email_ids);
                    } else {
                        OrganisationEmail::where('organisation_id', $organisation->id)->delete();
                    }

                    $hours = '';
                    if (!empty($data['time'])) {
                        foreach ($data['time'] as $day => $times) {
                            if ($hours) {
                                $hours .= ', ';
                            }
                            $hours .= "{$day}: ";
                            $t = '';
                            foreach ($times as $time) {
                                if ($t) {
                                    $t .= ', ';
                                }
                                $d = !empty($time['day']) ? ($time['day'] . ' ') : '';
                                $t .= "{$d}{$time['from_time_hour']}:{$time['from_time_min']} {$time['from_time_type']} - {$time['to_time_hour']}:{$time['to_time_min']} {$time['to_time_type']}";
                            }
                            $hours .= $t;
                        }
                        $update['hours'] = ['text' => $hours, 'time' => $data['time']];
                    }

                    if (!empty($data['by_appointment_only'])) {
                        $update['hours'] = ['text' => 'By Appointment Only'];
                    }

                    if (!empty($data['address']['new_address'])) {
                        $new_address =
                            LocationService::getDataByAddress("{$data['address']['new_address']}, {$organisation->address->country->code} {$organisation->address->zip}",
                                $organisation->address->country_id,
                                $organisation->address->state_id);
                        $address = Address::create([
                            'country_id' => $organisation->address->country_id,
                            'city_id' => $new_address['city_id'],
                            'state_id' => $organisation->address->state_id,
                            'address' => !empty($new_address['address']) ? $new_address['address']
                                : $organisation->address->address,
                            'zip' => !empty($new_address['zip']) ? $new_address['zip']
                                : $organisation->address->zip,
                            'lat' => $new_address['lat'],
                            'lng' => $new_address['lng'],
                        ]);
                        $update['address_id'] = $address->id;
                    }
                    $update['website'] = substr(!empty($update['website'])?$update['website']:'', 0, 191);
                    $organisation->update($update);
                    $item->update([
                        'status_id' => $this->baseModel::STATUS_APPROVE,
                        'approve_at' => Date('Y-m-d H:i:s')
                    ]);
                    break;
                case 'delete':
                    $item->update(['status_id' => $this->baseModel::STATUS_DELETE]);
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }

    public function getDataValue($data, $name)
    {
        $_name = $name;
        $name = $name == 'have_website' ? 'website' : $name;
        if (!empty($data[$name]['new_' . $name])) {
            return $data[$name]['new_' . $name];
        } elseif (!empty($data[$name]['correct'])
            || ($_name == 'have_website' && !empty($data[$name]['have_website']))
        ) {
            return 'Yes';
        } elseif (!empty($data[$name]['not_correct'])
            || ($_name == 'have_website' && !empty($data[$name]['not_have_website']))
        ) {
            return 'No';
        }
        return '-';
    }
}
