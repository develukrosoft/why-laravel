<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Services\ContactService;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public $baseModel = Contact::class;
    public $viewName = 'contact';

    public function index(Request $request)
    {
        return view("backend.$this->viewName.index",
            [
                'pagination' => $this->baseModel::paginate(10, ['*'], 'page',
                    $request->get('page')),
                'filters' => [
                    'attach_organisation_id' => $request->get('organisation_id'),
                    'attach_project_id' => $request->get('project_id'),
                ]
            ]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data =
                $request->only([
                    'title',
                    'first_name',
                    'middle_initial',
                    'last_name',
                    'address',
                    'phones',
                    'emails'
                ]);

            $item = ContactService::save($data, $item);

            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
