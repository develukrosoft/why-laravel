<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\City;
use App\Models\Country;
use App\Models\Email;
use App\Models\ImportFile;
use App\Models\Organisation;
use App\Models\OrganisationLog;
use App\Models\Phone;
use App\Models\ProgramType;
use App\Models\Project;
use App\Models\State;
use App\Services\HelperService;
use App\Services\ImportService;
use App\Services\LocationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImportController extends Controller
{

    public $baseModel = ImportFile::class;
    public $viewName = 'import';

    public $organisation_headers = [
        'title' => 'Title',
        'address' => 'Address',
        'city' => 'City',
        'state' => 'State',
        'zip' => 'Zip',
        'status_id' => 'Status',
        'type1' => 'Program Type #1',
        'type2' => 'Program Type #2',
        'program_type_description' => 'Program Type Description',
        'phone1' => 'Phone #1',
        'phone1ext' => 'Phone #1 Ext',
        'phone2' => 'Phone #2',
        'phone2ext' => 'Phone #2 Ext',
        'emails' => 'Email',
        'hours' => 'Hours',
        'website' => 'Website',
        'mission' => 'Mission Statement',
        'notes' => 'Notes',
        'confirmation_email_id' => 'Confirmation email to',
        'year_founded' => 'Year founded',
        'is_hcsraw' => 'Is Harry Chapin Self Reliance Award Winner',
        'is_receives_newsletter' => 'Receives Newsletter?',
        'annual_budget' => 'Annual budget',
        'number_volunteers' => 'Number of Volunteers',
        'number_part_time_staff' => 'Number of Part-Time Staff',
        'number_full_time_staff' => 'Number of Full-Time Staff',
    ];

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $file = $request->file('file');
            $type_id = $request->get('type_id');
            $item = null;
            if ($file && $file->getClientOriginalExtension() == 'csv') {
                $publicDirectory = public_path('/import');
                if (!is_dir($publicDirectory)) {
                    mkdir($publicDirectory, 0777, true);
                }
                $name = md5(strtotime('')) . '.csv';
                $file->move($publicDirectory, $name);
                $path = "$publicDirectory/$name";
                $total_tows = ImportService::getTotalRows($path);
                if ($total_tows >= 2) {
                    $item = $this->baseModel::create([
                        'type_id' => $type_id,
                        'path' => $path,
                        'current_row' => 1,
                        'total_rows' => $total_tows
                    ]);
                }
            }
            if (!$item) {
                return redirect(route("backend.$this->viewName.index"));
            }
            if ($item->type_id == ImportFile::TYPE_ORGANISATION) {
                return redirect(route("backend.$this->viewName.mapping",
                    ['item' => $item->id]));
            } else {
                return redirect(route("backend.$this->viewName.import",
                    ['item' => $item->id]));
            }
        }

        return view("backend.$this->viewName.index");
    }

    public function mapping(Request $request, ImportFile $item)
    {
        if ($request->isMethod('post')) {
            $item->update(['mapping' => $request->get('mapping', [])]);
            return redirect(route("backend.$this->viewName.import",
                ['item' => $item->id]));
        }

        $headers = ImportService::getHeaders($item->path);
        $fields = [];
        if ($item->type_id == ImportFile::TYPE_ORGANISATION) {
            $fields = $this->organisation_headers;
        }
        return view("backend.$this->viewName.mapping",
            ['headers' => $headers, 'fields' => $fields]);
    }

    public function import(Request $request, ImportFile $item)
    {
        if ($request->isMethod('post')) {
            if ($item->current_row >= $item->total_rows) {
                return [
                    'text' => '<li><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-check-circle fa-w-16 fa-2x"><path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" class=""></path></svg>Import success</li>',
                    'success' => true,
                    'item' => $item
                ];
            }
            $data = ImportService::getRow($item->path, $item->current_row);
            if ($data['data']) {
                if ($item->type_id == ImportFile::TYPE_ORGANISATION) {
                    $headers = $data['header'];
                    $data = $data['data'];
                    if (array_search('ID', $headers) !== false) {
                        $org = Organisation::find($data[array_search('ID', $headers)]);
                    } else {
                        $org =
                            Organisation::where('title', $data[array_search('Title', $headers)])
                                ->first();
                    }
                    $org_data = [];

                    $program_type_ids = [];
                    $email_ids = [];
                    $phone_ids = [];
                    $phones = [
                        'phone1' => ['phone' => '', 'ext' => ''],
                        'phone2' => ['phone' => '', 'ext' => '']
                    ];
                    $new_address = [
                        'address' => '',
                        'state' => '',
                        'city' => '',
                        'zip' => '',
                    ];
                    foreach ($item->mapping as $key => $number) {
                        if ($number) {
                            $value = $data[$number - 1];
                            switch ($key) {
                                case 'address':
                                case 'state':
                                case 'city':
                                case 'zip':
                                    $new_address[$key] = $value;
                                    break;
                                case 'is_hcsraw':
                                case 'is_receives_newsletter':
                                    $org_data[$key] = $value == 'Yes' ? 1 : 0;;
                                    break;
                                case 'status_id':
                                    if (array_search($value,
                                        Organisation::STATUSES)
                                    ) {
                                        $org_data[$key] =
                                            array_search($value,
                                                Organisation::STATUSES);
                                    }
                                    break;
                                case 'confirmation_email_id':
                                    if (array_search($value,
                                        Organisation::CONFIRMATION_EMAILS)
                                    ) {
                                        $org_data[$key] =
                                            array_search($value,
                                                Organisation::CONFIRMATION_EMAILS);
                                    }
                                    break;
                                case 'type1':
                                case 'type2':
                                    $pt = ProgramType::where(['name' => $value])->first();
                                    if ($pt) {
                                        $program_type_ids[] = $pt->id;
                                    }
                                    break;
                                case 'phone1':
                                case 'phone2':
                                    $phones[$key]['phone'] = $value;
                                    break;
                                case 'phone1ext':
                                    $phones['phone1']['ext'] = $value;
                                    break;
                                case 'phone2ext':
                                    $phones['phone2']['ext'] = $value;
                                    break;
                                case 'emails':
                                    $_items = explode(',', $value);
                                    foreach ($_items as $_i) {
                                        $email_ids[] =
                                            Email::firstOrCreate(['email' => $_i])->id;
                                    }
                                    break;
                                case 'hours':
                                    $org_data[$key] = ['text' => $value];
                                    break;
                                //                                case 'title':
                                //                                case 'hours':
                                //                                case 'annual_budget':
                                //                                case 'number_volunteers':
                                //                                case 'number_part_time_staff':
                                //                                case 'number_full_time_staff':
                                //                                case 'year_founded':
                                //                                case 'program_type_description':
                                //                                case 'mission':
                                //                                case 'notes':
                                //                                case 'website':
                                //                                    break;
                                default:
                                    $org_data[$key] = $value;
                                    break;
                            }
                        }
                    }
                    try {
                        $new_address =
                            LocationService::getDataByAddress("{$new_address['address']} {$new_address['state']} {$new_address['city']} {$new_address['zip']}");
                        $new_address = Address::create([
                            'address' => !empty($new_address['address'])
                                ? $new_address['address']
                                : '',
                            'zip' => !empty($new_address['zip'])
                                ? $new_address['zip']
                                : '',
                            'lat' => $new_address['lat'],
                            'lng' => $new_address['lng'],
                            'city_id' => $new_address['city_id'],
                            'state_id' => $new_address['state_id'],
                            'country_id' => $new_address['country_id'],
                        ]);
                        $org_data['address_id'] = $new_address->id;
                    } catch (\Exception $e) {

                    }
                    if (!count($program_type_ids)) {
                        $program_type_ids[] = ProgramType::firstOrCreate(['name' => 'Other'])->id;
                    }
                    foreach ($phones as $phone) {
                        $_i =
                            preg_replace('/[^0-9]+/', '', trim($phone['phone'], " '"));
                        if ($_i) {
                            $phone_ids[] =
                                Phone::firstOrCreate(['phone' => $_i, 'ext' => $phone['ext']])->id;
                        }
                    }
                    try {
                        if ($org) {
                            $org->update($org_data);
                            $text = "ORGANIZATION: '{$org->title}' find. Updated.";
                            OrganisationLog::create([
                                'title' => $org->title,
                                'organisation_id' => $org->id,
                                'type_id' => OrganisationLog::TYPE_IMPORT,
                                'user_id' => Auth::id()
                            ]);
                        } elseif ($new_address) {
                            $org = Organisation::create($org_data);
                            $text = "ORGANIZATION: '{$org->title}' created";
                            OrganisationLog::create([
                                'title' => $org->title,
                                'organisation_id' => $org->id,
                                'type_id' => OrganisationLog::TYPE_IMPORT,
                                'user_id' => Auth::id()
                            ]);
                        } else {
                            $text = "ORGANIZATION: ROW#{$item->current_row} not parsed address";
                        }

                        if ($org) {
                            $org->phones()->sync($phone_ids);
                            $org->emails()->sync($email_ids);
                            $org->types()->sync($program_type_ids);
                        }

                    } catch (\Exception $e) {
                        $text = "ORGANIZATION: ROW#{$item->current_row} not parsed";
                    }

                    if ($item->current_row < $item->total_rows) {
                        $item->update(['current_row' => $item->current_row + 1]);
                        return [
                            'text' => '<li><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-check-circle fa-w-16 fa-2x"><path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" class=""></path></svg>'
                                . $text . '</li>',
                            'item' => $item
                        ];
                    } else {
                        return [
                            'text' => '<li><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-check-circle fa-w-16 fa-2x"><path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" class=""></path></svg>Import success</li>',
                            'success' => true,
                            'item' => $item
                        ];
                    }
                }
                if ($item->type_id == ImportFile::TYPE_PROJECT) {
                    $headers = $data['header'];
                    $data = $data['data'];
                    $project = null;
                    if (array_search('ID', $headers) !== false) {
                        $project = Project::find(intval($data[array_search('ID', $headers)]));
                    }
                    if (!$project && array_search('Title', $headers) !== false) {
                        $project =
                            Project::where('title', $data[array_search('Title', $headers)])
                                ->first();
                    }

                    $project_data = [];
                    $address_data = [];

                    $address_fields = [
                        'country' => 'Country',
                        'state' => 'State',
                        'city' => 'City',
                        'address' => 'Address',
                        'zip' => 'Zip',
                    ];
                    $fields = [
                        'title' => 'Title',
                        'status_id' => 'Status',
                        'notes' => 'Notes',
                        'date_start' => 'Date start',
                        'date_end' => 'Date end',
                        'operation_days' => 'Operation days',
                        'breakfast' => 'Breakfast',
                        'lunch' => 'Lunch',
                        'snack' => 'Snack',
                        'dinner' => 'Dinner',
                    ];

                    foreach ($fields as $key => $title) {
                        $value = '';
                        if (in_array($key, ['status_id'])) {
                            if (array_search($title, $headers) !== false
                                && isset($data[array_search($title, $headers)])
                            ) {
                                $value = $data[array_search($title, $headers)];
                                switch ($key) {
                                    case 'status_id':
                                        if (array_search($value, Project::STATUSES)) {
                                            $value = array_search($value, Project::STATUSES);
                                        }
                                        break;
                                }
                            }
                        } elseif (in_array($key, ['breakfast', 'lunch', 'snack', 'dinner'])) {
                            if (array_search($title, $headers) !== false
                                && isset($data[array_search($title, $headers)])
                            ) {
                                $parts =
                                    HelperService::getTimes($data[array_search($title, $headers)]);
                                if ($parts[0] && $parts[1]) {
                                    $project_data[$key . '_from'] = $parts[0];
                                    $project_data[$key . '_to'] = $parts[1];
                                }
                            }
                        } elseif (in_array($key, ['date_start', 'date_end'])) {
                            if (array_search($title, $headers) !== false
                                && isset($data[array_search($title, $headers)])
                            ) {
                                $value =
                                    Date('Y-m-d', strtotime($data[array_search($title, $headers)]));
                            }
                        } elseif (in_array($key, ['operation_days'])) {
                            if (array_search($title, $headers) !== false
                                && isset($data[array_search($title, $headers)])
                            ) {
                                $operation_days = [];
                                $shorts = [
                                    'M' => 'Monday',
                                    'T' => 'Tuesday',
                                    'W' => 'Wednesday',
                                    'TH' => 'Thursday',
                                    'F' => 'Friday',
                                    'SA' => 'Saturday',
                                    'SU' => 'Sunday'
                                ];
                                $do = strtoupper($data[array_search($title, $headers)]);
                                foreach ($shorts as $short => $full) {
                                    if (strpos($do, strtoupper($full)) !== false) {
                                        $operation_days[] = $full;
                                    }
                                }
                                $value = $operation_days;
                            }
                        } else {
                            if (array_search($title, $headers) !== false
                                && isset($data[array_search($title, $headers)])
                            ) {
                                $value = $data[array_search($title, $headers)];
                            }
                        }
                        if ($value) {
                            $project_data[$key] = $value;
                        }
                    }

                    foreach ($address_fields as $key => $title) {
                        $value = null;
                        if (array_search($title, $headers) !== false
                            && isset($data[array_search($title, $headers)])
                        ) {
                            $value = $data[array_search($title, $headers)];
                        }
                        if ($value) {
                            $address_data[$key] = $value;
                        }
                    }
                    $new_address = null;
                    try {
                        if ($project && $project->address->address == $address_data['address']) {
                            $project_data['address_id'] = $project->address->id;
                        } else {
                            $country = Country::where('name', $address_data['country'])->first();
                            $address_data['country_id'] = $country->id;
                            unset($address_data['country']);
                            $state =
                                State::where([
                                    'name' => $address_data['state'],
                                    'country_id' => $country->id
                                ])
                                    ->first();
                            $address_data['state_id'] = $state->id;
                            unset($address_data['state']);
                            $city = City::firstOrCreate([
                                'name' => $address_data['city'],
                                'state_id' => $state->id
                            ]);
                            $address_data['city_id'] = $city->id;
                            unset($address_data['city']);
                            $new_address = Address::create($address_data);
                            $location =
                                LocationService::getDataByAddress("{$new_address->address}, {$new_address->country->code} {$new_address->zip}",
                                    $new_address->country_id, $new_address->state_id);
                            $new_address->update([
                                'address' => !empty($location['address']) ? $location['address']
                                    : $new_address->address,
                                'zip' => !empty($location['zip']) ? $location['zip']
                                    : $new_address->zip,
                                'lat' => $location['lat'],
                                'lng' => $location['lng'],
                                'city_id' => $location['city_id'],
                            ]);

                            $project_data['address_id'] = $new_address->id;
                        }
                    } catch (\Exception $e) {

                    }

                    try {
                        if ($project) {
                            $project->update($project_data);
                            $text = "PROJECT: '{$project->title}' find. Updated.";
                        } elseif ($new_address) {
                            $project = Project::create($project_data);
                            $text = "PROJECT: '{$project->title}' created";
                        } else {
                            $text = "PROJECT: ROW#{$item->current_row} not parsed address";
                        }
                    } catch (\Exception $e) {
                        $text = "ORGANIZATION: ROW#{$item->current_row} not parsed";
                    }

                    if ($item->current_row < $item->total_rows) {
                        $item->update(['current_row' => $item->current_row + 1]);
                        return [
                            'text' => '<li><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-check-circle fa-w-16 fa-2x"><path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" class=""></path></svg>'
                                . $text . '</li>',
                            'item' => $item
                        ];
                    } else {
                        return [
                            'text' => '<li><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-check-circle fa-w-16 fa-2x"><path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" class=""></path></svg>Import success</li>',
                            'success' => true,
                            'item' => $item
                        ];
                    }
                }

            }

        }
        return view("backend.$this->viewName.import", ['item' => $item]);
    }
}
