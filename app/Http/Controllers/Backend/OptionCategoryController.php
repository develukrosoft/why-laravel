<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OptionCategory;
use Illuminate\Http\Request;

class OptionCategoryController extends Controller
{
    public $baseModel = OptionCategory::class;
    public $viewName = 'optionCategory';

    public function index(Request $request)
    {
        return view("backend.$this->viewName.index",
            ['pagination' => $this->baseModel::paginate(10, ['*'], 'page', $request->get('page'))]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->only(['name', 'description', 'is_active', 'fontawesome_icon']);

            if ($item) {
                $item->update($data);
            } else {
                $item = $this->baseModel::create($data);
            }
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
