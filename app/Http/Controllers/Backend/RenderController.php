<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;

class RenderController extends Controller
{
    public function stateByCountry(Request $request)
    {
        $html = "<option value=''>---</option>";
        foreach (State::where('country_id', $request->get('country_id'))->orderBy('name')->get() as $s) {
            $html .= "<option value='{$s->id}'>{$s->name}</option>";
        }
        return $html;
    }

    public function cityByState(Request $request)
    {
        $html = "<option value=''>---</option>";
        foreach (City::where('state_id', $request->get('state_id'))->orderBy('name')->get() as $c) {
            $html .= "<option value='{$c->id}'>{$c->name}</option>";
        }
        return $html;
    }
}
