<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ModalMessage;
use Illuminate\Http\Request;

class ModalMessageController extends Controller
{
    public $baseModel = ModalMessage::class;
    public $viewName = 'modalMessage';

    public function index(Request $request)
    {
        return view("backend.$this->viewName.index",
            ['pagination' => $this->baseModel::paginate(10, ['*'], 'page', $request->get('page'))]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->only([
                'message',
                'start_at',
                'end_at',
                'show_type_id',
                'locale',
                'class_name',
                'position'
            ]);
            if ($item) {
                $item->update($data);
            } else {
                $item = $this->baseModel::create($data);
            }
            //            \Cache::delete('modals');
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
