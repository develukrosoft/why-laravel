<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('backend.index'));
        }
        $errors = [];
        $fields = [];
        if ($request->isMethod('post')) {
            $fields = $request->only(['email', 'password']);
            if (empty($fields['email'])) {
                $errors['email'] = 'Required';
            }
            if (empty($fields['password'])) {
                $errors['password'] = 'Required';
            }

            $user = User::where('email', $fields['email'])->orWhere('username', $fields['email'])
                ->first();
            if ($user) {
                if (!Hash::check($fields['password'], $user->password)) {
                    $errors['password'] = 'Invalid password';
                }
            } else {
                $errors['email'] = 'Bad credentials';
            }
            if (!count($errors)) {
                Auth::login($user);
                return redirect(route('backend.index'));
            }
        }
        return view('backend.auth.login', ['fields' => $fields, 'errors' => $errors]);
    }


    public function logout()
    {
        Auth::logout();
        return redirect(route('backend.auth.login'));
    }
}
