<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\CalendarEvent;
use App\Services\CalendarEventService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CalendarEventController extends Controller
{
    public $baseModel = CalendarEvent::class;
    public $viewName = 'calendarEvent';

    public function index(Request $request)
    {
        if ($action = $request->get('batch')) {
            switch ($action) {
                case 'delete':
                    $this->baseModel::whereIn('id', $request->get('ids'))
                        ->delete();
                    break;
            }
            return ['status' => 200];
        }

        if ($action = $request->get('action')) {
            switch ($action) {
                case 'export':
                    $columns = [
                        'ID',
                        'Title',
                        'Url',
                        'Type',
                        'Status',
                        'Date Event',
                        'Date Start',
                        'Date End',
                        'Contact ID',
                        'Address',
                        'Description',
                        'Image 1920x1080',
                        'Image 1080x1920',
                    ];
                    $headers = [
                        "Content-type" => "text/csv",
                        "Content-Disposition" => "attachment; filename=export.csv",
                        "Pragma" => "no-cache",
                        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                        "Expires" => "0"
                    ];
                    $callback = function () use ($columns) {
                        $file = fopen('php://output', 'w');
                        fputcsv($file, $columns);

                        foreach ($this->baseModel::cursor() as $item) {
                            $row = [
                                $item->id,
                                $item->title,
                                $item->url,
                                CalendarEvent::TYPES[$item->type_id],
                                CalendarEvent::STATUSES[$item->status_id],
                                Date('Y-m-d H:i', strtotime($item->date_at)),
                                Date('Y-m-d H:i', strtotime($item->date_start)),
                                Date('Y-m-d H:i', strtotime($item->date_end)),
                                $item->contact_id,
                                $item->address->FullAddress,
                                $item->description,
                                config('app.url') . '' . $item->image_1920x1080,
                                config('app.url') . '' . $item->image_1080x1920,
                            ];

                            fputcsv($file, $row);
                        }

                        fclose($file);
                    };
                    return response()->stream($callback, 200, $headers);
                    break;
            }
        }
        $query = $this->baseModel::query();

        $filters = $request->only([
            'country_id',
            'city_id',
            'state_id',
            'address',
            'zip',
            'title',
            'status_id',
            'date_start',
            'date_end',
            'per_page',
            'url',
            'type_id',
            'date_at',
        ]);

        if (!empty($filters['title'])) {
            $query->where('title', 'LIKE', '%' . $filters['title'] . '%');
        }

        if (!empty($filters['status_id'])) {
            $query->where('status_id', $filters['status_id']);
        }
        if (!empty($filters['date_start'])) {
            $query->where('date_start', '>=', $filters['date_start']);
        }
        if (!empty($filters['date_end'])) {
            $query->where('date_end', '<=', $filters['date_end']);
        }
        if (!empty($filters['date_at'])) {
            $query->where('date_at', 'LIKE', "%{$filters['date_at']}%");
        }

        if (!empty($filters['country_id']) || !empty($filters['address'])
            || !empty($filters['zip'])
        ) {
            $query->whereHas('address', function (Builder $query) use ($filters) {
                if (!empty($filters['country_id'])) {
                    $query->where('country_id', $filters['country_id']);
                }
                if (!empty($filters['state_id'])) {
                    $query->where('state_id', $filters['state_id']);
                }
                if (!empty($filters['city_id'])) {
                    $query->where('city_id', $filters['city_id']);
                }
                if (!empty($filters['address'])) {
                    $query->where('address', 'LIKE', '%' . $filters['address'] . '%');
                }
                if (!empty($filters['zip'])) {
                    $query->where('zip', $filters['zip']);
                }
            });
        }

        return view("backend.$this->viewName.index",
            [
                'pagination' => $query->paginate($request->get('per_page', 25), ['*'], 'page',
                    $request->get('page')),
                'filters' => $filters
            ]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $data = $request->only([
                'country_id',
                'city_id',
                'state_id',
                'address',
                'zip',
                'title',
                'status_id',
                'date_start',
                'date_end',
                'url',
                'type_id',
                'date_at',
                'contact',
                'phones',
                'emails',
            ]);
            $data['image_1920x1080'] = $request->file('image_1920x1080');
            $data['image_1080x1920'] = $request->file('image_1080x1920');
            $item = CalendarEventService::save($data, $item);

            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
