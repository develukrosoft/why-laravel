<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\EmailLog;
use App\Models\OrganisationLog;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function email(Request $request)
    {
        $filters = [
            'email' => $request->get('email'),
            'type_id' => $request->get('type_id'),
            'parent_type_id' => $request->get('parent_type_id'),
            'user_id' => $request->get('user_id'),
            'from_created_at' => $request->get('from_created_at'),
            'to_created_at' => $request->get('to_created_at'),
        ];
        $query = EmailLog::query();
        if ($filters['email']) {
            $query->where('email', 'LIKE', "%{$filters['email']}%");
        }
        if ($filters['type_id']) {
            $query->where('type_id', $filters['type_id']);
        }
        if ($filters['parent_type_id']) {
            $query->where('parent_type_id', $filters['parent_type_id']);
        }
        if ($filters['user_id']) {
            $query->where('user_id', $filters['user_id']);
        }
        if (!empty($filters['from_created_at']) || !empty($filters['to_created_at'])) {
            $query->whereBetween('created_at',
                [
                    !empty($filters['from_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['from_created_at'])) . ' 00:00:00')
                        : '2021-10-19 00:00:00',
                    !empty($filters['to_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['to_created_at'])) . ' 00:00:00')
                        : Date('Y-m-d') . ' 00:00:00',
                ]);
        }
        return view("backend.log.email",
            [
                'pagination' => $query->paginate(10, ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]);
    }

    public function orgDelete(Request $request)
    {
        $filters = [
            'title' => $request->get('title'),
            'user_id' => $request->get('user_id'),
            'from_created_at' => $request->get('from_created_at'),
            'to_created_at' => $request->get('to_created_at'),
        ];
        $query = OrganisationLog::where('type_id', OrganisationLog::TYPE_DELETE);
        if ($filters['title']) {
            $query->where('title', 'LIKE', "%{$filters['title']}%");
        }
        if ($filters['user_id']) {
            $query->where('user_id', $filters['user_id']);
        }
        if (!empty($filters['from_created_at']) || !empty($filters['to_created_at'])) {
            $query->whereBetween('created_at',
                [
                    !empty($filters['from_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['from_created_at'])) . ' 00:00:00')
                        : '2021-10-19 00:00:00',
                    !empty($filters['to_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['to_created_at'])) . ' 00:00:00')
                        : Date('Y-m-d') . ' 00:00:00',
                ]);
        }
        return view("backend.log.orgDelete",
            [
                'pagination' => $query->paginate(10, ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]);
    }

    public function orgUpdate(Request $request)
    {
        $filters = [
            'title' => $request->get('title'),
            'user_id' => $request->get('user_id'),
            'from_created_at' => $request->get('from_created_at'),
            'to_created_at' => $request->get('to_created_at'),
        ];
        $query = OrganisationLog::where('type_id', OrganisationLog::TYPE_UPDATE);
        if ($filters['title']) {
            $query->where('title', 'LIKE', "%{$filters['title']}%");
        }
        if ($filters['user_id']) {
            $query->where('user_id', $filters['user_id']);
        }
        if (!empty($filters['from_created_at']) || !empty($filters['to_created_at'])) {
            $query->whereBetween('created_at',
                [
                    !empty($filters['from_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['from_created_at'])) . ' 00:00:00')
                        : '2021-10-19 00:00:00',
                    !empty($filters['to_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['to_created_at'])) . ' 00:00:00')
                        : Date('Y-m-d') . ' 00:00:00',
                ]);
        }
        return view("backend.log.orgList",
            [
                'pagination' => $query->paginate(10, ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]);
    }
    public function orgImport(Request $request)
    {
        $filters = [
            'title' => $request->get('title'),
            'user_id' => $request->get('user_id'),
            'from_created_at' => $request->get('from_created_at'),
            'to_created_at' => $request->get('to_created_at'),
        ];
        $query = OrganisationLog::where('type_id', OrganisationLog::TYPE_IMPORT);
        if ($filters['title']) {
            $query->where('title', 'LIKE', "%{$filters['title']}%");
        }
        if ($filters['user_id']) {
            $query->where('user_id', $filters['user_id']);
        }
        if (!empty($filters['from_created_at']) || !empty($filters['to_created_at'])) {
            $query->whereBetween('created_at',
                [
                    !empty($filters['from_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['from_created_at'])) . ' 00:00:00')
                        : '2021-10-19 00:00:00',
                    !empty($filters['to_created_at']) ? (Date('Y-m-d',
                            strtotime($filters['to_created_at'])) . ' 00:00:00')
                        : Date('Y-m-d') . ' 00:00:00',
                ]);
        }
        return view("backend.log.orgList",
            [
                'pagination' => $query->paginate(10, ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]);
    }
}
