<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Organisation;
use App\Models\OrganisationVolunteer;
use App\Models\Project;
use App\Models\User;
use App\Models\VolunteerProfile;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function index(Request $request)
    {
        return view("backend.index",
            [
                'organisation' => [
                    'count' => Organisation::count(),
                    'items' => Organisation::limit(10)->latest()->get()
                ],
                'project' => [
                    'count' => Project::count(),
                    'items' => Project::limit(10)->latest()->get()
                ],
                'contact' => [
                    'count' => Contact::count(),
                    'items' => Contact::limit(10)->latest()->get()
                ],
                'user' => [
                    'count' => User::count(),
                    'items' => User::limit(10)->latest()->get()
                ],
                'volunteer' => [
                    'count' => VolunteerProfile::count(),
                    'items' => VolunteerProfile::limit(10)->latest()->get()
                ],
                'volunteer_organisation' => [
                    'count' => OrganisationVolunteer::count(),
                    'items' => OrganisationVolunteer::limit(10)->latest()->get()
                ],
            ]);
    }
}
