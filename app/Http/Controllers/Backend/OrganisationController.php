<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Mail\TemplateMail;
use App\Models\EmailLog;
use App\Models\Organisation;
use App\Models\OrganisationConfirmation;
use App\Models\OrganisationContact;
use App\Models\OrganisationLog;
use App\Services\MailService;
use App\Services\OrganisationService;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrganisationController extends Controller
{
    public $baseModel = Organisation::class;
    public $viewName = 'organisation';

    public function index(Request $request)
    {
        if ($action = $request->get('batch')) {
            switch ($action) {
                case 'approve':
                    $this->baseModel::whereIn('id', $request->get('ids'))
                        ->update(['status_id' => Organisation::TYPE_APPROVE]);
                    break;
                case 'pending':
                    $this->baseModel::whereIn('id', $request->get('ids'))
                        ->update(['status_id' => Organisation::TYPE_PENDING]);
                    break;
                case 'delete':
                    foreach (Organisation::whereIn('id', $request->get('ids'))->cursor() as $org) {
                        OrganisationLog::create([
                            'title' => $org->title,
                            'type_id' => OrganisationLog::TYPE_DELETE,
                            'user_id' => Auth::id()
                        ]);
                    }
                    $this->baseModel::whereIn('id', $request->get('ids'))
                        ->delete();
                    break;
                case 'mass_mail':
                    $mail_template = $request->get('mail_template');
                    $items = $this->baseModel::whereIn('id', $request->get('ids'))
                        ->get();
                    foreach ($items as $item) {
                        $emails = [];
                        if ($item->confirmation_email_id == Organisation::CONFIRMATION_EMAIL_ORG
                            || $item->confirmation_email_id == Organisation::CONFIRMATION_EMAIL_BOTH
                        ) {
                            foreach ($item->emails as $email) {
                                $emails[$email->email] = $email->email;
                            }
                        }
                        if ($item->confirmation_email_id == Organisation::CONFIRMATION_EMAIL_CONTACT
                            || $item->confirmation_email_id == Organisation::CONFIRMATION_EMAIL_BOTH
                        ) {
                            foreach ($item->contacts as $contact) {
                                foreach ($contact->emails as $email) {
                                    $emails[$email->email] = $email->email;
                                }
                            }
                        }
                        if (count($emails)) {
                            $hash = md5($item->id . $item->created_at . strtotime('now'));
                            OrganisationConfirmation::create([
                                'organisation_id' => $item->id,
                                'hash' => $hash,
                                'type_id' => OrganisationConfirmation::TYPE_UPDATE
                            ]);
                            Mail::to($emails)->send(new TemplateMail(
                                'Organization Update Request',
                                MailService::getMailByTemplate($mail_template, [
                                    'name' => $item->title,
                                    'update_request_link' => route('front.organisation.update',
                                        ['hash' => $hash])
                                ])
                            ));
                            foreach ($emails as $email) {
                                EmailLog::create([
                                    'email' => $email,
                                    'type_id' => EmailLog::TYPE_ORG_UPDATE_REQUEST,
                                    'parent_type_id' => EmailLog::PARENT_TYPE_ORG,
                                    'parent_id' => $item->id,
                                    'user_id' => Auth::id()
                                ]);
                            }

                        }
                    }
                    break;
            }
            return ['status' => 200];
        }

        $query = $this->baseModel::query();
        $filters = $request->only([
            'year_founded',
            'is_front',
            'is_hcsraw',
            'is_receives_newsletter',
            'annual_budget_from',
            'annual_budget_to',
            'number_volunteers_from',
            'number_volunteers_to',
            'number_part_time_staff_from',
            'number_part_time_staff_to',
            'number_full_time_staff_from',
            'number_full_time_staff_to',
            'country_id',
            'city_id',
            'state_id',
            'title',
            'phone',
            'email',
            'zip',
            'program_type_id',
            'address',
            'options',
            'status_id',
            'per_page',
            'mail_exists',
        ]);
        $orderField = $request->get('s_f', 'id');
        $orderBy = $request->get('s_o', 'desc');
        $orderBy = in_array($orderBy, ['asc', 'desc']) ? $orderBy : 'desc';
        $query->orderBy($orderField, $orderBy);
        $filters['s_f'] = $orderField;
        $filters['s_o'] = $orderBy;
        $blanks = [];
        if ($request->get('blank')) {
            $filters = [];
            $blanks = $request->only(['address', 'zip', 'phone', 'email', 'website', 'pending']);
            if (!empty($blanks['address'])) {
                $query->whereHas('address', function (Builder $query) {
                    $query->whereNull('address');
                });
            }
            if (!empty($blanks['zip'])) {
                $query->whereHas('address', function (Builder $query) {
                    $query->whereNull('zip');
                });
            }
            if (!empty($blanks['phone'])) {
                $query->whereHas('phones', function (Builder $query) {
                    $query->having(DB::raw('count(*)'), '=', 0);
                });
            }
            if (!empty($blanks['email'])) {
                $query->whereHas('emails', function (Builder $query) {
                    $query->having(DB::raw('count(*)'), '=', 0);
                });
            }
            if (!empty($blanks['website'])) {
                $query->whereNull('website');
            }
            if (!empty($blanks['pending'])) {
                $query->where('status_id', Organisation::TYPE_PENDING);
            }
        }

        if ($table_status_id = $request->get('table_status_id')) {
            $filters['status_id'] = $table_status_id;
        }
        if (!empty($filters['mail_exists'])) {
            $query->whereHas('emails', function (Builder $query) {
                $query->having(DB::raw('count(*)'), '!=', 0);
            });
        }
        if (!empty($filters['is_hcsraw']) && $filters['is_hcsraw'] == 'unset') {
            unset($filters['is_hcsraw']);
        }

        if (!empty($filters['is_front']) && $filters['is_front'] == 'unset') {
            unset($filters['is_front']);
        }

        if (!empty($filters['status_id']) && $filters['status_id'] == 'unset'
            && empty($blanks['pending'])
        ) {
            unset($filters['status_id']);
        }

        if (!empty($filters['is_receives_newsletter'])
            && $filters['is_receives_newsletter'] == 'unset'
        ) {
            unset($filters['is_receives_newsletter']);
        }

        $whereBetween = [
            'annual_budget',
            'number_volunteers',
            'number_part_time_staff',
            'number_full_time_staff'
        ];
        foreach ($whereBetween as $item) {
            if (!empty($filters[$item . '_from']) || !empty($filters[$item . '_to'])) {
                $query->whereBetween($item,
                    [
                        !empty($filters[$item . '_from']) ? intval($filters[$item . '_from']) : 0,
                        !empty($filters[$item . '_to']) ? intval($filters[$item . '_to'])
                            : 999999999,
                    ]);
            }
        }

        if (isset($filters['is_receives_newsletter'])) {
            $query->where('is_receives_newsletter', $filters['is_receives_newsletter']);
        }

        if (isset($filters['is_hcsraw'])) {
            $query->where('is_hcsraw', $filters['is_hcsraw']);
        }

        if (isset($filters['is_front'])) {
            $query->where('is_front', $filters['is_front']);
        }

        if (!empty($filters['title'])) {
            $query->where('title', 'LIKE', '%' . $filters['title'] . '%');
        }

        if (!empty($filters['year_founded'])) {
            $query->where('year_founded', $filters['year_founded']);
        }

        if (!empty($filters['status_id'])) {
            $query->where('status_id', $filters['status_id']);
        }

        if (!empty($filters['country_id']) || !empty($filters['address'])
            || !empty($filters['zip'])
        ) {
            $query->whereHas('address', function (Builder $query) use ($filters) {
                if (!empty($filters['country_id'])) {
                    $query->where('country_id', $filters['country_id']);
                }
                if (!empty($filters['state_id'])) {
                    $query->where('state_id', $filters['state_id']);
                }
                if (!empty($filters['city_id'])) {
                    $query->where('city_id', $filters['city_id']);
                }
                if (!empty($filters['address'])) {
                    $query->where('address', 'LIKE', '%' . $filters['address'] . '%');
                }
                if (!empty($filters['zip'])) {
                    $query->where('zip', $filters['zip']);
                }
            });
        }

        if (!empty($filters['program_type_id'])) {
            $query->whereHas('types', function (Builder $query) use ($filters) {
                $query->where('type_id', $filters['program_type_id']);
            });
        }

        if (!empty($filters['phone'])) {
            $query->whereHas('phones', function (Builder $query) use ($filters) {
                $query->where('phone', 'LIKE', "%{$filters['phone']}%");
            });
        }

        if (!empty($filters['email'])) {
            $query->whereHas('emails', function (Builder $query) use ($filters) {
                $query->where('email', 'LIKE', "%{$filters['email']}%");
            });
        }

        if (!empty($filters['options'])) {
            $query->whereHas('options', function (Builder $query) use ($filters) {
                $query->whereIn('option_id', $filters['options'])
                    ->having(DB::raw('count(*)'), '=', count($filters['options']));
            });
        }
        $fields = [
            'title' => 'Title',
            'status_id' => 'Status',
            'website' => 'Website',
            'annual_budget' => 'Annual Budget',
            'year_founded' => 'Year Founded',
            'number_volunteers' => 'Number Volunteers',
            'number_part_time_staff' => 'Number part time staff',
            'number_full_time_staff' => 'Number full time staff',
            'is_receives_newsletter' => 'Is receives newsletter',
            'is_hcsraw' => 'Is hcsraw',
            'hours' => 'Hours',
            'mission' => 'Mission',
            'notes' => 'Notes',
            'program_type_description' => 'Program type description',
            'is_front' => 'Is front',
            'address_country' => 'Address - Country',
            'address_state' => 'Address - State',
            'address_city' => 'Address - City',
            'address' => 'Address',
            'address_zip' => 'Zip',
            'address_lat' => 'Latitude',
            'address_lng' => 'Longitude',
            'phones' => 'Phones',
            'emails' => 'Emails',
            'confirmation_email_id' => 'Confirmation Email',
            'types' => 'Program Types',
            'created_at' => 'Created At',
        ];
        if ($action = $request->get('action')) {
            switch ($action) {
                case 'export':
                    $columns = array_merge(['id' => 'ID'], $request->get('fields', []));

                    //                    $columns = [
                    //                        'ID',
                    //                        'Title',
                    //                        'Status',
                    //                        'Country',
                    //                        'State',
                    //                        'City',
                    //                        'Address',
                    //                        'Zip',
                    //                        'Program Type',
                    //                        'Program Type Description',
                    //                        'Website',
                    //                        'Phone',
                    //                        'Email',
                    //                        'Hours',
                    //                        'Mission Statement',
                    //                        'Notes',
                    //                        'Confirmation email to',
                    //                        'Year founded',
                    //                        'Is Harry Chapin Self Reliance Award Winner',
                    //                        'Receives Newsletter?',
                    //                        'Annual budget',
                    //                        'Number of Volunteers',
                    //                        'Number of Part-Time Staff',
                    //                        'Number of Full-Time Staff',
                    //                    ];
                    $headers = [
                        "Content-type" => "text/csv",
                        "Content-Disposition" => "attachment; filename=export.csv",
                        "Pragma" => "no-cache",
                        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                        "Expires" => "0"
                    ];
                    $callback = function () use ($columns, $query) {
                        $file = fopen('php://output', 'w');
                        fputcsv($file, $columns);

                        foreach ($query->cursor() as $org) {
                            $types = '';
                            foreach ($org->types as $id => $type) {
                                $types .= $type->name .
                                    (count($org->types) - 1 === $id ? '' : ', ');
                            }
                            $phones = '';
                            foreach ($org->phones as $id => $phone) {
                                $phones .= $phone->phone .
                                    (count($org->phones) - 1 === $id ? '' : ', ');
                            }
                            $emails = '';
                            foreach ($org->emails as $id => $email) {
                                $emails .= $email->email .
                                    (count($org->emails) - 1 === $id ? '' : ', ');
                            }
                            $row = [
                                $org->id,
                                $org->title,
                                Organisation::STATUSES[$org->status_id],
                                $org->address->country->name,
                                $org->address->state->name,
                                $org->address->city->name,
                                $org->address->address,
                                $org->address->zip,
                                $types,
                                $org->program_type_description,
                                $org->website,
                                $phones,
                                $emails,
                                !empty($org->hours->text) ? $org->hours->text : '',
                                $org->mission,
                                $org->notes,
                                Organisation::CONFIRMATION_EMAILS[$org->confirmation_email_id],
                                $org->year_founded,
                                $org->is_hcsraw ? 'Yes' : 'No',
                                $org->is_receives_newsletter ? 'Yes' : 'No',
                                $org->annual_budget,
                                $org->number_volunteers,
                                $org->number_part_time_staff,
                                $org->number_full_time_staff,
                            ];
                            $row = [];
                            foreach ($columns as $column => $name) {
                                if ($column === 'phones') {
                                    $row[] = $phones;
                                } elseif ($column === 'emails') {
                                    $row[] = $emails;
                                } elseif ($column === 'address_country') {
                                    $row[] = $org->address->country->name;
                                } elseif ($column === 'address_state') {
                                    $row[] = $org->address->state->name;
                                } elseif ($column === 'address_city') {
                                    $row[] = $org->address->city->name;
                                } elseif ($column === 'address') {
                                    $row[] = $org->address->address;
                                } elseif ($column === 'address_zip') {
                                    $row[] = $org->address->zip;
                                } elseif ($column === 'address_lat') {
                                    $row[] = $org->address->lat;
                                } elseif ($column === 'address_lng') {
                                    $row[] = $org->address->lng;
                                } elseif ($column === 'created_at') {
                                    $row[] = Date('Y-m-d H:i:s', strtotime($org->created_at));
                                } elseif ($column === 'types') {
                                    $row[] = $types;
                                } elseif ($column === 'confirmation_email_id') {
                                    $row[] =
                                        Organisation::CONFIRMATION_EMAILS[$org->confirmation_email_id];
                                } elseif ($column === 'is_hcsraw') {
                                    $row[] = $org->is_hcsraw ? 'Yes' : 'No';
                                } elseif ($column === 'is_receives_newsletter') {
                                    $row[] = $org->is_receives_newsletter ? 'Yes' : 'No';
                                } elseif ($column === 'status_id') {
                                    $row[] = Organisation::STATUSES[$org->status_id];
                                } elseif ($column === 'hours') {
                                    $row[] = !empty($org->hours->text) ? $org->hours->text : '';
                                } else {
                                    $row[] = $org->$column;
                                }
                            }
                            fputcsv($file, $row);
                        }
                        fclose($file);
                    };
                    return response()->stream($callback, 200, $headers);
                    break;
                case 'override_pending':
                    $this->baseModel::where(['status_id' => Organisation::TYPE_PENDING])
                        ->update(['status_id' => Organisation::TYPE_APPROVE]);
                    return redirect(route("backend.$this->viewName.index"));
                    break;
            }
        }

        return view("backend.$this->viewName.index",
            [
                'pagination' => $query->paginate($request->get('per_page', 25), ['*'],
                    'page', $request->get('page')),
                'filters' => $filters,
                'blanks' => $blanks,
                'fields' => $fields,
            ]);
    }

    public function form(Request $request, $id = null)
    {
        $item = $this->baseModel::find($id);
        if ($request->isMethod('post')) {
            $item = OrganisationService::form($request, $item);
            $request->session()->flash('alert_message', 'Save success');
            return redirect(route("backend.$this->viewName.form.id", $item->id));
        }
        return view("backend.$this->viewName.form", ['item' => $item]);
    }

    public function renderPhone()
    {
        return view("backend.$this->viewName.partials.phone")->render();
    }

    public function renderEmail()
    {
        return view("backend.$this->viewName.partials.email")->render();
    }

    public function action(Request $request, $id)
    {
        $item = $this->baseModel::find($id);
        if ($item) {
            switch ($request->get('action')) {
                case 'approve':
                    $item->update(['status_id' => $this->baseModel::TYPE_APPROVE]);
                    break;
                case 'delete':
                    OrganisationLog::create([
                        'title' => $item->title,
                        'type_id' => OrganisationLog::TYPE_DELETE,
                        'user_id' => Auth::id()
                    ]);
                    $item->delete();
                    break;
                case 'delete_contact':
                    OrganisationContact::where([
                        'organisation_id' => $id,
                        'contact_id' => $request->get('contact_id')
                    ])->delete();
                    return redirect(route("backend.$this->viewName.form.id", ['id' => $id]));
                    break;
                case 'attach_contact':
                    OrganisationContact::create([
                        'organisation_id' => $id,
                        'contact_id' => $request->get('contact_id')
                    ]);
                    return redirect(route("backend.$this->viewName.form.id", ['id' => $id]));
                    break;
            }
        }
        return redirect(route("backend.$this->viewName.index"));
    }
}
