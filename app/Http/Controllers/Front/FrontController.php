<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Organisation;
use App\Models\Phone;
use App\Models\Project;
use App\Services\LocationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class FrontController extends Controller
{

    public function setLocale($locale)
    {
        $locale = !in_array($locale, ['en', 'es']) ? 'en' : $locale;
        Cookie::queue('locale', $locale);
        return redirect()->back();
    }

    public function sms(Request $request)
    {
/*        $xml = '<?xml version="1.0" encoding="UTF-8"?>';*/
//        $xml .= '<Response>';
//        $xml .= '<Message>Error</Message>';
//        $xml .= '</Response>';
//
//        return response($xml, 200)->header('Content-Type', 'text/xml');
        $body = explode(' ', $request->get('Body'));

        $type = 'org';
        $center_zip = false;

        if (!isset($body[1])) {
            $center_zip = $body[0];
            /*$tmp = strtolower(trim($body[0]));
            $body[1] = (int)str_replace('summer', '', $tmp);
            $body[0] ='summer';*/
        }

        if (sizeof($body) > 1 && strtolower(trim($body[0])) == 'summer') {
            $type = 'summermeals';//trim($body[0])!=''?trim($body[0]):false;
            $center_zip = trim($body[1]) != '' ? trim($body[1]) : false;
        } else {
            $center_zip = trim($body[0]);
        }
        if ($request->get('debug')) {
            $center_zip = $request->get('debug');
        }
        if ($request->get('debug_summermeals')) {
            $type = 'summermeals';
            $center_zip = $request->get('debug_summermeals');
        }
        $center_zip =
            preg_replace('/[^0-9]+/', '',
                trim($center_zip, " '"));

        //die($zip);

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<Response>';
        if ($type != false && $center_zip != false) {
            try {
                $type_query = 'organisations';
                if ($type == 'summermeals') {
                    $query = Project::query();
                    $type_query = 'projects';
                } else {
                    $query = Organisation::where('status_id', Organisation::TYPE_APPROVE);
                }

                $radius_quantity = 3959;

                $distance = 25;
                $res = LocationService::getDataByAddress($center_zip);
                $lat = $res['lat'];
                $lng = $res['lng'];
                if ($lat && $lng) {
                    $query->whereHas('address', function (Builder $query) use (
                        $lat,
                        $lng,
                        $radius_quantity,
                        $center_zip,
                        $distance
                    ) {
                        $whereRaw =
                            "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
                        $query->selectRaw("{$whereRaw} AS distance")
                            ->whereRaw("{$whereRaw} < ?", [$distance])
                            ->orderByRaw('FIELD (id, ' . $center_zip . ') ASC');
                    });
                    $query->join('addresses', 'addresses.id', '=', $type_query . '.address_id')
                        ->orderByRaw('FIELD (addresses.zip, ' . $center_zip . ') DESC')
                        ->select($type_query . '.*');
                }
                $results = $query->limit(9)->get();
                $response = array();
                if ($size = $results->count()) {
                    $response[] = 'Nearest {{SIZE}}:';
                    $items = [];
                    foreach ($results as $item) {
                        if (empty($items[$item->address->zip])) {
                            $items[$item->address->zip] = [];
                        }
                        $items[$item->address->zip][] = $item;
                    }
                    $count = 0;
                    if (!empty($items[$center_zip])) {
                        foreach ($items[$center_zip] as $id => $item) {
                            $count++;
                            $line = '#' . ($count) . ' ' . $item->title . "\n";
                            $line .= $item->address->ShortAddress . "\n";
                            $phones = $item->phones;
                            if ($phones && sizeof($phones) > 0) {
                                $line .= 'tel: ';
                                $_phones = array();
                                foreach ($phones as $phone) {
                                    $_phones[] =
                                        preg_replace('/(\d{3})(\d{3})(\d{4})/m', '(\\1) \\2-\\3',
                                            $phone->phone);
                                }
                                $line .= implode(', ', $_phones) . "\n";
                            }
                            $response[] = $line;
                        }
                    }
                    foreach ($items as $zip => $results) {
                        if ($zip !== $center_zip) {
                            foreach ($results as $id => $item) {
                                $count++;
                                $line = '#' . ($count) . ' ' . $item->title . "\n";
                                $line .= $item->address->ShortAddress . "\n";
                                $phones = $item->phones;
                                if ($phones && sizeof($phones) > 0) {
                                    $line .= 'tel: ';
                                    $_phones = array();
                                    foreach ($phones as $phone) {
                                        $_phones[] =
                                            preg_replace('/(\d{3})(\d{3})(\d{4})/m',
                                                '(\\1) \\2-\\3',
                                                $phone->phone);
                                    }
                                    $line .= implode(', ', $_phones) . "\n";
                                }
                                $response[] = $line;
                            }
                        }
                    }
                }
                $xml .= '<Message>';
                $xml .= ' If you cannot find a site near you please call WhyHunger at 1.800.548.6479'
                    . "\n";
                if (is_array($response) && sizeof($response) > 0) {
                    foreach ($response as $rsp) {
                        $rsp = str_replace('{{SIZE}}', $size, $rsp);
                        if (mb_strlen($xml) < 1400) {
                            $xml .= htmlspecialchars($rsp, ENT_COMPAT, 'UTF-8') . "\n";
                        }
                    }
                } else {
                    $xml .= ($type == 'summermeals' ? 'Site' : 'Organization') . 's not found';
                }
                $xml .= '</Message>';
            } catch (\Exception $e) {
                $xml .= '<Message>' . $e->getMessage() . '</Message>';
            }
        } else {
            $xml .= '<Message>Incorrect SMS format</Message>';
        }
        $xml .= '</Response>';

        return response($xml, 200)->header('Content-Type', 'text/xml');
    }

    public function dataOrganisations(Request $request)
    {
        $data = [];
        $center_zip =
            preg_replace('/[^0-9]+/', '',
                trim($request->get('zip'), " '"));
        $offset = $request->get('offset', 0);
        $count = $request->get('count', 10);
        $count = $count > 100 ? 100 : $count;
        //        try {
        $query = Organisation::where('status_id', Organisation::TYPE_APPROVE);

        $radius_quantity = 3959;

        $distance = 25;
        $res = LocationService::getDataByAddress($center_zip);
        $lat = $res['lat'];
        $lng = $res['lng'];
        if ($lat && $lng) {
            $query->whereHas('address', function (Builder $query) use (
                $lat,
                $lng,
                $radius_quantity,
                $center_zip,
                $distance
            ) {
                $whereRaw =
                    "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
                $query->selectRaw("{$whereRaw} AS distance")
                    ->whereRaw("{$whereRaw} < ?", [$distance])
                    ->orderByRaw('FIELD (id, ' . $center_zip . ') ASC');
            });
            $query->join('addresses', 'addresses.id', '=', 'organisations.address_id')
                ->orderByRaw('FIELD (addresses.zip, ' . $center_zip . ') DESC')
                ->select('organisations.*');
        }
        $results = $query->limit($count)->offset($offset)->get();
        if ($size = $results->count()) {
            foreach ($results as $item) {
                $emails = [];
                $phones = [];
                foreach ($item->phones as $phone) {
                    $phones[] = array(
                        'phone' => $phone->phone,
                        'ext' => $phone->ext,
                        'type' => Phone::TYPES[$phone->type]
                    );
                }
                foreach ($item->emails as $email) {
                    $emails[] = $email->email;
                }
                $org = array(
                    'name' => $item->title,
                    'annualBudget' => $item->annual_budget,
                    'yearFounded' => $item->year_founded,
                    'website' => $item->website,
                    'notes' => $item->notes,
                    'otherDescription' => $item->program_type_description,
                    'oldId' => $item->old_id,
                    'missionStatement' => $item->mission,
                    'foodAcception' => $item->hours,
                    'volunteers' => $item->number_volunteers,
                    'partTime' => $item->number_part_time_staff,
                    'fullTime' => $item->number_full_time_staff,
                    'status' => Organisation::STATUSES[$item->status_id],
                    'hcsra' => $item->is_hcsraw ? true : false,
                    'receives_newsletter' => $item->is_receives_newsletter ? true : false,
                    'phones' => $phones,
                    'emails' => $emails,
                    'created_at' => Date('Y-m-d', strtotime($item->created_at)),
                    'updated_at' => Date('Y-m-d', strtotime($item->updated_at)),
                );

                if ($item->address) {
                    $org['zip'] = $item->address->zip;
                    $org['state'] = $item->address->state->name;
                    $org['state_code'] = $item->address->state->code;
                    $org['country'] = $item->address->country->name;
                    $org['city'] = $item->address->city->name;
                    $org['short_address'] = $item->address->ShortAddress;
                    $org['address_text'] = $item->address->FullAddress;
                    $org['latitude'] = $item->address->lat;
                    $org['lng'] = $item->address->lng;
                }
                $org['types'] = $item->types()->get()->pluck('name')->toArray();
                $data[] = $org;
            }

        }
        //        } catch (\Exception $e) {
        //        }
        return response($data, 200);
    }
}
