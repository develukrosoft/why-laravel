<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\CalendarEvent;
use App\Services\CalendarEventService;
use App\Services\LocationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CalendarEventController extends Controller
{
    public $baseModel = CalendarEvent::class;
    public $viewName = 'calendarEvent';

    public function index(Request $request)
    {
        $query = $this->baseModel::where(
            [
                ['status_id', CalendarEvent::STATUS_APPROVE],
                ['date_start', '<=', Date('Y-m-d H:i:s')],
                ['date_end', '>=', Date('Y-m-d H:i:s')],
            ]
        );
        $filters = $request->only([
            'center_zip',
            'country_id',
            'city_id',
            'state_id',
            'title',
            'zip',
            'address',
            'radius_quantity',
            'date_at'
        ]);
        if ($request->get('s')) {
            $filters['s'] = 1;
            $center_zip =
                preg_replace('/[^0-9]+/', '', $request->get('center_zip'));
            if ($center_zip) {

                $radius_quantity =
                    !empty($filters['radius_quantity']) ? $filters['radius_quantity'] : 3959;

                $distance = $request->get('distance', 25);
                $distance = $distance >= 5 ? $distance : 5;
                $filters['distance'] = $distance;
                $res = LocationService::getDataByAddress($center_zip);
                $lat = $res['lat'];
                $lng = $res['lng'];

                if ($lat && $lng) {
                    $query->whereHas('address', function (Builder $query) use (
                        $lat,
                        $lng,
                        $radius_quantity,
                        $center_zip,
                        $distance
                    ) {
                        $whereRaw =
                            "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
                        $query->selectRaw("{$whereRaw} AS distance")
                            ->whereRaw("{$whereRaw} < ?", [$distance])
                            ->orderByRaw('FIELD (zip, ' . $center_zip . ') DESC');
                    });
                    $query->join('addresses', 'addresses.id', '=', 'calendar_events.address_id')
                        ->orderByRaw('FIELD (addresses.zip, ' . $center_zip . ') DESC')
                        ->select('calendar_events.*');
                } else {
                    $query->where('id', -1);
                }

            } else {
                if (!empty($filters['title'])) {
                    $query->where('title', 'LIKE', '%' . $filters['title'] . '%');
                }
                if (!empty($filters['date_at'])) {
                    $query->where('title', 'LIKE', '%' . $filters['date_at'] . '%');
                }
                if (!empty($filters['country_id']) || !empty($filters['address'])
                    || !empty($filters['zip'])
                ) {
                    $query->whereHas('address', function (Builder $query) use ($filters) {
                        if (!empty($filters['country_id'])) {
                            $query->where('country_id', $filters['country_id']);
                        }
                        if (!empty($filters['state_id'])) {
                            $query->where('state_id', $filters['state_id']);
                        }
                        if (!empty($filters['city_id'])) {
                            $query->where('city_id', $filters['city_id']);
                        }
                        if (!empty($filters['address'])) {
                            $query->where('address', 'LIKE', '%' . $filters['address'] . '%');
                        }
                        $filters['zip'] = preg_replace('/[^0-9]+/', '', !empty($filters['zip'])?$filters['zip']:'');
                        if (!empty($filters['zip'])) {
                            $query->where('zip', $filters['zip']);
                        }
                    });
                }

            }
        }

        $orderField = $request->get('s_f', 'id');
        $orderBy = $request->get('s_o', 'desc');
        $orderBy = in_array($orderBy, ['asc', 'desc']) ? $orderBy : 'desc';

        $filters['s_f'] = $orderField;
        $filters['s_o'] = $orderBy;

        switch ($orderField) {
            case 'address':
                $query->join('addresses', 'addresses.id', '=', 'calendar_events.address_id')
                    ->orderBy('addresses.address', $orderBy)
                    ->select('calendar_events.*');
                break;
            default:
                $query->orderBy($orderField, $orderBy);
                break;
        }

        return view("front.$this->viewName.index",
            [
                'pagination' => $query->paginate(15, ['*'], 'page',
                    $request->get('page', 1)),
                'filters' => $filters
            ]);
    }

    public function view(Request $request, CalendarEvent $item)
    {
        return view("front.$this->viewName.view",
            [
                'item' => $item
            ]);
    }

    public function form(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->only([
                'country_id',
                'city_id',
                'state_id',
                'address',
                'zip',
                'title',
                'date_start',
                'date_end',
                'url',
                'type_id',
                'date_at',
                'contact',
            ]);
            $data['phones'] = [
                ['phone' => $data['contact']['phone']]
            ];
            $data['emails'] = [
                ['email' => $data['contact']['email']]
            ];
            $data['image_1920x1080'] = $request->file('image_1920x1080');
            $data['image_1080x1920'] = $request->file('image_1080x1920');
            CalendarEventService::save($data);
            $request->session()->flash('alert_message', 'Event added, await confirmation');
            return redirect(route("front.$this->viewName.index"));
        }

        return view("front.$this->viewName.form");
    }
}
