<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Organisation;
use App\Models\OrganisationVolunteer;
use App\Models\UserHowFind;
use App\Models\UserHowFindValue;
use App\Models\VolunteerProfile;
use App\Services\LocationService;
use App\Services\SettingService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class VolunteerController extends Controller
{
    public $baseModel = OrganisationVolunteer::class;
    public $viewName = 'volunteer';

    public function index(Request $request)
    {
        return view("front.$this->viewName.index");
    }

    public function search(Request $request)
    {
        $volunteer_days = SettingService::getValueBySlug('volunteer_days_orgs', 90);
        $query = Organisation::where('status_id', Organisation::TYPE_APPROVE)
            ->where(function (Builder $query) use ($volunteer_days) {
                $query->whereNull('volunteer_at')
                    ->orWhere('volunteer_at', '<=',
                        Date('Y-m-d H:i:s', strtotime("- $volunteer_days days")));
            });
        if ($how_find = $request->get('how_find')) {
            try {
                if ($uhfv = UserHowFindValue::find($request->get('how_find'))) {
                    $how_find_other = $request->get('how_find_other');

                    $vp = VolunteerProfile::find(Cookie::get('volunteer_id'));
                    if ($vp && md5($vp->created_at) !== Cookie::get('volunteer_hash')) {
                        $vp = null;
                    }
                    $uhf = UserHowFind::create([
                        'value_id' => $uhfv->id,
                        'parent_type' => UserHowFind::TYPE_ORGANISATION_VOLUNTEER,
                        'parent_id' => $vp ? $vp->id : null,
                        'how_find_other' => $how_find_other
                    ]);

                    Cookie::queue('uhf_id', $uhf->id);
                    Cookie::queue('uhf_hash', md5($uhf->created_at));
                }
            } catch (\Exception $e) {

            }

        }
        $zip = preg_replace('/[^0-9]+/', '', $request->get('zip'));
        $zip = mb_strlen($zip) >= 3 ? $zip : $zip;
        if ($zip) {
            $query->whereHas('address', function (Builder $query) use ($zip) {
                $query->where('zip', $zip);
                $query->inRandomOrder();
            });
        }
        $items = $query->limit(5)->get();
        $not_ids = $items->pluck('id')->toArray();
        try{
        if ($zip && $items->count() < 5) {
            $radius_quantity = 3959;
            $distance = 50;
            $res = LocationService::getDataByAddress($zip);
            $lat = $res['lat'];
            $lng = $res['lng'];
            $whereRaw =
                "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
            $address_ids = Address::select('*')->selectRaw("{$whereRaw} AS distance")
                ->whereRaw("{$whereRaw} < ?", [$distance])->get()->pluck('id')->toArray();

            $query = Organisation::where('status_id', Organisation::TYPE_APPROVE)
                ->where(function (Builder $query) use ($volunteer_days) {
                    $query->whereNull('volunteer_at')
                        ->orWhere('volunteer_at', '<=',
                            Date('Y-m-d H:i:s', strtotime("- $volunteer_days days")));
                })
                ->whereIn('address_id', $address_ids);
            $_items =
                $query->whereNotIn('id', $not_ids)->limit(5 - $items->count())->inRandomOrder()
                    ->get();
            $new_items = [];
            foreach ($items as $item) {
                $new_items[] = $item;
            }
            foreach ($_items as $item) {
                $not_ids[] = $item->id;
                $new_items[] = $item;
            }
            $items = $new_items;
        }
        }catch (\Exception $e){

        }
        if (count($items) < 5) {
            $query = Organisation::where('status_id', Organisation::TYPE_APPROVE)
                ->where(function (Builder $query) use ($volunteer_days) {
                    $query->whereNull('volunteer_at')
                        ->orWhere('volunteer_at', '<=',
                            Date('Y-m-d H:i:s', strtotime("- $volunteer_days days")));
                });
            $_items =
                $query->whereNotIn('id', $not_ids)->limit(5 - count($items))->inRandomOrder()
                    ->get();
            $new_items = [];
            foreach ($items as $item) {
                $new_items[] = $item;
            }
            foreach ($_items as $item) {
                $new_items[] = $item;
            }
            $items = $new_items;
        }
        Cookie::queue('volunteer_zip', $zip);
        return view("front.$this->viewName.search", ['items' => $items]);
    }

    public function form(Request $request, Organisation $item)
    {
        if ($request->isMethod('post')) {
            $data = $request->get('answers', []);

            if (!empty($data['unable_complete']['unable_complete_1'])
                || !empty($data['unable_complete']['unable_complete_4'])
                || !empty($data['unable_complete']['unable_complete_7'])
            ) {
                // Ok
            } else {
                $vp = VolunteerProfile::find(Cookie::get('volunteer_id'));
                if ($vp && md5($vp->created_at) !== Cookie::get('volunteer_hash')) {
                    $vp = null;
                }
                if (!$vp) {
                    $vp = VolunteerProfile::create([]);
                }
                Cookie::queue('volunteer_id', $vp->id);
                Cookie::queue('volunteer_hash', md5($vp->created_at));
                $this->baseModel::create([
                    'data' => $data,
                    'organisation_id' => $item->id,
                    'volunteer_id' => $vp->id
                ]);
                $item->update(['volunteer_at' => Date('Y-m-d H:i:s')]);
                $vp->update(['last_at' => Date('Y-m-d H:i:s')]);
                if (Cookie::has('uhf_id') && $uhf = UserHowFind::find(Cookie::get('uhf_id'))) {
                    $uhf->update([
                        'parent_id' => $vp->id
                    ]);
                }
            }
            return redirect(route('front.volunteer.search') . '?zip='
                . Cookie::get('volunteer_zip'));
        }
        return view("front.$this->viewName.form", ['item' => $item]);
    }

    public function feedback(Request $request)
    {
        $vp = VolunteerProfile::find(Cookie::get('volunteer_id'));
        if ($vp && md5($vp->created_at) !== Cookie::get('volunteer_hash')) {
            $vp = null;
        }

        if (!$vp) {
            $vp = VolunteerProfile::create([]);
        }
        $feedback = $request->get('feedback') . 'asd';
        $data = [
            'feedback' => [
                'date' => Date('Y-m-d H:i:s'),
                'value' => $feedback
            ]
        ];
        if ($vp->data && !empty($vp->data['feedback'])) {
            $data = $vp->data;
            $data['feedback'][] = [
                'date' => Date('Y-m-d H:i:s'),
                'value' => $feedback
            ];
        }
        $vp->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'data' => $data
        ]);

        Cookie::queue('volunteer_id', $vp->id);
        Cookie::queue('volunteer_hash', md5($vp->created_at));

        if (Cookie::has('uhf_id') && $uhf = UserHowFind::find(Cookie::get('uhf_id'))) {
            $uhf->update([
                'parent_id' => $vp->id
            ]);
        }

        return redirect(route('front.volunteer.search') . '?zip=' . Cookie::get('volunteer_zip'));
    }
}
