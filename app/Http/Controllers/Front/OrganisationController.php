<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\TemplateMail;
use App\Models\CalendarEvent;
use App\Models\EmailLog;
use App\Models\Organisation;
use App\Models\OrganisationConfirmation;
use App\Models\ProgramType;
use App\Services\LocationService;
use App\Services\MailService;
use App\Services\OrganisationService;
use App\Services\SettingService;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrganisationController extends Controller
{
    public $baseModel = Organisation::class;
    public $viewName = 'organisation';

    public function index(Request $request)
    {
        $query =
            $this->baseModel::where(['status_id' => Organisation::TYPE_APPROVE, 'is_front' => 1]);

        $filters = $request->only([
            'center_zip',
            'country_id',
            'city_id',
            'state_id',
            'title',
            'zip',
            'program_type_id',
            'address',
            'radius_quantity',
            'options',
            'distance',
            's',
        ]);
        $orderField = $request->get('s_f', 'id');
        $orderBy = $request->get('s_o', 'desc');
        $orderBy = in_array($orderBy, ['asc', 'desc']) ? $orderBy : 'desc';
        if ($find_data = $request->get('whyhunger_search_organisations_type')) {
            $filters['s'] = 1;
            $filters['center_zip'] = !empty($find_data['_center']) ? $find_data['_center'] : null;
            $filters['distance'] = !empty($find_data['_distance']) ? $find_data['_distance'] : null;
            $filters['radius_quantity'] = !empty($find_data['_units']) ? 6371 : 3959;
            if (!empty($find_data['programType'])) {
                $pt = ProgramType::where('name', trim($find_data['programType']))->first();
                if ($pt) {
                    $filters['program_type_id'] = $pt->id;
                }
            }
        }
        $filters['center_zip'] =
            preg_replace('/[^0-9]+/', '',
                !empty($filters['center_zip']) ? $filters['center_zip'] : '');
        $filters['zip'] =
            preg_replace('/[^0-9]+/', '',
                !empty($filters['zip']) ? $filters['zip'] : '');
        if ($search = $request->get('Search')) {
            if ($search == 'Encontrar comida') {
                Cookie::queue('locale', 'es');
            } else {
                Cookie::queue('locale', 'en');
            }

            App::setLocale(Cookie::get('locale'));
        }

        if (!empty($filters['program_type_id'])) {
            $program_type_id = $filters['program_type_id'];
            $query->whereHas('types', function (Builder $query) use ($program_type_id) {
                $query->where('type_id', $program_type_id);
            });
        }
        if (!empty($filters['s'])) {
            if (!empty($filters['center_zip'])) {
                $center_zip = $filters['center_zip'];
                $radius_quantity =
                    !empty($filters['radius_quantity']) ? $filters['radius_quantity'] : 3959;
                $distance =
                    !empty($filters['distance']) && $filters['distance'] >= 5 ? $filters['distance']
                        : 5;
                $filters['distance'] = $distance;
                $res = LocationService::getDataByAddress($center_zip,
                    !empty($filters['country_id']) ? $filters['country_id'] : null);
                $lat = $res['lat'];
                $lng = $res['lng'];
                //                $whereRaw =
                //                    "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
                //                $address_ids = Address::select('*')->selectRaw("{$whereRaw} AS distance")
                //                    ->whereRaw("{$whereRaw} < ?", [$distance])
                //                    ->orderByRaw('FIELD (zip, ' . $center_zip . ') ASC')->get()->pluck('id')->toArray();
                //                $query->whereIn('address_id', $address_ids)
                //                    ->orderByRaw('FIELD (address_id, ' . implode(',',$address_ids) . ') ASC');
                if ($lat && $lng) {
                    $query->whereHas('address', function (Builder $query) use (
                        $lat,
                        $lng,
                        $radius_quantity,
                        $center_zip,
                        $distance
                    ) {
                        $whereRaw =
                            "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
                        $query->selectRaw("{$whereRaw} AS distance")
                            ->whereRaw("{$whereRaw} < ?", [$distance])
                            ->orderByRaw('FIELD (zip, ' . $center_zip . ') DESC');
                    });
                    if ($orderField == 'id') {
                        $query->join('addresses', 'addresses.id', '=', 'organisations.address_id')
                            ->orderByRaw('FIELD (addresses.zip, ' . $center_zip . ') DESC')
                            ->select('organisations.*');
                    }

                } else {
                    $query->where('id', 0);
                }
            } else {
                if (!empty($filters['title'])) {
                    $query->where('title', 'LIKE', '%' . $filters['title'] . '%');
                }
                if (!empty($filters['country_id'])
                    || !empty($filters['address'])
                    || !empty($filters['zip'])
                    || !empty($filters['state_id'])
                    || !empty($filters['city_id'])
                ) {
                    $query->whereHas('address', function (Builder $query) use ($filters) {
                        if (!empty($filters['country_id'])) {
                            $query->where('country_id', $filters['country_id']);
                        }
                        if (!empty($filters['state_id'])) {
                            $query->where('state_id', $filters['state_id']);
                        }
                        if (!empty($filters['city_id'])) {
                            $query->where('city_id', $filters['city_id']);
                        }
                        if (!empty($filters['address'])) {
                            $query->where('address', 'LIKE', '%' . $filters['address'] . '%');
                        }
                        if (!empty($filters['zip'])) {
                            $query->where('zip', $filters['zip']);
                        }
                    });
                }
            }
        }

        if (!empty($filters['options'])) {
            $query->whereHas('options', function (Builder $query) use ($filters) {
                $query->whereIn('option_id', $filters['options'])
                    ->having(DB::raw('count(*)'), '=', count($filters['options']));
            });
        }


        $filters['s_f'] = $orderField;
        $filters['s_o'] = $orderBy;

        switch ($orderField) {
            case 'address':
                $query->join('addresses', 'addresses.id', '=', 'organisations.address_id')
                    ->orderBy('addresses.address', $orderBy)
                    ->select('organisations.*');
                break;
            case 'type_id':
                $query->join('organisation_types', 'organisation_types.organisation_id', '=',
                    'organisations.id')
                    ->orderBy('organisation_types.type_id', $orderBy)
                    ->select('organisations.*');
                //                $query->whereHas('types', function (Builder $query) use ($orderBy) {
                //                    $query->orderBy('type_id', $orderBy);
                //                });
                break;
            default:
                $query->orderBy($orderField, $orderBy);
                break;
        }
        if (!empty($filters['center_zip']) || !empty($filters['zip'])) {
            $events = CalendarEvent::where([
                ['status_id', CalendarEvent::STATUS_APPROVE],
                ['date_start', '<=', Date('Y-m-d H:i:s')],
                ['date_end', '>=', Date('Y-m-d H:i:s')],
            ])
                ->whereHas('address', function (Builder $q) use ($filters) {
                    $q->where('zip',
                        !empty($filters['center_zip']) ? $filters['center_zip'] : $filters['zip']);
                })
                ->limit(5)->inRandomOrder()->get();
        } else {
            $events = CalendarEvent::where([
                ['status_id', CalendarEvent::STATUS_APPROVE],
                ['date_start', '<=', Date('Y-m-d H:i:s')],
                ['date_end', '>=', Date('Y-m-d H:i:s')],
            ])
                ->limit(5)->inRandomOrder()->get();
        }
//        $query =
//            $this->baseModel::where(['status_id' => Organisation::TYPE_APPROVE, 'is_front' => 1]);
        return view("front.$this->viewName.index",
            [
                'pagination' => $query->paginate(15, ['*'], 'page',
                    $request->get('page', 1)),
                'filters' => $filters,
                'events' => $events,
            ]);
    }

    public function view(Request $request, Organisation $item)
    {
        $events = CalendarEvent::where([
            ['status_id', CalendarEvent::STATUS_APPROVE],
            ['date_start', '<=', Date('Y-m-d H:i:s')],
            ['date_end', '>=', Date('Y-m-d H:i:s')],
        ])
            ->whereHas('address', function (Builder $query) use ($item) {
                $query->where('zip', $item->address->zip);
            })
            ->limit(5)->inRandomOrder()->get();
        return view("front.$this->viewName.view",
            [
                'item' => $item,
                'events' => $events,
            ]);
    }

    public function confirm(Request $request, $hash)
    {
        $oc = OrganisationConfirmation::where([
            'hash' => $hash,
            'status_id' => OrganisationConfirmation::STATUS_NEW,
            'type_id' => OrganisationConfirmation::TYPE_NEW,
        ])->first();
        if ($oc) {
            $item = $this->baseModel::find($oc->organisation_id);
            $oc->update(['status_id' => OrganisationConfirmation::STATUS_DONE]);
            $request->session()->flash('alert_message', "ORGANIZATION {$item->title} CONFIRMED");
        } else {
            $request->session()->flash('alert_message', "LINK EXPIRED");
        }
        return redirect(route("front.index"));
    }

    public function search()
    {
        return view("front.$this->viewName.search");
    }

    public function add(Request $request)
    {
        if ($request->isMethod('post')) {
            $item = OrganisationService::form($request);
            $hash = md5($item->id . $item->created_at);
            OrganisationConfirmation::create([
                'organisation_id' => $item->id,
                'hash' => $hash,
                'type_id' => OrganisationConfirmation::TYPE_NEW
            ]);
            $contact_fullname = 'User';
            $emails = [];
            if ($item->contacts) {
                $contact = $item->contacts()->first();
                $contact_fullname = "{$contact->first_name} {$contact->last_name}";
                if ($contact->emails) {
                    foreach ($contact->emails as $email) {
                        $emails[] = $email->email;
                    }
                }
            }
            if (!count($emails)) {
                foreach ($item->emails as $email) {
                    $emails[] = $email->email;
                }
            }
            if (count($emails)) {
                try {
                    Mail::to($emails)->send(new TemplateMail(
                        'Add New Organisation Request Confirmation',
                        MailService::getMailByTemplateName('email_template_confirmation', [
                            'contact_fullname' => $contact_fullname,
                            'company_name' => $item->title,
                            'confirmation_link' => route('front.organisation.confirm',
                                ['hash' => $hash])
                        ])
                    ));
                } catch (\Exception $e) {
                    Log::info('OrganisationController.286' . $e->getMessage());
                }
                foreach ($emails as $email) {
                    EmailLog::create(array(
                        'email' => $email,
                        'type_id' => EmailLog::TYPE_ORG_NEW_REQUEST,
                        'parent_type_id' => EmailLog::PARENT_TYPE_ORG,
                        'parent_id' => $item->id,
                        'user_id' => Auth::id()
                    ));
                }
            }
            try {
                $email = SettingService::getValueBySlug('email_from_new_org',
                    'patricia@whyhunger.org');
                try {
                    Mail::to($email)
                        ->send(new TemplateMail(
                            'Add New Organisation Request Admin Confirmation',
                            MailService::getMailByTemplateName('email_template_notification_confirmation_admin',
                                [
                                    'contact_fullname' => $contact_fullname,
                                    'company_name' => $item->title,
                                ])
                        ));
                } catch (\Exception $e) {
                    Log::info('OrganisationController.311' . $e->getMessage());
                }
                EmailLog::create(array(
                    'email' => $email,
                    'type' => EmailLog::TYPE_ORG_NEW_REQUEST_ADMIN,
                    'parent_type_id' => EmailLog::PARENT_TYPE_ORG,
                    'parent_id' => $item->id,
                    'user_id' => Auth::id()
                ));
            } catch (\Exception $e) {

            }
            $request->session()->flash('alert_message', 'Organization added, await confirmation');
            return redirect(route("front.index"));
        }

        return view("front.$this->viewName.form");
    }

    public function update(Request $request, $hash)
    {
        $oc = OrganisationConfirmation::where([
            'hash' => $hash,
            'status_id' => OrganisationConfirmation::STATUS_NEW,
            'type_id' => OrganisationConfirmation::TYPE_UPDATE,
        ])->first();
        $item = null;
        if ($oc) {
            $item = $this->baseModel::find($oc->organisation_id);
            if ($request->isMethod('post')) {
                OrganisationService::form($request, $item);
                $oc->update(['status_id' => OrganisationConfirmation::STATUS_DONE]);
                $request->session()->flash('alert_message', 'Organization Updated');
                return redirect(route("front.index"));
            }
            return view("front.$this->viewName.form", ['item' => $item]);

        }
        $request->session()->flash('alert_message', "LINK EXPIRED");
        return redirect(route("front.index"));
    }
}
