<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleMapController extends Controller
{

    public function index(Request $request)
    {

        if ($request->get('lat') && $request->get('lng')) {
            $keyword = $request->get('keyword');
            $lat = $request->get('lat', 39.10652688856946);
            $lng = $request->get('lng', -94.58589170968715);
            $url =
                "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$lat},{$lng}&radius=30000&type=bar,cafe,meal_takeaway,restaurant&keyword={$keyword}&key=AIzaSyDfkS3g2smm4new9R2s0o4p464tiRKP0ds";
            $r = json_decode(file_get_contents($url), true);
            $locations = array();

            if (!empty($r['results']) && count($r['results'])) {
                foreach ($r['results'] as $item) {
                    $locations[] = array(
                        'lat' => $item['geometry']['location']['lat'],
                        'lng' => $item['geometry']['location']['lng'],
                    );
                }
            }
            return response()->json(['locations' => $locations]);
        }
        return view('front.googleMap.index');
    }
}
