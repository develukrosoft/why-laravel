<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\CalendarEvent;
use App\Models\Project;
use App\Services\LocationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public $baseModel = Project::class;
    public $viewName = 'project';

    public function index(Request $request)
    {
        $query = $this->baseModel::query();
        $filters = $request->only([
            'center_zip',
            'country_id',
            'city_id',
            'state_id',
            'title',
            'zip',
            'address',
            'radius_quantity',
        ]);
        if ($request->get('s')) {
            $filters['s'] = 1;
            if ($center_zip = $request->get('center_zip')) {
                $radius_quantity =
                    !empty($filters['radius_quantity']) ? $filters['radius_quantity'] : 3959;

                $distance = $request->get('distance', 25);
                $distance = $distance >= 5 ? $distance : 5;
                $filters['distance'] = $distance;
                $res = LocationService::getDataByAddress($center_zip, !empty($filters['country_id']) ? $filters['country_id'] : null);
                $lat = $res['lat'];
                $lng = $res['lng'];
                if ($lat && $lng) {
                    $query->whereHas('address', function (Builder $query) use (
                        $lat,
                        $lng,
                        $radius_quantity,
                        $center_zip,
                        $distance
                    ) {
                        $whereRaw =
                            "($radius_quantity * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat))))";
                        $query->selectRaw("{$whereRaw} AS distance")
                            ->whereRaw("{$whereRaw} < ?", [$distance])
                            ->orderByRaw('FIELD (zip, ' . $center_zip . ') DESC');
                    });
                } else {
                    $query->where('id', -1);
                }
                $query->join('addresses', 'addresses.id', '=', 'projects.address_id')
                    ->orderByRaw('FIELD (addresses.zip, ' . $center_zip . ') DESC')
                    ->select('projects.*');
            } else {
                if (!empty($filters['title'])) {
                    $query->where('title', 'LIKE', '%' . $filters['title'] . '%');
                }
                if (!empty($filters['country_id']) || !empty($filters['address'])
                    || !empty($filters['zip'])
                ) {
                    $query->whereHas('address', function (Builder $query) use ($filters) {
                        if (!empty($filters['country_id'])) {
                            $query->where('country_id', $filters['country_id']);
                        }
                        if (!empty($filters['state_id'])) {
                            $query->where('state_id', $filters['state_id']);
                        }
                        if (!empty($filters['city_id'])) {
                            $query->where('city_id', $filters['city_id']);
                        }
                        if (!empty($filters['address'])) {
                            $query->where('address', 'LIKE', '%' . $filters['address'] . '%');
                        }
                        if (!empty($filters['zip'])) {
                            $query->where('zip', $filters['zip']);
                        }
                    });
                }
                $orderField = 'id';
                $orderBy = 'desc';
                $query->orderBy($orderField, $orderBy);
            }
        }
        if (!empty($filters['center_zip']) || !empty($filters['zip'])) {
            $events = CalendarEvent::where([
                ['status_id', CalendarEvent::STATUS_APPROVE],
                ['date_start', '<=', Date('Y-m-d H:i:s')],
                ['date_end', '>=', Date('Y-m-d H:i:s')],
            ])
                ->whereHas('address', function (Builder $query) use ($filters) {
                    $query->where('zip',
                        !empty($filters['center_zip']) ? $filters['center_zip'] : $filters['zip']);
                })
                ->limit(5)->inRandomOrder()->get();
        } else {
            $events = CalendarEvent::where([
                ['status_id', CalendarEvent::STATUS_APPROVE],
                ['date_start', '<=', Date('Y-m-d H:i:s')],
                ['date_end', '>=', Date('Y-m-d H:i:s')],
            ])
                ->limit(5)->inRandomOrder()->get();
        }
        return view("front.$this->viewName.index",
            [
                'pagination' => $query->paginate(15, ['*'], 'page',
                    $request->get('page', 1)),
                'filters' => $filters,
                'events' => $events
            ]);
    }

    public function view(Request $request, Project $item)
    {
        $events = CalendarEvent::where([
            ['status_id', CalendarEvent::STATUS_APPROVE],
            ['date_start', '<=', Date('Y-m-d H:i:s')],
            ['date_end', '>=', Date('Y-m-d H:i:s')],
        ])
            ->whereHas('address', function (Builder $query) use ($item) {
                $query->where('zip', $item->address->zip);
            })
            ->limit(5)->inRandomOrder()->get();
        return view("front.$this->viewName.view",
            [
                'item' => $item,
                'events' => $events
            ]);
    }
}
